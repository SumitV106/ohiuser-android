package com.ohicabs.user.ohi.Activity;

import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.R;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class ChangePasswordActivity extends BaseActivity {
    @BindView(R.id.edt_old_password)
    AppCompatEditText edt_old_password;

    @BindView(R.id.edt_new_password)
    AppCompatEditText edt_new_password;

    @BindView(R.id.edt_confirm_password)
    AppCompatEditText edt_confirm_password;

    @BindView(R.id.btn_change_password)
    AppCompatButton btn_change_password;

    @BindView(R.id.img_back)
    ImageView _img_back;

    @BindView(R.id.txt_header)
    TextView _txt_header;

    @BindView(R.id.oldpwdWrapper)
    TextInputLayout _oldpwdWrapper;

    @BindView(R.id.newpwdWrapper)
    TextInputLayout _newpwdWrapper;
    @BindView(R.id.confirmpwdWrapper)
    TextInputLayout _confirmpwdWrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        setContent();

    }
    private void setContent() {

        _txt_header.setTypeface(global_typeface.Sansation_Bold());
        edt_old_password.setTypeface(global_typeface.Sansation_Regular());
        edt_new_password.setTypeface(global_typeface.Sansation_Regular());
        edt_confirm_password.setTypeface(global_typeface.Sansation_Regular());

        _confirmpwdWrapper.setTypeface(global_typeface.Sansation_Regular());
        _newpwdWrapper.setTypeface(global_typeface.Sansation_Regular());
        _oldpwdWrapper.setTypeface(global_typeface.Sansation_Regular());

        btn_change_password.setTypeface(global_typeface.Sansation_Bold());

        btn_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(ChangePasswordActivity.this);

                String stMessage = IsValidate();
                if (stMessage.equalsIgnoreCase("true")) {

                    if(isInternetOn(getApplicationContext())) {

                        try {

                            Call<JsonObject> call = apiService.change_password(preferencesUtility.getuser_id(),
                                    edt_old_password.getText().toString(),
                                    edt_new_password.getText().toString());
                            call.enqueue(new Callback<JsonObject>() {
                                @Override
                                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                    Log.e("response", response.body().toString());
                                    try {
                                        JSONObject objResponse = new JSONObject(response.body().toString());
                                        if (objResponse.getString("status").equalsIgnoreCase("0")) {
                                            toastDialog.ShowToastMessage(objResponse.getString("message"));
                                            edt_new_password.setText("");
                                            edt_confirm_password.setText("");
                                            edt_old_password.setText("");
                                        } else {
                                            toastDialog.ShowToastMessage(objResponse.getString("message"));
                                            edt_new_password.setText("");
                                            edt_confirm_password.setText("");
                                            edt_old_password.setText("");
                                        }
                                    } catch (Exception e) {

                                    }

                                }

                                @Override
                                public void onFailure(Call<JsonObject> call, Throwable t) {

                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    else{
                        toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                    }
                } else {
                    toastDialog.ShowToastMessage(stMessage);
                }
            }
        });

        _img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(ChangePasswordActivity.this);
                finish();
            }
        });

    }



    /**
     * Form Validation
     *
     * @return
     */
    private String IsValidate() {
        if (edt_old_password.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_enter_old_pass);
        }
        else if (edt_old_password.getText().toString().trim().length()<6) {
            return getResources().getString(R.string.st_password_minimum);

        }else if (edt_new_password.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_enter_new_pass);

        }
        else if (edt_new_password.getText().toString().trim().length()<6) {
            return getResources().getString(R.string.st_password_minimum);

        }else if (edt_confirm_password.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_confirm_pass);

        } else if (!checkPassWordAndConfirmPassword(edt_new_password.getText().toString(), edt_confirm_password.getText().toString())) {
            return getResources().getString(R.string.st_pass_no_match);

        } else {
            return "true";
        }
    }

    /**
     * Check Password and Confirm Password
     *
     * @param password
     * @param confirmPassword
     * @return
     */
    public boolean checkPassWordAndConfirmPassword(String password, String confirmPassword) {
        boolean pstatus = false;
        if (confirmPassword != null && password != null) {
            if (password.equals(confirmPassword)) {
                pstatus = true;
            }
        }
        return pstatus;
    }
}

