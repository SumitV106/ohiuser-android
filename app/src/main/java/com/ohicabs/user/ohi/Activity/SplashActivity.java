package com.ohicabs.user.ohi.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.Constant;
import com.ohicabs.user.ohi.Utils.GpsStatusDetector;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class SplashActivity extends BaseActivity implements GpsStatusDetector.GpsStatusDetectorCallBack {

    private static int SPLASH_TIME_OUT = 3000;
    private GpsStatusDetector mGpsStatusDetector;

    private String stNotificationID = "", stServerTime = "", stMessage = "", formattedDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (isInternetOn(getApplicationContext())) {

            /* Socket Event Connect with Server */
            try {
                mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {

                    }
                });
                mSocket.connect();

            } catch (Exception ex) {
                ex.printStackTrace();
            }

            /* Check Location Service Start */
            mGpsStatusDetector = new GpsStatusDetector(this);
            mGpsStatusDetector.checkGpsStatus();

        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mGpsStatusDetector.checkOnActivityResult(requestCode, resultCode);
    }

    @Override
    public void onGpsSettingStatus(boolean enabled) {
        requestPermission();
    }

    @Override
    public void onGpsAlertCanceledByUser() {
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == Constant.REQUEST_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                StartActivity();
            } else {
                finish();
            }
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, Constant.REQUEST_PERMISSION);
        } else {
            StartActivity();
        }
    }

    private void StartActivity() {
//        setNotificationData();

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
            @Override
            public void run() {

                if (preferencesUtility.getLogedin()) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(getApplicationContext(), IntroActivity.class);
                    startActivity(i);
                    finish();
                }

            }
        }, SPLASH_TIME_OUT);
    }

    private void setNotificationData() {
        try {
            if (getIntent().getExtras() != null) {
                for (String key : getIntent().getExtras().keySet()) {
                    Object value = getIntent().getExtras().get(key);

                    if (key.equalsIgnoreCase("notification_id")) {
                        stNotificationID = value.toString();
                    }

                    if (key.equalsIgnoreCase("server_time")) {
                        stServerTime = value.toString();
                    }

                    if (key.equalsIgnoreCase("message")) {
                        stMessage = value.toString();
                    }
                }

                if (!stServerTime.equalsIgnoreCase("")) {
                    String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                    SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                    df.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date date = df.parse(stServerTime);
                    df.setTimeZone(TimeZone.getDefault());
                    SimpleDateFormat date_fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
                    formattedDate = date_fmt.format(date);
                }

                if (!stNotificationID.equalsIgnoreCase("") && !stMessage.equalsIgnoreCase("") && !stServerTime.equalsIgnoreCase("")) {

                    if (stNotificationID.equalsIgnoreCase("0")) {
                        mHelper.InsertPushNotificationData(stNotificationID, stMessage, formattedDate);

                    } else if (stNotificationID.equalsIgnoreCase("9")) {
                        mHelper.InsertPushNotificationData(stNotificationID, stMessage, formattedDate);

                    } else if (stNotificationID.equalsIgnoreCase("10")) {
                        mHelper.InsertPushNotificationData(stNotificationID, stMessage, formattedDate);

                    } else if (stNotificationID.equalsIgnoreCase("12")) {
                        mHelper.InsertPushNotificationData(stNotificationID, stMessage, formattedDate);

                    } else if (stNotificationID.equalsIgnoreCase("14")) {
                        mHelper.InsertPushNotificationData(stNotificationID, stMessage, formattedDate);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
