package com.ohicabs.user.ohi.Activity;


import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.ohicabs.user.ohi.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaytmTermsActivity extends BaseActivity {

    @BindView(R.id.webView) WebView webView;
    @BindView(R.id.img_back) ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paytm_terms);
        ButterKnife.bind(this);


        setView();
    }

    private void setView() {

        if (getIntent().hasExtra("stZoneID")) {

            String stZoneID = getIntent().getStringExtra("stZoneID");
            ArrayList<HashMap> arr_hashMap_ZoneList = mHelper.GetGSTTax(stZoneID);
            if (arr_hashMap_ZoneList.size() > 0) {

                webView.loadDataWithBaseURL(null, arr_hashMap_ZoneList.get(0).get("paytm_term_html").toString().replaceAll("\n","<br/>"),
                        "text/html", "utf-8", null);

            }
        } else if (getIntent().hasExtra("subscribe_term_condition")) {
            webView.loadDataWithBaseURL(null, getIntent().getStringExtra("subscribe_term_condition").toString().replaceAll("\n","<br/>"),
                    "text/html", "utf-8", null);

        } else if (getIntent().hasExtra("refer_condition")) {
            webView.loadDataWithBaseURL(null, getIntent().getStringExtra("refer_condition").toString().replaceAll("\n","<br/>"),
                    "text/html", "utf-8", null);
        }

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
