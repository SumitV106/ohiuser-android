package com.ohicabs.user.ohi.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.Activity.BaseActivity;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.NetworkStatus;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserPromoFragment extends Fragment {

    @BindView(R.id.edt_car_number) AppCompatEditText edtCarNumber;
    @BindView(R.id.btn_submit) AppCompatButton btnSubmit;

    private SharedPreferencesUtility preferencesUtility;
    ToastDialog toastDialog;
    ApiInterface apiService;
    ProgressDialog progressDialog;

    public UserPromoFragment() {
        super();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferencesUtility = new SharedPreferencesUtility(getActivity());
        toastDialog = new ToastDialog(getActivity());
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_promo, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.st_please_wait));
        progressDialog.setCancelable(false);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!edtCarNumber.getText().toString().equalsIgnoreCase("OHI100")) {
                    toastDialog.ShowToastMessage("Please Enter valid PromoCode!");

                } else {

                    if (NetworkStatus.getConnectivityStatus(getActivity())) {
                        progressDialog.show();
                        getPromoFromDriver();
                    } else {
                        toastDialog.ShowToastMessage(getResources().getString(R.string.st_internet_not_available));
                    }
                }
            }
        });
    }

    private void getPromoFromDriver() {

        try {
            Call<JsonObject> call = apiService.applyPromoCode(preferencesUtility.getuser_id(), edtCarNumber.getText().toString());

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        progressDialog.dismiss();
                        JSONObject obj = new JSONObject(response.body().toString());
                        if (obj.getString("status").equalsIgnoreCase("1")) {
                            toastDialog.ShowToastMessage(obj.getString("message"));
                            edtCarNumber.setText("");
                        } else {
                            toastDialog.ShowToastMessage(obj.getString("message"));
                            edtCarNumber.setText("");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });

        } catch (Exception ex) {
            progressDialog.dismiss();
            ex.printStackTrace();
        }
    }
}
