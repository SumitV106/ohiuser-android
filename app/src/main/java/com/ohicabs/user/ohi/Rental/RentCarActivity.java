package com.ohicabs.user.ohi.Rental;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Activity.BaseActivity;
import com.ohicabs.user.ohi.Activity.BookingActivityStep2Activity;
import com.ohicabs.user.ohi.Adpater.RentalPriceAdapter;
import com.ohicabs.user.ohi.Utils.ExpandableHeightListView;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ohicabs.user.ohi.Utils.Constant.BACK_TO_ACTIVITY;

public class RentCarActivity extends BaseActivity {

    @BindView(R.id.txt_pickup) TextView txt_pickup;
    @BindView(R.id.img_back) ImageView img_back;
    @BindView(R.id.list_rent) ExpandableHeightListView list_rent;
    @BindView(R.id.btncontinue) Button btncontinue;

    String stZoneID = "", stServiceID = "", stServiceName = "", stPriceID = "",
            st_TotalAmt = "", st_paytm_offer = "", st_max_offer_amount = "";

    private ArrayList<HashMap> _arr_hashMap_carList = new ArrayList<>();
    private View mLastView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rent_car);
        ButterKnife.bind(this);

        setViews();
    }

    private void setViews() {
        if (getIntent().hasExtra("stServiceID")) {

            Intent intent = getIntent();
            stServiceID = intent.getStringExtra("stServiceID");
            stZoneID = intent.getStringExtra("stZoneID");
            txt_pickup.setText(intent.getStringExtra("pickup"));
        }

        list_rent.setExpanded(true);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btncontinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (stPriceID.equalsIgnoreCase("")) {
                    ShowToast("Please first select package!");

                } else {

                    Intent intent_data = getIntent();

                    Intent intent = new Intent(RentCarActivity.this, BookingActivityStep2Activity.class);
                    intent.putExtra("stServiceID", stServiceID);
                    intent.putExtra("stServiceName", stServiceName);
                    intent.putExtra("stPriceID", stPriceID);
                    intent.putExtra("mPickUpLat", intent_data.getStringExtra("mPickUpLat"));
                    intent.putExtra("mPickUpLng", intent_data.getStringExtra("mPickUpLng"));
                    intent.putExtra("pickup", txt_pickup.getText().toString());
                    intent.putExtra("mDropLat", "");
                    intent.putExtra("mDropLng", "");
                    intent.putExtra("dropto", "");
                    intent.putExtra("pick_date", intent_data.getStringExtra("pick_date"));
                    intent.putExtra("stPaymentStatus", intent_data.getStringExtra("stPaymentStatus"));
                    intent.putExtra("stRideType", intent_data.getStringExtra("stRideType"));
                    intent.putExtra("stCardID", intent_data.getStringExtra("stCardID"));
                    intent.putExtra("time", "");
                    intent.putExtra("isTexiService", intent_data.getStringExtra("isTexiService"));

                    intent.putExtra("2seatprice", "");
                    intent.putExtra("price", st_TotalAmt);

                    intent.putExtra("est_duration", "");
                    intent.putExtra("est_total_distance", "");
                    intent.putExtra("max_offer_amount", st_max_offer_amount);
                    intent.putExtra("paytm_offer", st_paytm_offer);

                    intent.putExtra("stZoneID", stZoneID);
                    intent.putExtra("isRentalRide", false);

                    startActivityForResult(intent, BACK_TO_ACTIVITY);// Activity is started with requestCode
                }
            }
        });

        _arr_hashMap_carList = mHelper.GetRentalPriceList(stServiceID, stZoneID);

        RentalPriceAdapter adapter = new RentalPriceAdapter(getApplicationContext(),
                R.layout.layout_rental_row, _arr_hashMap_carList, new RentalPriceAdapter.BtnClickListener() {
            @Override
            public void onBtnClick(int position, String service_id, String IsTaxi, View view) {
                if (mLastView != null)
                    deselectListItem(mLastView);

                selectedListItem(view);
                mLastView = view;

                stServiceName = _arr_hashMap_carList.get(position).get("service_name").toString();
                stServiceID = _arr_hashMap_carList.get(position).get("service_id").toString();
                stPriceID = _arr_hashMap_carList.get(position).get("price_id").toString();
                st_TotalAmt = _arr_hashMap_carList.get(position).get("minimum_fair").toString();
                st_max_offer_amount = _arr_hashMap_carList.get(position).get("max_offer_amount").toString();
                st_paytm_offer = _arr_hashMap_carList.get(position).get("paytm_offer").toString();
                }
        });

        list_rent.setAdapter(adapter);

        list_rent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });

    }

    private void selectedListItem(View v) {
        RelativeLayout parent = (RelativeLayout) (v.getParent());
        RadioButton radioButton = (RadioButton) parent.findViewById(R.id.radioButton);
        radioButton.setChecked(true);
    }

    /**
     * for deselect item from listview
     *
     * @param v
     */
    private void deselectListItem(View v) {
        RelativeLayout parent = (RelativeLayout) (v.getParent());
        RadioButton radioButton = (RadioButton) parent.findViewById(R.id.radioButton);
        radioButton.setChecked(false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == BACK_TO_ACTIVITY) {
            String message = data.getStringExtra("MESSAGE");

            if (data.getStringExtra("MESSAGE").toString().equals("finish")) {

                Intent intent = new Intent();
                intent.putExtra("MESSAGE", "finish");
                setResult(BACK_TO_ACTIVITY, intent);
                finish();
            }
        }
    }
}
