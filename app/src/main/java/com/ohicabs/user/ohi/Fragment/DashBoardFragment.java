package com.ohicabs.user.ohi.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.github.vivchar.viewpagerindicator.ViewPagerIndicator;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.Activity.BookingDetailsActivity;
import com.ohicabs.user.ohi.Activity.NewBookingActivity;
import com.ohicabs.user.ohi.Activity.ReviewRideActivity;
import com.ohicabs.user.ohi.Adpater.ExclusiveOfferPagerAdapter;
import com.ohicabs.user.ohi.Adpater.UserBookingListAdapter_0;
import com.ohicabs.user.ohi.Adpater.UserBookingListAdapter_1;
import com.ohicabs.user.ohi.Adpater.UserBookingListAdapter_2;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.CustomWidgets.CustomBoldFontButton;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontEditText;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontTextView;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.PojoModel.ExclusiveOfferModel;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Rental.RentalBookingActivity;
import com.ohicabs.user.ohi.StorageManager.DatabaseHelper;
import com.ohicabs.user.ohi.Utils.AlarmReceiver;
import com.ohicabs.user.ohi.Utils.Constant;
import com.ohicabs.user.ohi.Utils.DataParser;
import com.ohicabs.user.ohi.Utils.ExpandableHeightListView;
import com.ohicabs.user.ohi.Utils.Global_Typeface;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;
import com.ohicabs.user.ohi.ohiOutStation.OutStationBookingActivity;
import com.ohicabs.user.ohi.ohiOutStation.OutStationBookingDetailsActivity;
import com.ohicabs.user.ohi.ui.subscription.SubscriptionActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.ALARM_SERVICE;
import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashBoardFragment extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveStartedListener {

    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    @BindView(R.id.list_user_booking_history_0) ExpandableHeightListView listUserBookingHistory0;
    @BindView(R.id.list_user_booking_history_1) ExpandableHeightListView listUserBookingHistory1;
    @BindView(R.id.list_user_booking_history_2) ExpandableHeightListView listUserBookingHistory2;
    @BindView(R.id.scrollview_list) ScrollView scrollViewList;

    @BindView(R.id.textSeparator_running) TextView txtSeparatorRunning;
    @BindView(R.id.textSeparator_next) TextView txtSeparatorNext;
    @BindView(R.id.textSeparator_schedule) TextView txtSeparatorSchedule;

    @BindView(R.id.view101) View view101;
    @BindView(R.id.view102) View view102;
    @BindView(R.id.view103) View view103;
    @BindView(R.id.view104) View view104;
    @BindView(R.id.view105) View view105;
    @BindView(R.id.view106) View view106;
    @BindView(R.id.view107) View view107;

    @BindView(R.id.btn_ride_later) AppCompatButton btnRideLater;
    @BindView(R.id.btn_ride_now) AppCompatButton btnRideNow;
    @BindView(R.id.imgfab) ImageButton imgFab;
    @BindView(R.id.btn_regular_ride) TextView btnRegularRide;
    @BindView(R.id.btn_rental_ride) TextView btnRentalRide;
    @BindView(R.id.btn_outstation_ride) TextView btnOutStationRide;
    @BindView(R.id.layout_ride) LinearLayout layoutRide;

    // Offer Layout
    @BindView(R.id.viewpager_offer) ViewPager viewPagerOffer;
    @BindView(R.id.cardView_offer) CardView cardViewOffer;
    @BindView(R.id.view_pager_indicator) ViewPagerIndicator viewPagerIndicator;
    @BindView(R.id.ll_ExclusiveOffer) LinearLayout llExclusiveOffer;

    private TextView txtSOS;
    private ToastDialog toastDialog;
    private Global_Typeface global_typeface;
    private SharedPreferencesUtility preferencesUtility;
    private DatabaseHelper mDBHelper;
    private ApiInterface apiService;
    private View view;

    private ArrayList<HashMap<String, String>> arrBookingList0 = new ArrayList<>();
    private ArrayList<HashMap<String, String>> arrBookingList1 = new ArrayList<>();
    private ArrayList<HashMap<String, String>> arrBookingList2 = new ArrayList<>();
    private JSONArray jsonArrayPendingPaymentList;
    private ArrayList<Marker> arrPickupMarkers = new ArrayList<>();
    private List<Polyline> listPolyLines = new ArrayList<Polyline>();

    private Location prevLoc, newLoc;
    public double mPickUpLat = 0.0, mPickUpLng = 0.0, mDropLat = 0.0, mDropLng = 0.0,
            mDriverLat = 0.0, mDriverLng = 0.0, mUserCurrentLat = 0.0, mUserCurrentLng = 0.0;
    private Location mLastLocation;
    private Marker mMarker_pickup, mMarker_drop, mMarker_driver, mCurrLocationMarker;

    private SupportMapFragment mSupportMapFragment;
    private GoogleMap mGoogleMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private String stBookingID, stSelectDatTime, stServiceId, stBookingStatusForMarker = "", isTaxiRide;
    private long alertTime;

    double TotalKm = 0, TotalDuration = 0;

    HashMap<String, Double> getTotalKmPrice = new HashMap<String, Double>();

    int rideType;

    public DashBoardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferencesUtility = new SharedPreferencesUtility(getContext());
        global_typeface = new Global_Typeface(getActivity());
        toastDialog = new ToastDialog(getActivity());
        mDBHelper = new DatabaseHelper(getActivity());
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            if (view == null) {
                view = inflater.inflate(R.layout.fragment_dash_board, container, false);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rideType = Constant.RIDE_TYPE.REGULAR_RIDE;

        mSupportMapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.google_map_dashboard);
        mSupportMapFragment.getMapAsync(this);

        listUserBookingHistory0.setExpanded(true);
        listUserBookingHistory1.setExpanded(true);
        listUserBookingHistory2.setExpanded(true);

        txtSOS = (TextView) getActivity().findViewById(R.id.txt_sos);
        txtSOS.setTypeface(global_typeface.Sansation_Regular());
        txtSOS.setVisibility(View.GONE);

        btnRideNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (jsonArrayPendingPaymentList != null) {
                    if (jsonArrayPendingPaymentList.length() > 0) {

                        // Open Payment Popup

                        Intent intent_vale_request = new Intent(getActivity(), ReviewRideActivity.class);
                        intent_vale_request.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent_vale_request.putExtra("panding_payment", jsonArrayPendingPaymentList.toString());
                        startActivity(intent_vale_request);

                    } else {
                        CallBookingActivity();
                    }
                } else {
                    CallBookingActivity();
                }
            }
        });

        btnRideLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i_ride_now = new Intent(getActivity(), NewBookingActivity.class);
                i_ride_now.putExtra("ride_type", "ride_later");
                startActivity(i_ride_now);
            }
        });

        imgFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  get current location on button click
                try {
                    if (mUserCurrentLat != 0.0 && mUserCurrentLng != 0.0) {
                        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mUserCurrentLat, mUserCurrentLng)));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        txtSOS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
                            || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                            == PackageManager.PERMISSION_GRANTED) {

                        Intent intent_call = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "100"));
                        startActivity(intent_call);
                    }
                } catch (Exception ex) {
                    toastDialog.ShowToastMessage(getResources().getString(R.string.call_error));
                    ex.printStackTrace();
                }
            }
        });

        btnRentalRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                rideType = Constant.RIDE_TYPE.RENTAL_RIDE;

                btnRentalRide.setBackground(getResources().getDrawable(R.drawable.rental_ride_selected));
                btnRegularRide.setBackground(getResources().getDrawable(R.drawable.regular_ride));
                btnOutStationRide.setBackground(getResources().getDrawable(R.drawable.outstation_ride));

                btnRentalRide.setTextColor(getResources().getColor(R.color.colorwhite));
                btnRegularRide.setTextColor(getResources().getColor(R.color.colorLightGreen));
                btnOutStationRide.setTextColor(getResources().getColor(R.color.colorLightGreen));
            }
        });

        btnRegularRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                rideType = Constant.RIDE_TYPE.REGULAR_RIDE;

                btnOutStationRide.setBackground(getResources().getDrawable(R.drawable.outstation_ride));
                btnRentalRide.setBackground(getResources().getDrawable(R.drawable.rental_ride));
                btnRegularRide.setBackground(getResources().getDrawable(R.drawable.regular_ride_selected));

                btnRentalRide.setTextColor(getResources().getColor(R.color.colorLightGreen));
                btnOutStationRide.setTextColor(getResources().getColor(R.color.colorLightGreen));
                btnRegularRide.setTextColor(getResources().getColor(R.color.colorwhite));

            }
        });

        btnOutStationRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                rideType = Constant.RIDE_TYPE.OUTSTATION_RIDE;

                btnOutStationRide.setBackground(getResources().getDrawable(R.drawable.outstation_ride_selected));
                btnRentalRide.setBackground(getResources().getDrawable(R.drawable.rental_ride));
                btnRegularRide.setBackground(getResources().getDrawable(R.drawable.regular_ride));

                btnOutStationRide.setTextColor(getResources().getColor(R.color.colorwhite));
                btnRentalRide.setTextColor(getResources().getColor(R.color.colorLightGreen));
                btnRegularRide.setTextColor(getResources().getColor(R.color.colorLightGreen));

            }
        });
    }

    private void CallBookingActivity() {

        switch (rideType) {
            case Constant.RIDE_TYPE.REGULAR_RIDE:
                Intent intentRegular = new Intent(getActivity(), NewBookingActivity.class);
                intentRegular.putExtra("ride_type", "ride_now");
                startActivity(intentRegular);
                break;

            case Constant.RIDE_TYPE.RENTAL_RIDE:
                Intent intentRental = new Intent(getActivity(), RentalBookingActivity.class);
                intentRental.putExtra("ride_type", "ride_now");
                startActivity(intentRental);
                break;

            case Constant.RIDE_TYPE.OUTSTATION_RIDE:
                Intent intentOutStation = new Intent(getActivity(), OutStationBookingActivity.class);
                intentOutStation.putExtra("ride_type", "out_station_ride");
                startActivity(intentOutStation);
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
        }

        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
        mGoogleMap.getUiSettings().setCompassEnabled(false);
        mGoogleMap.getUiSettings().setTiltGesturesEnabled(false);
        mGoogleMap.setOnCameraIdleListener(this);
        mGoogleMap.setOnCameraMoveStartedListener(this);

        //Disable Marker click event
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });
    }

    /**
     * initializing location listener and
     * other related stuffs.
     */
    protected synchronized void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {

            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            mGoogleApiClient.connect();

            if (!isGPSEnabled()) {
                turnGPSOn();
            }
        }
    }

    private boolean isGPSEnabled() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        return gpsEnabled;
    }

    /**
     * Turn On GPS
     */
    private void turnGPSOn() {

        if (canToggleGPS()) {
            String provider = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

            if (!provider.contains("gps")) { //if gps is disabled
                final Intent poke = new Intent();
                poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
                poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
                poke.setData(Uri.parse("3"));
                getActivity().sendBroadcast(poke);
            }
        } else {
            Intent settingsIntent = new Intent(
                    Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
        }

    }

    /**
     * Check GPS Toggle enable or not
     *
     * @return
     */
    private boolean canToggleGPS() {
        PackageManager pacman = getActivity().getPackageManager();
        PackageInfo pacInfo = null;

        try {
            pacInfo = pacman.getPackageInfo("com.android.settings", PackageManager.GET_RECEIVERS);
        } catch (PackageManager.NameNotFoundException e) {
            return false; //package not found
        }

        if (pacInfo != null) {
            for (ActivityInfo actInfo : pacInfo.receivers) {
                //test if recevier is exported. if so, we can toggle GPS.
                if (actInfo.name.equals("com.android.settings.widget.SettingsAppWidgetProvider") && actInfo.exported) {
                    return true;
                }
            }
        }
        return false; //default
    }

    /*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        // the maximal rates currently possible.
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location != null) {
                Log.e("location", "location from FusedLocationApi.getLastLocation()");
                this.onLocationChanged(location);
            }

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        }
    }

    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            toastDialog.ShowToastMessage(getResources().getString(R.string.network_disconnect));
        } else if (i == CAUSE_NETWORK_LOST) {
            toastDialog.ShowToastMessage(getResources().getString(R.string.network_lost));
        }
    }

    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(getActivity(),
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("TAG", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        mUserCurrentLat = location.getLatitude();
        mUserCurrentLng = location.getLongitude();

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(getResources().getString(R.string.st_current_location));
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_point));

        /* Add User Current Location Marker */
//        mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);

        //move map camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onCameraIdle() {
    }

    @Override
    public void onCameraMoveStarted(int i) {
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mSupportMapFragment.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver_UserBookingList,
                new IntentFilter("User_Current_Booking_List"));

        /*This Call Receive After getting user ride details after tracking service call */
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver_UserAllBookingList,
                new IntentFilter("User_Ride_Tracking"));

        /* This call Receive after user_request_accept */
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver_UserRideRequest,
                new IntentFilter("User_Ride_Request"));

        /* This call Receive after driver ride cancel */
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver_CancelRequest,
                new IntentFilter("Driver_Cancel_Request"));

        /* This Call Receive After User Give a Review to Driver */
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver_ReviewFinishActivity,
                new IntentFilter("Review_Activity_finish"));

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver_RideStartActivity,
                new IntentFilter("User_Ride_Start"));

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver_UserWaitForDriver,
                new IntentFilter("User_Wait_For_Driver_Data"));

        /* Call When ride is OutStation and complete one-side trip */
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver_OutStationRideStop,
                new IntentFilter("OutStation_Trip_One_Stop"));

        GetRideList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("TAG_HOME_DESTROY", "Destroy");
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver_UserBookingList);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver_UserAllBookingList);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver_UserRideRequest);

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver_UserRideRequest);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver_CancelRequest);

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver_OutStationRideStop);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    @NonNull
    private final ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public
        void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) { }

        @Override
        public
        void onPageSelected(final int position) { }

        @Override
        public
        void onPageScrollStateChanged(final int state) { }
    };

    /**
     * Get User Ride List Service Call
     */
    private void GetRideList() {
        try {
             /* Fragment not attached to Activity */
            Activity activity = getActivity();
            if (activity != null && isAdded()) {
                if (isInternetOn(getContext())) {
                    Call<JsonObject> call = apiService.user_current_booking_list(preferencesUtility.getuser_id());
                    call.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            GetBookingList(response.body().toString());
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) { }
                    });
                } else {
                    toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Reload Ride Service List - After Given a Review to Driver
     */
    private BroadcastReceiver mMessageReceiver_RideStartActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            GetRideList();
        }
    };

    /**
     * Reload Ride Service List - After OutStation one-side trip complete
     */
    private BroadcastReceiver mMessageReceiver_OutStationRideStop = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            GetRideList();

            toastDialog.ShowToastMessage("RoundTrip Pickup To Drop concluded \n \n" + intent.getStringExtra("message"));
        }
    };

    /**
     * Reload Ride Service List - After Given a Review to Driver
     */
    private BroadcastReceiver mMessageReceiver_UserWaitForDriver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            GetRideList();
        }
    };

    /**
     * Reload Ride Service List - After Given a Review to Driver
     */
    private BroadcastReceiver mMessageReceiver_ReviewFinishActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            GetRideList();
        }
    };

    /**
     * Reload Ride Service List
     */
    private BroadcastReceiver mMessageReceiver_CancelRequest = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_DriverBookingList = intent.getStringExtra("driver_cancel_request");
            GetRideList();
        }
    };

    /**
     * Refresh User Ride List Service
     */
    private BroadcastReceiver mMessageReceiver_UserRideRequest = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_UserBookingList = intent.getStringExtra("user_ride_request_data");
            Log.e("TAG_REQUEST_DATA", mResult_UserBookingList);
            GetRideList();
        }
    };

    /**
     * User - Driver Tracking Details
     */
    private BroadcastReceiver mMessageReceiver_UserAllBookingList = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_RideTracking = intent.getStringExtra("user_ride_track_data");

            prevLoc = new Location(LocationManager.GPS_PROVIDER);
            newLoc = new Location(LocationManager.GPS_PROVIDER);

            try {
                JSONObject objTracking = new JSONObject(mResult_RideTracking);
                if (objTracking.getString("status").equalsIgnoreCase("2")) {

                    JSONObject objPayload = objTracking.getJSONObject("payload");
                    if (objPayload.getString("booking_id").equalsIgnoreCase(stBookingID)) {

                        mDriverLat = Double.parseDouble(objPayload.getString("latitude"));
                        mDriverLng = Double.parseDouble(objPayload.getString("longitude"));

                        if (mMarker_driver == null) {
                            if (stServiceId != null) {
                                if (stServiceId.equals("1")) {
                                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(mDriverLat, mDriverLng))
                                            .title(getResources().getString(R.string.st_driver_location))
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_1)));
                                } else if (stServiceId.equals("2") || stServiceId.equals("13") || stServiceId.equals("18")) {
                                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(mDriverLat, mDriverLng))
                                            .title(getResources().getString(R.string.st_driver_location))
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_2)));
                                } else if (stServiceId.equals("3") || stServiceId.equals("19")) {
                                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(mDriverLat, mDriverLng))
                                            .title(getResources().getString(R.string.st_driver_location))
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_3)));
                                } else if (stServiceId.equals("11")) {
                                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(mDriverLat, mDriverLng))
                                            .title(getResources().getString(R.string.st_driver_location))
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_11)));
                                } else if (stServiceId.equals("12")) {
                                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(mDriverLat, mDriverLng))
                                            .title(getResources().getString(R.string.st_driver_location))
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_12)));
                                } else if (stServiceId.equals("14")) {
                                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(mDriverLat, mDriverLng))
                                            .title(getResources().getString(R.string.st_driver_location))
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_14)));
                                } else if (stServiceId.equals("15")) {
                                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(mDriverLat, mDriverLng))
                                            .title(getResources().getString(R.string.st_driver_location))
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_15)));
                                } else if (stServiceId.equals("16") || stServiceId.equals("17")) {
                                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(mDriverLat, mDriverLng))
                                            .title(getResources().getString(R.string.st_driver_location))
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_16)));
                                } else {
                                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(mDriverLat, mDriverLng))
                                            .title(getResources().getString(R.string.st_driver_location))
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_taxi)));
                                }
                            } else {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_taxi)));
                            }
                        } else {
                            prevLoc.setLatitude(mMarker_driver.getPosition().latitude);
                            prevLoc.setLongitude(mMarker_driver.getPosition().longitude);

                            mMarker_driver.setPosition(new LatLng(mDriverLat, mDriverLng));

                            newLoc.setLatitude(mMarker_driver.getPosition().latitude);
                            newLoc.setLongitude(mMarker_driver.getPosition().longitude);

                            float bearing = prevLoc.bearingTo(newLoc);
                            mMarker_driver.setRotation(bearing);
                        }

                        if (objPayload.getString("booking_status").equalsIgnoreCase(Constant.STATUS_GOTO_USER) ||
                                objPayload.getString("booking_status").equalsIgnoreCase(Constant.STATUS_USER_WAIT)) {


                            if (getTotalKmPrice.size() > 0) {

                                if (getTotalKmPrice.containsKey("totalKM")) {

                                    TotalKm = getTotalKmPrice.get("totalKM");
                                } else {
                                    TotalKm = 0;
                                }

                                if (getTotalKmPrice.containsKey("totalMin")) {

                                    TotalDuration = getTotalKmPrice.get("totalMin");
                                } else {
                                    TotalDuration = 0;
                                }
                            }

                            for (Polyline line : listPolyLines) {
                                line.remove();
                            }
                            listPolyLines.clear();

                            LatLng pickup = new LatLng(mDriverLat, mDriverLng);
                            LatLng dest = new LatLng(mPickUpLat, mPickUpLng);

                            if (mDriverLat != 0.0 && mDriverLng != 0.0 && mPickUpLat != 0.0 && mPickUpLng != 0.0) {


                                if (isTaxiRide.equalsIgnoreCase("3") && objPayload.getString("booking_status").equalsIgnoreCase(Constant.STATUS_USER_WAIT)) {
                                    for (Polyline line : listPolyLines) {
                                        line.remove();
                                    }
                                    listPolyLines.clear();

                                } else {
                                    String url = getUrl(pickup, dest);
                                    FetchUrl FetchUrl = new FetchUrl();
                                    FetchUrl.execute(url);
                                }

                                String stTitle = "", stSnippet = "";
                                if (TotalDuration == 0) {
                                    stTitle = "wait...";
                                    stSnippet = "......";
                                } else {
                                    stTitle = "Arrival";
                                    stSnippet = Math.round(TotalDuration) + " mins";
                                }

                                if (mMarker_pickup != null) {
                                    mMarker_pickup.remove();
                                }

                                if (mMarker_drop != null) {
                                    mMarker_drop.remove();
                                }

                                mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mPickUpLat, mPickUpLng))
                                        .title(stTitle)
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin))
                                        .snippet(stSnippet));
                                mMarker_pickup.showInfoWindow();
                                arrPickupMarkers.add(mMarker_pickup);
                            }

                        } else if (objPayload.getString("booking_status").equalsIgnoreCase(Constant.STATUS_START_TRIP)) {

                            for (Polyline line : listPolyLines) {
                                line.remove();
                            }
                            listPolyLines.clear();

                            if (mMarker_pickup != null) {
                                mMarker_pickup.remove();
                            }

                            LatLng driverLatLng = new LatLng(mDriverLat, mDriverLng);
                            LatLng dropLatLng = new LatLng(mDropLat, mDropLng);

                            if (isTaxiRide.equalsIgnoreCase("3")) {
                                for (Polyline line : listPolyLines) {
                                    line.remove();
                                }
                                listPolyLines.clear();
                            } else {
                                String url = getUrl(driverLatLng, dropLatLng);
                                FetchUrl FetchUrl = new FetchUrl();
                                FetchUrl.execute(url);
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    private BroadcastReceiver mMessageReceiver_UserBookingList = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_UserBookingList = intent.getStringExtra("user_booking_list");
            GetBookingList(mResult_UserBookingList);
        }
    };

    /**
     * Set User Booking List
     *
     * @param mResult
     */
    private void GetBookingList(String mResult) {
        try {

            if (!mResult.equalsIgnoreCase("")) {

                JSONObject objBookingList = new JSONObject(mResult);

                if (objBookingList.getString("status").equalsIgnoreCase("1")) {

                    JSONArray jarrPayload = objBookingList.getJSONArray("payload");
                    jsonArrayPendingPaymentList = objBookingList.getJSONArray("panding_payment_list");

                    arrBookingList0.clear();
                    arrBookingList1.clear();
                    arrBookingList2.clear();

                    for (Polyline line : listPolyLines) {
                        line.remove();
                    }

                    listPolyLines.clear();

                    if (mMarker_pickup != null) {
                        mMarker_pickup.remove();
                    }

                    if (mMarker_drop != null) {
                        mMarker_drop.remove();
                    }

                    if (mMarker_driver != null) {
                        mMarker_driver.remove();
                        mMarker_driver = null;
                    }

                    if (jarrPayload.length() > 0) {

                        arrBookingList0.clear();
                        arrBookingList1.clear();
                        arrBookingList2.clear();

                        for (int i = 0; i < jarrPayload.length(); i++) {
                            JSONObject objBookingData = jarrPayload.getJSONObject(i);

                            Iterator<String> iter = objBookingData.keys();
                            HashMap<String, String> map_keyValue = new HashMap<>();
                            while (iter.hasNext()) {
                                String key = iter.next();
                                try {
                                    Object value = objBookingData.get(key);
                                    map_keyValue.put(key, value.toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            if (map_keyValue.get("booking_status").equalsIgnoreCase(Constant.STATUS_PENDING)) {
                                arrBookingList0.add(map_keyValue);
                            } else if (map_keyValue.get("booking_status").equalsIgnoreCase(Constant.STATUS_ACCEPT)) {
                                arrBookingList1.add(map_keyValue);
                            } else if (map_keyValue.get("booking_status").equalsIgnoreCase(Constant.STATUS_GOTO_USER)
                                    || map_keyValue.get("booking_status").equalsIgnoreCase(Constant.STATUS_USER_WAIT)
                                    || map_keyValue.get("booking_status").equalsIgnoreCase(Constant.STATUS_START_TRIP)) {
                                arrBookingList2.add(map_keyValue);
                            }
                        }
                    }
                } else {
                /*  View Gone
                    Google Map Clear
                    Add new Current Location Marker
                 */
                    scrollViewList.setVisibility(View.GONE);
                    arrBookingList0.clear();
                    arrBookingList1.clear();
                    arrBookingList2.clear();
                    mGoogleMap.clear();

                    //Place current location marker
                    LatLng latLng = new LatLng(mUserCurrentLat, mUserCurrentLng);

                    //move map camera
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(13.0f));
                }

                if (arrBookingList0.size() == 0 && arrBookingList1.size() == 0 && arrBookingList2.size() == 0) {
                    scrollViewList.setVisibility(View.GONE);
                }

                /* Ride Running */
                if (arrBookingList2.size() > 0) {

                    view101.setVisibility(View.VISIBLE);
                    txtSeparatorRunning.setVisibility(View.VISIBLE);
                    view102.setVisibility(View.VISIBLE);
                    listUserBookingHistory2.setVisibility(View.VISIBLE);
                    view103.setVisibility(View.VISIBLE);

                    UserBookingListAdapter_2 userBookingListAdapter_2 = new UserBookingListAdapter_2(getActivity(), R.layout.layout_booking_history_row, arrBookingList2,
                            new UserBookingListAdapter_2.BtnClickListener() {
                                @Override
                                public void onBtnClick(int position, String ride_id, String stRideType) {
                                    final int rideType = Integer.parseInt(stRideType);
                                    if (rideType > 3) {
                                        Intent _intent = new Intent(getActivity(), OutStationBookingDetailsActivity.class);
                                        _intent.putExtra("booking_id", ride_id);
                                        startActivity(_intent);

                                    } else {
                                        Intent _intent = new Intent(getActivity(), BookingDetailsActivity.class);
                                        _intent.putExtra("booking_id", ride_id);
                                        startActivity(_intent);
                                    }
                                }
                            });
                    listUserBookingHistory2.setAdapter(userBookingListAdapter_2);
                    userBookingListAdapter_2.notifyDataSetChanged();

                    btnRideNow.setAlpha(0.4f);
                    btnRideNow.setEnabled(false);
                    scrollViewList.setVisibility(View.VISIBLE);

                    layoutRide.setVisibility(View.GONE);

                    SetRideDataOnMap();

                    /* Get Tracking Data */
                    callTracking(arrBookingList2.get(0).get("booking_id"), arrBookingList2.get(0).get("booking_status"), arrBookingList2.get(0).get("driver_id"));

                } else {
                    view101.setVisibility(View.GONE);
                    txtSeparatorRunning.setVisibility(View.GONE);
                    view102.setVisibility(View.GONE);
                    listUserBookingHistory2.setVisibility(View.GONE);
                    view103.setVisibility(View.GONE);

                    btnRideNow.setAlpha(1.0f);
                    btnRideNow.setEnabled(true);
                    layoutRide.setVisibility(View.VISIBLE);
                }

                /* Ride Accept */
                if (arrBookingList1.size() > 0) {
                    scrollViewList.setVisibility(View.VISIBLE);

                    UserBookingListAdapter_1 userBookingListAdapter_1 = new UserBookingListAdapter_1(getActivity(), R.layout.layout_booking_history_row, arrBookingList1,
                            new UserBookingListAdapter_1.BtnClickListener() {
                                @Override
                                public void onBtnClick(int position, String ride_id, String stRideType) {
                                    final int rideType = Integer.parseInt(stRideType);
                                    if (rideType > 3) {
                                        Intent _intent = new Intent(getActivity(), OutStationBookingDetailsActivity.class);
                                        _intent.putExtra("booking_id", ride_id);
                                        startActivity(_intent);

                                    } else {
                                        Intent _intent = new Intent(getActivity(), BookingDetailsActivity.class);
                                        _intent.putExtra("booking_id", ride_id);
                                        startActivity(_intent);
                                    }
                                }
                            },
                            new UserBookingListAdapter_1.AlertBtnClickListener() {
                                @Override
                                public void onBtnClick(final int position, String ride_id) {

                                    SetAlertDialog(position, ride_id);
                                }
                            });
                    listUserBookingHistory1.setAdapter(userBookingListAdapter_1);
                    userBookingListAdapter_1.notifyDataSetChanged();

                    txtSeparatorNext.setVisibility(View.VISIBLE);
                    view104.setVisibility(View.VISIBLE);
                    view105.setVisibility(View.VISIBLE);
                    listUserBookingHistory1.setVisibility(View.VISIBLE);

                } else {
                    txtSeparatorNext.setVisibility(View.GONE);
                    view104.setVisibility(View.GONE);
                    view105.setVisibility(View.GONE);
                    listUserBookingHistory1.setVisibility(View.GONE);
                }

                /* Ride Pending */
                if (arrBookingList0.size() > 0) {
                    scrollViewList.setVisibility(View.VISIBLE);

                    UserBookingListAdapter_0 userBookingListAdapter_0 = new UserBookingListAdapter_0(getActivity(), R.layout.layout_booking_history_row, arrBookingList0,
                            new UserBookingListAdapter_0.BtnClickListener() {
                                @Override
                                public void onBtnClick(int position, String ride_id, String stRideType) {
                                    final int rideType = Integer.parseInt(stRideType);

                                    if (rideType > 3) {
                                        Intent _intent = new Intent(getActivity(), OutStationBookingDetailsActivity.class);
                                        _intent.putExtra("booking_id", ride_id);
                                        startActivity(_intent);

                                    } else {
                                        Intent _intent = new Intent(getActivity(), BookingDetailsActivity.class);
                                        _intent.putExtra("booking_id", ride_id);
                                        startActivity(_intent);
                                    }
                                }
                            });

                    listUserBookingHistory0.setAdapter(userBookingListAdapter_0);
                    userBookingListAdapter_0.notifyDataSetChanged();

                    txtSeparatorSchedule.setVisibility(View.VISIBLE);
                    view106.setVisibility(View.VISIBLE);
                    view107.setVisibility(View.VISIBLE);
                    listUserBookingHistory0.setVisibility(View.VISIBLE);

                } else {
                    txtSeparatorSchedule.setVisibility(View.GONE);
                    view106.setVisibility(View.GONE);
                    view107.setVisibility(View.GONE);
                    listUserBookingHistory0.setVisibility(View.GONE);
                }

                // To check user have already buy or view an offer
                if (objBookingList.optString("is_view_offer").equalsIgnoreCase("1")) {
                    startActivity(new Intent(getActivity(), SubscriptionActivity.class));
                }

                // Exclusive Offer
                JSONArray jarrOffer = objBookingList.getJSONArray("exclusive_offer");
                if (jarrOffer.length() > 0) {
                    llExclusiveOffer.setVisibility(View.VISIBLE);
                    cardViewOffer.setVisibility(View.VISIBLE);

                    ArrayList<ExclusiveOfferModel> arrExclusiveOffer = new ArrayList<>();

                    for (int i = 0; i < jarrOffer.length(); i++) {

                        JSONObject jsonObjectData = jarrOffer.getJSONObject(i);
                        ExclusiveOfferModel exclusiveOfferModel = new ExclusiveOfferModel();

                        exclusiveOfferModel.setDetails(jsonObjectData.optString("details"));
                        exclusiveOfferModel.setPlan_name(jsonObjectData.optString("plan_name"));
                        exclusiveOfferModel.setExclusiveOfferDetail(jsonObjectData.toString());
                        exclusiveOfferModel.setImage(jsonObjectData.optString("image"));
                        exclusiveOfferModel.setIs_offer(jsonObjectData.optString("is_offer"));

                        arrExclusiveOffer.add(exclusiveOfferModel);
                    }

                    // set ViewPager Adapter
                    viewPagerOffer.setAdapter(new ExclusiveOfferPagerAdapter(getContext(), arrExclusiveOffer));

                    // set ViewPager Indicator
                    viewPagerIndicator.setupWithViewPager(viewPagerOffer);
                    viewPagerIndicator.addOnPageChangeListener(mOnPageChangeListener);

                } else {
                    llExclusiveOffer.setVisibility(View.GONE);
                    cardViewOffer.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Get Driver Current Lat-Lng
     */
    private void callTracking(String stBookingID, String stBooking_Status, String stDriverId) {

        /* Get Ride Tracking Details */
        if (stBooking_Status.equalsIgnoreCase("2") || stBooking_Status.equalsIgnoreCase("3")) {
            if (isInternetOn(getActivity())) {
                Call<JsonObject> call = apiService.tracking(preferencesUtility.getuser_id(), stDriverId, stBookingID, stBooking_Status, "1");

                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        Intent intent = new Intent("User_Ride_Tracking");
                        intent.putExtra("user_ride_track_data", response.body().toString());
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) { }
                });
            } else {
                toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
            }
        }
    }


    private void SetAlertDialog(final int position, final String ride_id) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_ride_alert);
        dialog.setTitle("");
        dialog.setCancelable(false);

        CustomBoldFontButton btn_cancel = (CustomBoldFontButton) dialog.findViewById(R.id.btn_cancel);
        CustomBoldFontButton btn_set = (CustomBoldFontButton) dialog.findViewById(R.id.btn_set);
        final CustomRegularFontTextView txt_booking_id = (CustomRegularFontTextView) dialog.findViewById(R.id.txt_booking_id);
        final CustomRegularFontTextView txt_select_alert_time = (CustomRegularFontTextView) dialog.findViewById(R.id.txt_select_alert_time);
        final CustomRegularFontEditText edt_alert_message = (CustomRegularFontEditText) dialog.findViewById(R.id.edt_alert_message);

        txt_booking_id.setText("#" + ride_id);
        txt_select_alert_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.layout_date_time_picker);
                dialog.setTitle("");
                dialog.setCancelable(false);

                final AppCompatButton btn_next = (AppCompatButton) dialog.findViewById(R.id.btn_next);
                final AppCompatButton btn_done = (AppCompatButton) dialog.findViewById(R.id.btn_done);
                final TimePicker timePicker = (TimePicker) dialog.findViewById(R.id.timePicker);
                final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.datePicker);

                btn_next.setTypeface(global_typeface.Sansation_Bold());
                btn_done.setTypeface(global_typeface.Sansation_Bold());

                timePicker.setIs24HourView(true);
                btn_next.setVisibility(View.GONE);
                btn_done.setVisibility(View.VISIBLE);
                timePicker.setVisibility(View.VISIBLE);
                datePicker.setVisibility(View.GONE);
                timePicker.setIs24HourView(false);

                btn_done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String currentTime;

                        if (Build.VERSION.SDK_INT >= 23) {
                            currentTime = timePicker.getHour() + ":" + timePicker.getMinute() + ":00";
                        } else {
                            currentTime = timePicker.getCurrentHour() + ":" + timePicker.getCurrentMinute() + ":00";
                        }

                        StringBuilder builder = new StringBuilder();
                        builder.append((datePicker.getMonth() + 1) + "/");
                        builder.append(datePicker.getDayOfMonth() + "/");
                        builder.append(datePicker.getYear());

                        stSelectDatTime = datePicker.getYear() + "-" + (datePicker.getMonth() + 1) + "-" + datePicker.getDayOfMonth() + " " + currentTime;
                        txt_select_alert_time.setText(stSelectDatTime);

                        Calendar calendar = Calendar.getInstance();
                        if (Build.VERSION.SDK_INT >= 23) {
                            calendar.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(),
                                    timePicker.getHour(), timePicker.getMinute(), 0);
                        } else {
                            calendar.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(),
                                    timePicker.getCurrentHour(), timePicker.getCurrentMinute(), 0);
                        }
                        alertTime = calendar.getTimeInMillis();
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edt_alert_message.getText().toString().equalsIgnoreCase("")) {
                    toastDialog.ShowToastMessage(getResources().getString(R.string.st_enter_alert_details));

                } else if (txt_select_alert_time.getText().toString().equalsIgnoreCase(getResources().getString(R.string.st_alert_time))) {
                    toastDialog.ShowToastMessage(getResources().getString(R.string.st_enter_alert_date));

                } else {
                    mDBHelper.InsertAlertData(edt_alert_message.getText().toString(), ride_id, txt_select_alert_time.getText().toString());

                    handleNotification(alertTime, edt_alert_message.getText().toString(), ride_id);

                    View v1 = listUserBookingHistory1.getChildAt(position - listUserBookingHistory1.getFirstVisiblePosition());
                    if (v1 == null)
                        return;
                    AppCompatImageView img_alert = (AppCompatImageView) v1.findViewById(R.id.img_alert);
                    img_alert.setVisibility(View.GONE);

                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }


    /**
     * Track Data on Map
     */
    private void SetRideDataOnMap() {
        try {
            for (int i = 0; i < arrBookingList2.size(); i++) {

                stBookingID = arrBookingList2.get(i).get("booking_id").toString();
                stServiceId = arrBookingList2.get(i).get("service_id").toString();

                mPickUpLat = Double.parseDouble(arrBookingList2.get(i).get("pickup_lati").toString());
                mPickUpLng = Double.parseDouble(arrBookingList2.get(i).get("pickup_longi").toString());

                mDropLat = Double.parseDouble(arrBookingList2.get(i).get("drop_lati").toString());
                mDropLng = Double.parseDouble(arrBookingList2.get(i).get("drop_longi").toString());

                mDriverLat = Double.parseDouble(arrBookingList2.get(i).get("lati").toString());
                mDriverLng = Double.parseDouble(arrBookingList2.get(i).get("longi").toString());

                isTaxiRide = arrBookingList2.get(i).get("is_taxi").toString();

                prevLoc = new Location(LocationManager.GPS_PROVIDER);
                newLoc = new Location(LocationManager.GPS_PROVIDER);

                if (mMarker_driver == null) {
                    if (stServiceId != null) {
                        if (stServiceId.equals("1")) {
                            mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(mDriverLat, mDriverLng))
                                    .title(getResources().getString(R.string.st_driver_location))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_1)));
                        } else if (stServiceId.equals("2") || stServiceId.equals("13") || stServiceId.equals("18")) {
                            mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(mDriverLat, mDriverLng))
                                    .position(new LatLng(mDriverLat, mDriverLng))
                                    .title(getResources().getString(R.string.st_driver_location))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_2)));
                        } else if (stServiceId.equals("3") || stServiceId.equals("19")) {
                            mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(mDriverLat, mDriverLng))
                                    .title(getResources().getString(R.string.st_driver_location))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_3)));
                        } else if (stServiceId.equals("11")) {
                            mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(mDriverLat, mDriverLng))
                                    .title(getResources().getString(R.string.st_driver_location))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_11)));
                        } else if (stServiceId.equals("12")) {
                            mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(mDriverLat, mDriverLng))
                                    .title(getResources().getString(R.string.st_driver_location))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_12)));
                        } else if (stServiceId.equals("14")) {
                            mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(mDriverLat, mDriverLng))
                                    .title(getResources().getString(R.string.st_driver_location))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_14)));
                        } else if (stServiceId.equals("15")) {
                            mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(mDriverLat, mDriverLng))
                                    .title(getResources().getString(R.string.st_driver_location))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_15)));
                        } else if (stServiceId.equals("16") || stServiceId.equals("17")) {
                            mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(mDriverLat, mDriverLng))
                                    .title(getResources().getString(R.string.st_driver_location))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_16)));
                        } else {
                            mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(mDriverLat, mDriverLng))
                                    .title(getResources().getString(R.string.st_driver_location))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_taxi)));
                        }
                    } else {
                        mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(mDriverLat, mDriverLng))
                                .title(getResources().getString(R.string.st_driver_location))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_taxi)));
                    }
                } else {

                    mMarker_driver.setPosition(new LatLng(mDriverLat, mDriverLng));

                    prevLoc.setLatitude(mMarker_driver.getPosition().latitude);
                    prevLoc.setLongitude(mMarker_driver.getPosition().longitude);

                    newLoc.setLatitude(mMarker_driver.getPosition().latitude);
                    newLoc.setLongitude(mMarker_driver.getPosition().longitude);

                    float bearing = prevLoc.bearingTo(newLoc);
                    mMarker_driver.setRotation(bearing);
                }

                String stBookingStatus = arrBookingList2.get(i).get("booking_status").toString();
                stBookingStatusForMarker = stBookingStatus;
                if (stBookingStatus.equalsIgnoreCase(Constant.STATUS_GOTO_USER)) {

                    mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(mPickUpLat, mPickUpLng))
                            .title(getResources().getString(R.string.st_pickup_location))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin)));
                    arrPickupMarkers.add(mMarker_pickup);

                    LatLng pickup = new LatLng(mDriverLat, mDriverLng);
                    LatLng dest = new LatLng(mPickUpLat, mPickUpLng);

                    if (mDriverLat != 0.0 && mDriverLng != 0.0 && mPickUpLat != 0.0 && mPickUpLng != 0.0) {

                        String url = getUrl(pickup, dest);
                        FetchUrl FetchUrl = new FetchUrl();

                        // Start downloading json data from Google Directions API
                        FetchUrl.execute(url);

                        String stTitle = "", stSnippet = "";
                        if (TotalDuration == 0) {
                            stTitle = "wait...";
                            stSnippet = "......";
                        } else {
                            stTitle = "Arrival";
                            stSnippet = Math.round(TotalDuration) + " mins";
                        }

                        if (mMarker_pickup != null) {
                            mMarker_pickup.remove();
                        }

                        mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(mPickUpLat, mPickUpLng))
                                .title(stTitle)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin))
                                .snippet(stSnippet));
                        mMarker_pickup.showInfoWindow();
                        arrPickupMarkers.add(mMarker_pickup);
                    }

                } else if (stBookingStatus.equalsIgnoreCase(Constant.STATUS_USER_WAIT)) {

                    for (Polyline line : listPolyLines) {
                        line.remove();
                    }

                    listPolyLines.clear();

                    LatLng pickup = new LatLng(mDriverLat, mDriverLng);
                    LatLng dest = new LatLng(mPickUpLat, mPickUpLng);

                    if (mDriverLat != 0.0 && mDriverLng != 0.0 && mPickUpLat != 0.0 && mPickUpLng != 0.0) {

                        String url = getUrl(pickup, dest);
                        FetchUrl FetchUrl = new FetchUrl();

                        // Start downloading json data from Google Directions API
                        FetchUrl.execute(url);

                        String stTitle = "", stSnippet = "";
                        if (TotalDuration == 0) {
                            stTitle = "wait...";
                            stSnippet = "......";
                        } else {
                            stTitle = "Arrival";
                            stSnippet = Math.round(TotalDuration) + " mins";
                        }

                        if (mMarker_pickup != null) {
                            mMarker_pickup.remove();
                        }

                        mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(mPickUpLat, mPickUpLng))
                                .title(stTitle)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin))
                                .snippet(stSnippet));
                        mMarker_pickup.showInfoWindow();
                        arrPickupMarkers.add(mMarker_pickup);
                    }

                } else if (stBookingStatus.equalsIgnoreCase(Constant.STATUS_START_TRIP)) {

                    if (!isTaxiRide.equalsIgnoreCase("3")) {

                        mMarker_drop = mGoogleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(mDropLat, mDropLng))
                                .title(getResources().getString(R.string.st_drop_location))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin)));
                        arrPickupMarkers.add(mMarker_drop);

                        for (Polyline line : listPolyLines) {
                            line.remove();
                        }

                        listPolyLines.clear();

                        LatLng pickup = new LatLng(mDriverLat, mDriverLng);
                        LatLng dest = new LatLng(mDropLat, mDropLng);

                        if (mDriverLat != 0.0 && mDriverLng != 0.0 && mDropLat != 0.0 && mDropLng != 0.0) {

                            String url = getUrl(pickup, dest);
                            FetchUrl FetchUrl = new FetchUrl();

                            // Start downloading json data from Google Directions API
                            FetchUrl.execute(url);

                            if (mMarker_drop == null) {
                                mMarker_drop = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDropLat, mDropLng))
                                        .title(getResources().getString(R.string.st_drop_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin)));
                                arrPickupMarkers.add(mMarker_drop);
                            } else {
                                mMarker_drop.setPosition(new LatLng(mDropLat, mDropLng));
                            }
                        }

                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        builder.include(new LatLng(mPickUpLat, mPickUpLng));
                        builder.include(new LatLng(mDropLat, mDropLng));
                        LatLngBounds bounds = builder.build();
                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 130));
                    }

                } else {

                    // For Rental Ride Remove all things from google map, Show only driver Marker pin on map
                    for (Polyline line : listPolyLines) {
                        line.remove();
                    }
                    listPolyLines.clear();

                    if (mMarker_drop != null) {
                        mMarker_drop.remove();
                    }

                    if (mMarker_pickup != null) {
                        mMarker_pickup.remove();
                    }

                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(mDriverLat, mDriverLng)).zoom(18.0f).build();
                    mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void handleNotification(long alertTime, String message, String notificationId) {

        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;

        Intent alarmIntent = new Intent(getContext(), AlarmReceiver.class);
        alarmIntent.putExtra("title", getResources().getString(R.string.app_name));
        alarmIntent.putExtra("message", message);
        alarmIntent.putExtra("OBJECT_ID", m);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), m, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
        int ALARM_TYPE = AlarmManager.RTC_WAKEUP;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(ALARM_TYPE, alertTime, pendingIntent);
        } else {
            alarmManager.set(ALARM_TYPE, alertTime, pendingIntent);
        }
    }

    // Generate direcation url
    private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        String mode = "mode=driving";

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode + "&key=AIzaSyAfye39JZYcYdS196GZrvxRBfMShOCaA3w";

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    //  Fetching the data from web service
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Draw route on map
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DataParser parser = new DataParser();
                // Starts parsing data
                routes = parser.parse(jObject);
                getTotalKmPrice = parser.parseTotal(jObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;


            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                points.clear();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.BLUE);
            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
//                mGoogleMap.addPolyline(lineOptions);
                listPolyLines.add(mGoogleMap.addPolyline(lineOptions));
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }

            if (getTotalKmPrice.size() > 0) {

                if (getTotalKmPrice.containsKey("totalKM")) {

                    TotalKm = getTotalKmPrice.get("totalKM");
                } else {
                    TotalKm = 0;
                }

                if (getTotalKmPrice.containsKey("totalMin")) {

                    TotalDuration = getTotalKmPrice.get("totalMin");
                } else {
                    TotalDuration = 0;
                }
            }

            String stTitle = "", stSnippet = "";
            if (TotalDuration == 0) {
                stTitle = "wait...";
                stSnippet = "......";
            } else {
                stTitle = "Arrival";
                stSnippet = Math.round(TotalDuration) + " mins";
            }

            if (stBookingStatusForMarker.equalsIgnoreCase(Constant.STATUS_GOTO_USER) || stBookingStatusForMarker.equalsIgnoreCase(Constant.STATUS_USER_WAIT)) {
                if (mMarker_pickup != null) {
                    mMarker_pickup.remove();
                }

                mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(mPickUpLat, mPickUpLng))
                        .title(stTitle)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin))
                        .snippet(stSnippet));
                mMarker_pickup.showInfoWindow();
                arrPickupMarkers.add(mMarker_pickup);
            }

        }
    }

}

