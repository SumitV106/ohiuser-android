package com.ohicabs.user.ohi.ohiOutStation;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.Activity.BaseActivity;
import com.ohicabs.user.ohi.Activity.BookingDetailsActivity;
import com.ohicabs.user.ohi.Activity.MonthlySpendingTermsActivity;
import com.ohicabs.user.ohi.Activity.ReviewRideActivity;
import com.ohicabs.user.ohi.Activity.UserDriverSupportActivity;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontTextView;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.CircularImageView;
import com.ohicabs.user.ohi.Utils.Constant;
import com.ohicabs.user.ohi.Utils.DataParser;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.SHARE_LINK;
import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class OutStationBookingDetailsActivity extends BaseActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "BookingDetailsActivity";

    @BindView(R.id.img_profile) CircularImageView imgProfile;
    @BindView(R.id.img_ride_status) AppCompatImageView imgRideStatus;
    @BindView(R.id.img_car_type) AppCompatImageView imgCarType;
    @BindView(R.id.txt_ride_status) AppCompatTextView txtRideStatus;
    @BindView(R.id.txt_code) AppCompatTextView txtCode;

    @BindView(R.id.txt_ride_date_time) AppCompatTextView txtRideDateTime;
    @BindView(R.id.txt_ride_id) AppCompatTextView txtRideID;

    @BindView(R.id.txt_car_type) AppCompatTextView txtCarType;
    @BindView(R.id.txt_driver_name) AppCompatTextView txtDriverName;
    @BindView(R.id.txt_driver_mobile_no) AppCompatTextView txtDriverMobileNo;
    @BindView(R.id.txt_driver_car_number) AppCompatTextView txtDriverCarNumber;

    @BindView(R.id.txt_ride_price) AppCompatTextView txtRidePrice;
    @BindView(R.id.txt_ride_distance) AppCompatTextView txtRideDistance;
    @BindView(R.id.txt_ride_duration) AppCompatTextView txtRideDuration;

    @BindView(R.id.txt_pickup_address) AppCompatTextView txtPickupAddress;
    @BindView(R.id.txt_drop_address) AppCompatTextView txtDropAddress;

    @BindView(R.id.txt_call) AppCompatTextView txtCall;
    @BindView(R.id.txt_cancel_ride) AppCompatTextView txtCancelRide;
    @BindView(R.id.txt_support) AppCompatTextView txtSupport;

    @BindView(R.id.layout_cancel_ride) LinearLayout layoutCancelRide;
    @BindView(R.id.layout_call) LinearLayout layoutCall;
    @BindView(R.id.layout_support) LinearLayout layoutSupport;

    @BindView(R.id.img_call) AppCompatImageView imgCall;
    @BindView(R.id.img_cancel) AppCompatImageView imgCancel;
    @BindView(R.id.img_support) AppCompatImageView imgSupport;

    @BindView(R.id.txt_ride_message) AppCompatTextView txtRideMessage;

    @BindView(R.id.layout_pickup_address) RelativeLayout layoutPickupAddress;
    @BindView(R.id.layout_drop_address) RelativeLayout layoutDropAddress;
    @BindView(R.id.layout_driver_info) RelativeLayout layoutDriverInfo;
    @BindView(R.id.layout_ride_time_details) RelativeLayout layoutRideTimeDetails;
    @BindView(R.id.layout_ride_price) RelativeLayout layoutRideTollPrice;
    @BindView(R.id.view_toll_tip) View viewTollTip;

    @BindView(R.id.layout_driver_name) LinearLayout layoutDriverName;
    @BindView(R.id.layout_driver_call) LinearLayout layoutDriverCall;
    @BindView(R.id.layout_driver_vehical_no) LinearLayout layoutDriverVehicalNo;

    @BindView(R.id.txt_ride_toll_price) AppCompatTextView txtRideTollPrice;
    @BindView(R.id.tv_pickup_location) AppCompatTextView tvPickupLocation;
    @BindView(R.id.tv_drop_location) AppCompatTextView tvDropLocation;
    @BindView(R.id.txt_payment_type) AppCompatTextView txtPaymentType;

    @BindView(R.id.imgfab_share) ImageButton imgFabShare;
    @BindView(R.id.img_help) ImageButton imgHelp;
    @BindView(R.id.imgfab) ImageButton imgFab;
    @BindView(R.id.img_refresh) ImageButton imgRefresh;

    @BindView(R.id.txt_car_brand) AppCompatTextView txtCarBrand;
    @BindView(R.id.txt_car_model) AppCompatTextView txtCarModel;
    @BindView(R.id.txt_car_color) AppCompatTextView txtCarColor;

    @BindView(R.id.layout_waiting_payment) RelativeLayout layoutWaitingPayment;
    @BindView(R.id.txt_pay) CustomRegularFontTextView txtPay;
    @BindView(R.id.txt_refund_initiated) AppCompatTextView txtRefundInitiated;
    @BindView(R.id.txt_refund_pending) AppCompatTextView txtRefundPending;
    @BindView(R.id.txt_price) AppCompatTextView txtPrice;
    @BindView(R.id.txt_price_t_n_c) AppCompatTextView txtPriceTNC;
    @BindView(R.id.txt_waiting_payment) AppCompatTextView txtPaytmPayment;

    @BindView(R.id.layout_car_details) RelativeLayout rlCarBrandDetails;
    @BindView(R.id.layout_on_address) RelativeLayout rlOnTripAddress;
    @BindView(R.id.layout_return_address) RelativeLayout rlReturnAddress;
    @BindView(R.id.txt_return_trip) AppCompatTextView txtReturnTrip;

    @BindView(R.id.txt_go_pickup_address) AppCompatTextView txtGoPickupAddress;
    @BindView(R.id.txt_go_drop_address) AppCompatTextView txtGoDropAddress;
    @BindView(R.id.tv_go_pickup_location) AppCompatTextView tvGoPickupAddress;
    @BindView(R.id.tv_go_drop_location) AppCompatTextView tvGoDropAddress;

    @BindView(R.id.view_brand) View viewBrandDivider;
    @BindView(R.id.view_ride_details) View viewRideDetails;
    @BindView(R.id.view_on_address) View viewOnAddress;
    @BindView(R.id.view_ride_price) View viewRidePrice;

    @BindView(R.id.ll_driverDetails) LinearLayout lldriverDetails;

    private SlidingUpPanelLayout mLayout;

    SimpleDateFormat date_format;
    String stDriverId, stBooking_Status, stBookingID, stDriverContactNumber, stBookingId, isTaxiRide,
            stServiceId, userPaymentStatus;

    private SupportMapFragment mSupportMapFragment;
    private GoogleMap mGoogleMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    public double mPickUpLat = 0.0, mPickUpLng = 0.0, mDropLat = 0.0, mDropLng = 0.0,
            mDriverLat = 0.0, mDriverLng = 0.0, mUserCurrentLat = 0.0, mUserCurrentLng = 0.0,
            mGoPickupLat = 0.0, mGoPickupLng = 0.0, mGoDropLat = 0.0, mGoDropLng = 0.0;

    private Marker mMarker_pickup, mMarker_drop, mMarkerPickupGO, mMarkerDropGO, mMarker_driver;
    Location prevLoc, newLoc;

    String stDistance = "", stDuration = "", stAmount = "", stBookingStatusForMarker = "";
    String paytmTransactionID = "", orderID = "", failPayload = "", successPayload = "";

    double TotalKm = 0, TotalDuration = 0;
    long totalSeconds = 300000;

    public PolylineOptions lineOptions = null, goLineOptions = null;
    private static final String FORMAT = "%02d:%02d:%02d";
    private String SCROLL_STATE = "";

    private ArrayList<Marker> arrMarkersPin = new ArrayList<>();
    public ArrayList<LatLng> routeArray = new ArrayList<LatLng>();
    public ArrayList<LatLng> routeArrayGoRide = new ArrayList<LatLng>();

    ArrayList<LatLng> arrPoints = new ArrayList<>();
    List<Polyline> arrPolyLines = new ArrayList<Polyline>();
    HashMap<String, Double> getTotalKmPrice = new HashMap<String, Double>();
    ArrayList<HashMap> arrPriceList;

    CountDownTimer cT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outstation_booking_details);
        ButterKnife.bind(this);

        init();
    }

    private void init() {
        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        ActionBar mActionBar = getSupportActionBar();
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));

        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setTitle(getResources().getString(R.string.ride_details));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SCROLL_STATE.equalsIgnoreCase("EXPANDED")) {
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                } else {
                    finish();
                }
            }
        });
        stBookingId = getIntent().getStringExtra("booking_id");

        setContent();
    }

    private void setContent() {

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_DetailsFinishActivity,
                new IntentFilter("Details_Activity_finish"));

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_minActivity,
                new IntentFilter("MIN_data"));


        lineOptions = new PolylineOptions();
        goLineOptions = new PolylineOptions();

        BookingDetails();

        if (getIntent().hasExtra("new_request_accepted")) {
            toastDialog.showAlertDialog(getResources().getString(R.string.st_request_accepted_title), getResources().getString(R.string.st_request_accepted_msg));
        }

        mSupportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.google_map);
        mSupportMapFragment.getMapAsync(this);


        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }

        String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        date_format = new SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.getDefault());

        setListener();
    }

    private void setListener() {
        imgHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog Dialog = new Dialog(OutStationBookingDetailsActivity.this);
                Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                Dialog.setContentView(R.layout.layout_dialog_call);

                Dialog.show();
                final Button btn_yes = (Button) Dialog.findViewById(R.id.btn_yes);
                final Button btn_no = (Button) Dialog.findViewById(R.id.btn_no);

                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            if (ContextCompat.checkSelfPermission(OutStationBookingDetailsActivity.this,
                                    Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
                                    || ContextCompat.checkSelfPermission(OutStationBookingDetailsActivity.this, Manifest.permission.READ_PHONE_STATE)
                                    == PackageManager.PERMISSION_GRANTED) {
                                Dialog.dismiss();
                                Intent intent_call = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "100"));
                                startActivity(intent_call);
                            }
                        } catch (Exception ex) {
                            toastDialog.ShowToastMessage(getResources().getString(R.string.call_error));
                            ex.printStackTrace();
                        }

                    }
                });

                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Dialog.dismiss();
                    }
                });
            }
        });

        layoutCancelRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog onRideCancelDialog = new Dialog(OutStationBookingDetailsActivity.this);
                onRideCancelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                onRideCancelDialog.setContentView(R.layout.layout_dialog_ride_cancel);

                onRideCancelDialog.show();
                final Button btn_yes = (Button) onRideCancelDialog.findViewById(R.id.btn_yes);
                final Button btn_no = (Button) onRideCancelDialog.findViewById(R.id.btn_no);

                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isInternetOn(getApplicationContext())) {

                            try {
                                progressDialog.show();
                                Call<JsonObject> call = apiService.user_ride_cancel(stBookingID, preferencesUtility.getuser_id(), stBooking_Status);
                                call.enqueue(new Callback<JsonObject>() {
                                    @Override
                                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                        String mResult = response.body().toString();
                                        progressDialog.dismiss();
                                        Log.e("TAG_Cancel_Booking_Data", mResult);
                                        try {
                                            JSONObject objCancelData = new JSONObject(mResult);
                                            if (objCancelData.getString("status").equalsIgnoreCase("1")) {
                                                toastDialog.ShowToastMessage(objCancelData.getString("message"));
                                                onRideCancelDialog.dismiss();
                                                finish();
                                            } else {
                                                toastDialog.ShowToastMessage(objCancelData.getString("message"));
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<JsonObject> call, Throwable t) {
                                        t.printStackTrace();
                                        progressDialog.dismiss();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                        }
                    }
                });

                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onRideCancelDialog.dismiss();
                    }
                });


            }
        });

        layoutCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallToDriver(stDriverContactNumber);
            }
        });

        layoutSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent _intent = new Intent(OutStationBookingDetailsActivity.this, UserDriverSupportActivity.class);
                _intent.putExtra("driverID", stDriverId);
                startActivity(_intent);
            }
        });

        //Drop Address
        txtDropAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mDropLat + "," + mDropLng + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //Pickup Address
        txtPickupAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mPickUpLat + "," + mPickUpLng + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        tvPickupLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mPickUpLat + "," + mPickUpLng + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        tvDropLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mDropLat + "," + mDropLng + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        tvGoPickupAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        tvGoDropAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        imgFabShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String message = "My Ohicabs Ride Detail \n";
                message = message + "————————————————" + "\n";
                message = message + "Pickup Location: " + txtPickupAddress.getText() + "\n";
                message = message + "Drop Location: " + txtDropAddress.getText() + "\n";
                message = message + "————————————————" + "\n";
                message = message + "Driver Information" + "\n";
                message = message + "————————————————" + "\n";
                message = message + "Name:" + txtDriverName.getText().toString() + "\n";
                message = message + "Contact No:" + txtDriverMobileNo.getText().toString() + "\n";
                message = message + "Vehicle No:" + txtDriverCarNumber.getText() + "\n";
                message = message + "Model :" + txtCarModel.getText() + "\n";
                message = message + "————————————————\n";
//                message = message + "Click here for map\n";
//                message=message+"https://maps.google.com/?q"+mUserCurrentLat+","+mUserCurrentLng;
//                message = message + "https://maps.google.com?saddr=" + mPickUpLat + "," + mPickUpLng + "&daddr=" + mDropLat + "," + mDropLng+"\n";
                message = message + SHARE_LINK + stBookingID;

//                String message = SHARE_LINK+stBookingID;

                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, message);

                startActivity(Intent.createChooser(share, "Share information"));
            }
        });

        imgFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  get current location on button click
                try {
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mUserCurrentLat, mUserCurrentLng)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        imgRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BookingDetails();
            }
        });

        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.e(TAG, "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                Log.e(TAG, "onPanelStateChanged " + newState);
                SCROLL_STATE = newState.toString();

                if (SCROLL_STATE.equalsIgnoreCase("EXPANDED")) {
                    imgFabShare.setVisibility(View.GONE);
                    imgHelp.setVisibility(View.GONE);
                    imgFab.setVisibility(View.GONE);
                    imgRefresh.setVisibility(View.GONE);

                } else {
                    if (stBooking_Status.equalsIgnoreCase(Constant.STATUS_START_TRIP)) {
                        imgHelp.setVisibility(View.VISIBLE);
                    } else {
                        imgHelp.setVisibility(View.GONE);
                    }
                    imgFab.setVisibility(View.VISIBLE);
                    imgFabShare.setVisibility(View.VISIBLE);
                    imgRefresh.setVisibility(View.VISIBLE);
                }
            }
        });

        mLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });

        if (mLayout.getAnchorPoint() == 1.0f) {
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        }

        txtPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // This is for Get get height and width of Screen
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                try {
                    display.getRealSize(size);
                } catch (NoSuchMethodError err) {
                    display.getSize(size);
                }
                final int width = size.x;
                int height = size.y;


                final Dialog dialog = new Dialog(OutStationBookingDetailsActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.layout_ride_payment);
                dialog.setTitle("");
                dialog.setCancelable(true);
                dialog.show();

                LinearLayout mainLayout = (LinearLayout) dialog.findViewById(R.id.mainLayout);
                mainLayout.setMinimumWidth((int) (width / 0.7));

                TextView txt_total_km = (TextView) dialog.findViewById(R.id.txt_total_km);
                TextView txt_total_time = (TextView) dialog.findViewById(R.id.txt_total_time);
                TextView txt_total_amount = (TextView) dialog.findViewById(R.id.txt_total_amount);
                TextView txt_booking_id = (TextView) dialog.findViewById(R.id.txt_booking_id);

                txt_total_km.setText(" " + stDistance + " km");
                txt_total_time.setText(" " + stDuration + " min");
                txt_total_amount.setText(" ₹" + stAmount);
                txt_booking_id.setText("#" + stBookingID);

                Button btn_pay = (Button) dialog.findViewById(R.id.btn_pay);

                btn_pay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        PaytmPayment();
                    }
                });

            }
        });

        txtPriceTNC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), MonthlySpendingTermsActivity.class);
                intent.putExtra("term_type", "6");
                startActivity(intent);

            }
        });
    }

    protected synchronized void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {

            mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
    }

    private void BookingDetails() {
        if (isInternetOn(getApplicationContext())) {
            progressDialog.show();
            Call<JsonObject> call = apiService.booking_detail(stBookingId, "1");
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    progressDialog.dismiss();
                    GetRideBookingDetails(response.body().toString());
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }

    private void PaytmPayment() {

        final PaytmPGService paytmPGService = PaytmPGService.getProductionService(); // Production

        int amt = (int) Math.round(Double.parseDouble(stAmount));

        Call<JsonObject> call = apiService.generate_checksum(stBookingId, preferencesUtility.getuser_id(), stAmount + "", "0");
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    JSONObject objData = new JSONObject(response.body().toString());
                    if (objData.getString("status").equalsIgnoreCase("1")) {

                        JSONObject objPayload = objData.getJSONObject("payload");

                        // Create a HASHMAP Object holding the Order Information
                        HashMap<String, String> paramMap = new HashMap<String, String>();
                        paramMap.put("MID", objPayload.getString("MID"));
                        paramMap.put("ORDER_ID", objPayload.getString("ORDER_ID"));
                        paramMap.put("CUST_ID", objPayload.getString("CUST_ID"));
                        paramMap.put("INDUSTRY_TYPE_ID", objPayload.getString("INDUSTRY_TYPE_ID"));
                        paramMap.put("CHANNEL_ID", objPayload.getString("CHANNEL_ID"));
                        paramMap.put("TXN_AMOUNT", objPayload.getString("TXN_AMOUNT"));
                        paramMap.put("WEBSITE", objPayload.getString("WEBSITE"));
                        paramMap.put("CALLBACK_URL", objPayload.getString("CALLBACK_URL"));

                        if (objPayload.has("EMAIL")) {
                            paramMap.put("EMAIL", objPayload.getString("EMAIL"));
                        }
                        if (objPayload.has("MOBILE_NO")) {
                            paramMap.put("MOBILE_NO", objPayload.getString("MOBILE_NO"));
                        }

                        paramMap.put("CHECKSUMHASH", objPayload.getString("CHECKSUMHASH"));

                        // Create Paytm Order Object
                        PaytmOrder paytmOrder = new PaytmOrder(paramMap);

                        paytmPGService.initialize(paytmOrder, null);

                        paytmPGService.startPaymentTransaction(OutStationBookingDetailsActivity.this, true, true,
                                new PaytmPaymentTransactionCallback() {
                                    @Override
                                    public void onTransactionResponse(Bundle bundle) {
                                        try {

                                            if (bundle.getString("STATUS").equals("TXN_SUCCESS")) {
                                                JSONObject json = new JSONObject();
                                                Set<String> keys = bundle.keySet();
                                                for (String key : keys) {
                                                    try {
                                                        // json.put(key, bundle.get(key)); see edit below
                                                        json.put(key, JSONObject.wrap(bundle.get(key)));
                                                    } catch (JSONException e) {
                                                        //Handle exception here
                                                    }
                                                }

                                                Log.e("JSon_data", json.toString());
                                                successPayload = json.toString();
                                                if (bundle.containsKey("TXNID")) {
                                                    paytmTransactionID = bundle.getString("TXNID").toString();
                                                }
                                                orderID = bundle.getString("ORDERID").toString();

                                                RidePaytmPaymentServiceCall();

                                            } else {
                                                JSONObject json = new JSONObject();
                                                Set<String> keys = bundle.keySet();
                                                for (String key : keys) {
                                                    try {
                                                        // json.put(key, bundle.get(key)); see edit below
                                                        json.put(key, JSONObject.wrap(bundle.get(key)));
                                                    } catch (JSONException e) {
                                                        //Handle exception here
                                                    }
                                                }

                                                Log.e("JSon_data", json.toString());
                                                failPayload = json.toString();
                                                if (bundle.containsKey("TXNID")) {
                                                    paytmTransactionID = bundle.getString("TXNID").toString();
                                                }
                                                orderID = bundle.getString("ORDERID").toString();

                                                failServiceCall();
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void networkNotAvailable() {
                                        Log.d("LOG", "UI Error Occur.");
                                        Toast.makeText(getApplicationContext(), " UI Error Occur. ", Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void clientAuthenticationFailed(String inErrorMessage) {
                                        Log.d("LOG", "clientAuthenticationFailed.");
                                        Toast.makeText(getApplicationContext(), " Sever-side Error " + inErrorMessage, Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void someUIErrorOccurred(String s) {
                                        Log.d("LOG", "UI Error Occur.");
                                    }

                                    @Override
                                    public void onErrorLoadingWebPage(int i, String s, String s1) {
                                        Log.d("LOG", "onErrorLoadingWebPage.");
                                    }

                                    @Override
                                    public void onBackPressedCancelTransaction() {
                                        Log.d("LOG", "onBackPressedCancelTransaction");
                                    }

                                    @Override
                                    public void onTransactionCancel(String s, Bundle bundle) {
                                        Log.d("LOG", "Payment_Transaction_Failed " + bundle);
                                        Toast.makeText(getBaseContext(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
                                    }
                                });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });


    }

    private void failServiceCall() {
        Call<JsonObject> call = apiService.paytm_payment_fail(stBookingId, failPayload, paytmTransactionID, orderID);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e("paytm_payment_fail", response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    /**
     * User give Tip to Driver
     */
    private void RidePaytmPaymentServiceCall() {
        try {

            String tip_amount = "";

            progressDialog.show();
            Call<JsonObject> call = apiService.ride_paytm_payment(stBookingId, preferencesUtility.getuser_id(), "0",
                    paytmTransactionID, successPayload, orderID, "1");

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        String mResult = response.body().toString();
                        progressDialog.dismiss();

                        JSONObject objData = new JSONObject(mResult);
                        if (objData.getString("status").equalsIgnoreCase("1")) {
                            toastDialog.ShowToastMessage(objData.getString("message"));
                            BookingDetails();
                        }
                    } catch (Exception e) {
                        progressDialog.dismiss();
                        Log.e("Exception", e.toString());
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("onFailure", t.getMessage().toString());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onLocationChanged(Location location) {

        mUserCurrentLat = location.getLatitude();
        mUserCurrentLng = location.getLongitude();

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mGoogleMap = map;

        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(OutStationBookingDetailsActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();




                mGoogleMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
        }

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
        mGoogleMap.getUiSettings().setCompassEnabled(false);
        mGoogleMap.getUiSettings().setTiltGesturesEnabled(false);
        mGoogleMap.setBuildingsEnabled(false);

        //Disable Marker click event
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        mLocationRequest.setSmallestDisplacement(10);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
                return;
            } else {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                        mLocationRequest, this);
            }
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            ShowToast(getResources().getString(R.string.network_disconnect));
        } else if (i == CAUSE_NETWORK_LOST) {
            ShowToast(getResources().getString(R.string.network_lost));
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("TAG", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    /**
     * Booking Details Parse JSON
     *
     * @param mResult
     */
    private void GetRideBookingDetails(String mResult) {
        String st_ride_price = "";

        try {
            JSONObject objBookingList = new JSONObject(mResult);
            if (objBookingList.getString("status").equalsIgnoreCase("1")) {
                JSONArray jarrPayload = objBookingList.getJSONArray("payload");
                if (jarrPayload.length() > 0) {
                    for (int i = 0; i < jarrPayload.length(); i++) {
                        JSONObject objBookingData = jarrPayload.getJSONObject(i);

                        stDriverId = objBookingData.getString("driver_id");
                        stBooking_Status = objBookingData.getString("booking_status");
                        stBookingID = objBookingData.getString("booking_id");
                        isTaxiRide = objBookingData.getString("is_taxi");
                        stServiceId = objBookingData.getString("service_id");

                        stBookingStatusForMarker = objBookingData.getString("booking_status");

                        timeCountDownForRide();

                        // Check upfront amount paid or not
                        if (objBookingData.optString("is_front_pay").equalsIgnoreCase("0")) {
                            txtPaytmPayment.setText("First pay upfront amount for confirm booking.");
                            layoutWaitingPayment.setVisibility(View.VISIBLE);
                        } else {
                            txtPaytmPayment.setText("UpFront already paid. Start your ride.");
                            layoutWaitingPayment.setVisibility(View.GONE);
                        }

                        //Check payment type - Paytm/Cash - to Pay remain amount
                        if (objBookingData.getString("payment_type").equalsIgnoreCase("0")) {
                            if (objBookingData.getString("user_payment_status").equals("0")) {
                                layoutWaitingPayment.setVisibility(View.VISIBLE);
                            }
                            txtPaymentType.setText("Paytm");
                            st_ride_price = getResources().getString(R.string.st_paid_via_paytm);
                        } else {
                            txtPaymentType.setText("Cash");
                            st_ride_price = getResources().getString(R.string.st_paid_via_cash);
                        }

                        // Change by Divya on 27-12-2017
                        // payment type
                        userPaymentStatus = objBookingData.getString("user_payment_status");

                        //check payment done or not
                        if (userPaymentStatus.equals("0") && objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_COMPLETE_TRIP)) {
                            Intent intent = new Intent(OutStationBookingDetailsActivity.this, ReviewRideActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("payment_details", objBookingData.toString());
                            startActivity(intent);
                        }

                        txtRideID.setText("#" + objBookingData.getString("booking_id"));
                        txtPickupAddress.setText(objBookingData.getString("pickup_address"));
                        txtDropAddress.setText(objBookingData.getString("drop_address"));
                        txtCarType.setText(objBookingData.getString("service_name"));

                        if (isTaxiRide.equalsIgnoreCase("5") || stBooking_Status.equalsIgnoreCase("5")) {
                            txtGoPickupAddress.setText(objBookingData.optString("go_pickup_location"));
                            txtGoDropAddress.setText(objBookingData.optString("go_drop_location"));
                        }

                        if (objBookingData.has("otp_code")) {
                            if (!objBookingData.getString("otp_code").equals("")) {
                                txtCode.setText("Code : " + objBookingData.getString("otp_code"));
                            }
                        }

                        txtDriverName.setText(objBookingData.getString("firstname") + " " + objBookingData.getString("lastname"));

                        if (objBookingData.getString("display_mobile").equalsIgnoreCase("")) {
                            txtDriverMobileNo.setText("---");
                        } else {
                            txtDriverMobileNo.setText(objBookingData.getString("display_mobile"));

                        }

                        if (objBookingData.getString("car_number").equalsIgnoreCase("")) {
                            txtDriverCarNumber.setText("---");
                        } else {
                            txtDriverCarNumber.setText(objBookingData.getString("car_number"));
                        }

                        if (objBookingData.optString("display_mobile").equalsIgnoreCase("")) {
                            stDriverContactNumber = objBookingData.optString("mobile");
                        } else {
                            stDriverContactNumber = objBookingData.optString("display_mobile");
                        }

                        txtCarModel.setText(objBookingData.getString("model_name"));
                        txtCarBrand.setText(objBookingData.getString("brand_name"));
                        txtCarColor.setText(objBookingData.getString("series_name"));

                        txtRidePrice.setText("₹ " + Math.round(Double.parseDouble(objBookingData.getString("amount"))));
                        txtRidePrice.setVisibility(View.VISIBLE);

                        txtPrice.setText(Html.fromHtml(st_ride_price + " " + getResources().getString(R.string.st_rupee) +
                                Math.round(Double.parseDouble(objBookingData.getString("amount")))));

                        txtRideDuration.setText(objBookingData.getString("duration") + " " + getResources().getString(R.string.st_minute));
                        txtRideDistance.setText(roundTwoDecimals(Double.parseDouble(objBookingData.getString("total_distance"))) + " " + getResources().getString(R.string.st_km));

                        if (objBookingData.getString("toll_tax").equalsIgnoreCase("")) {
                            txtRideTollPrice.setText(getResources().getString(R.string.st_toll) + ": ₹0.00");
                        } else {
                            txtRideTollPrice.setText(getResources().getString(R.string.st_toll) + ": ₹" + Math.round(Double.parseDouble(objBookingData.getString("toll_tax"))));
                        }

                        Glide.with(this).load(Global_ServiceApi.API_IMAGE_HOST + objBookingData.getString("icon"))
                                .crossFade()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imgCarType);
                        String img_url = Global_ServiceApi.API_IMAGE_HOST + objBookingData.getString("image");
                        Glide.with(this).load(img_url)
                                .crossFade()
                                .placeholder(R.drawable.bg_avatar)
                                .into(imgProfile);

                        /* Price Data */
                        arrPriceList = mHelper.GetPriceData(objBookingData.getString("price_id"));

                        mPickUpLat = Double.parseDouble(objBookingData.getString("pickup_lati"));
                        mPickUpLng = Double.parseDouble(objBookingData.getString("pickup_longi"));

                        mDropLat = Double.parseDouble(objBookingData.getString("drop_lati"));
                        mDropLng = Double.parseDouble(objBookingData.getString("drop_longi"));

                        mDriverLat = Double.parseDouble(objBookingData.getString("lati"));
                        mDriverLng = Double.parseDouble(objBookingData.getString("longi"));

                        stDistance = objBookingData.getString("est_total_distance");
                        stDuration = objBookingData.getString("est_duration");
                        stAmount = objBookingData.getString("amount");

                        txtCarType.setTextColor(Color.parseColor("#" + objBookingData.getString("color")));

                        if (isTaxiRide.equalsIgnoreCase("5") && stBooking_Status.equalsIgnoreCase("5")) {
                            mGoPickupLat = Double.parseDouble(objBookingData.optString("go_pickup_lati"));
                            mGoPickupLng = Double.parseDouble(objBookingData.optString("go_pickup_longi"));

                            mGoDropLat = Double.parseDouble(objBookingData.optString("go_drop_lati"));
                            mGoDropLng = Double.parseDouble(objBookingData.optString("go_drop_longi"));
                        }

                        // map zoom
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        builder.include(new LatLng(mPickUpLat, mPickUpLng));
                        builder.include(new LatLng(mDropLat, mDropLng));
                        LatLngBounds bounds = builder.build();
                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));

                        if (objBookingData.optString("is_return").equalsIgnoreCase("1")) {
                            txtReturnTrip.setText("Return Trip");
                        } else {
                            txtReturnTrip.setText("On Trip");
                        }

                        String stUrl = "";
                        FetchUrl fetchUrl;
                        switch (stBooking_Status) {

                            case Constant.BOOKING_STATUS.BOOKING_PENDING:

                                imgRideStatus.setImageResource(R.drawable.ic_pending);
                                txtRideStatus.setText(getResources().getString(R.string.st_ride_pending));
                                txtRideDateTime.setText(convertDateFormat(objBookingData.getString("pickup_date")));

                                // Add Marker on Map
                                setMarkerOnMap(objBookingData.getString("pickup_address"), objBookingData.getString("drop_address"), stBooking_Status);

                                // Drop path b/w pickup and drop location
                                stUrl = getUrl(new LatLng(mPickUpLat, mPickUpLng), new LatLng(mDropLat, mDropLng));
                                fetchUrl = new FetchUrl();
                                fetchUrl.execute(stUrl);

                                // Disable all button click event
                                setButtonED();

                                layoutCancelRide.setEnabled(true);
                                imgCancel.setEnabled(true);
                                imgCancel.setAlpha(1.0f);
                                txtCancelRide.setAlpha(1.0f);

                                rlReturnAddress.setVisibility(View.VISIBLE);
                                txtRideMessage.setVisibility(View.VISIBLE);
                                break;

                            case Constant.BOOKING_STATUS.BOOKING_ACCEPT:
                                imgRideStatus.setImageResource(R.drawable.ic_accepted);
                                txtRideStatus.setText(getResources().getString(R.string.st_ride_accept));
                                txtRideDateTime.setText(convertDateFormat(objBookingData.getString("accept_time")));

                                // Add Marker on Map
                                setMarkerOnMap(objBookingData.getString("pickup_address"), objBookingData.getString("drop_address"), stBooking_Status);

                                // Drop path b/w pickup and drop location
                                stUrl = getUrl(new LatLng(mPickUpLat, mPickUpLng), new LatLng(mDropLat, mDropLng));
                                fetchUrl = new FetchUrl();
                                fetchUrl.execute(stUrl);

                                // Disable all button click event
                                setButtonED();

                               if (!objBookingData.optString("is_return").equalsIgnoreCase("1")) {
                                   layoutCancelRide.setEnabled(true);
                                   imgCancel.setEnabled(true);
                                   imgCancel.setAlpha(1.0f);
                                   txtCancelRide.setAlpha(1.0f);
                               }

                                layoutSupport.setEnabled(true);
                                imgSupport.setEnabled(true);
                                imgSupport.setAlpha(1.0f);
                                txtSupport.setAlpha(1.0f);

                                layoutCall.setEnabled(true);
                                imgCall.setEnabled(true);
                                imgCall.setAlpha(1.0f);

                                rlCarBrandDetails.setVisibility(View.VISIBLE);
                                rlReturnAddress.setVisibility(View.VISIBLE);
                                txtCode.setVisibility(View.VISIBLE);
                                viewBrandDivider.setVisibility(View.VISIBLE);
                                lldriverDetails.setVisibility(View.VISIBLE);
                                break;

                            case Constant.BOOKING_STATUS.BOOKING_USER_WAIT:
                                imgRideStatus.setImageResource(R.drawable.ic_wait_user);
                                txtRideStatus.setText(getResources().getString(R.string.st_driver_has_arrived));
                                txtRideDateTime.setText(convertDateFormat(objBookingData.getString("start_time")));

                                // Add Marker on Map
                                setMarkerOnMap(objBookingData.getString("pickup_address"), objBookingData.getString("drop_address"), stBooking_Status);
                                createDriverMarker(mDriverLat, mDriverLng,"Driver Location", stServiceId);

                                // Drop path b/w pickup and drop location
                                stUrl = getUrl(new LatLng(mPickUpLat, mPickUpLng), new LatLng(mDropLat, mDropLng));
                                fetchUrl = new FetchUrl();
                                fetchUrl.execute(stUrl);

                                // Disable all button click event
                                setButtonED();

                                if (!objBookingData.optString("is_return").equalsIgnoreCase("1")) {
                                    layoutCancelRide.setEnabled(true);
                                    imgCancel.setEnabled(true);
                                    imgCancel.setAlpha(1.0f);
                                    txtCancelRide.setAlpha(1.0f);
                                }

                                layoutSupport.setEnabled(true);
                                imgSupport.setEnabled(true);
                                imgSupport.setAlpha(1.0f);
                                txtSupport.setAlpha(1.0f);

                                layoutCall.setEnabled(true);
                                imgCall.setEnabled(true);
                                imgCall.setAlpha(1.0f);
                                txtCall.setAlpha(1.0f);

                                rlCarBrandDetails.setVisibility(View.VISIBLE);
                                rlReturnAddress.setVisibility(View.VISIBLE);
                                txtCode.setVisibility(View.VISIBLE);
                                viewBrandDivider.setVisibility(View.VISIBLE);
                                lldriverDetails.setVisibility(View.VISIBLE);

                                // Start driver tracking
                                rideTracking();
                                break;

                            case Constant.BOOKING_STATUS.BOOKING_START:
                                imgRideStatus.setImageResource(R.drawable.ic_started);
                                txtRideStatus.setText(getResources().getString(R.string.st_ride_start));
                                txtRideDateTime.setText(convertDateFormat(objBookingData.getString("start_time")));

                                // Add Marker on Map
                                setMarkerOnMap(objBookingData.getString("pickup_address"), objBookingData.getString("drop_address"), stBooking_Status);
                                createDriverMarker(mDriverLat, mDriverLng,"Driver Location", stServiceId);

                                // Draw path b/w pickup and drop location
                                stUrl = getUrl(new LatLng(mDriverLat, mDriverLng), new LatLng(mDropLat, mDropLng));
                                fetchUrl = new FetchUrl();
                                fetchUrl.execute(stUrl);

                                // Disable all button click event
                                setButtonED();

                                layoutSupport.setEnabled(true);
                                imgSupport.setEnabled(true);
                                imgSupport.setAlpha(1.0f);
                                txtSupport.setAlpha(1.0f);

                                layoutCall.setEnabled(true);
                                imgCall.setEnabled(true);
                                imgCall.setAlpha(1.0f);
                                txtCall.setAlpha(1.0f);

                                rlCarBrandDetails.setVisibility(View.VISIBLE);
                                viewBrandDivider.setVisibility(View.VISIBLE);
                                layoutRideTimeDetails.setVisibility(View.VISIBLE);
                                viewRideDetails.setVisibility(View.VISIBLE);
                                rlReturnAddress.setVisibility(View.VISIBLE);
                                lldriverDetails.setVisibility(View.VISIBLE);

                                // Start driver tracking
                                rideTracking();
                                break;

                            case Constant.BOOKING_STATUS.BOOKING_COMPLETE:
                                imgRideStatus.setImageResource(R.drawable.ic_completed);
                                txtRideStatus.setText(getResources().getString(R.string.st_ride_complete));
                                txtRideDateTime.setText(convertDateFormat(objBookingData.getString("stop_time")));

                                // Add Marker on Map
                                setMarkerOnMap(objBookingData.getString("pickup_address"), objBookingData.getString("drop_address"), stBooking_Status);

                                if (isTaxiRide.equalsIgnoreCase("5")) {

                                    if (mMarkerPickupGO != null)
                                        mMarkerPickupGO.remove();

                                    if (mMarkerDropGO != null)
                                        mMarkerDropGO.remove();

                                    mMarkerPickupGO = mGoogleMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(mGoPickupLat, mGoPickupLng))
                                            .title("Pickup Go Address")
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin))
                                            .snippet(objBookingData.optString("go_pickup_location")));
                                    mMarkerPickupGO.setTag(4);

                                    mMarkerDropGO = mGoogleMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(mGoDropLat, mGoDropLng))
                                            .title("Drop Go Address")
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin))
                                            .snippet(objBookingData.optString("go_drop_location")));
                                    mMarkerDropGO.setTag(5);

                                    // add GO pickup-drop pin to array - to clear old pin and add new markers
                                    arrMarkersPin.add(mMarkerPickupGO);
                                    arrMarkersPin.add(mMarkerDropGO);

                                    /* Draw Path b/w pickup and drop location for GO complete ride */
                                    JSONArray _arrRouteLocation = objBookingData.getJSONArray("return_location");
                                    if (_arrRouteLocation.length() > 0) {
                                        for (int k = 0; k < _arrRouteLocation.length(); k++) {
                                            JSONObject objRoute = _arrRouteLocation.getJSONObject(k);
                                            routeArrayGoRide.add(new LatLng(objRoute.getDouble("latitude"), objRoute.getDouble("longitude")));
                                        }

                                        goLineOptions.addAll(routeArrayGoRide);
                                        goLineOptions.color(Color.parseColor("#27ae60"));
                                        goLineOptions.width(7.0f);
                                        mGoogleMap.addPolyline(goLineOptions);
                                    }

                                    rlOnTripAddress.setVisibility(View.VISIBLE);
                                    viewOnAddress.setVisibility(View.VISIBLE);

                                }

                                /* Draw Path b/w pickup and drop location for complete ride */
                                JSONArray _arrRouteLocation = objBookingData.getJSONArray("location");
                                if (_arrRouteLocation.length() > 0) {
                                    for (int k = 0; k < _arrRouteLocation.length(); k++) {
                                        JSONObject objRoute = _arrRouteLocation.getJSONObject(k);
                                        routeArray.add(new LatLng(objRoute.getDouble("latitude"), objRoute.getDouble("longitude")));
                                    }

                                    lineOptions.addAll(routeArray);
                                    lineOptions.color(Color.parseColor("#168E5A"));
                                    lineOptions.width(7.0f);
                                    mGoogleMap.addPolyline(lineOptions);
                                }

                                // Disable all button click event
                                setButtonED();

                                rlCarBrandDetails.setVisibility(View.VISIBLE);
                                viewBrandDivider.setVisibility(View.VISIBLE);
                                layoutRideTimeDetails.setVisibility(View.VISIBLE);
                                viewRideDetails.setVisibility(View.GONE);
                                layoutRideTollPrice.setVisibility(View.VISIBLE);
                                lldriverDetails.setVisibility(View.VISIBLE);

                                break;

                            case Constant.BOOKING_STATUS.BOOKING_CANCEL:
                                imgRideStatus.setImageResource(R.drawable.ic_cancelled);
                                txtRideStatus.setText(getResources().getString(R.string.st_ride_cancel));
                                txtRideDateTime.setText(convertDateFormat(objBookingData.getString("stop_time")));

                                // Add Marker on Map
                                setMarkerOnMap(objBookingData.getString("pickup_address"), objBookingData.getString("drop_address"), stBooking_Status);

                                // Drop path b/w pickup and drop location
                                stUrl = getUrl(new LatLng(mPickUpLat, mPickUpLng), new LatLng(mDropLat, mDropLng));
                                fetchUrl = new FetchUrl();
                                fetchUrl.execute(stUrl);

                                // Disable all button click event
                                setButtonED();

                                rlCarBrandDetails.setVisibility(View.VISIBLE);
                                viewBrandDivider.setVisibility(View.VISIBLE);
                                layoutRideTimeDetails.setVisibility(View.VISIBLE);
                                viewRideDetails.setVisibility(View.GONE);
                                rlReturnAddress.setVisibility(View.VISIBLE);
                                lldriverDetails.setVisibility(View.VISIBLE);

                                txtReturnTrip.setText("On Trip");
                                if (objBookingData.getString("payment_type").equalsIgnoreCase("0")) {
                                    if (objBookingData.getString("user_payment_status").equals("2")) {
                                        txtRefundPending.setVisibility(View.VISIBLE);
                                    } else if (objBookingData.getString("user_payment_status").equals("3")) {
                                        txtRefundInitiated.setVisibility(View.VISIBLE);
                                    }
                                }
                                break;

                            case Constant.BOOKING_STATUS.BOOKING_AUTO_REJECT:
                                imgRideStatus.setImageResource(R.drawable.ic_auto_rejected);
                                txtRideStatus.setText("Driver Not Available");
                                txtRideDateTime.setText(convertDateFormat(objBookingData.getString("stop_time")));

                                // Add Marker on Map
                                setMarkerOnMap(objBookingData.getString("pickup_address"), objBookingData.getString("drop_address"), stBooking_Status);

                                // Drop path b/w pickup and drop location
                                stUrl = getUrl(new LatLng(mPickUpLat, mPickUpLng), new LatLng(mDropLat, mDropLng));
                                fetchUrl = new FetchUrl();
                                fetchUrl.execute(stUrl);

                                // Disable all button click event
                                setButtonED();

                                layoutRideTimeDetails.setVisibility(View.VISIBLE);
                                viewRideDetails.setVisibility(View.GONE);
                                rlReturnAddress.setVisibility(View.VISIBLE);
                                lldriverDetails.setVisibility(View.VISIBLE);

                                txtReturnTrip.setText("On Trip");
                                break;

                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * set Button click event Enable-Disable according to ride status
     */
    private void setButtonED() {

        layoutCall.setEnabled(false);
        layoutSupport.setEnabled(false);
        layoutCancelRide.setEnabled(false);

        imgCall.setEnabled(false);
        imgSupport.setEnabled(false);
        imgCancel.setEnabled(false);

        txtCall.setEnabled(false);
        txtSupport.setEnabled(false);
        txtCancelRide.setEnabled(false);

        imgCall.setAlpha(0.2f);
        imgSupport.setAlpha(0.2f);
        imgCancel.setAlpha(0.2f);

        txtCall.setAlpha(0.2f);
        txtCancelRide.setAlpha(0.2f);
        txtSupport.setAlpha(0.2f);

        txtRefundPending.setVisibility(View.GONE);
        txtRefundInitiated.setVisibility(View.GONE);

        viewBrandDivider.setVisibility(View.GONE);
        viewOnAddress.setVisibility(View.GONE);
        viewRideDetails.setVisibility(View.GONE);
        viewRidePrice.setVisibility(View.GONE);

        rlCarBrandDetails.setVisibility(View.GONE);
        layoutRideTimeDetails.setVisibility(View.GONE);
        rlOnTripAddress.setVisibility(View.GONE);
        layoutRideTollPrice.setVisibility(View.GONE);
        rlReturnAddress.setVisibility(View.GONE);
        txtCode.setVisibility(View.INVISIBLE);

        txtRideMessage.setVisibility(View.GONE);
        lldriverDetails.setVisibility(View.GONE);
    }

    /**
     * set Pickup and Drop address Marker on Map according to Ride Status
     */
    private void setMarkerOnMap(String pickupAddress, String dropAddress, String stStatus) {

        // Remove old position marker
        if (mMarker_pickup != null) {
            mMarker_pickup.remove();
        }

        // Remove old position marker
        if (mMarker_drop != null) {
            mMarker_drop.remove();
        }

        switch (stStatus) {
            case Constant.BOOKING_STATUS.BOOKING_PENDING:
            case Constant.BOOKING_STATUS.BOOKING_ACCEPT:
            case Constant.BOOKING_STATUS.BOOKING_USER_WAIT:
            case Constant.BOOKING_STATUS.BOOKING_COMPLETE:
            case Constant.BOOKING_STATUS.BOOKING_CANCEL:
            case Constant.BOOKING_STATUS.BOOKING_AUTO_REJECT:

                mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(mPickUpLat, mPickUpLng))
                        .title("Pickup Address")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin))
                        .snippet(pickupAddress));
                mMarker_pickup.setTag(0);

                mMarker_drop = mGoogleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(mDropLat, mDropLng))
                        .title("Drop Address")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin))
                        .snippet(dropAddress));
                mMarker_drop.setTag(1);

                // add pickup-drop pin to array - to clear old pin and add new markers
                arrMarkersPin.add(mMarker_pickup);
                arrMarkersPin.add(mMarker_drop);
                break;

            case Constant.BOOKING_STATUS.BOOKING_START:
                mMarker_drop = mGoogleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(mDropLat, mDropLng))
                        .title("Drop Address")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin))
                        .snippet(dropAddress));
                mMarker_drop.setTag(1);

                arrMarkersPin.add(mMarker_drop);
                break;

        }
    }

    /**
     * Convert server date to Date Format
     *
     *
     * @param stDate
     * @return
     */
    private String convertDateFormat(String stDate) {

        try {
            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = df.parse(stDate);
            df.setTimeZone(TimeZone.getDefault());
            SimpleDateFormat date_fmt = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.getDefault());
            String _date = date_fmt.format(date);

            return _date;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Add Driver marker on google map
     *
     * @param latitude
     * @param longitude
     * @param title
     */
    protected void createDriverMarker(double latitude, double longitude, String title, String service_id) {

        if (mMarker_driver != null) {
            mMarker_driver.remove();
        }

        if (service_id != null) {

            switch (service_id) {

                case "1":
                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_1)));
                    break;

                case "2":
                case "13":
                case "18":
                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_2)));
                    break;

                case "3":
                case "19":
                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_3)));
                    break;

                case "11":
                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_11)));
                    break;

                case "12":
                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_12)));
                    break;

                case "14":
                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_14)));
                    break;

                case "15":
                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_15)));
                    break;

                case "16":
                case "17":
                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_16)));
                    break;

                default:
                    mMarker_driver = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_taxi)));
            }

        } else {
            mMarker_driver = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_taxi)));
        }

        mMarker_driver.setTag(3);
        arrMarkersPin.add(mMarker_driver);
    }

    /**
     *
     *  Get Ride Tracking Details
     *
     */
    private void rideTracking() {
        if (stBooking_Status.equalsIgnoreCase(Constant.BOOKING_STATUS.BOOKING_USER_WAIT)
                || stBooking_Status.equalsIgnoreCase(Constant.BOOKING_STATUS.BOOKING_START)) {

            if (isInternetOn(getApplicationContext())) {
                Call<JsonObject> call = apiService.tracking(preferencesUtility.getuser_id(), stDriverId, stBookingID, stBooking_Status, "1");

                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        Intent intent = new Intent("User_Ride_Tracking");
                        intent.putExtra("user_ride_track_data", response.body().toString());
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });
            } else {
                toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
            }
        }
    }

    private void CallToDriver(String ValeMobile) {
        try {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {

                Intent intent_call = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ValeMobile));
                startActivity(intent_call);
            }
        } catch (Exception ex) {
            toastDialog.ShowToastMessage(getResources().getString(R.string.call_error));
            ex.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        /* Ride Tracking */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_UserRideTracking,
                new IntentFilter("User_Ride_Tracking"));

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_UserWaitForDriver,
                new IntentFilter("User_Wait_For_Driver_Data"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_DriverAcceptUserRequest,
                new IntentFilter("User_Ride_Request"));

        /* This Call Receive After Driver start trip from @OhiApplication */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_RideStartActivity,
                new IntentFilter("User_Ride_Start"));

        /* This Call Receive After User Give a Review to Driver For Finish This Activity */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_ReviewFinishActivity,
                new IntentFilter("Review_Activity_finish"));

        /* Call When ride is OutStation and complete one-side trip */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_OutStationRideStop,
                new IntentFilter("OutStation_Trip_One_Stop"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /* Ride Tracking */
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_UserRideTracking);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_UserWaitForDriver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_DriverAcceptUserRequest);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_DetailsFinishActivity);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_ReviewFinishActivity);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_RideStartActivity);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_OutStationRideStop);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Reload Ride Service List - After Start Ride
     */
    private BroadcastReceiver mMessageReceiver_RideStartActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mMarker_pickup != null) {
                mMarker_pickup.remove();
            }

            for (Polyline line : arrPolyLines) {
                line.remove();
            }

            arrPolyLines.clear();

            if (cT != null) {
                cT.cancel();
            }
            finish();
            startActivity(getIntent());
        }
    };

    /**
     * Finish This Activity After Giving Review & Tips
     */
    private BroadcastReceiver mMessageReceiver_ReviewFinishActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("called", "yes");
            finish();
        }
    };

    /**
     * Reload Ride Service List - After OutStation one-side trip complete
     */
    private BroadcastReceiver mMessageReceiver_OutStationRideStop = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final Dialog dialog = new Dialog(OutStationBookingDetailsActivity.this);
            dialog.setContentView(R.layout.layout_dialog_popup);
            dialog.setTitle("");
            dialog.setCancelable(false);

            AppCompatTextView txt_popup_header = (AppCompatTextView) dialog.findViewById(R.id.txt_popup_header);
            AppCompatTextView txt_popup_message = (AppCompatTextView) dialog.findViewById(R.id.txt_popup_message);
            AppCompatButton btn_ok = (AppCompatButton) dialog.findViewById(R.id.btn_ok);

            txt_popup_header.setText("RoundTrip Pickup To Drop concluded");
            txt_popup_message.setText(intent.getStringExtra("message"));

            txt_popup_header.setTypeface(global_typeface.Sansation_Bold());
            txt_popup_message.setTypeface(global_typeface.Sansation_Regular());
            btn_ok.setTypeface(global_typeface.Sansation_Regular());

            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BookingDetails();
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    };

    /**
     * Receiver Call From Dialog Activity
     * <p>
     * Finish Detail Activity on Cancel Request
     */
    private BroadcastReceiver mMessageReceiver_DetailsFinishActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };

    private BroadcastReceiver mMessageReceiver_minActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Double sec = Double.valueOf(intent.getIntExtra("min_data", 0));
            txtRideDuration.setText(sec + " min");
        }
    };

    /**
     * User Wait Timer for Driver
     */
    private BroadcastReceiver mMessageReceiver_UserWaitForDriver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            BookingDetails();

            String mResult_UserWaitDriver = intent.getStringExtra("user_wait_driver_data");
            try {
                JSONObject objResult = new JSONObject(mResult_UserWaitDriver);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                if (objResult.getString("status").equalsIgnoreCase("1")) {
                    JSONArray arrPayload = objResult.getJSONArray("payload");
                    if (arrPayload.length() > 0) {
                        JSONObject objData = arrPayload.getJSONObject(0);
                        preferencesUtility.setBookingIdOnRide(objData.getString("booking_id"));
                        preferencesUtility.setBookingStatusOnRide(objData.getString("booking_status"));
                        int wait_time = objData.getInt("waiting");

                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.SECOND, wait_time);
                        String formattedDate = df.format(calendar.getTime());
                        preferencesUtility.setDriverWaitTime(formattedDate);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    /**
     * Request Accept Timer
     */
    private BroadcastReceiver mMessageReceiver_DriverAcceptUserRequest = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_UserWaitDriver = intent.getStringExtra("user_wait_driver_data");
            BookingDetails();

            try {
                JSONObject objResult = new JSONObject(mResult_UserWaitDriver);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                if (objResult.getString("status").equalsIgnoreCase("1")) {
                    JSONArray arrPayload = objResult.getJSONArray("payload");
                    if (arrPayload.length() > 0) {
                        JSONObject objData = arrPayload.getJSONObject(0);
                        preferencesUtility.setBookingIdOnRide(objData.getString("booking_id"));
                        preferencesUtility.setBookingStatusOnRide(objData.getString("booking_status"));
                        int wait_time = objData.getInt("waiting");

                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.SECOND, wait_time);
                        String formattedDate = df.format(calendar.getTime());
                        preferencesUtility.setDriverWaitTime(formattedDate);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    /**
     * User Ride Booking Tracking Receiver
     */
    private BroadcastReceiver mMessageReceiver_UserRideTracking = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_RideTracking = intent.getStringExtra("user_ride_track_data");
            prevLoc = new Location(LocationManager.GPS_PROVIDER);
            newLoc = new Location(LocationManager.GPS_PROVIDER);

            try {
                JSONObject objTracking = new JSONObject(mResult_RideTracking);
                if (objTracking.getString("status").equalsIgnoreCase("2")) {

                    if (mMarker_pickup != null) {
                        mMarker_pickup.remove();
                    }

                    if (mMarker_drop != null) {
                        mMarker_drop.remove();
                    }

                    JSONObject objPayload = objTracking.getJSONObject("payload");

                    mDriverLat = Double.parseDouble(objPayload.getString("latitude"));
                    mDriverLng = Double.parseDouble(objPayload.getString("longitude"));

                    if (objPayload.getString("booking_id").equalsIgnoreCase(stBookingID)) {

                        if (objPayload.getString("booking_status").equalsIgnoreCase(Constant.BOOKING_STATUS.BOOKING_START)) {

                            if (mDriverLat != 0.0 && mDriverLng != 0.0 && mPickUpLat != 0.0 && mPickUpLng != 0.0) {

                                // Remove old polyline from map
                                for (Polyline line : arrPolyLines) {
                                    line.remove();
                                }
                                arrPolyLines.clear();

                                LatLng pickup = new LatLng(mDriverLat, mDriverLng);
                                LatLng destination = new LatLng(mDropLat, mDropLng);

                                String url = getUrl(pickup, destination);
                                FetchUrl FetchUrl = new FetchUrl();
                                FetchUrl.execute(url);

                                mMarker_drop = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDropLat, mDropLng))
                                        .title(getResources().getString(R.string.st_drop_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin)));
                            }
                        } else if (objPayload.getString("booking_status").equalsIgnoreCase(Constant.BOOKING_STATUS.BOOKING_USER_WAIT)) {

                            // Remove old polyline from map
                            /*
                            for (Polyline line : arrPolyLines) {
                                line.remove();
                            }
                            arrPolyLines.clear();

                            LatLng pickup = new LatLng(mDriverLat, mDriverLng);
                            LatLng destination = new LatLng(mPickUpLat, mPickUpLng); */

                            if (mDriverLat != 0.0 && mDriverLng != 0.0 && mPickUpLat != 0.0 && mPickUpLng != 0.0) {

                                /*
                                String url = getUrl(pickup, destination);
                                FetchUrl FetchUrl = new FetchUrl();
                                FetchUrl.execute(url); */

                                String stTitle = "", stSnippet = "";
                                if (TotalDuration == 0) {
                                    stTitle = "wait ...";
                                    stSnippet = "... ...";
                                } else {
                                    stTitle = "Arrival";
                                    stSnippet = Math.round(TotalDuration) + " mins";
                                }

                                mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mPickUpLat, mPickUpLng))
                                        .title(stTitle)
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin))
                                        .snippet(stSnippet));

                                mMarker_drop = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDropLat, mDropLng))
                                        .title(getResources().getString(R.string.st_drop_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin)));

                                mMarker_pickup.showInfoWindow();
                            }
                        }

                        // Create or Update Driver Marker pin
                        if (mMarker_driver == null) {
                            createDriverMarker(mDriverLat, mDriverLng, "Driver Location", stServiceId);

                        } else {
                            prevLoc.setLatitude(mMarker_driver.getPosition().latitude);
                            prevLoc.setLongitude(mMarker_driver.getPosition().longitude);

                            mMarker_driver.setPosition(new LatLng(mDriverLat, mDriverLng));

                            newLoc.setLatitude(mMarker_driver.getPosition().latitude);
                            newLoc.setLongitude(mMarker_driver.getPosition().longitude);

                            float bearing = prevLoc.bearingTo(newLoc);
                            mMarker_driver.setRotation(bearing);
                        }
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    CountDownTimer mCountDownTimer;

    /**
     * Timer for Current Ride
     */
    private void timeCountDownForRide() {

        try {
            final String format = "%1$02d"; // two digits

            if (preferencesUtility.getIsRideStarted().equalsIgnoreCase("true") && preferencesUtility.getStartRideBookindId().equals(stBookingID)) {
                if (mCountDownTimer == null) {

                    mCountDownTimer = new CountDownTimer(totalSeconds * 1000, 1 * 1000) {

                        public void onTick(long millisUntilFinished) {

                            long totalDifferent = rideTimeDifference();

                            long secondsInMilli = 1000;
                            long minutesInMilli = secondsInMilli * 60;
                            long hoursInMilli = minutesInMilli * 60;
                            long daysInMilli = hoursInMilli * 24;

                            long elapsedDays = totalDifferent / daysInMilli;
                            totalDifferent = totalDifferent % daysInMilli;

                            long elapsedHours = totalDifferent / hoursInMilli;
                            totalDifferent = totalDifferent % hoursInMilli;

                            long elapsedMinutes = totalDifferent / minutesInMilli;
                            totalDifferent = totalDifferent % minutesInMilli;

                            long elapsedSeconds = totalDifferent / secondsInMilli;

                            txtRideDuration.setText(String.format(format, elapsedHours) + ":" + String.format(format, elapsedMinutes) + ":" + String.format(format, elapsedSeconds) + " min");
                        }

                        public void onFinish() {
                            Log.d("done!", "Time's up!");
                        }
                    };

                    mCountDownTimer.start();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Calculate difference between two dates
     */
    private long rideTimeDifference() {

        //1 minute = 60 seconds
        //1 hour = 60 x 60 = 3600
        //1 day = 3600 x 24 = 86400

        try {
            // Date Formatter
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy HH:mm:ss");

            // Get Current Date
            Calendar calendar = Calendar.getInstance();
            String strDate = simpleDateFormat.format(calendar.getTime());

            Date startDate = simpleDateFormat.parse(preferencesUtility.getCurrentTimeRideStart());
            Date endDate = simpleDateFormat.parse(strDate);

            //milliseconds
            long differentMilliSec = endDate.getTime() - startDate.getTime();

            long differentTime = endDate.getTime() - startDate.getTime();
            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = differentTime / daysInMilli;
            differentTime = differentTime % daysInMilli;

            long elapsedHours = differentTime / hoursInMilli;
            differentTime = differentTime % hoursInMilli;

            long elapsedMinutes = differentTime / minutesInMilli;
            differentTime = differentTime % minutesInMilli;

            long elapsedSeconds = differentTime / secondsInMilli;

//            System.out.printf(
//                    "%d days, %d hours, %d minutes, %d seconds%n",
//                    elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

            return differentMilliSec;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mSupportMapFragment.onLowMemory();
    }

    private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        String mode = "mode=driving";

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode + "&key=AIzaSyAfye39JZYcYdS196GZrvxRBfMShOCaA3w";

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DataParser parser = new DataParser();

                // Starts parsing data
                routes = parser.parse(jObject);
                getTotalKmPrice = parser.parseTotal(jObject);

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
//            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;
            arrPoints.clear();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
//                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    arrPoints.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(arrPoints);
                lineOptions.width(10);
                lineOptions.color(Color.BLUE);

                if (getTotalKmPrice.size() > 0) {

                    if (getTotalKmPrice.containsKey("totalKM")) {
                        TotalKm = getTotalKmPrice.get("totalKM");
                    } else {
                        TotalKm = 0;
                    }

                    if (getTotalKmPrice.containsKey("totalMin")) {
                        TotalDuration = getTotalKmPrice.get("totalMin");
                    } else {
                        TotalDuration = 0;
                    }
                }

                String stTitle = "", stSnippet = "";
                if (TotalDuration == 0) {
                    stTitle = "wait...";
                    stSnippet = "......";
                } else {
                    stTitle = "Arrival";
                    stSnippet = Math.round(TotalDuration) + " mins";
                }

                if (stBookingStatusForMarker.equalsIgnoreCase(Constant.STATUS_GOTO_USER) || stBookingStatusForMarker.equalsIgnoreCase(Constant.STATUS_USER_WAIT)) {
                    if (mMarker_pickup != null) {
                        mMarker_pickup.remove();
                    }

                    mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(mPickUpLat, mPickUpLng))
                            .title(stTitle)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin))
                            .snippet(stSnippet));
                    mMarker_pickup.showInfoWindow();
                    arrMarkersPin.add(mMarker_pickup);
                }
            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                arrPolyLines.add(mGoogleMap.addPolyline(lineOptions));
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }
    }
}

