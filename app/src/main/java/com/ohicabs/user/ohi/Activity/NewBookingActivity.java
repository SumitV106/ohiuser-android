package com.ohicabs.user.ohi.Activity;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.Adpater.CarAdapter;
import com.ohicabs.user.ohi.CustomWidgets.CustomBoldFontButton;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontTextView;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.ServiceManager.VolleyResponseListener;
import com.ohicabs.user.ohi.ServiceManager.VolleyUtils;
import com.sdoward.rxgooglemap.MapObservableProvider;

import org.json.JSONArray;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;
import rx.subscriptions.Subscriptions;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;
import static com.ohicabs.user.ohi.Utils.Constant.ADD_CARDS;
import static com.ohicabs.user.ohi.Utils.Constant.BACK_TO_ACTIVITY;
import static com.ohicabs.user.ohi.Utils.Constant.CONNECTION_FAILURE_RESOLUTION_REQUEST;
import static com.ohicabs.user.ohi.Utils.Constant.PLACE_AUTOCOMPLETE_REQUEST_CODE_DROPTO;
import static com.ohicabs.user.ohi.Utils.Constant.PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP;
import static com.ohicabs.user.ohi.Utils.Constant.REQUEST_CODE_DROP_ADDRESS;
import static com.ohicabs.user.ohi.Utils.Constant.REQUEST_CODE_PICKUP_ADDRESS;

public class NewBookingActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener,
        View.OnClickListener, GoogleMap.OnMyLocationButtonClickListener, RoutingListener {

    /* Tag to Log */
    private static String TAG = NewBookingActivity.class.getSimpleName();

    @BindView(R.id.imgfab) ImageButton imgFabCurrentLocation;
    @BindView(R.id.txt_pickup) CustomRegularFontTextView txtPickupLocation;
    @BindView(R.id.txt_dropto) CustomRegularFontTextView txtDropLocation;
    @BindView(R.id.btnbooking) CustomBoldFontButton btnBooking;
    @BindView(R.id.imgback) ImageView imgBack;
    @BindView(R.id.img_pickup_marker) ImageView imgPickupMarker;
    @BindView(R.id.img_drop_marker) ImageView imgDropMarker;
    @BindView(R.id.img_save_drop_address) ImageView imgSaveDropAddress;
    @BindView(R.id.img_save_pickup_address) ImageView imgSavePickupAddress;

    @BindView(R.id.img_search_drop_address) ImageView imgSearchDropLocation;
    @BindView(R.id.img_search_pickup_address) ImageView imgSearchPickupLocation;
    @BindView(R.id.img_saved_drop_address) ImageView imgSavedDropAddress;
    @BindView(R.id.img_saved_pickup_address) ImageView imgSavedPickupAddress;
    @BindView(R.id.lv_carItems) TwoWayView listCarServiceType;
    @BindView(R.id.txt_duration) AppCompatTextView txtDurationFromDriver;

    @BindView(R.id.cardview_est_price) CardView cardViewEstimatePrice;
    @BindView(R.id.cardview_noservice) CardView cardViewNoService;
    @BindView(R.id.cardview_taxi) CardView cardViewTaxi;

    @BindView(R.id.lypayment) LinearLayout llPayment;
    @BindView(R.id.lydatetime) LinearLayout llDateTime;

    @BindView(R.id.txt_estimated_price) AppCompatTextView txtEstimatedPrice;
    @BindView(R.id.txt_pickup_time) AppCompatTextView txtPickupTime;
    @BindView(R.id.txt_set_payment) AppCompatTextView txtSetPayment;
    @BindView(R.id.txt_estimated_time) CustomRegularFontTextView txtEstimatedTime;
    @BindView(R.id.lydropto) RelativeLayout rlDropToLayout;

    private ArrayList<Marker> arrCarMarkers = new ArrayList<>();
    private ArrayList<HashMap> arrPriceList = new ArrayList<>();
    private ArrayList<HashMap> arrZoneList = new ArrayList<>();
    private ArrayList<HashMap> arrCarList = new ArrayList<>();
    private ArrayList<LatLng> arrPoints = new ArrayList<>();
    private List<Polyline> arrPolyLines = new ArrayList<Polyline>();

    private static final int[] COLORS = new int[]{R.color.colorPolyLine};

    private CarAdapter carAdapter;

    /* Check view in Horizontal ListView when item selected */
    private View mLastView;

    private Marker mMarkerPickupDrop;

    private GoogleMap mMap;
    private SupportMapFragment mMapFragment;

    private MapObservableProvider mapObservableProvider;
    private CompositeSubscription subscriptions = Subscriptions.from();

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;

    private String stLocationTypeSelected, stZoneID, stTempZoneID = "0", stDropZoneID, stRideType,
            stServiceID = "", isTaxiService, stServiceName, stPriceID, stCardResult, stTotalAmount,
            stMaxOfferAmount = "", stPaytmOffer = "", stEstTotalDistance = "", stEstDuration = "", stSecondSeatAmt, stTwoSeatPrice;
    double gstTax = 0.0, dPrice, dTotalKm = 0, dTotalDuration = 0;

    private double mUserCurrentLat, mUserCurrentLang, mPickUpLat, mPickUpLng, mDropLat ,mDropLng;
    private boolean isClickOnCar = false;

    // used for differentiate b/w search address and saved address
    private String stLocationType = "";

    // used for differentiate b/w search address and saved address
    private boolean isMapTouched = false;

    // use only when screen load first time
    private boolean isFirstLaunch = true;

    private static final long INTERVAL = 10 * 1000; //* 10
    private static final long FASTEST_INTERVAL = 7 * 1000; // * 7

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        ButterKnife.bind(this);

        initViews();
    }

    /*
     * Initialize views
     */
    private void initViews() {

        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map);
        mapObservableProvider = new MapObservableProvider(mMapFragment);

        // set Views once all views are initialized
        setViews();
    }

    /*
     * Set defaults for views and fill default data to views.
     */
    private void setViews() {

        String rideType = getIntent().getStringExtra("ride_type");
        if (rideType.equalsIgnoreCase("ride_now")) {
            stRideType = "1";
            llDateTime.setVisibility(View.GONE);
        } else {
            stRideType = "0";
            llDateTime.setVisibility(View.VISIBLE);
        }

        setContent();

        // implement views event listeners once all views are initialized
        initViewsEventListeners();
    }

    /*
     * Set default activity content and details
     */
    private void setContent() {

        stLocationTypeSelected = "pickup_location";

        subscriptions.add(mapObservableProvider.getMapReadyObservable()
                .subscribe(new Action1<GoogleMap>() {
                    @Override
                    public void call(GoogleMap googleMap) {

                        mMap = googleMap;

                        if (ActivityCompat.checkSelfPermission(NewBookingActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(NewBookingActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(NewBookingActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
                        }

                        buildGoogleApiClient();
                        mMap.setMyLocationEnabled(true);

                        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        mMap.getUiSettings().setZoomControlsEnabled(false);
                        mMap.getUiSettings().setRotateGesturesEnabled(false);
                        mMap.getUiSettings().setCompassEnabled(false);
                        mMap.getUiSettings().setTiltGesturesEnabled(false);
                        mMap.setBuildingsEnabled(false);

                        //Disable Marker click event
                        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                                return true;
                            }
                        });

                        mMap.setOnMyLocationButtonClickListener(NewBookingActivity.this);

                        mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
                            @Override
                            public void onCameraMoveStarted(int reason) {
                                if (reason == REASON_GESTURE) {
                                    isMapTouched = true;

                                } else if (reason == REASON_API_ANIMATION) {
                                    isMapTouched = false;

                                } else if (reason == REASON_DEVELOPER_ANIMATION) {
                                    isMapTouched = false;
                                }
                            }
                        });

                        }
                }));

        subscriptions.add(mapObservableProvider.getCameraMoveObservable()
                .subscribe(new Action1<Void>() {
                    @Override
                    public void call(Void aVoid) {
                        Log.d(MapsActivity.class.getName(), "map move");
                    }
                }));

        subscriptions.add(mapObservableProvider.getCameraMoveCanceledObservable()
                .subscribe(new Action1<Void>() {
                    @Override
                    public void call(Void aVoid) {
                        Log.d(MapsActivity.class.getName(), "map move canceled");
                    }
                }));

        subscriptions.add(mapObservableProvider.getCameraIdleObservable()
                .subscribe(new Action1<Void>() {
                    @Override
                    public void call(Void aVoid) {
                        if (isFirstLaunch) {
                            getRelatedCabService();

                        } else {
                            if (isMapTouched || stLocationType.equalsIgnoreCase("search_address")) {

                                LatLng mPosition = mMap.getCameraPosition().target;
                                if (stLocationTypeSelected.equalsIgnoreCase("pickup_location")) {

                                    mPickUpLat = mPosition.latitude;
                                    mPickUpLng = mPosition.longitude;
                                    imgPickupMarker.setVisibility(View.VISIBLE);
                                    imgDropMarker.setVisibility(View.GONE);

                                    /*
                                     * To checkout Pickup Lat-Lng inside Zone area or not!
                                     */
                                    if (CheckZoneArea(mPickUpLat, mPickUpLng)) {

                                        HashMap<String, String> mapZoneDetails = GetZoneID(mPickUpLat, mPickUpLng, "AllZone");
                                        stZoneID = mapZoneDetails.get("ZoneId");
                                        if (!stZoneID.equalsIgnoreCase(stTempZoneID)) {
                                            stTempZoneID = stZoneID;

                                            /* Get CarList diff with Zone Area */
                                            GetCarList();
                                        }

                                    } else {

                                        // Set Default zone if area not found
                                        HashMap<String, String> mapZoneDetails = GetZoneID(mPickUpLat, mPickUpLng, "DefaultZone");
                                        stZoneID = mapZoneDetails.get("ZoneId");
                                        if (!stZoneID.equalsIgnoreCase(stTempZoneID)) {
                                            stTempZoneID = stZoneID;

                                            /* Get CarList diff with Zone Area */
                                            GetCarList();
                                        }
                                    }

                                    //Get Address from Client current lat-lng
                                    GetAddressFromLatLng(mPickUpLat, mPickUpLng);


                                } else if (stLocationTypeSelected.equalsIgnoreCase("drop_location")) {

                                    mDropLat = mPosition.latitude;
                                    mDropLng = mPosition.longitude;

                                    imgPickupMarker.setVisibility(View.GONE);
                                    imgDropMarker.setVisibility(View.VISIBLE);
                                    txtDurationFromDriver.setVisibility(View.GONE);
                                    txtDurationFromDriver.setText("");

                                    if (CheckZoneArea(mDropLat, mDropLng)) {
                                        HashMap<String, String> mapZoneDetails = GetZoneID(mDropLat, mDropLng, "AllZone");
                                        stDropZoneID = mapZoneDetails.get("ZoneId");

                                    } else {
                                        // Set Default zone if area not found
                                        HashMap<String, String> mapZoneDetails = GetZoneID(mDropLat, mDropLng, "DefaultZone");
                                        stDropZoneID = mapZoneDetails.get("ZoneId");
                                    }

                                    //Get Address from Client current lat-lng
                                    GetAddressFromLatLng(mDropLat, mDropLng);
                                }

                                /*
                                 * Check selected location inside zone are or not
                                 */
                                if (mDropLat != 0.0 || mDropLng != 0.0) {
                                    try {
                                        if (stZoneID.equals(stDropZoneID)) {
                                            if (arrCarList.size() == 0) {
                                                cardViewNoService.setVisibility(View.VISIBLE);
                                            } else {
                                                cardViewNoService.setVisibility(View.GONE);
                                                if (isClickOnCar) {
                                                    cardViewEstimatePrice.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        } else {
                                            cardViewNoService.setVisibility(View.VISIBLE);
                                            cardViewEstimatePrice.setVisibility(View.GONE);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                /*
                                 * Draw polyline on map and calculate estimate ride time and km
                                 */
                                CalculationByDistance(mPickUpLat, mPickUpLng, mDropLat, mDropLng);

                                /*
                                 * Update driver on map according to zone selection
                                 */
                                updateDriverOnMap(String.valueOf(mPickUpLat), String.valueOf(mPickUpLng), stServiceID, stZoneID);
                            }
                        }
                    }
                }));
    }

    /**
     * this function call only once when Activity launch
     *
     * -To get related ride services
     *
     */
    private void getRelatedCabService() {
        isFirstLaunch = false;

        LatLng mPosition = mMap.getCameraPosition().target;

        if (stLocationTypeSelected.equalsIgnoreCase("pickup_location")) {

            mPickUpLat = mPosition.latitude;
            mPickUpLng = mPosition.longitude;
            imgPickupMarker.setVisibility(View.VISIBLE);
            imgDropMarker.setVisibility(View.GONE);

            /*
             * To checkout Pickup Lat-Lng inside Zone area or not!
             */
            if (CheckZoneArea(mPickUpLat, mPickUpLng)) {

                HashMap<String, String> mapZoneDetails = GetZoneID(mPickUpLat, mPickUpLng, "AllZone");
                stZoneID = mapZoneDetails.get("ZoneId");
                if (!stZoneID.equalsIgnoreCase(stTempZoneID)) {
                    stTempZoneID = stZoneID;

                    /* Get CarList diff with Zone Area */
                    GetCarList();
                }

            } else {

                // Set Default zone if area not found
                HashMap<String, String> mapZoneDetails = GetZoneID(mPickUpLat, mPickUpLng, "DefaultZone");
                stZoneID = mapZoneDetails.get("ZoneId");
                if (!stZoneID.equalsIgnoreCase(stTempZoneID)) {
                    stTempZoneID = stZoneID;

                    /* Get CarList diff with Zone Area */
                    GetCarList();
                }
            }

            //Get Address from Client current lat-lng
            GetAddressFromLatLng(mPickUpLat, mPickUpLng);

        } else if (stLocationTypeSelected.equalsIgnoreCase("drop_location")) {

            mDropLat = mPosition.latitude;
            mDropLng = mPosition.longitude;

            imgPickupMarker.setVisibility(View.GONE);
            imgDropMarker.setVisibility(View.VISIBLE);
            txtDurationFromDriver.setVisibility(View.GONE);
            txtDurationFromDriver.setText("");

            if (CheckZoneArea(mDropLat, mDropLng)) {
                HashMap<String, String> mapZoneDetails = GetZoneID(mDropLat, mDropLng, "AllZone");
                stDropZoneID = mapZoneDetails.get("ZoneId");

            } else {
                // Set Default zone if area not found
                HashMap<String, String> mapZoneDetails = GetZoneID(mDropLat, mDropLng, "DefaultZone");
                stDropZoneID = mapZoneDetails.get("ZoneId");
            }

            //Get Address from Client current lat-lng
            GetAddressFromLatLng(mDropLat, mDropLng);
        }

        /*
         * Check selected location inside zone are or not
         */
        if (mDropLat != 0.0 || mDropLng != 0.0) {
            try {
                if (!stZoneID.equals(stDropZoneID)) { // out-station not in same zone
                    if (arrCarList.size() == 0) {
                        cardViewNoService.setVisibility(View.VISIBLE);
                    } else {
                        cardViewNoService.setVisibility(View.GONE);
                        if (isClickOnCar) {
                            cardViewEstimatePrice.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    cardViewNoService.setVisibility(View.VISIBLE);
                    cardViewEstimatePrice.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*
         * Draw polyline on map and calculate estimate ride time and km
         */
        CalculationByDistance(mPickUpLat, mPickUpLng, mDropLat, mDropLng);

        /*
         * Update driver on map according to zone selection
         */
        updateDriverOnMap(String.valueOf(mPickUpLat), String.valueOf(mPickUpLng), stServiceID, stZoneID);
    }

    /**
     *
     * Check pickup and drop location inside zone area
     *
     * update driver on map
     *
     * calculate distance b/w pickup address & drop address
     *
     */
    private void checkRideAvailable() {

        if (stLocationTypeSelected.equalsIgnoreCase("pickup_location")) {

            imgPickupMarker.setVisibility(View.VISIBLE);
            imgDropMarker.setVisibility(View.GONE);

            /*
             * To checkout Pickup Lat-Lng inside Zone area or not!
             */
            if (CheckZoneArea(mPickUpLat, mPickUpLng)) {

                HashMap<String, String> mapZoneDetails = GetZoneID(mPickUpLat, mPickUpLng, "AllZone");
                stZoneID = mapZoneDetails.get("ZoneId");
                if (!stZoneID.equalsIgnoreCase(stTempZoneID)) {
                    stTempZoneID = stZoneID;

                    /* Get CarList diff with Zone Area */
                    GetCarList();
                }

            } else {

                // Set Default zone if area not found
                HashMap<String, String> mapZoneDetails = GetZoneID(mPickUpLat, mPickUpLng, "DefaultZone");
                stZoneID = mapZoneDetails.get("ZoneId");
                if (!stZoneID.equalsIgnoreCase(stTempZoneID)) {
                    stTempZoneID = stZoneID;

                    /* Get CarList diff with Zone Area */
                    GetCarList();
                }
            }

        } else if (stLocationTypeSelected.equalsIgnoreCase("drop_location")) {

            imgPickupMarker.setVisibility(View.GONE);
            imgDropMarker.setVisibility(View.VISIBLE);
            txtDurationFromDriver.setVisibility(View.GONE);
            txtDurationFromDriver.setText("");

            if (CheckZoneArea(mDropLat, mDropLng)) {
                HashMap<String, String> mapZoneDetails = GetZoneID(mDropLat, mDropLng, "AllZone");
                stDropZoneID = mapZoneDetails.get("ZoneId");

            } else {
                // Set Default zone if area not found
                HashMap<String, String> mapZoneDetails = GetZoneID(mDropLat, mDropLng, "DefaultZone");
                stDropZoneID = mapZoneDetails.get("ZoneId");
            }
        }

        /*
         * Check selected location inside zone are or not
         */
        if (mDropLat != 0.0 || mDropLng != 0.0) {
            try {
                if (stZoneID.equals(stDropZoneID)) {
                    if (arrCarList.size() == 0) {
                        cardViewNoService.setVisibility(View.VISIBLE);
                    } else {
                        cardViewNoService.setVisibility(View.GONE);
                        if (isClickOnCar) {
                            cardViewEstimatePrice.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    cardViewNoService.setVisibility(View.VISIBLE);
                    cardViewEstimatePrice.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*
         * Draw polyline on map and calculate estimate ride time and km
         */
        CalculationByDistance(mPickUpLat, mPickUpLng, mDropLat, mDropLng);

        /*
         * Update driver on map according to zone selection
         */
        updateDriverOnMap(String.valueOf(mPickUpLat), String.valueOf(mPickUpLng), stServiceID, stZoneID);

    }

    /*
     * Initialize all event listeners of all views
     */
    private void initViewsEventListeners() {

        listCarServiceType.setChoiceMode(TwoWayView.ChoiceMode.SINGLE);

        imgBack.setOnClickListener(this);
        imgFabCurrentLocation.setOnClickListener(this);
        txtPickupLocation.setOnClickListener(this);
        txtDropLocation.setOnClickListener(this);
        btnBooking.setOnClickListener(this);
        imgSearchDropLocation.setOnClickListener(this);
        imgSearchPickupLocation.setOnClickListener(this);

        imgSavedDropAddress.setOnClickListener(this);
        imgSavedPickupAddress.setOnClickListener(this);
        imgSaveDropAddress.setOnClickListener(this);
        imgSavePickupAddress.setOnClickListener(this);

        listCarServiceType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    if (mLastView != null)
                        deSelectedServiceTypeListItem(mLastView);

                    selectedServiceTypeListItem(view.findViewById(R.id.lyvehicle));
                    mLastView = view.findViewById(R.id.lyvehicle);

                    for (int i = 0; i < arrCarList.size(); i++) {

                        /* Compare Selected item from ListView and ArrayList item */
                        if (position == i) {
                            stServiceID = arrCarList.get(i).get("service_id").toString();
                            isTaxiService = arrCarList.get(i).get("is_taxi").toString();
                            stServiceName = arrCarList.get(i).get("service_name").toString();

                            /* Get PriceList according to Zone wise & Service Type wise */
                            arrPriceList = mHelper.GetPriceList(stZoneID, stServiceID);

                            if (arrPriceList.size() > 0) {
                                stPriceID = arrPriceList.get(0).get("price_id").toString();

                                /*
                                 * Draw polyline on map and calculate estimate ride time and km
                                 */
                                CalculationByDistance(mPickUpLat, mPickUpLng, mDropLat, mDropLng);

                                /*
                                 * Update driver on map according to zone selection
                                 */
                                updateDriverOnMap(String.valueOf(mPickUpLat), String.valueOf(mPickUpLng), stServiceID, stZoneID); // Changed by sumit

                                isClickOnCar = true;
                            }

                            if (mDropLat != 0.0 && mDropLng != 0.0) {
                                if (isTaxiService.equalsIgnoreCase("1")) {
                                    cardViewEstimatePrice.setVisibility(View.GONE);
                                    cardViewTaxi.setVisibility(View.VISIBLE);
                                } else {
                                    cardViewEstimatePrice.setVisibility(View.VISIBLE);
                                    cardViewTaxi.setVisibility(View.GONE);
                                }
                            } else {
                                Log.e("TAG_E", "Please select first drop location!");
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        listCarServiceType.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                for (int i = 0; i < arrCarList.size(); i++) {
                    if (position == i) {

                        stServiceID = arrCarList.get(i).get("service_id").toString();

                        getSelectedCarServiceInfo(stServiceID);
                    }
                }

                return false;
            }
        });
    }

    private void getSelectedCarServiceInfo(String serviceID) {

        ArrayList<HashMap> _arr_hashMap_service = mHelper.GetServiceDetail(serviceID);

        if (_arr_hashMap_service.size() > 0) {

            // This is for Get get height and width of Screen
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            try {
                display.getRealSize(size);
            } catch (NoSuchMethodError err) {
                display.getSize(size);
            }

            final int width = size.x, height = size.y;

            final Dialog dialogServiceTypeInfo = new Dialog(NewBookingActivity.this);
            dialogServiceTypeInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogServiceTypeInfo.setContentView(R.layout.layout_service_details_dailog);
            dialogServiceTypeInfo.setCancelable(false);
            dialogServiceTypeInfo.show();

            LinearLayout llMainLayout = (LinearLayout) dialogServiceTypeInfo.findViewById(R.id.layout_main);
            llMainLayout.setMinimumWidth((int) (width / 0.7));
            llMainLayout.setMinimumHeight((int) (height / 1.5));

            ImageView imgClose = dialogServiceTypeInfo.findViewById(R.id.img_cancel);
            ImageView imgServiceIcon = dialogServiceTypeInfo.findViewById(R.id.img_icon);
            ImageView imgBackgrounnd = dialogServiceTypeInfo.findViewById(R.id.img_background);
            TextView txtName = dialogServiceTypeInfo.findViewById(R.id.txt_name);
            TextView txtSeat = dialogServiceTypeInfo.findViewById(R.id.txt_seat);
            TextView txtDesc = dialogServiceTypeInfo.findViewById(R.id.txt_desc);
            SimpleDraweeView imgIcon = dialogServiceTypeInfo.findViewById(R.id.img_task_image);

            txtName.setText(_arr_hashMap_service.get(0).get("service_name").toString());
            txtSeat.setText(_arr_hashMap_service.get(0).get("seat").toString());
            txtDesc.setText(Html.fromHtml(_arr_hashMap_service.get(0).get("description").toString().replaceAll("\n", "<br/>")));

            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogServiceTypeInfo.dismiss();
                }
            });

            try {
                Uri imageUri = Uri.parse(Global_ServiceApi.API_IMAGE_HOST + _arr_hashMap_service.get(0).get("icon").toString());
                imgIcon.setImageURI(imageUri);

                Glide.with(NewBookingActivity.this).load(Global_ServiceApi.API_IMAGE_HOST + _arr_hashMap_service.get(0).get("image_details").toString())
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgBackgrounnd);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapFragment.onLowMemory();
    }

    @Override
    public void onLocationChanged(Location location) {

        mLastLocation = location;

        try {
            if (stLocationTypeSelected.equalsIgnoreCase("pickup_location")) {
                mPickUpLat = location.getLatitude();
                mPickUpLng = location.getLongitude();

            } else if (stLocationTypeSelected.equalsIgnoreCase("drop_location")) {
                mDropLat = location.getLatitude();
                mDropLng = location.getLongitude();
            } else {

                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(13.0f));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * Call when the map is connected
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(NewBookingActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        }

        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            handleNewLocation(location);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            ShowToast(getResources().getString(R.string.network_disconnect));
        } else if (i == CAUSE_NETWORK_LOST) {
            ShowToast(getResources().getString(R.string.network_lost));
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    final int LOCATION_REQUEST_CODE = 1111;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case LOCATION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    ShowToast("Please provide the location permission & Restart the Application");
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        /* This call is for near by driver list  */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_NearByCarList, new IntentFilter("NearBy_Car_List"));


        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_UserCardList, new IntentFilter("User_Card_List"));
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        subscriptions.unsubscribe();
    }

    /**
     * Receiver for find Near By Driver Car list
     */
    private BroadcastReceiver mMessageReceiver_NearByCarList = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mCarResult = intent.getStringExtra("nearest_car_list");
            SetDriverOnMap(mCarResult);
        }
    };

    /**
     * User Card List Receiver
     */
    private BroadcastReceiver mMessageReceiver_UserCardList = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            stCardResult = intent.getStringExtra("user_card_list");
        }
    };

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.imgback:
                finish();
                break;

            case R.id.imgfab:
                try {
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mUserCurrentLat, mUserCurrentLang)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.txt_pickup:
            case R.id.img_search_pickup_address:
                stLocationType = "search_address";
                searchLocation("pickup_location", PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP);
                break;

            case R.id.txt_dropto:
            case R.id.img_search_drop_address:
                stLocationType = "search_address";
                searchLocation("drop_location", PLACE_AUTOCOMPLETE_REQUEST_CODE_DROPTO);
                break;

                //Saved Address
            case R.id.img_saved_pickup_address:
                stLocationType = "saved_address";
                stLocationTypeSelected = "pickup_location";
                Intent intentPickup = new Intent(NewBookingActivity.this, SavedLocationActivity.class);
                startActivityForResult(intentPickup, REQUEST_CODE_PICKUP_ADDRESS);
                break;

                //Saved Address
            case R.id.img_saved_drop_address:
                stLocationType = "saved_address";
                stLocationTypeSelected = "drop_location";
                Intent intentDrop = new Intent(NewBookingActivity.this, SavedLocationActivity.class);
                startActivityForResult(intentDrop, REQUEST_CODE_DROP_ADDRESS);
                break;

            case R.id.img_save_pickup_address:
                saveNewAddress("Pickup");
                break;

            case R.id.img_save_drop_address:
                saveNewAddress("Drop");
                break;

            case R.id.btnbooking:
                UserBookingRide();
                break;
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        if(e != null) {
            Log.e("TAG_ERROR", "Error: " + e.getMessage());
        } else {
            Log.e("TAG_ERROR", "Something went wrong, Try again");
        }
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {

        if(arrPolyLines.size()>0) {
            for (Polyline poly : arrPolyLines) {
                poly.remove();
            }
        }

        arrPolyLines = new ArrayList<>();
        //add route(s) to the map.
        for (int i = 0; i < route.size(); i++) {

            //In case of more than 5 alternative routes
            int colorIndex = i % COLORS.length;

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
            polyOptions.width(10 + i * 3);
            polyOptions.addAll(route.get(i).getPoints());
            Polyline polyline = mMap.addPolyline(polyOptions);
            arrPolyLines.add(polyline);

            if (route.get(i).getDistanceText().contains("km")) {
                dTotalKm = Double.parseDouble(route.get(i).getDistanceText().replace(" km","").replace(",",""));
            } else if (route.get(i).getDistanceText().contains("m")) {
                double distanceInMeter = Double.parseDouble(route.get(i).getDistanceText().replace(" m","").replace(",",""));
                dTotalKm = distanceInMeter / 1000;
            }

            if (route.get(i).getDurationText().toString().contains("hour")) {

                String[] durationSplit = route.get(i).getDurationText().toString().split(" ");
                if (durationSplit.length > 0) {

                    double hoursToMin = Double.parseDouble(durationSplit[0]) * 60;
                    double minutes = Double.parseDouble(durationSplit[2]);
                    dTotalDuration = hoursToMin + minutes;
                }

            } else {

                if (route.get(i).getDurationText().contains("mins")) {
                    dTotalDuration = Double.parseDouble(route.get(i).getDurationText().replace(" mins",""));
                } else if (route.get(i).getDurationText().contains("min")) {
                    dTotalDuration = Double.parseDouble(route.get(i).getDurationText().replace(" min",""));
                } else {
                    dTotalDuration = Double.parseDouble(route.get(i).getDurationText().replace(" mins",""));
                }
            }
        }

        setEstimatedPriceDistance();
    }

    /**
     * Set Estimated Price & Distance
     *
     */
    private void setEstimatedPriceDistance() {

        txtEstimatedTime.setText("ETA : " + Math.round(dTotalDuration) + " min");

        if (arrPriceList.size() > 0) {
            double totalAmount = Double.parseDouble(arrPriceList.get(0).get("base_charge").toString()) +
                    (Double.parseDouble(arrPriceList.get(0).get("per_km_charge").toString()) * dTotalKm) +
                    (Double.parseDouble(arrPriceList.get(0).get("per_minute_charge").toString()) * dTotalDuration);

            totalAmount = totalAmount + Double.parseDouble(arrPriceList.get(0).get("booking_charge").toString());

            double minimumKM = 0.0;
            if (!arrPriceList.get(0).get("minimum_km").equals("")) {
                minimumKM = Double.parseDouble(arrPriceList.get(0).get("minimum_km").toString());
            }

            if (minimumKM >= dTotalKm) {
                totalAmount = Double.parseDouble(arrPriceList.get(0).get("minimum_fair").toString());
            }

            if (Double.parseDouble(arrPriceList.get(0).get("minimum_fair").toString()) > totalAmount) {
                totalAmount = Double.parseDouble(arrPriceList.get(0).get("minimum_fair").toString());
            }

            // Get GST Tax of selected Zone
            arrZoneList = mHelper.GetGSTTax(stZoneID);

            if (arrZoneList.size() > 0) {
                gstTax = Double.parseDouble(arrZoneList.get(0).get("gst_tax").toString());
            }

            totalAmount = totalAmount + (totalAmount * gstTax / 100);

            txtEstimatedPrice.setText("₹" + Math.round(totalAmount));

            stTotalAmount = String.valueOf(Math.round(totalAmount));

            dPrice = totalAmount;

            double dOnePersonaPrice = dPrice;
            double dMinScePrice = 0.0;

            if (arrPriceList.get(0).get("sec_ride_per").equals("")) {
                dMinScePrice = dPrice + (dPrice * 0.0 / 100);
            } else {
                dMinScePrice = dPrice + (dPrice * Double.parseDouble(arrPriceList.get(0).get("sec_ride_per").toString()) / 100);
            }

            dMinScePrice = dMinScePrice + (dMinScePrice * gstTax / 100);
            stMaxOfferAmount = arrPriceList.get(0).get("max_offer_amount").toString();
            stPaytmOffer = arrPriceList.get(0).get("paytm_offer").toString();


            Double hours = (dTotalDuration) / 60; //since both are ints, you get an int
            Double minutes = (dTotalDuration) % 60;

            String str_hours = String.valueOf(Math.round(hours));
            if (str_hours.length() == 1) {
                str_hours = "0" + str_hours;
            }
            String str_minutes = String.valueOf(Math.round(minutes));
            if (str_minutes.length() == 1) {
                str_minutes = "0" + str_minutes;
            }

            stEstDuration = str_hours + ":" + str_minutes;
            stEstTotalDistance = String.valueOf(roundTwoDecimals(dTotalKm));
            stTwoSeatPrice = "₹" + Math.round((dMinScePrice));
            stSecondSeatAmt = String.valueOf(Math.round(dMinScePrice));
        }
    }

    @Override
    public void onRoutingCancelled() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP) {
            if (resultCode == RESULT_OK) {
                isMapTouched = true;
                stLocationType = "search_address";

                Place place = PlaceAutocomplete.getPlace(this, data);
                mPickUpLat = place.getLatLng().latitude;
                mPickUpLng = place.getLatLng().longitude;

                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mPickUpLat, mPickUpLng)));

                if (mDropLat != 0.0 && mDropLng != 0.0) {

                    if (mMarkerPickupDrop == null) {

                        mMarkerPickupDrop = mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(mDropLat, mDropLng))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin)));
                    } else {
                        mMarkerPickupDrop.setPosition(new LatLng(mDropLat, mDropLng));
                        mMarkerPickupDrop.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin));
                    }
                }

                GetAddressFromLatLng(mPickUpLat, mPickUpLng);

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.e("msg", status.getStatusMessage());
            }

        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_DROPTO) {
            if (resultCode == RESULT_OK) {
                isMapTouched = true;
                stLocationType = "search_address";

                Place place = PlaceAutocomplete.getPlace(this, data);
                mDropLat = place.getLatLng().latitude;
                mDropLng = place.getLatLng().longitude;

                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mDropLat, mDropLng)));

                if (mMarkerPickupDrop == null) {
                    mMarkerPickupDrop = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(mPickUpLat, mPickUpLng))
                            .title(getResources().getString(R.string.st_pickup_location))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin)));
                } else {
                    mMarkerPickupDrop.setPosition(new LatLng(mPickUpLat, mPickUpLng));
                    mMarkerPickupDrop.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin));
                }

                GetAddressFromLatLng(mDropLat, mDropLng);

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.e("msg", status.getStatusMessage());
            }

            // Get Address from saved address book
        } else if (requestCode == REQUEST_CODE_PICKUP_ADDRESS) {
            if (resultCode == RESULT_OK) {
                stLocationType = "saved_address";

                String address = data.getStringExtra("address");
                mPickUpLat = data.getDoubleExtra("latitude", 0.0);
                mPickUpLng = data.getDoubleExtra("longitude", 0.0);
                txtPickupLocation.setText(address);

                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mPickUpLat, mPickUpLng)));

                checkRideAvailable();

                if (mDropLat != 0.0 && mDropLng != 0.0) {

                    if (mMarkerPickupDrop == null) {

                        mMarkerPickupDrop = mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(mDropLat, mDropLng))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin)));
                    } else {
                        mMarkerPickupDrop.setPosition(new LatLng(mDropLat, mDropLng));
                        mMarkerPickupDrop.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin));
                    }
                }

                setButtonVisibility();
            }

            // Get Address from saved address book
        } else if (requestCode == REQUEST_CODE_DROP_ADDRESS) {
            if (resultCode == RESULT_OK) {
                stLocationType = "saved_address";

                String address = data.getStringExtra("address");
                mDropLat = data.getDoubleExtra("latitude", 0.0);
                mDropLng = data.getDoubleExtra("longitude", 0.0);
                txtDropLocation.setText(address);

                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mDropLat, mDropLng)));

                checkRideAvailable();

                if (mMarkerPickupDrop == null) {
                    mMarkerPickupDrop = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(mPickUpLat, mPickUpLng))
                            .title(getResources().getString(R.string.st_pickup_location))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin)));
                } else {
                    mMarkerPickupDrop.setPosition(new LatLng(mPickUpLat, mPickUpLng));
                    mMarkerPickupDrop.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin));
                }

                setButtonVisibility();
            }

        } else if (resultCode == ADD_CARDS) {
            String message = data.getStringExtra("ADD_CARD");

        } else if (resultCode == BACK_TO_ACTIVITY) {
            String message = data.getStringExtra("MESSAGE");
            if (data.getStringExtra("MESSAGE").toString().equals("finish")) {
                finish();
            }
        }

    }

    private void handleNewLocation(Location location) {
        Log.d(TAG, location.toString());
        if (mLocationRequest != null) {
            try {
                mPickUpLat = location.getLatitude();
                mPickUpLng = location.getLongitude();
                location.setAccuracy(10);

                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(mPickUpLat, mPickUpLng)).zoom(14.0f).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                mUserCurrentLat = mPickUpLat;
                mUserCurrentLang = mPickUpLng;
                GetAddressFromLatLng(mPickUpLat, mPickUpLng);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     *
     * Search Pickup or Drop Location
     *
     * @param type
     * @param requestCode
     */
    private void searchLocation(String type, int requestCode) {

        try {
            stLocationTypeSelected = type;

            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    .build(); //.setCountry("IN")

            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(typeFilter).build(this);
            startActivityForResult(intent, requestCode);

        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get Car List diff with Zone Area- Selected by User
     */
    private void GetCarList() {
        try {
            arrCarList = mHelper.GetCarList(preferencesUtility.getgender(), stZoneID);
            if (arrCarList.size() > 0) {
                carAdapter = new CarAdapter(getApplicationContext(), R.layout.vehical_item, arrCarList, new CarAdapter.BtnClickListener() {
                    @Override
                    public void onBtnClick(int position, String service_id, String IsTaxi) {

                    }
                });

                listCarServiceType.setAdapter(carAdapter);
                txtDurationFromDriver.setVisibility(View.VISIBLE);

            } else {
                progressDialog.dismiss();
                txtDurationFromDriver.setVisibility(View.INVISIBLE);
                ShowToast("Ohi-Cab service not available in this area!");
                arrCarList.clear();

                carAdapter = new CarAdapter(getApplicationContext(), R.layout.vehical_item, arrCarList, new CarAdapter.BtnClickListener() {
                    @Override
                    public void onBtnClick(int position, String service_id, String IsTaxi) {

                    }
                });
                listCarServiceType.setAdapter(carAdapter);
            }

            if (listCarServiceType.getAdapter() != null) {
                progressDialog.dismiss();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Update driver list on GoogleMap when service type changed
     *
     * @param pickupLat
     * @param pickupLang
     * @param stServiceID
     * @param stZoneID
     */
    private void updateDriverOnMap(String pickupLat, String pickupLang, String stServiceID, String stZoneID) {

        try {
            JSONObject objCarList = new JSONObject();
            objCarList.put("latitude", pickupLat);
            objCarList.put("longitude", pickupLang);
            objCarList.put("service_id", stServiceID);
            objCarList.put("zone_id", stZoneID);
            objCarList.put("is_taxi", "");

            appDelegate.emit("near_by_driver", objCarList.toString(), mSocket.id());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Driver location On Map
     * <p>
     * set Driver car location and calculate approx time duration between driver and pickup location
     *
     * @param stResult
     */
    private void SetDriverOnMap(String stResult) {

        try {
            ArrayList<HashMap> arrMarkersList = new ArrayList<>();

            JSONObject jsonObjectCarList = new JSONObject(stResult);
            if (jsonObjectCarList.getString("status").equalsIgnoreCase("1")) {

                JSONArray jsonArrayCarDetails = jsonObjectCarList.getJSONArray("payload");
                if (jsonArrayCarDetails.length() > 0) {
                    for (int i = 0; i < jsonArrayCarDetails.length(); i++) {
                        JSONObject objVale = jsonArrayCarDetails.getJSONObject(i);

                        HashMap<String, String> mapMarkerList = new HashMap<>();
                        mapMarkerList.put("DriverName", objVale.getString("user_id"));
                        mapMarkerList.put("vale_lat", objVale.getString("lati"));
                        mapMarkerList.put("vale_lng", objVale.getString("longi"));
                        mapMarkerList.put("top_icon", objVale.getString("top_icon"));
                        mapMarkerList.put("service_id", objVale.getString("service_id"));

                        arrMarkersList.add(mapMarkerList);
                    }
                }
            }
            removeMarkers();

            for (int i = 0; i < arrMarkersList.size(); i++) {
                dropMarkerPinOnMap(Double.parseDouble(arrMarkersList.get(i).get("vale_lat").toString()),
                        Double.parseDouble(arrMarkersList.get(i).get("vale_lng").toString()),
                        arrMarkersList.get(i).get("DriverName").toString(),
                        arrMarkersList.get(i).get("service_id").toString()
                );
            }

            if (arrMarkersList.size() > 0) {
                float[] results = new float[1];
                Location.distanceBetween(mPickUpLat, mPickUpLng, Double.parseDouble(arrMarkersList.get(0).get("vale_lat").toString()),
                        Double.parseDouble(arrMarkersList.get(0).get("vale_lng").toString()), results);
                float distance = results[0];
                double TotalKm = distance / 1000;
                double TotalDuration = TotalKm * 2.5;

                if (Math.round(TotalDuration) == 0) {
                    txtDurationFromDriver.setText("1 min");

                } else {
                    txtDurationFromDriver.setText(Math.round(TotalDuration) + " min");
                }

                txtDurationFromDriver.setVisibility(View.VISIBLE);

            } else {
                txtDurationFromDriver.setVisibility(View.GONE);
                txtDurationFromDriver.setText("");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * Calculate distance between pickup and drop location
     *
     * @param mPickUpLat
     * @param mPickUpLng
     * @param mDropLat
     * @param mDropLng
     */
    private void CalculationByDistance(double mPickUpLat, double mPickUpLng, double mDropLat, double mDropLng) {

        if (mPickUpLat != 0.0 && mPickUpLng != 0.0 && mDropLat != 0.0 && mDropLng != 0.0) {

            getRouteToMarker(new LatLng(mPickUpLat, mPickUpLng), new LatLng(mDropLat, mDropLng));
        }
    }

    /**
     *
     * Get Route Polyline
     *
     * @param currentLatLng
     * @param pickupLatLng
     */
    private void getRouteToMarker(LatLng currentLatLng, LatLng pickupLatLng) {
        if (pickupLatLng != null && currentLatLng != null){
            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .alternativeRoutes(false)
                    .waypoints(currentLatLng, pickupLatLng)
                    .key("AIzaSyAfye39JZYcYdS196GZrvxRBfMShOCaA3w")
                    .build();
            routing.execute();
        }
    }

    /**
     * Add marker on google map
     *
     * @param latitude
     * @param longitude
     * @param title
     */
    protected void dropMarkerPinOnMap(double latitude, double longitude, String title, String service_id) {

        Marker driverMarkerPin = null;

        if (service_id != null) {

            switch (service_id) {

                case "1":
                    driverMarkerPin = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_1)));
                    break;

                case "2":
                case "13":
                case "18":
                    driverMarkerPin = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_2)));
                    break;

                case "3":
                case "19":
                    driverMarkerPin = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_3)));
                    break;

                case "11":
                    driverMarkerPin = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_11)));
                    break;

                case "12":
                    driverMarkerPin = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_12)));
                    break;

                case "14":
                    driverMarkerPin = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_14)));
                    break;

                case "15":
                    driverMarkerPin = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_15)));
                    break;

                case "16":
                case "17":
                    driverMarkerPin = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_16)));
                    break;

                default:
                    driverMarkerPin = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_taxi)));
            }

        } else {
            driverMarkerPin = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_taxi)));
        }

        arrCarMarkers.add(driverMarkerPin);
    }

    /**
     * Booking Details Validation
     *
     * @return
     */
    private String IsValidate() {

        if (txtPickupLocation.getText().toString().equalsIgnoreCase(getResources().getString(R.string.st_pickup_location))) {
            return getResources().getString(R.string.st_select_pickup_location);

        } else if (txtDropLocation.getText().toString().equalsIgnoreCase(getResources().getString(R.string.st_drop_location))) {
            return getResources().getString(R.string.st_select_dropto_location);

        } else if (stServiceID.equalsIgnoreCase("")) {
            return getResources().getString(R.string.st_select_car_type);

        } else if (!stZoneID.equals(stDropZoneID)) {
            return "Pick up Location and Drop Location Must be in Same Zone";

        } else {
            return "true";
        }
    }

    /**
     * Book user ride
     */
    private void UserBookingRide() {


        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final String stCurrentDate = df.format(c.getTime());

        String stMessage = IsValidate();
        if (stMessage.equalsIgnoreCase("true")) {
            String pickupDate = "";
            if (stRideType.equalsIgnoreCase("1")) {
                pickupDate = stCurrentDate;
            }

            Intent intent = new Intent(this, BookingActivityStep2Activity.class);
            intent.putExtra("stServiceID", stServiceID);
            intent.putExtra("stServiceName", stServiceName);
            intent.putExtra("stPriceID", stPriceID);

            intent.putExtra("mPickUpLat", String.valueOf(mPickUpLat));
            intent.putExtra("mPickUpLng", String.valueOf(mPickUpLng));
            intent.putExtra("pickup", txtPickupLocation.getText().toString());

            intent.putExtra("mDropLat", String.valueOf(mDropLat));
            intent.putExtra("mDropLng", String.valueOf(mDropLng));
            intent.putExtra("dropto", txtDropLocation.getText().toString());

            intent.putExtra("pick_date", pickupDate);
            intent.putExtra("stRideType", stRideType);
            intent.putExtra("time", txtDurationFromDriver.getText().toString());
            intent.putExtra("isTexiService", isTaxiService);

            intent.putExtra("2seatprice", stSecondSeatAmt);
            intent.putExtra("price", stTotalAmount);

            intent.putExtra("est_duration", stEstDuration);
            intent.putExtra("est_total_distance", stEstTotalDistance);
            intent.putExtra("max_offer_amount", stMaxOfferAmount);
            intent.putExtra("paytm_offer", stPaytmOffer);
            intent.putExtra("stZoneID", stZoneID);

            startActivityForResult(intent, BACK_TO_ACTIVITY);

        } else {
            toastDialog.ShowToastMessage(stMessage);
        }
    }

    /**
     *
     * Get Geo Address from Latitude - Longitude
     *
     * @param lat
     * @param lng
     */
    private void GetAddressFromLatLng(final double lat, final double lng) {

        try {
            String stLatLng = lat + "," + lng;
            String GetAddress_URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng="
                    + stLatLng + "&sensor=false&key=" + getResources().getString(R.string.google_maps_key);

            VolleyUtils.GET_METHOD(this, GetAddress_URL, new VolleyResponseListener() {
                @Override
                public void onError(String message) {
                    System.out.println("Error" + message);
                }

                @Override
                public void onResponse(Object response) {

                    try {
                        if (response != null) {
                            JSONObject jsonObjectAddress = new JSONObject(response.toString());
                            JSONArray jsonArrayAddress = jsonObjectAddress.getJSONArray("results");

                            if (jsonArrayAddress.length() > 0) {

                                String stFormattedAddress = jsonArrayAddress.getJSONObject(0).getString("formatted_address");
                                JSONObject jsonObjectGeoMetry = jsonArrayAddress.getJSONObject(0).getJSONObject("geometry");

                                JSONObject jsonObjectLocation = jsonObjectGeoMetry.getJSONObject("location");
                                String stLatitude = jsonObjectLocation.getString("lat");
                                String stLongitude = jsonObjectLocation.getString("lng");

                                if (stLocationTypeSelected.equalsIgnoreCase("drop_location")) {
                                    txtDropLocation.setText(stFormattedAddress);
                                    mDropLat = Double.parseDouble(stLatitude);
                                    mDropLng = Double.parseDouble(stLongitude);

                                } else if (stLocationTypeSelected.equalsIgnoreCase("pickup_location")) {
                                    txtPickupLocation.setText(stFormattedAddress);
                                    mPickUpLat = Double.parseDouble(stLatitude);
                                    mPickUpLng = Double.parseDouble(stLongitude);
                                }

                                setButtonVisibility();

                            } else {
                                setButtonVisibility();
                                imgSearchDropLocation.setVisibility(View.VISIBLE);
                                imgSearchPickupLocation.setVisibility(View.VISIBLE);
                            }
                        } else {
                            ShowToast(getResources().getString(R.string.no_address_found));

                            setButtonVisibility();
                            imgSearchDropLocation.setVisibility(View.VISIBLE);
                            imgSearchPickupLocation.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Show - hide search address button
     */
    private void setButtonVisibility() {
        if (txtPickupLocation.getText().toString().equalsIgnoreCase(getResources().getString(R.string.st_pickup_location))) {
            imgSearchPickupLocation.setVisibility(View.VISIBLE);
            imgSavePickupAddress.setVisibility(View.GONE);

        } else {
            imgSearchPickupLocation.setVisibility(View.GONE);
            imgSavePickupAddress.setVisibility(View.VISIBLE);
        }

        if (txtDropLocation.getText().toString().equalsIgnoreCase(getResources().getString(R.string.st_drop_location))) {
            imgSearchDropLocation.setVisibility(View.VISIBLE);
            imgSaveDropAddress.setVisibility(View.GONE);
        } else {
            imgSearchDropLocation.setVisibility(View.GONE);
            imgSaveDropAddress.setVisibility(View.VISIBLE);
        }
    }

    /**
     *
     * Add new address dialog
     *
     *
     * @return
     */
    public Dialog saveNewAddress(String type) {

        final Dialog dialog = new Dialog(NewBookingActivity.this);
        dialog.setContentView(R.layout.dialog_save_address);
        dialog.setTitle("");
        dialog.setCancelable(false);

        AppCompatEditText edtTagName = dialog.findViewById(R.id.edt_tag_name);
        AppCompatButton btnSave = dialog.findViewById(R.id.btn_save);
        AppCompatButton btnClose = dialog.findViewById(R.id.btn_close);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!txtPickupLocation.getText().toString().equalsIgnoreCase(getResources().getString(R.string.st_pickup_location)) ||
                        !txtDropLocation.getText().toString().equalsIgnoreCase(getResources().getString(R.string.st_drop_location))) {

                    if (!edtTagName.getText().toString().equalsIgnoreCase("")) {

                        if (type.equalsIgnoreCase("Pickup")) {
                            addNewAddress(edtTagName.getText().toString(), txtPickupLocation.getText().toString(), mPickUpLat, mPickUpLng);

                        } else if (type.equalsIgnoreCase("Drop")) {
                            addNewAddress(edtTagName.getText().toString(), txtDropLocation.getText().toString(), mDropLat, mDropLng);
                        }

                    } else {
                        toastDialog.ShowToastMessage("Please enter tag name");
                    }
                } else {
                    toastDialog.ShowToastMessage("Please select your location!");
                }
                dialog.dismiss();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        return null;
    }

    /**
     * add new address
     */
    private void addNewAddress(String tagName, String address, double latitude, double longitude) {
        if (isInternetOn(getApplicationContext())) {
            Call<JsonObject> call = apiService.user_location_add(preferencesUtility.getuser_id(),preferencesUtility.getauth_token(), tagName, address, latitude, longitude);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        JSONObject jsonObjectDetails = new JSONObject(response.body().toString());
                        if (jsonObjectDetails.optString("status").equalsIgnoreCase("1") &&
                                jsonObjectDetails.optString("success").equalsIgnoreCase("true")) {

                            toastDialog.ShowToastMessage(jsonObjectDetails.optString("message"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    toastDialog.ShowToastMessage("Sorry, something went wrong. Please try again.");

                }
            });
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }

    /**
     * First Remove All Marker from Map
     */
    private void removeMarkers() {
        for (Marker marker : arrCarMarkers) {
            marker.remove();
        }
        arrCarMarkers.clear();
    }
}