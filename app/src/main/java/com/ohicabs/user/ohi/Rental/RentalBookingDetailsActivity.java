package com.ohicabs.user.ohi.Rental;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Activity.BaseActivity;
import com.ohicabs.user.ohi.Activity.MonthlySpendingTermsActivity;
import com.ohicabs.user.ohi.Activity.ReviewRideActivity;
import com.ohicabs.user.ohi.Activity.UserDriverSupportActivity;
import com.ohicabs.user.ohi.Activity.YourRouteActivity;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontTextView;
import com.ohicabs.user.ohi.Utils.CircularImageView;
import com.ohicabs.user.ohi.Utils.Constant;
import com.ohicabs.user.ohi.Utils.DataParser;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.SHARE_LINK;
import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class RentalBookingDetailsActivity extends BaseActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "BookingDetailsActivity";

    @BindView(R.id.img_profile)
    CircularImageView _img_profile;
    @BindView(R.id.img_ride_status)
    AppCompatImageView _img_ride_status;
    @BindView(R.id.img_car_type)
    AppCompatImageView _img_car_type;

    @BindView(R.id.txt_ride_status)
    AppCompatTextView _txt_ride_status;

    @BindView(R.id.txt_code)
    AppCompatTextView _txt_code;

    @BindView(R.id.txt_ride_date_time)
    AppCompatTextView _txt_ride_date_time;
    @BindView(R.id.txt_ride_id)
    AppCompatTextView _txt_ride_id;

    @BindView(R.id.txt_car_type)
    AppCompatTextView _txt_car_type;
    @BindView(R.id.txt_driver_name)
    AppCompatTextView _txt_driver_name;
    @BindView(R.id.txt_driver_mobile_no)
    AppCompatTextView _txt_driver_mobile_no;
    @BindView(R.id.txt_driver_car_number)
    AppCompatTextView _txt_driver_car_number;

    @BindView(R.id.txt_ride_price)
    AppCompatTextView _txt_ride_price;
    @BindView(R.id.txt_ride_distance)
    AppCompatTextView _txt_ride_distance;
    @BindView(R.id.txt_ride_duration)
    AppCompatTextView _txt_ride_duration;

    @BindView(R.id.txt_from_address)
    AppCompatTextView _txt_from_address;
    @BindView(R.id.txt_to_address)
    AppCompatTextView _txt_to_address;

    @BindView(R.id.txt_call)
    AppCompatTextView _txt_call;
    @BindView(R.id.txt_cancel_ride)
    AppCompatTextView _txt_cancel_ride;
    @BindView(R.id.txt_support)
    AppCompatTextView _txt_support;

    @BindView(R.id.layout_cancel_ride)
    LinearLayout _layout_cancel_ride;
    @BindView(R.id.layout_call)
    LinearLayout _layout_call;
    @BindView(R.id.layout_support)
    LinearLayout _layout_support;

    @BindView(R.id.img_call)
    AppCompatImageView _img_call;
    @BindView(R.id.img_cancel)
    AppCompatImageView _img_cancel;
    @BindView(R.id.img_support)
    AppCompatImageView _img_support;

    @BindView(R.id.txt_ride_message)
    AppCompatTextView _txt_ride_message;

    @BindView(R.id.layout_address)
    RelativeLayout _layout_address;
    @BindView(R.id.layout_driver_info)
    RelativeLayout _layout_driver_info;
    @BindView(R.id.layout_ride_time_details)
    RelativeLayout _layout_ride_time_details;
    @BindView(R.id.layout_ride_price)
    RelativeLayout _layout_ride_price;
    @BindView(R.id.view_toll_tip)
    View view_toll_tip;

    @BindView(R.id.layout_driver_name)
    LinearLayout _layout_driver_name;
    @BindView(R.id.layout_driver_call)
    LinearLayout _layout_driver_call;
    @BindView(R.id.layout_driver_vehical_no)
    LinearLayout _layout_driver_vehical_no;

    @BindView(R.id.txt_ride_toll_price)
    AppCompatTextView _txt_ride_toll_price;
    @BindView(R.id.txt_ride_tip_price)
    AppCompatTextView _txt_ride_tip_price;

    @BindView(R.id.tv_pickup_location)
    AppCompatTextView _tv_pickup_location;
    @BindView(R.id.tv_drop_location)
    AppCompatTextView _tv_drop_location;

    @BindView(R.id.txt_driver_delay_time)
    AppCompatTextView _txt_driver_delay_time;

    @BindView(R.id.txt_payment_type)
    AppCompatTextView _txt_payment_type;

    @BindView(R.id.view_ride_details)
    View view_ride_details;

    @BindView(R.id.imgfab_share)
    ImageButton _imgfab_share;

    @BindView(R.id.img_help)
    ImageButton _img_help;

    @BindView(R.id.imgfab)
    ImageButton _imgfab;

    @BindView(R.id.img_refresh)
    ImageButton _img_refresh;


    @BindView(R.id.txt_car_brand)
    AppCompatTextView _txt_car_brand;
    @BindView(R.id.txt_car_model)
    AppCompatTextView _txt_car_model;
    @BindView(R.id.txt_car_color)
    AppCompatTextView _txt_car_color;
    @BindView(R.id.txt_total_person)
    AppCompatTextView _txt_total_person;
    @BindView(R.id.layout_person)
    LinearLayout _layout_person;
    @BindView(R.id.ly_total_person)
    RelativeLayout ly_total_person;

    @BindView(R.id.layout_waiting_payment)
    RelativeLayout layout_waiting_payment;

    @BindView(R.id.txt_pay)
    CustomRegularFontTextView txt_pay;

    @BindView(R.id.txt_person)
    AppCompatTextView _txt_person;

    @BindView(R.id.txt_refund_initiated)
    AppCompatTextView _txt_refund_initiated;

    @BindView(R.id.txt_refund_pending)
    AppCompatTextView _txt_refund_pending;

    @BindView(R.id.txt_price)
    AppCompatTextView _txt_price;

    @BindView(R.id.txt_price_t_n_c)
    AppCompatTextView _txt_price_t_n_c;

    @BindView(R.id.layout_to_address)
    RelativeLayout _layout_to_address;

    private SlidingUpPanelLayout mLayout;

    SimpleDateFormat date_format;
    String stDriverId, stBooking_Status, stBookingID, stDriverContactNumber, stBookingId, isTaxiRide, stServiceId, user_payment_status;

    private SupportMapFragment mSupportMapFragment;
    private GoogleMap mGoogleMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    public double mPickUpLat = 0.0, mPickUpLng = 0.0, mDropLat = 0.0, mDropLng = 0.0, mDriverLat = 0.0, mDriverLng = 0.0, mUserCurrentLat = 0.0, mUserCurrentLng = 0.0;
    private Marker mMarker_user, mMarker_pickup, mMarker_drop, mMarker_driver;
    Location prevLoc, newLoc;

    private ArrayList<Marker> _arr_Markers_Pickup = new ArrayList<>();
    public ArrayList<LatLng> routeArray = new ArrayList<LatLng>();
    ArrayList<HashMap> _arrPriceList;
    public PolylineOptions lineOptions = null;
    private static final String FORMAT = "%02d:%02d";
    private String SCROLL_STATE = "";
    ArrayList<LatLng> points = new ArrayList<>();
    List<Polyline> polylines = new ArrayList<Polyline>();
    List<Polyline> polylines_complete = new ArrayList<Polyline>();

    CountDownTimer cT;

    String stDistance = "", stDuration = "", stAmount = "", stBookingStatusForMarker = "";
    String paytm_transaction_id = "", order_id = "", fail_payload = "", success_payload = "", toll_amount, payment_type = "";

    HashMap<String, Double> getTotalKmPrice = new HashMap<String, Double>();
    double TotalKm = 0, TotalDuration = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_details);

        ButterKnife.bind(this);

        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        ActionBar mActionBar = getSupportActionBar();
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));

        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setTitle(getResources().getString(R.string.ride_details));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SCROLL_STATE.equalsIgnoreCase("EXPANDED")) {
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                } else {
                    finish();
                }
            }
        });
        stBookingId = getIntent().getStringExtra("booking_id");

        setContent();

    }

    private void BookingDetails() {
        if (isInternetOn(getApplicationContext())) {
            progressDialog.show();
            Call<JsonObject> call = apiService.booking_detail(stBookingId, "1");
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    progressDialog.dismiss();
                    GetRideBookingDetails(response.body().toString());
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }

    private void setContent() {

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_DetailsFinishActivity,
                new IntentFilter("Details_Activity_finish"));

        lineOptions = new PolylineOptions();
        BookingDetails();

        if (getIntent().hasExtra("new_request_accepted")) {
            toastDialog.showAlertDialog(getResources().getString(R.string.st_request_accepted_title), getResources().getString(R.string.st_request_accepted_msg));
        }

        mSupportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.google_map);
        mSupportMapFragment.getMapAsync(this);


        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }

        String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        date_format = new SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.getDefault());

        _txt_ride_status.setTypeface(global_typeface.Sansation_Bold());
        _txt_code.setTypeface(global_typeface.Sansation_Regular());

        _txt_ride_date_time.setTypeface(global_typeface.Sansation_Regular());
        _txt_ride_id.setTypeface(global_typeface.Sansation_Regular());

        _txt_car_type.setTypeface(global_typeface.Sansation_Bold());
        _txt_driver_name.setTypeface(global_typeface.Sansation_Regular());
        _txt_driver_mobile_no.setTypeface(global_typeface.Sansation_Regular());
        _txt_driver_car_number.setTypeface(global_typeface.Sansation_Regular());

        _txt_ride_price.setTypeface(global_typeface.Sansation_Regular());
        _txt_ride_distance.setTypeface(global_typeface.Sansation_Regular());
        _txt_ride_duration.setTypeface(global_typeface.Sansation_Regular());

        _txt_from_address.setTypeface(global_typeface.Sansation_Regular());
        _txt_to_address.setTypeface(global_typeface.Sansation_Regular());

        _txt_call.setTypeface(global_typeface.Sansation_Regular());
        _txt_cancel_ride.setTypeface(global_typeface.Sansation_Regular());
        _txt_support.setTypeface(global_typeface.Sansation_Regular());
        _txt_person.setTypeface(global_typeface.Sansation_Regular());

        _txt_ride_toll_price.setTypeface(global_typeface.Sansation_Regular());
        _txt_ride_tip_price.setTypeface(global_typeface.Sansation_Regular());

        _txt_ride_message.setTypeface(global_typeface.Sansation_Regular());
        _txt_driver_delay_time.setTypeface(global_typeface.Sansation_Regular());
        _txt_payment_type.setTypeface(global_typeface.Sansation_Bold());

        _txt_car_brand.setTypeface(global_typeface.Sansation_Regular());
        _txt_car_model.setTypeface(global_typeface.Sansation_Regular());
        _txt_car_color.setTypeface(global_typeface.Sansation_Regular());

        _txt_total_person.setTypeface(global_typeface.Sansation_Regular());

        _txt_refund_pending.setTypeface(global_typeface.Sansation_Regular());
        _txt_refund_initiated.setTypeface(global_typeface.Sansation_Regular());

        _txt_price.setTypeface(global_typeface.Sansation_Regular());
        _txt_price_t_n_c.setTypeface(global_typeface.Sansation_Regular());

        _img_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog Dialog = new Dialog(RentalBookingDetailsActivity.this);
                Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                Dialog.setContentView(R.layout.layout_dialog_call);

                Dialog.show();
                final Button btn_yes = (Button) Dialog.findViewById(R.id.btn_yes);
                final Button btn_no = (Button) Dialog.findViewById(R.id.btn_no);

                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            if (ContextCompat.checkSelfPermission(RentalBookingDetailsActivity.this,
                                    Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
                                    || ContextCompat.checkSelfPermission(RentalBookingDetailsActivity.this, Manifest.permission.READ_PHONE_STATE)
                                    == PackageManager.PERMISSION_GRANTED) {
                                Dialog.dismiss();
                                Intent intent_call = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "100"));
                                startActivity(intent_call);
                            }
                        } catch (Exception ex) {
                            toastDialog.ShowToastMessage(getResources().getString(R.string.call_error));
                            ex.printStackTrace();
                        }

                    }
                });

                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Dialog.dismiss();
                    }
                });


            }
        });

        _layout_cancel_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog onRideCancelDialog = new Dialog(RentalBookingDetailsActivity.this);
                onRideCancelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                onRideCancelDialog.setContentView(R.layout.layout_dialog_ride_cancel);

                onRideCancelDialog.show();
                final Button btn_yes = (Button) onRideCancelDialog.findViewById(R.id.btn_yes);
                final Button btn_no = (Button) onRideCancelDialog.findViewById(R.id.btn_no);

                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isInternetOn(getApplicationContext())) {

                            try {
                                progressDialog.show();
                                Call<JsonObject> call = apiService.user_ride_cancel(stBookingID, preferencesUtility.getuser_id(), stBooking_Status);
                                call.enqueue(new Callback<JsonObject>() {
                                    @Override
                                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                        String mResult = response.body().toString();
                                        progressDialog.dismiss();
                                        Log.e("TAG_Cancel_Booking_Data", mResult);
                                        try {
                                            JSONObject objCancelData = new JSONObject(mResult);
                                            if (objCancelData.getString("status").equalsIgnoreCase("1")) {
                                                toastDialog.ShowToastMessage(objCancelData.getString("message"));
                                                onRideCancelDialog.dismiss();
                                                finish();
                                            } else {
                                                toastDialog.ShowToastMessage(objCancelData.getString("message"));
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<JsonObject> call, Throwable t) {
                                        t.printStackTrace();
                                        progressDialog.dismiss();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                        }
                    }
                });

                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onRideCancelDialog.dismiss();
                    }
                });


            }
        });

        _layout_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallToDriver(stDriverContactNumber);
            }
        });

        _layout_support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent _intent = new Intent(RentalBookingDetailsActivity.this, UserDriverSupportActivity.class);
                _intent.putExtra("driverID", stDriverId);
                startActivity(_intent);
            }
        });

        //Drop Address
        _txt_to_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mDropLat + "," + mDropLng + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //Pickup Address
        _txt_from_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mPickUpLat + "," + mPickUpLng + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        _tv_pickup_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mPickUpLat + "," + mPickUpLng + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        _tv_drop_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mDropLat + "," + mDropLng + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        _imgfab_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String message = "My Ohicabs Ride Detail \n";
                message = message + "————————————————" + "\n";
                message = message + "Pickup Location: " + _txt_from_address.getText() + "\n";
                message = message + "Drop Location: " + _txt_to_address.getText() + "\n";
                message = message + "————————————————" + "\n";
                message = message + "Driver Information" + "\n";
                message = message + "————————————————" + "\n";
                message = message + "Name:" + _txt_driver_name.getText().toString() + "\n";
                message = message + "Contact No:" + _txt_driver_mobile_no.getText().toString() + "\n";
                message = message + "Vehicle No:" + _txt_driver_car_number.getText() + "\n";
                message = message + "Model :" + _txt_car_model.getText() + "\n";
                message = message + "————————————————\n";
//                message = message + "Click here for map\n";
//                message=message+"https://maps.google.com/?q"+mUserCurrentLat+","+mUserCurrentLng;
//                message = message + "https://maps.google.com?saddr=" + mPickUpLat + "," + mPickUpLng + "&daddr=" + mDropLat + "," + mDropLng+"\n";
                message = message + SHARE_LINK + stBookingID;

//                String message = SHARE_LINK+stBookingID;

                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, message);

                startActivity(Intent.createChooser(share, "Share information"));
            }
        });

        _imgfab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  get current location on button click
                try {
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mUserCurrentLat, mUserCurrentLng)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        _img_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BookingDetails();
            }
        });

        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.e(TAG, "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                Log.e(TAG, "onPanelStateChanged " + newState);
                SCROLL_STATE = newState.toString();

                if (SCROLL_STATE.equalsIgnoreCase("EXPANDED")) {
                    _txt_driver_delay_time.setVisibility(View.GONE);
                    _imgfab_share.setVisibility(View.GONE);
                    _img_help.setVisibility(View.GONE);
                    _imgfab.setVisibility(View.GONE);
                    _img_refresh.setVisibility(View.GONE);

                } else {
                    if ((stBooking_Status.equalsIgnoreCase(Constant.STATUS_ACCEPT)
                            || stBooking_Status.equalsIgnoreCase(Constant.STATUS_GOTO_USER)
                            || stBooking_Status.equalsIgnoreCase(Constant.STATUS_USER_WAIT)) && !preferencesUtility.getDriverWaitTime().equalsIgnoreCase("")) {
                        _txt_driver_delay_time.setVisibility(View.GONE);

                    } else {
                        _txt_driver_delay_time.setVisibility(View.GONE);
                    }
                    if (stBooking_Status.equalsIgnoreCase(Constant.STATUS_START_TRIP)) {
                        _img_help.setVisibility(View.VISIBLE);
                    } else {
                        _img_help.setVisibility(View.GONE);
                    }
                    _imgfab.setVisibility(View.VISIBLE);
                    _imgfab_share.setVisibility(View.VISIBLE);
                    _img_refresh.setVisibility(View.VISIBLE);
                }
            }
        });

        mLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });

        if (mLayout.getAnchorPoint() == 1.0f) {
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        }


        txt_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // This is for Get get height and width of Screen
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                try {
                    display.getRealSize(size);
                } catch (NoSuchMethodError err) {
                    display.getSize(size);
                }
                final int width = size.x;
                int height = size.y;


                final Dialog dialog = new Dialog(RentalBookingDetailsActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.layout_ride_payment);
                dialog.setTitle("");
                dialog.setCancelable(true);
                dialog.show();

                LinearLayout mainLayout = (LinearLayout) dialog.findViewById(R.id.mainLayout);
                mainLayout.setMinimumWidth((int) (width / 0.7));
//                mainLayout.setMinimumHeight((int) (height/0.5));

                TextView txt_total_km = (TextView) dialog.findViewById(R.id.txt_total_km);
                TextView txt_total_time = (TextView) dialog.findViewById(R.id.txt_total_time);
                TextView txt_total_amount = (TextView) dialog.findViewById(R.id.txt_total_amount);
                TextView txt_booking_id = (TextView) dialog.findViewById(R.id.txt_booking_id);

                txt_total_km.setText(" " + stDistance + " km");
                txt_total_time.setText(" " + stDuration + " min");
                txt_total_amount.setText(" ₹" + stAmount);
                txt_booking_id.setText("#" + stBookingID);

                Button btn_pay = (Button) dialog.findViewById(R.id.btn_pay);

                btn_pay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        PaytmPayment();
                    }
                });

            }
        });


        _txt_price_t_n_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), MonthlySpendingTermsActivity.class);
                intent.putExtra("term_type","3");
                startActivity(intent);

            }
        });
    }

    private void PaytmPayment() {

        Log.e("PaytmPayment", "called");


//        final PaytmPGService paytmPGService = PaytmPGService.getStagingService(); // Stagging
        final PaytmPGService paytmPGService = PaytmPGService.getProductionService(); // Production

        int amt = (int) Math.round(Double.parseDouble(stAmount));
        Log.e("stAmount", stAmount + "--");

        Call<JsonObject> call = apiService.generate_checksum(stBookingId, preferencesUtility.getuser_id(), stAmount + "", "0");
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e("generate_checksum", response.body().toString());
                try {
                    JSONObject objData = new JSONObject(response.body().toString());
                    if (objData.getString("status").equalsIgnoreCase("1")) {

                        JSONObject objPayload = objData.getJSONObject("payload");

                        // Create a HASHMAP Object holding the Order Information
                        HashMap<String, String> paramMap = new HashMap<String, String>();
                        paramMap.put("MID", objPayload.getString("MID"));
                        paramMap.put("ORDER_ID", objPayload.getString("ORDER_ID"));
                        paramMap.put("CUST_ID", objPayload.getString("CUST_ID"));
                        paramMap.put("INDUSTRY_TYPE_ID", objPayload.getString("INDUSTRY_TYPE_ID"));
                        paramMap.put("CHANNEL_ID", objPayload.getString("CHANNEL_ID"));
                        paramMap.put("TXN_AMOUNT", objPayload.getString("TXN_AMOUNT"));
                        paramMap.put("WEBSITE", objPayload.getString("WEBSITE"));
                        paramMap.put("CALLBACK_URL", objPayload.getString("CALLBACK_URL"));

                        if (objPayload.has("EMAIL")) {
                            paramMap.put("EMAIL", objPayload.getString("EMAIL"));
                        }
                        if (objPayload.has("MOBILE_NO")) {
                            paramMap.put("MOBILE_NO", objPayload.getString("MOBILE_NO"));
                        }

                        paramMap.put("CHECKSUMHASH", objPayload.getString("CHECKSUMHASH"));

                        // Create Paytm Order Object
                        PaytmOrder paytmOrder = new PaytmOrder(paramMap);

                        paytmPGService.initialize(paytmOrder, null);

                        paytmPGService.startPaymentTransaction(RentalBookingDetailsActivity.this, true, true,
                                new PaytmPaymentTransactionCallback() {
                                    @Override
                                    public void onTransactionResponse(Bundle bundle) {
                                        try {

                                            Log.e("LOG", "Payment Transaction : " + bundle);
                                            Log.e("LOG", "Payment Transaction : " + bundle.getString("STATUS"));

                                            if (bundle.getString("STATUS").equals("TXN_SUCCESS")) {
                                                JSONObject json = new JSONObject();
                                                Set<String> keys = bundle.keySet();
                                                for (String key : keys) {
                                                    try {
                                                        // json.put(key, bundle.get(key)); see edit below
                                                        json.put(key, JSONObject.wrap(bundle.get(key)));
                                                    } catch (JSONException e) {
                                                        //Handle exception here
                                                    }
                                                }

                                                Log.e("JSon_data", json.toString());
                                                success_payload = json.toString();

                                                if (bundle.containsKey("TXNID")) {
                                                    paytm_transaction_id = bundle.getString("TXNID").toString();
                                                }
                                                order_id = bundle.getString("ORDERID").toString();

                                                RidePaytmPaymentServiceCall();

                                            } else {
                                                JSONObject json = new JSONObject();
                                                Set<String> keys = bundle.keySet();
                                                for (String key : keys) {
                                                    try {
                                                        // json.put(key, bundle.get(key)); see edit below
                                                        json.put(key, JSONObject.wrap(bundle.get(key)));
                                                    } catch (JSONException e) {
                                                        //Handle exception here
                                                    }
                                                }

                                                Log.e("JSon_data", json.toString());
                                                fail_payload = json.toString();
                                                if (bundle.containsKey("TXNID")) {
                                                    paytm_transaction_id = bundle.getString("TXNID").toString();
                                                }
                                                order_id = bundle.getString("ORDERID").toString();

                                                failServiceCall();
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void networkNotAvailable() {
                                        Log.d("LOG", "UI Error Occur.");
                                        Toast.makeText(getApplicationContext(), " UI Error Occur. ", Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void clientAuthenticationFailed(String inErrorMessage) {
                                        Log.d("LOG", "clientAuthenticationFailed.");
                                        Toast.makeText(getApplicationContext(), " Sever-side Error " + inErrorMessage, Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void someUIErrorOccurred(String s) {
                                        Log.d("LOG", "UI Error Occur.");
                                    }

                                    @Override
                                    public void onErrorLoadingWebPage(int i, String s, String s1) {
                                        Log.d("LOG", "onErrorLoadingWebPage.");
                                    }

                                    @Override
                                    public void onBackPressedCancelTransaction() {
                                        Log.d("LOG", "onBackPressedCancelTransaction");
                                    }

                                    @Override
                                    public void onTransactionCancel(String s, Bundle bundle) {
                                        Log.d("LOG", "Payment_Transaction_Failed " + bundle);
                                        Toast.makeText(getBaseContext(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
                                    }
                                });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });


    }

    private void failServiceCall() {
        Call<JsonObject> call = apiService.paytm_payment_fail(stBookingId, fail_payload, paytm_transaction_id, order_id);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e("paytm_payment_fail", response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    /**
     * User give Tip to Driver
     */
    private void RidePaytmPaymentServiceCall() {
        try {

            String tip_amount = "";

//            Log.e("payUmoney_trans_id", paytm_transaction_id);
            progressDialog.show();
            Call<JsonObject> call = apiService.ride_paytm_payment(stBookingId, preferencesUtility.getuser_id(), "0",
                    paytm_transaction_id, success_payload, order_id, "1");

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        String mResult = response.body().toString();
                        Log.e("ride_stop_tip", mResult);
                        progressDialog.dismiss();

                        JSONObject objData = new JSONObject(mResult);
                        if (objData.getString("status").equalsIgnoreCase("1")) {
                            toastDialog.ShowToastMessage(objData.getString("message"));

                            BookingDetails();

                        } else {
//                            finish();
                        }

                    } catch (Exception e) {
                        progressDialog.dismiss();
                        Log.e("Exception", e.toString());
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("onFailure", t.getMessage().toString());
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.ly_total_person)
    public void persononclick() {
        Intent i = new Intent(this, YourRouteActivity.class);
        i.putExtra("driverID", stDriverId);
        startActivity(i);
    }

    @Override
    public void onLocationChanged(Location location) {

        mUserCurrentLat = location.getLatitude();
        mUserCurrentLng = location.getLongitude();

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mGoogleMap = map;

        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(RentalBookingDetailsActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
        }

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
        mGoogleMap.getUiSettings().setCompassEnabled(false);
        mGoogleMap.getUiSettings().setTiltGesturesEnabled(false);
        mGoogleMap.setBuildingsEnabled(false);

        //Disable Marker click event
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });
    }

    protected synchronized void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {

            mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        mLocationRequest.setSmallestDisplacement(10);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
                return;
            } else {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                        mLocationRequest, this);
            }
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            ShowToast(getResources().getString(R.string.network_disconnect));
        } else if (i == CAUSE_NETWORK_LOST) {
            ShowToast(getResources().getString(R.string.network_lost));
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("TAG", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    /**
     * Booking Details Parse JSON
     *
     * @param mResult
     */
    private void GetRideBookingDetails(String mResult) {
        Log.e("Carbooking_Details", mResult);
        Log.e("User_Current_Lat", mUserCurrentLat + "--" + mUserCurrentLng);

        String st_ride_price = "";

        try {
            JSONObject objBookingList = new JSONObject(mResult);
            if (objBookingList.getString("status").equalsIgnoreCase("1")) {
                JSONArray jarrPayload = objBookingList.getJSONArray("payload");
                if (jarrPayload.length() > 0) {
                    for (int i = 0; i < jarrPayload.length(); i++) {
                        JSONObject objBookingData = jarrPayload.getJSONObject(i);

                        stDriverId = objBookingData.getString("driver_id");
                        stBooking_Status = objBookingData.getString("booking_status");
                        stBookingID = objBookingData.getString("booking_id");
                        isTaxiRide = objBookingData.getString("is_taxi");
                        stServiceId = objBookingData.getString("service_id");

                        if (isTaxiRide.equals("2")) {
                            if (Integer.parseInt(objBookingData.getString("book_seat").toString()) > 0) {
                                _txt_person.setText("Seat: " + objBookingData.getString("book_seat"));
                                _layout_person.setVisibility(View.VISIBLE);
                            } else {
                                _layout_person.setVisibility(View.GONE);
                            }
                        } else {
                            _layout_person.setVisibility(View.GONE);
                        }
                        if (isTaxiRide.equals("2")) {
                            if (Integer.parseInt(objBookingData.getString("total_person").toString()) > 0) {
                                if ((Integer.parseInt(objBookingData.getString("total_person")) - Integer.parseInt(objBookingData.getString("book_seat"))) > 0) {
                                    _txt_total_person.setText("Travelling with " + String.valueOf(Integer.parseInt(objBookingData.getString("total_person")) - Integer.parseInt(objBookingData.getString("book_seat")) + " other persons"));
                                    ly_total_person.setVisibility(View.VISIBLE);
                                } else {
                                    ly_total_person.setVisibility(View.GONE);
                                }
                            } else {
                                ly_total_person.setVisibility(View.GONE);
                            }
                        } else {
                            ly_total_person.setVisibility(View.GONE);
                        }
                        // Renatl  Service
                        if (isTaxiRide.equals("3")) {
                            _layout_to_address.setVisibility(View.GONE);
                        }

                        // Change by Divya on 27-12-2017
                        // payment type
                        user_payment_status = objBookingData.getString("user_payment_status");
                        Log.e("user_payment_status", user_payment_status + "-");
                        //check payment done or not
                        if (user_payment_status.equals("0") && objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_COMPLETE_TRIP)) {
                            Intent intent = new Intent(RentalBookingDetailsActivity.this, ReviewRideActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("payment_details", objBookingData.toString());
                            startActivity(intent);

                        }
                        _txt_ride_id.setText("#" + objBookingData.getString("booking_id"));
                        _txt_from_address.setText(objBookingData.getString("pickup_address"));
                        _txt_to_address.setText(objBookingData.getString("drop_address"));
                        _txt_car_type.setText(objBookingData.getString("service_name"));

                        if (objBookingData.has("otp_code")) {
                            if (!objBookingData.getString("otp_code").equals("")) {
                                _txt_code.setText("Code : " + objBookingData.getString("otp_code"));
                            }
                        }

                        _txt_driver_name.setText(objBookingData.getString("firstname") + " " + objBookingData.getString("lastname"));
                        _txt_driver_mobile_no.setText(objBookingData.getString("mobile"));
                        _txt_driver_car_number.setText(objBookingData.getString("car_number"));
                        stDriverContactNumber = objBookingData.getString("mobile");

                        _txt_car_model.setText(objBookingData.getString("model_name"));
                        _txt_car_brand.setText(objBookingData.getString("brand_name"));
                        _txt_car_color.setText(objBookingData.getString("series_name"));

                        /* Check Is Taxi Ride or Normal Ride */
//                        if (isTaxiRide.equalsIgnoreCase("0")) {
//                            _txt_ride_price.setText("₹ " +Math.round(Double.parseDouble(objBookingData.getString("amount"))));
//                            _txt_ride_price.setVisibility(View.VISIBLE);
//                        } else {
//                            _txt_ride_price.setVisibility(View.INVISIBLE);
//                        }

                        _txt_ride_price.setText("₹ " + Math.round(Double.parseDouble(objBookingData.getString("amount"))));
                        _txt_ride_price.setVisibility(View.VISIBLE);

                        if (objBookingData.getString("payment_type").equalsIgnoreCase("4")) {
                            _txt_payment_type.setText("Cash");
                            st_ride_price = getResources().getString(R.string.st_paid_via_cash);
                        } else if (objBookingData.getString("payment_type").equalsIgnoreCase("3")) {
//                            _txt_payment_type.setText("Cash");
                        } else if (objBookingData.getString("payment_type").equalsIgnoreCase("2")) {
//                            _txt_payment_type.setText("Cash");
                        } else if (objBookingData.getString("payment_type").equalsIgnoreCase("1")) {
//                            _txt_payment_type.setText("Cash");
                        } else if (objBookingData.getString("payment_type").equalsIgnoreCase("0")) {
                            _txt_payment_type.setText("Paytm");
                            st_ride_price = getResources().getString(R.string.st_paid_via_paytm);
                        }

                        _txt_price.setText(Html.fromHtml(st_ride_price + " <font color=\"" + getResources().getColor(R.color.colorGreen) + "\">" +
                                Math.round(Double.parseDouble(objBookingData.getString("amount"))) + "</font>"));

                        _txt_ride_duration.setText(objBookingData.getString("duration") + " " + getResources().getString(R.string.st_minute));
                        _txt_ride_distance.setText(roundTwoDecimals(Double.parseDouble(objBookingData.getString("total_distance"))) + " " + getResources().getString(R.string.st_km));

                        if (objBookingData.getString("toll_tax").equalsIgnoreCase("")) {
                            _txt_ride_toll_price.setText(getResources().getString(R.string.st_toll) + ": ₹0.00");
                        } else {
                            _txt_ride_toll_price.setText(getResources().getString(R.string.st_toll) + ": ₹" + Math.round(Double.parseDouble(objBookingData.getString("toll_tax"))));
                        }

                        if (objBookingData.getString("tip_amount").equalsIgnoreCase("")) {
                            _txt_ride_tip_price.setText(getResources().getString(R.string.st_tip) + ": ₹0.00");
                        } else {
                            _txt_ride_tip_price.setText(getResources().getString(R.string.st_tip) + ": ₹" + Math.round(Double.parseDouble(objBookingData.getString("tip_amount"))));
                        }

                        Glide.with(this).load(Global_ServiceApi.API_IMAGE_HOST + objBookingData.getString("icon"))
                                .crossFade()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(_img_car_type);

                        Glide.with(this).load(Global_ServiceApi.API_IMAGE_HOST + objBookingData.getString("image"))
                                .crossFade()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .placeholder(R.drawable.bg_avatar)
                                .into(_img_profile);

                        /* Price Data */
                        _arrPriceList = mHelper.GetPriceData(objBookingData.getString("price_id"));

                        mPickUpLat = Double.parseDouble(objBookingData.getString("pickup_lati"));
                        mPickUpLng = Double.parseDouble(objBookingData.getString("pickup_longi"));

                        mDropLat = Double.parseDouble(objBookingData.getString("drop_lati"));
                        mDropLng = Double.parseDouble(objBookingData.getString("drop_longi"));

                        mDriverLat = Double.parseDouble(objBookingData.getString("lati"));
                        mDriverLng = Double.parseDouble(objBookingData.getString("longi"));

                        stDistance = objBookingData.getString("est_total_distance");
                        stDuration = objBookingData.getString("est_duration");
                        stAmount = objBookingData.getString("amount");

                        layout_waiting_payment.setVisibility(View.GONE);
                        _txt_refund_pending.setVisibility(View.GONE);
                        _txt_refund_initiated.setVisibility(View.GONE);

                        if (mMarker_driver == null) {

                            if (stServiceId.equals("1")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_1)));
                            } else if (stServiceId.equals("2") || stServiceId.equals("13") || stServiceId.equals("18")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_2)));
                            } else if (stServiceId.equals("3") || stServiceId.equals("19")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_3)));
                            } else if (stServiceId.equals("11")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_11)));
                            } else if (stServiceId.equals("12")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_12)));
                            } else if (stServiceId.equals("13")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_13)));
                            } else if (stServiceId.equals("14")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_14)));
                            } else if (stServiceId.equals("15")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_15)));
                            } else if (stServiceId.equals("16") || stServiceId.equals("17")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_16)));
                            } else {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_taxi)));
                            }
//                            mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(mDriverLat, mDriverLng))
//                                    .title(getResources().getString(R.string.st_driver_location))
//                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_taxi)));
                        } else {
                            prevLoc.setLatitude(mMarker_driver.getPosition().latitude);
                            prevLoc.setLongitude(mMarker_driver.getPosition().longitude);

                            mMarker_driver.setPosition(new LatLng(mDriverLat, mDriverLng));

                            newLoc.setLatitude(mMarker_driver.getPosition().latitude);
                            newLoc.setLongitude(mMarker_driver.getPosition().longitude);

                            float bearing = prevLoc.bearingTo(newLoc);
                            mMarker_driver.setRotation(bearing);
                        }

                        stBookingStatusForMarker = objBookingData.getString("booking_status");
                        if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_PENDING)) {
                            //Ride  Pending
                            _img_call.setEnabled(false);
                            _txt_call.setEnabled(false);
                            _img_call.setAlpha(0.2f);
                            _txt_call.setAlpha(0.2f);
                            _layout_call.setEnabled(true);

                            _img_support.setEnabled(false);
                            _img_support.setAlpha(0.2f);
                            _txt_support.setAlpha(0.2f);
                            _layout_support.setEnabled(false);

                            _img_cancel.setEnabled(true);
                            _txt_cancel_ride.setEnabled(true);
                            _layout_cancel_ride.setEnabled(false);

                            _txt_ride_message.setVisibility(View.VISIBLE);
                            _layout_driver_name.setVisibility(View.GONE);
                            _layout_person.setVisibility(View.GONE);
                            _layout_driver_call.setVisibility(View.GONE);
                            _layout_driver_vehical_no.setVisibility(View.GONE);
                            view_ride_details.setVisibility(View.GONE);
                            _layout_ride_time_details.setVisibility(View.GONE);
                            _layout_ride_price.setVisibility(View.GONE);
                            view_toll_tip.setVisibility(View.GONE);
                            _txt_code.setVisibility(View.GONE);

                            _img_ride_status.setImageResource(R.drawable.ic_pending);
                            _txt_ride_status.setText(getResources().getString(R.string.st_ride_pending));

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("pickup_date"));
                            df.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.getDefault());
                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString().toLowerCase());

                        } else if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_ACCEPT)) {
                            //Ride Accepted
                            _txt_ride_message.setVisibility(View.GONE);
                            _layout_ride_time_details.setVisibility(View.GONE);
                            view_ride_details.setVisibility(View.GONE);
                            _layout_ride_price.setVisibility(View.GONE);
                            view_toll_tip.setVisibility(View.GONE);

                            _img_cancel.setEnabled(true);
                            _txt_cancel_ride.setEnabled(true);
                            _img_call.setEnabled(false);
                            _txt_call.setEnabled(false);
                            _layout_call.setEnabled(false);
                            _img_call.setAlpha(0.2f);
                            _txt_call.setAlpha(0.2f);

                            _img_ride_status.setImageResource(R.drawable.ic_accepted);
                            _txt_ride_status.setText(getResources().getString(R.string.st_ride_accept));

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("accept_time"));
                            df.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.getDefault());
                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString().toLowerCase());

                            // Start Time Tracker
//                            DriverWaitCountDown();


                            if (objBookingData.getString("payment_type").equalsIgnoreCase("0")) {
                                if (objBookingData.getString("user_payment_status").equals("0")) {
                                    layout_waiting_payment.setVisibility(View.VISIBLE);
                                }
                            }

//                            for (Polyline line : polylines) {
//                                line.remove();
//                            }
//
//                            polylines.clear();
//
//                            LatLng pickup = new LatLng(mDriverLat, mDriverLng);
//                            LatLng dest = new LatLng(mPickUpLat, mPickUpLng);
//
//                            String url = getUrl(pickup, dest);
//                            Log.e("onMapClick", url.toString() + "--");
//                            FetchUrl FetchUrl = new FetchUrl();
//
//                            // Start downloading json data from Google Directions API
//                            FetchUrl.execute(url);
//
//                            String stTitle = "", stSnippet = "";
//                            if (TotalDuration == 0) {
//                                stTitle = "wait...";
//                                stSnippet = "......";
//                            } else {
//                                stTitle = "Arrival";
//                                stSnippet = Math.round(TotalDuration) + " mins";
//                            }
//
//                            mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(mPickUpLat, mPickUpLng))
//                                    .title(stTitle)
//                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin))
//                                    .snippet(stSnippet));
//                            _arr_Markers_Pickup.add(mMarker_pickup);


                        } else if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_GOTO_USER)) {
                            // Go to User
                            _img_call.setEnabled(true);
                            _img_cancel.setEnabled(true);
                            _txt_call.setEnabled(true);
                            _txt_cancel_ride.setEnabled(true);
                            _txt_ride_message.setVisibility(View.GONE);
                            _layout_cancel_ride.setEnabled(true);

                            _layout_ride_time_details.setVisibility(View.GONE);
                            view_ride_details.setVisibility(View.GONE);
                            _layout_ride_price.setVisibility(View.GONE);
                            view_toll_tip.setVisibility(View.GONE);

                            _img_ride_status.setImageResource(R.drawable.ic_go_user);
                            _txt_ride_status.setText(getResources().getString(R.string.st_go_to_user));

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("start_time"));
                            df.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.getDefault());
                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString().toLowerCase());

                            // Start Time Tracker
//                            DriverWaitCountDown();
//                            for (Polyline line : polylines) {
//                                line.remove();
//                            }
//
//                            polylines.clear();
//
//                            LatLng pickup = new LatLng(mDriverLat, mDriverLng);
//                            LatLng dest = new LatLng(mPickUpLat, mPickUpLng);
//
//                            if (mDriverLat != 0.0 && mDriverLng != 0.0 && mPickUpLat != 0.0 && mPickUpLng != 0.0) {
//
//                                String url = getUrl(pickup, dest);
//                                Log.e("onMapClick", url.toString() + "--");
//                                FetchUrl FetchUrl = new FetchUrl();
//
//                                // Start downloading json data from Google Directions API
//                                FetchUrl.execute(url);
//
//                                String stTitle = "", stSnippet = "";
//                                if (TotalDuration == 0) {
//                                    stTitle = "wait...";
//                                    stSnippet = "......";
//                                } else {
//                                    stTitle = "Arrival";
//                                    stSnippet = Math.round(TotalDuration) + " mins";
//                                }
//
//                                mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
//                                        .position(new LatLng(mPickUpLat, mPickUpLng))
//                                        .title(stTitle)
//                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin))
//                                        .snippet(stSnippet));
//                                mMarker_pickup.showInfoWindow();
//                                _arr_Markers_Pickup.add(mMarker_pickup);
//                            }

                            if (objBookingData.getString("payment_type").equalsIgnoreCase("0")) {
                                if (objBookingData.getString("user_payment_status").equals("0")) {
                                    layout_waiting_payment.setVisibility(View.VISIBLE);
                                }
                            }

                        } else if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_USER_WAIT)) {
                            // Go to User Wait
                            _img_call.setEnabled(true);
                            _img_cancel.setEnabled(true);
                            _txt_call.setEnabled(true);
                            _txt_cancel_ride.setEnabled(true);
                            _txt_ride_message.setVisibility(View.GONE);
                            _layout_cancel_ride.setEnabled(true);

                            _layout_ride_time_details.setVisibility(View.GONE);
                            view_ride_details.setVisibility(View.GONE);
                            _layout_ride_price.setVisibility(View.GONE);
                            view_toll_tip.setVisibility(View.GONE);

                            _img_ride_status.setImageResource(R.drawable.ic_wait_user);
                            _txt_ride_status.setText(getResources().getString(R.string.st_driver_has_arrived));

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("start_time"));
                            df.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.getDefault());
                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString().toLowerCase());

////                            DriverWaitCountDown();
//                            for (Polyline line : polylines) {
//                                line.remove();
//                            }
//
//                            polylines.clear();
//
//                            LatLng pickup = new LatLng(mDriverLat, mDriverLng);
//                            LatLng dest = new LatLng(mPickUpLat, mPickUpLng);
//
//                            if (mDriverLat != 0.0 && mDriverLng != 0.0 && mPickUpLat != 0.0 && mPickUpLng != 0.0) {
//
//                                String url = getUrl(pickup, dest);
//                                Log.e("onMapClick", url.toString() + "--");
//                                FetchUrl FetchUrl = new FetchUrl();
//
//                                // Start downloading json data from Google Directions API
//                                FetchUrl.execute(url);
//
//                                String stTitle = "", stSnippet = "";
//                                if (TotalDuration == 0) {
//                                    stTitle = "wait...";
//                                    stSnippet = "......";
//                                } else {
//                                    stTitle = "Arrival";
//                                    stSnippet = Math.round(TotalDuration) + " mins";
//                                }
//
//                                mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
//                                        .position(new LatLng(mPickUpLat, mPickUpLng))
//                                        .title(stTitle)
//                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin))
//                                        .snippet(stSnippet));
//                                mMarker_pickup.showInfoWindow();
//                                _arr_Markers_Pickup.add(mMarker_pickup);
//                            }
//
//                            if (objBookingData.getString("payment_type").equalsIgnoreCase("0")) {
//                                if (objBookingData.getString("user_payment_status").equals("0")) {
//                                    layout_waiting_payment.setVisibility(View.VISIBLE);
//                                }
//                            }

                        } else if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_START_TRIP)) {
                            //Ride Start
                            _img_call.setEnabled(true);
                            _txt_call.setEnabled(true);
                            _txt_ride_message.setVisibility(View.GONE);

                            _layout_cancel_ride.setEnabled(false);
                            _img_cancel.setAlpha(0.2f);
                            _txt_cancel_ride.setAlpha(0.2f);
                            _img_cancel.setEnabled(false);
                            _txt_cancel_ride.setEnabled(false);
                            _txt_code.setVisibility(View.GONE);

                            _layout_ride_price.setVisibility(View.GONE);
                            view_toll_tip.setVisibility(View.GONE);

                            _img_ride_status.setImageResource(R.drawable.ic_started);
                            _txt_ride_status.setText(getResources().getString(R.string.st_ride_start));

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("start_time"));
                            df.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.getDefault());
                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString().toLowerCase());

//                            if (mMarker_pickup != null) {
//                                mMarker_pickup.remove();
//                            }

//                            mMarker_drop = mGoogleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(mDropLat, mDropLng))
//                                    .title(getResources().getString(R.string.st_drop_location))
//                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin)));
//                            _arr_Markers_Pickup.add(mMarker_drop);
//
//                            _img_help.setVisibility(View.VISIBLE);
//
//                            for (Polyline line : polylines) {
//                                line.remove();
//                            }
//
//                            polylines.clear();
//
//                            LatLng pickup = new LatLng(mDriverLat, mDriverLng);
//                            LatLng dest = new LatLng(mDropLat, mDropLng);
//
//                            String url = getUrl(pickup, dest);
//                            Log.e("onMapClick", url.toString() + "--");
//                            FetchUrl FetchUrl = new FetchUrl();
//
//                            // Start downloading json data from Google Directions API
//                            FetchUrl.execute(url);


                        } else if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_COMPLETE_TRIP)) {
                            //Ride Complete
                            _img_cancel.setEnabled(false);
                            _img_call.setEnabled(false);
                            _txt_call.setEnabled(false);
                            _txt_cancel_ride.setEnabled(false);

                            _layout_cancel_ride.setEnabled(false);
                            _layout_call.setEnabled(false);
                            _layout_support.setEnabled(false);

                            _img_cancel.setAlpha(0.2f);
                            _img_support.setAlpha(0.2f);
                            _img_call.setAlpha(0.2f);

                            _txt_cancel_ride.setAlpha(0.2f);
                            _txt_call.setAlpha(0.2f);
                            _txt_support.setAlpha(0.2f);

                            _txt_ride_message.setVisibility(View.GONE);
                            _layout_ride_price.setVisibility(View.VISIBLE);
                            view_toll_tip.setVisibility(View.VISIBLE);
                            _txt_code.setVisibility(View.GONE);

                            _img_ride_status.setImageResource(R.drawable.ic_completed);
                            _txt_ride_status.setText(getResources().getString(R.string.st_ride_complete));

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("stop_time"));
                            df.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.getDefault());
                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString().toLowerCase());

                            /* Draw Path b/w pickup and drop location */
                            JSONArray _arrRouteLocation = objBookingData.getJSONArray("location");
                            if (_arrRouteLocation.length() > 0) {
                                for (int k = 0; k < _arrRouteLocation.length(); k++) {
                                    JSONObject objRoute = _arrRouteLocation.getJSONObject(k);
                                    routeArray.add(new LatLng(objRoute.getDouble("latitude"), objRoute.getDouble("longitude")));
                                }

                                lineOptions.addAll(routeArray);
                                lineOptions.color(Color.parseColor("#168E5A"));
                                lineOptions.width(7.0f);
                                mGoogleMap.addPolyline(lineOptions);
                            }

//                            mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(mPickUpLat, mPickUpLng))
//                                    .title(getResources().getString(R.string.st_pickup_location))
//                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin)));
//                            _arr_Markers_Pickup.add(mMarker_pickup);
//
//                            mMarker_drop = mGoogleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(mDropLat, mDropLng))
//                                    .title(getResources().getString(R.string.st_drop_location))
//                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin)));
//                            _arr_Markers_Pickup.add(mMarker_drop);

//                            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(mPickUpLat, mPickUpLng)).zoom(14.0f).build();
//                            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                        } else if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_CANCEL_TRIP)) {
                            //Ride Canceled
                            _img_cancel.setEnabled(false);
                            _img_call.setEnabled(false);
                            _txt_call.setEnabled(false);
                            _txt_cancel_ride.setEnabled(false);

                            _layout_cancel_ride.setEnabled(false);
                            _layout_call.setEnabled(false);
                            _layout_support.setEnabled(false);

                            _img_cancel.setAlpha(0.2f);
                            _img_support.setAlpha(0.2f);
                            _img_call.setAlpha(0.2f);

                            _txt_cancel_ride.setAlpha(0.2f);
                            _txt_call.setAlpha(0.2f);
                            _txt_support.setAlpha(0.2f);

                            _txt_ride_message.setVisibility(View.GONE);
                            _layout_driver_name.setVisibility(View.GONE);
                            _layout_person.setVisibility(View.GONE);
                            _layout_driver_call.setVisibility(View.GONE);
                            _layout_driver_vehical_no.setVisibility(View.GONE);

                            _layout_ride_price.setVisibility(View.GONE);
                            view_toll_tip.setVisibility(View.GONE);
                            _txt_code.setVisibility(View.GONE);

                            _txt_ride_price.setText("₹ 0");
                            _txt_ride_distance.setText("0 " + getResources().getString(R.string.st_km));
                            _txt_ride_duration.setText("0 " + getResources().getString(R.string.st_minute));

                            _img_ride_status.setImageResource(R.drawable.ic_cancelled);
                            _txt_ride_status.setText(getResources().getString(R.string.st_ride_cancel));

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("stop_time"));
                            df.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.getDefault());
                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString().toLowerCase());

//                            mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(mPickUpLat, mPickUpLng))
//                                    .title(getResources().getString(R.string.st_pickup_location))
//                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin)));
//                            _arr_Markers_Pickup.add(mMarker_pickup);
//
//                            mMarker_drop = mGoogleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(mDropLat, mDropLng))
//                                    .title(getResources().getString(R.string.st_drop_location))
//                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin)));
//                            _arr_Markers_Pickup.add(mMarker_drop);

                            if (objBookingData.getString("payment_type").equalsIgnoreCase("0")) {
                                if (objBookingData.getString("user_payment_status").equals("2")) {
                                    _txt_refund_pending.setVisibility(View.VISIBLE);
                                } else if (objBookingData.getString("user_payment_status").equals("3")) {
                                    _txt_refund_initiated.setVisibility(View.VISIBLE);
                                }
                            }

//                            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(mPickUpLat, mPickUpLng)).zoom(14.0f).build();
//                            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                        } else if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_AUTO_REJECT)) {
                            //Ride Canceled Auto Reject- Driver not found
                            _img_cancel.setEnabled(false);
                            _img_call.setEnabled(false);
                            _txt_call.setEnabled(false);
                            _txt_cancel_ride.setEnabled(false);

                            _layout_cancel_ride.setEnabled(false);
                            _layout_call.setEnabled(false);
                            _layout_support.setEnabled(false);

                            _img_cancel.setAlpha(0.2f);
                            _img_support.setAlpha(0.2f);
                            _img_call.setAlpha(0.2f);

                            _txt_cancel_ride.setAlpha(0.2f);
                            _txt_call.setAlpha(0.2f);
                            _txt_support.setAlpha(0.2f);

                            _txt_ride_message.setVisibility(View.GONE);
                            _layout_driver_name.setVisibility(View.GONE);
                            _layout_driver_call.setVisibility(View.GONE);
                            _layout_driver_vehical_no.setVisibility(View.GONE);
                            _layout_ride_price.setVisibility(View.GONE);
                            view_toll_tip.setVisibility(View.GONE);
                            _txt_code.setVisibility(View.GONE);

                            _txt_ride_price.setText("₹ 0");
                            _txt_ride_distance.setText("0 " + getResources().getString(R.string.st_km));
                            _txt_ride_duration.setText("0 " + getResources().getString(R.string.st_minute));

                            _img_ride_status.setImageResource(R.drawable.ic_auto_rejected);
                            _txt_ride_status.setText("Driver Not Available");

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("stop_time"));
                            df.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.getDefault());
                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString().toLowerCase());

//                            mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(mPickUpLat, mPickUpLng))
//                                    .title(getResources().getString(R.string.st_pickup_location))
//                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin)));
//                            _arr_Markers_Pickup.add(mMarker_pickup);
//
//                            mMarker_drop = mGoogleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(mDropLat, mDropLng))
//                                    .title(getResources().getString(R.string.st_drop_location))
//                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin)));
//                            _arr_Markers_Pickup.add(mMarker_drop);

//                            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(mPickUpLat, mPickUpLng)).zoom(14.0f).build();
//                            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }

                        _txt_car_type.setTextColor(Color.parseColor("#" + objBookingData.getString("color")));


                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        builder.include(new LatLng(mPickUpLat, mPickUpLng));
//                        builder.include(new LatLng(mDropLat, mDropLng));
                        LatLngBounds bounds = builder.build();
                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));

//                        mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
//                                .position(new LatLng(mPickUpLat, mPickUpLng))
//                                .title(getResources().getString(R.string.st_pickup_location))
//                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin)));
//                        _arr_Markers_Pickup.add(mMarker_pickup);
//
//                        mMarker_drop = mGoogleMap.addMarker(new MarkerOptions()
//                                .position(new LatLng(mDropLat, mDropLng))
//                                .title(getResources().getString(R.string.st_drop_location))
//                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin)));
//                        _arr_Markers_Pickup.add(mMarker_drop);

//                        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(mPickUpLat, mPickUpLng)).zoom(14.0f).build();
//                        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                }

                 /* Get Ride Tracking Details */
                if (stBooking_Status.equalsIgnoreCase("2") || stBooking_Status.equalsIgnoreCase("3")) {
                    if (isInternetOn(getApplicationContext())) {
                        Call<JsonObject> call = apiService.tracking(preferencesUtility.getuser_id(), stDriverId, stBookingID, stBooking_Status, "1");

                        call.enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                Log.e("tracking_Response", response.body().toString());
                                Intent intent = new Intent("User_Ride_Tracking");
                                intent.putExtra("user_ride_track_data", response.body().toString());
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {

                            }
                        });
                    } else {
                        toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void CallToDriver(String ValeMobile) {
        try {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {

                Intent intent_call = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ValeMobile));
                startActivity(intent_call);
            }
        } catch (Exception ex) {
            toastDialog.ShowToastMessage(getResources().getString(R.string.call_error));
            ex.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        /* Ride Tracking */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_UserRideTracking,
                new IntentFilter("User_Ride_Tracking"));

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_UserWaitForDriver,
                new IntentFilter("User_Wait_For_Driver_Data"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_DriverAcceptUserRequest,
                new IntentFilter("User_Ride_Request"));

        /* This Call Receive After Driver start trip from @OhiApplication */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_RideStartActivity,
                new IntentFilter("User_Ride_Start"));

          /* This Call Receive After User Give a Review to Driver For Finish This Activity */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_ReviewFinishActivity,
                new IntentFilter("Review_Activity_finish"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /* Ride Tracking */
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_UserRideTracking);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_UserWaitForDriver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_DriverAcceptUserRequest);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_DetailsFinishActivity);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_ReviewFinishActivity);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_RideStartActivity);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Reload Ride Service List - After Start Ride
     */
    private BroadcastReceiver mMessageReceiver_RideStartActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            if (mMarker_pickup != null) {
                mMarker_pickup.remove();
            }

            for (Polyline line : polylines) {
                line.remove();
            }

            polylines.clear();

            if (cT != null) {
                cT.cancel();
            }
            finish();
            startActivity(getIntent());
//            _txt_driver_delay_time.setText("");
//            _txt_driver_delay_time.setVisibility(View.GONE);
//            BookingDetails();
        }
    };
    /**
     * Finish This Activity After Giving Review & Tips
     */
    private BroadcastReceiver mMessageReceiver_ReviewFinishActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("called", "yes");
            finish();
        }
    };

    /**
     * Receiver Call From Dialog Activity
     * <p>
     * Finish Detail Activity on Cancel Request
     */
    private BroadcastReceiver mMessageReceiver_DetailsFinishActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };

    /**
     * User Wait Timer for Driver
     */
    private BroadcastReceiver mMessageReceiver_UserWaitForDriver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

//            if (cT != null) {
//                cT.cancel();
//            }
//            _txt_driver_delay_time.setText("");
//            _txt_driver_delay_time.setVisibility(View.GONE);

            BookingDetails();

            String mResult_UserWaitDriver = intent.getStringExtra("user_wait_driver_data");
            try {
                JSONObject objResult = new JSONObject(mResult_UserWaitDriver);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                if (objResult.getString("status").equalsIgnoreCase("1")) {
                    JSONArray arrPayload = objResult.getJSONArray("payload");
                    if (arrPayload.length() > 0) {
                        JSONObject objData = arrPayload.getJSONObject(0);
                        preferencesUtility.setBookingIdOnRide(objData.getString("booking_id"));
                        preferencesUtility.setBookingStatusOnRide(objData.getString("booking_status"));
                        int wait_time = objData.getInt("waiting");

                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.SECOND, wait_time);
                        String formattedDate = df.format(calendar.getTime());
                        preferencesUtility.setDriverWaitTime(formattedDate);
                    }
                    // Start Time Tracker
//                    DriverWaitCountDown();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    /**
     * Request Accept Timer
     */
    private BroadcastReceiver mMessageReceiver_DriverAcceptUserRequest = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_UserWaitDriver = intent.getStringExtra("user_wait_driver_data");

            _txt_driver_delay_time.setVisibility(View.GONE);
            BookingDetails();

            try {
                JSONObject objResult = new JSONObject(mResult_UserWaitDriver);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                if (objResult.getString("status").equalsIgnoreCase("1")) {
                    JSONArray arrPayload = objResult.getJSONArray("payload");
                    if (arrPayload.length() > 0) {
                        JSONObject objData = arrPayload.getJSONObject(0);
                        preferencesUtility.setBookingIdOnRide(objData.getString("booking_id"));
                        preferencesUtility.setBookingStatusOnRide(objData.getString("booking_status"));
                        int wait_time = objData.getInt("waiting");

                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.SECOND, wait_time);
                        String formattedDate = df.format(calendar.getTime());
                        preferencesUtility.setDriverWaitTime(formattedDate);
                    }
                    // Start Time Tracker
//                    DriverWaitCountDown();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    /**
     * User Ride Booking Tracking Receiver
     */
    private BroadcastReceiver mMessageReceiver_UserRideTracking = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_RideTracking = intent.getStringExtra("user_ride_track_data");
            Log.e("receiver_user_tracking", "Got message: " + mResult_RideTracking);
            prevLoc = new Location(LocationManager.GPS_PROVIDER);
            newLoc = new Location(LocationManager.GPS_PROVIDER);

            try {
                JSONObject objTracking = new JSONObject(mResult_RideTracking);
                if (objTracking.getString("status").equalsIgnoreCase("1")) {

                } else if (objTracking.getString("status").equalsIgnoreCase("2")) {

                    if (mMarker_pickup != null) {
                        mMarker_pickup.remove();
                    }

                    if (mMarker_drop != null) {
                        mMarker_drop.remove();
                    }


                    JSONObject objPayload = objTracking.getJSONObject("payload");
                    stBookingStatusForMarker = objPayload.getString("booking_status");
                    if (objPayload.getString("booking_status").equalsIgnoreCase(Constant.STATUS_GOTO_USER) ||
                            objPayload.getString("booking_status").equalsIgnoreCase(Constant.STATUS_USER_WAIT)) {


                        if (getTotalKmPrice.size() > 0) {

                            if (getTotalKmPrice.containsKey("totalKM")) {

                                TotalKm = getTotalKmPrice.get("totalKM");
                            } else {
                                TotalKm = 0;
                            }

                            if (getTotalKmPrice.containsKey("totalMin")) {

                                TotalDuration = getTotalKmPrice.get("totalMin");
                            } else {
                                TotalDuration = 0;
                            }
                        }

                        Log.e("TotalKm", TotalKm + "--TotalDuration--" + TotalDuration);
                    }


                    if (objPayload.getString("booking_id").equalsIgnoreCase(stBookingID)) {

                        if (objPayload.getString("booking_status").equalsIgnoreCase(Constant.STATUS_START_TRIP)) {

                            _txt_ride_distance.setText(roundTwoDecimals(Double.parseDouble(objPayload.getString("total_distance"))) + " " + getResources().getString(R.string.st_km));
                            _txt_ride_duration.setText(objPayload.getString("duration") + " min");

                            if (_arrPriceList.size() > 0) {
                                double TotalAmnt = Double.parseDouble(_arrPriceList.get(0).get("base_charge").toString()) +
                                        (Double.parseDouble(_arrPriceList.get(0).get("base_charge").toString()) * Double.parseDouble(objPayload.getString("total_distance").toString())) +
                                        (Double.parseDouble(_arrPriceList.get(0).get("per_minute_charge").toString()) * Double.parseDouble(objPayload.getString("total_minutes").toString()));

                                if (Double.parseDouble(_arrPriceList.get(0).get("minimum_fair").toString()) > TotalAmnt) {
                                    TotalAmnt = Double.parseDouble(_arrPriceList.get(0).get("minimum_fair").toString());
                                }

                                TotalAmnt = TotalAmnt + Double.parseDouble(_arrPriceList.get(0).get("booking_charge").toString());

                                _txt_ride_price.setText(String.valueOf(Math.round(TotalAmnt)) + " ₹");

//                                if (isTaxiRide.equalsIgnoreCase("0")) {
//                                    _txt_ride_price.setText(String.valueOf(Math.round(TotalAmnt)) + " ₹");
//                                    _txt_ride_price.setVisibility(View.VISIBLE);
//                                } else {
//                                    _txt_ride_price.setVisibility(View.INVISIBLE);
//                                }

                                _txt_ride_price.setVisibility(View.VISIBLE);
                            }

                            for (Polyline line : polylines) {
                                line.remove();
                            }

                            polylines.clear();

                            LatLng pickup = new LatLng(mDriverLat, mDriverLng);
                            LatLng dest = new LatLng(mDropLat, mDropLng);

                            if (mDriverLat != 0.0 && mDriverLng != 0.0 && mPickUpLat != 0.0 && mPickUpLng != 0.0) {

                                String url = getUrl(pickup, dest);
                                Log.e("onMapClick", url.toString() + "--");
                                FetchUrl FetchUrl = new FetchUrl();

                                // Start downloading json data from Google Directions API
                                FetchUrl.execute(url);

                                mMarker_drop = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDropLat, mDropLng))
                                        .title(getResources().getString(R.string.st_drop_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin)));
                                _arr_Markers_Pickup.add(mMarker_drop);

                            }
                        } else if (objPayload.getString("booking_status").equalsIgnoreCase(Constant.STATUS_GOTO_USER)) {
                            for (Polyline line : polylines) {
                                line.remove();
                            }

                            polylines.clear();

                            LatLng pickup = new LatLng(mDriverLat, mDriverLng);
                            LatLng dest = new LatLng(mPickUpLat, mPickUpLng);

                            if (mDriverLat != 0.0 && mDriverLng != 0.0 && mPickUpLat != 0.0 && mPickUpLng != 0.0) {

                                String url = getUrl(pickup, dest);
                                Log.e("onMapClick", url.toString() + "--");
                                FetchUrl FetchUrl = new FetchUrl();

                                // Start downloading json data from Google Directions API
                                FetchUrl.execute(url);

                                String stTitle = "", stSnippet = "";
                                if (TotalDuration == 0) {
                                    stTitle = "wait...";
                                    stSnippet = "......";
                                } else {
                                    stTitle = "Arrival";
                                    stSnippet = Math.round(TotalDuration) + " mins";
                                }

                                mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mPickUpLat, mPickUpLng))
                                        .title(stTitle)
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin))
                                        .snippet(stSnippet));
                                mMarker_pickup.showInfoWindow();
                                _arr_Markers_Pickup.add(mMarker_pickup);
                            }

                        } else if (objPayload.getString("booking_status").equalsIgnoreCase(Constant.STATUS_USER_WAIT)) {
                            for (Polyline line : polylines) {
                                line.remove();
                            }

                            polylines.clear();

                            LatLng pickup = new LatLng(mDriverLat, mDriverLng);
                            LatLng dest = new LatLng(mPickUpLat, mPickUpLng);

                            if (mDriverLat != 0.0 && mDriverLng != 0.0 && mPickUpLat != 0.0 && mPickUpLng != 0.0) {

                                String url = getUrl(pickup, dest);
                                Log.e("onMapClick", url.toString() + "--");
                                FetchUrl FetchUrl = new FetchUrl();

                                // Start downloading json data from Google Directions API
                                FetchUrl.execute(url);

                                String stTitle = "", stSnippet = "";
                                if (TotalDuration == 0) {
                                    stTitle = "wait ...";
                                    stSnippet = "... ...";
                                } else {
                                    stTitle = "Arrival";
                                    stSnippet = Math.round(TotalDuration) + " mins";
                                }

                                mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mPickUpLat, mPickUpLng))
                                        .title(stTitle)
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin))
                                        .snippet(stSnippet));
                                mMarker_pickup.showInfoWindow();
                                _arr_Markers_Pickup.add(mMarker_pickup);
                            }
                        }


                        mDriverLat = Double.parseDouble(objPayload.getString("latitude"));
                        mDriverLng = Double.parseDouble(objPayload.getString("longitude"));
                        if (mMarker_driver == null) {

                            if (stServiceId.equals("1")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_1)));
                            } else if (stServiceId.equals("2") || stServiceId.equals("13") || stServiceId.equals("18")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_2)));
                            } else if (stServiceId.equals("3") || stServiceId.equals("19")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_3)));
                            } else if (stServiceId.equals("11")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_11)));
                            } else if (stServiceId.equals("12")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_12)));
                            } else if (stServiceId.equals("13")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_13)));
                            } else if (stServiceId.equals("14")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_14)));
                            } else if (stServiceId.equals("15")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_15)));
                            } else if (stServiceId.equals("16") || stServiceId.equals("17")) {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_16)));
                            } else {
                                mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(mDriverLat, mDriverLng))
                                        .title(getResources().getString(R.string.st_driver_location))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_taxi)));
                            }
//                            mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(mDriverLat, mDriverLng))
//                                    .title(getResources().getString(R.string.st_driver_location))
//                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_taxi)));
                        } else {
                            prevLoc.setLatitude(mMarker_driver.getPosition().latitude);
                            prevLoc.setLongitude(mMarker_driver.getPosition().longitude);

                            mMarker_driver.setPosition(new LatLng(mDriverLat, mDriverLng));

                            newLoc.setLatitude(mMarker_driver.getPosition().latitude);
                            newLoc.setLongitude(mMarker_driver.getPosition().longitude);

                            float bearing = prevLoc.bearingTo(newLoc);
                            mMarker_driver.setRotation(bearing);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Driver Wait Time CountDown
     */
    private void DriverWaitCountDown() {
        try {
            Log.e("TAG_FINISH", preferencesUtility.getDriverWaitTime() + "==");

            if (!preferencesUtility.getDriverWaitTime().equalsIgnoreCase("")) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date ServerDateTime = df.parse(preferencesUtility.getDriverWaitTime());

                Calendar c = Calendar.getInstance();
                String currentDate = df.format(c.getTime());
                Date currentDateTime = df.parse(currentDate);
                long totalTime = dateDifference(ServerDateTime, currentDateTime);

                if (totalTime > 0) {
                    _txt_driver_delay_time.setVisibility(View.VISIBLE);

                    cT = new CountDownTimer(totalTime, 1000) {
                        public void onTick(long millisUntilFinished) {

                            _txt_driver_delay_time.setText("" + String.format(FORMAT,
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                            TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),

                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                        }

                        public void onFinish() {
                            Log.e("TAG_FINISH", "finish");
                            _txt_driver_delay_time.setVisibility(View.GONE);
                        }
                    };
                    cT.start();

                } else {
                    Log.e("TAG_FINISH", "else");
                    _txt_driver_delay_time.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        String mode = "mode=driving";

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode + "&key=AIzaSyAfye39JZYcYdS196GZrvxRBfMShOCaA3w";

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());


                // Starts parsing data
                routes = parser.parse(jObject);
                getTotalKmPrice = parser.parseTotal(jObject);
                Log.e("TotalAmount", getTotalKmPrice.toString());
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
//            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;
            points.clear();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
//                points = new ArrayList<>();
                Log.e("points", points.size() + "-");
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.BLUE);

                Log.d("onPostExecute", "onPostExecute lineoptions decoded");


                if (getTotalKmPrice.size() > 0) {

                    if (getTotalKmPrice.containsKey("totalKM")) {

                        TotalKm = getTotalKmPrice.get("totalKM");
                    } else {
                        TotalKm = 0;
                    }

                    if (getTotalKmPrice.containsKey("totalMin")) {

                        TotalDuration = getTotalKmPrice.get("totalMin");
                    } else {
                        TotalDuration = 0;
                    }
                }

                String stTitle = "", stSnippet = "";
                if (TotalDuration == 0) {
                    stTitle = "wait...";
                    stSnippet = "......";
                } else {
                    stTitle = "Arrival";
                    stSnippet = Math.round(TotalDuration) + " mins";
                }

                if (stBookingStatusForMarker.equalsIgnoreCase(Constant.STATUS_GOTO_USER) || stBookingStatusForMarker.equalsIgnoreCase(Constant.STATUS_USER_WAIT)) {
                    if (mMarker_pickup != null) {
                        mMarker_pickup.remove();
                    }

                    mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(mPickUpLat, mPickUpLng))
                            .title(stTitle)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin))
                            .snippet(stSnippet));
                    mMarker_pickup.showInfoWindow();
                    _arr_Markers_Pickup.add(mMarker_pickup);
                }

            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
//                mGoogleMap.addPolyline(lineOptions);
                polylines.add(mGoogleMap.addPolyline(lineOptions));
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }
    }


}

