package com.ohicabs.user.ohi.Adpater;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.ohicabs.user.ohi.PojoModel.ExclusiveOfferModel;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.ui.subscription.SubscriptionDetailsActivity;

import java.util.ArrayList;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.API_IMAGE_HOST;

public class ExclusiveOfferPagerAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<ExclusiveOfferModel> arrExclusiveOffer;
    private LayoutInflater inflater;

    public ExclusiveOfferPagerAdapter(Context context, ArrayList<ExclusiveOfferModel> arrExclusiveOfferModel) {
        mContext = context;
        arrExclusiveOffer = arrExclusiveOfferModel;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public Object instantiateItem(ViewGroup collection, final int position) {
        View offerLayout = inflater.inflate(R.layout.cell_exclusive_offfer, collection, false);

        AppCompatImageView imgOffer = (AppCompatImageView) offerLayout.findViewById(R.id.img_offer);
        AppCompatTextView txtOfferTitle = offerLayout.findViewById(R.id.txt_offer_title);
        AppCompatTextView txtOfferDesc = offerLayout.findViewById(R.id.txt_offer_desc);
        RelativeLayout rlOfferLayout = offerLayout.findViewById(R.id.rl_offer_detail);

        Glide.with(mContext).load(API_IMAGE_HOST +  arrExclusiveOffer.get(position).getImage()).into(imgOffer);
        txtOfferDesc.setText(arrExclusiveOffer.get(position).getDetails());
        txtOfferTitle.setText(arrExclusiveOffer.get(position).getPlan_name());

        arrExclusiveOffer.get(position).getExclusiveOfferDetail().toString();

        if (!arrExclusiveOffer.get(position).getIs_offer().equalsIgnoreCase("1")) {

            rlOfferLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, SubscriptionDetailsActivity.class);
                    intent.putExtra("subscription_details", arrExclusiveOffer.get(position).getExclusiveOfferDetail());
                    intent.putExtra("subscription_history", "History");
                    mContext.startActivity(intent);
                }
            });
        }
        collection.addView(offerLayout);
        return offerLayout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return arrExclusiveOffer.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
