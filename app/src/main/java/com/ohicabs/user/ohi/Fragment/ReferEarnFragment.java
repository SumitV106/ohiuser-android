package com.ohicabs.user.ohi.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.Activity.BookingActivityStep2Activity;
import com.ohicabs.user.ohi.Activity.PaytmTermsActivity;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Rental.RentalBookingActivity;
import com.ohicabs.user.ohi.StorageManager.DatabaseHelper;
import com.ohicabs.user.ohi.Utils.NetworkStatus;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.SHARE_LINK;

public class ReferEarnFragment extends Fragment {

    @BindView(R.id.img_refer_banner) AppCompatImageView imgReferBanner;
    @BindView(R.id.txt_refer_title) AppCompatTextView txtReferTitle;
    @BindView(R.id.txt_refer_details) AppCompatTextView txtReferDetails;
    @BindView(R.id.btn_invite) AppCompatButton btnInvite;
    @BindView(R.id.txt_terms_condition) AppCompatTextView txtTermsCondition;
    @BindView(R.id.progressBar)ProgressBar progressBar;
    @BindView(R.id.ll_refer_details) LinearLayout llReferDetails;
    @BindView(R.id.txt_refer_code) AppCompatTextView txtReferCode;

    private ToastDialog toastDialog;
    private SharedPreferencesUtility sharedPreferencesUtility;
    ProgressDialog progressDialog;
    ApiInterface apiService;
    DatabaseHelper mHelper;

    private String stTermCondition;
    private String referralPoint = "";

    public ReferEarnFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_refer_earn, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        apiService = ApiClient.getClient().create(ApiInterface.class);
        toastDialog = new ToastDialog(getActivity());
        sharedPreferencesUtility = new SharedPreferencesUtility(getActivity());
        mHelper = new DatabaseHelper(getActivity());

        txtReferCode.setText(sharedPreferencesUtility.getmobile());

        init();
    }

    private void init() {

        btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String message = "Ohi Cabs Refer & Earn \n";

                if (referralPoint.equalsIgnoreCase("")) {
                    message = message + " Followed by app link : https://play.google.com/store/apps/details?id=com.ohicabs.user.ohi" + "  ";

                } else {
                    message = message + "Install OHI Cabs app via my referral code " + sharedPreferencesUtility.getmobile() + " ";
                    message = message + " and get " + referralPoint + " ";
                    message = message + " Credits on sign up. Credits can be used to avail free rides. T&C apply." + " ";
                    message = message + " Followed by app link : https://play.google.com/store/apps/details?id=com.ohicabs.user.ohi" + "  ";
                }
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, message);

                startActivity(Intent.createChooser(share, "Refer & Earn"));
            }
        });

        txtTermsCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PaytmTermsActivity.class);
                intent.putExtra("refer_condition", stTermCondition);
                startActivity(intent);
            }
        });

        if (NetworkStatus.getConnectivityStatus(getActivity()))
            getReferPlanDetails();
    }

    /**
     *
     * get referral plan details
     *
     */
    private void getReferPlanDetails() {
        try {
            progressBar.setVisibility(View.VISIBLE);

            Call<JsonObject> call = apiService.referral_plan_list(sharedPreferencesUtility.getuser_id(), sharedPreferencesUtility.getauth_token());

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        progressBar.setVisibility(View.GONE);

                        String mResult = response.body().toString();

                        JSONObject jsonObjectDetails = new JSONObject(mResult);
                        if (jsonObjectDetails.optString("status").equalsIgnoreCase("1") &&
                                jsonObjectDetails.optString("success").equalsIgnoreCase("true")) {
                            llReferDetails.setVisibility(View.VISIBLE);

                            JSONObject jsonObjectPayload = jsonObjectDetails.getJSONObject("payload");

                            txtReferTitle.setText(jsonObjectPayload.optString("plan_name"));
                            txtReferDetails.setText(jsonObjectPayload.optString("plan_description"));

                            stTermCondition = jsonObjectPayload.optString("term_and_condition");
                            referralPoint = jsonObjectPayload.optString("use_referral_point");

                            Glide.with(getActivity()).load(Global_ServiceApi.API_IMAGE_HOST + jsonObjectPayload.optString("image"))
                                    .into(imgReferBanner);

                        } else {
                            llReferDetails.setVisibility(View.GONE);
                            toastDialog.ShowToastMessage(jsonObjectDetails.optString("message"));
                        }
                    } catch (Exception e) {
                        Log.e("Exception", e.toString());
                        progressBar.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.e("onFailure", t.getMessage().toString());
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
