package com.ohicabs.user.ohi.Fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.PopupMenu;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.Activity.ChangePasswordActivity;
import com.ohicabs.user.ohi.Activity.LoginActivity;
import com.ohicabs.user.ohi.Activity.SuggestionActivity;
import com.ohicabs.user.ohi.Activity.UserSupportActivity;
import com.ohicabs.user.ohi.Adpater.ZoneSpinnerAdapter;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.DBModel.ZoneList_Model;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.StorageManager.DatabaseHelper;
import com.ohicabs.user.ohi.Utils.CircularImageView;
import com.ohicabs.user.ohi.Utils.Constant;
import com.ohicabs.user.ohi.Utils.CustomTypefaceSpan;
import com.ohicabs.user.ohi.Utils.Global_Typeface;
import com.ohicabs.user.ohi.Utils.MultipartVolleySingleton;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;
import com.ohicabs.user.ohi.Utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.socket.client.Socket;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {


    public ProfileFragment() {
        // Required empty public constructor
    }

    private Global_Typeface global_typeface;
    private SharedPreferencesUtility preferencesUtility;
    private Socket mSocket;
    Bitmap Gallery_imageBitmap;
    private ToastDialog toastDialog;
    private String selected_gender;

    @BindView(R.id.img_editprofile) CircularImageView _img_edit_profile;
    @BindView(R.id.img_upload) ImageView _img_upload;
    @BindView(R.id.btn_editprofile) Button _btn_editprofile;
    @BindView(R.id.rgroup_gender) RadioGroup _rgroup_gender;
    @BindView(R.id.edt_firstname) EditText _edt_firstname;
    @BindView(R.id.edt_lastname) EditText _edt_lastname;
    @BindView(R.id.edt_email) EditText _edt_email;
    @BindView(R.id.edt_mobile_no) EditText _edt_mobile_no;
    @BindView(R.id.rbtn_male) RadioButton _rbtn_male;
    @BindView(R.id.rbtn_female) RadioButton _rbtn_female;
    @BindView(R.id.firstnameWrapper) TextInputLayout firstnameWrapper;
    @BindView(R.id.lastnameWrapper) TextInputLayout lastnameWrapper;
    @BindView(R.id.mobileWrapper) TextInputLayout mobileWrapper;
    @BindView(R.id.emailWrapper) TextInputLayout emailWrapper;
    @BindView(R.id.txt_resend_email) AppCompatTextView _txt_resend_email;
    @BindView(R.id.spCity) Spinner spCity;

    DatabaseHelper mHelper;
    private ImageView img_more_menu;
    public ImageLoader mImageLoader;
    private ProgressDialog progressDialog;
    ApiInterface apiService;

    String Zone_Id;
    ArrayList<HashMap> hashMaps_zoneList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferencesUtility = new SharedPreferencesUtility(getContext());
        global_typeface = new Global_Typeface(getActivity());
        toastDialog = new ToastDialog(getActivity());
        apiService = ApiClient.getClient().create(ApiInterface.class);

        mHelper = new DatabaseHelper(getContext());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.st_please_wait));
        progressDialog.setCancelable(false);

        mImageLoader = ImageLoader.getInstance();
        mImageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));

        _btn_editprofile.setTypeface(global_typeface.Sansation_Bold());
        _edt_firstname.setTypeface(global_typeface.Sansation_Regular());
        _edt_lastname.setTypeface(global_typeface.Sansation_Regular());
        _edt_email.setTypeface(global_typeface.Sansation_Regular());
        _edt_mobile_no.setTypeface(global_typeface.Sansation_Regular());
        _rbtn_male.setTypeface(global_typeface.Sansation_Regular());
        _rbtn_female.setTypeface(global_typeface.Sansation_Regular());
        // _txt_gender.setTypeface(global_typeface.Sansation_Regular());

        firstnameWrapper.setTypeface(global_typeface.Sansation_Regular());
        lastnameWrapper.setTypeface(global_typeface.Sansation_Regular());
        mobileWrapper.setTypeface(global_typeface.Sansation_Regular());
        emailWrapper.setTypeface(global_typeface.Sansation_Regular());

        _edt_firstname.setText(preferencesUtility.getfirstname());
        _edt_lastname.setText(preferencesUtility.getlastname());
        _edt_email.setText(preferencesUtility.getemail());
        _edt_mobile_no.setText(preferencesUtility.getmobile());

        img_more_menu = (ImageView) getActivity().findViewById(R.id.img_more_menu);

        img_more_menu.setVisibility(View.VISIBLE);
        img_more_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenu(v);
            }
        });

        if (preferencesUtility.getgender().equalsIgnoreCase("M")) {
            _rbtn_male.setChecked(true);
            selected_gender = "M";
        } else {
            _rbtn_female.setChecked(true);
            selected_gender = "F";
        }

        _rgroup_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbtn_male:
                        selected_gender = "M";
                        break;
                    case R.id.rbtn_female:
                        selected_gender = "F";
                        break;
                }
            }
        });

        _img_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(getResources().getString(R.string.st_select_image))
                        .setPositiveButton(getResources().getString(R.string.st_take_picture), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                                    startActivityForResult(takePictureIntent, Constant.REQUEST_IMAGE_CAPTURE);
                                }
                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.st_choose_image), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(galleryIntent, Constant.RESULT_LOAD_IMG);
                            }
                        });
                builder.create();
                builder.show();
            }
        });

        _btn_editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String message = IsValidate();
                    if (message.equalsIgnoreCase("true")) {
                        String UserId = preferencesUtility.getuser_id().toString();

                        if (isInternetOn(getContext())) {
                            Call<JsonObject> call = apiService.profile_update(UserId, selected_gender,
                                    _edt_firstname.getText().toString(), "", "1", Zone_Id, _edt_mobile_no.getText().toString());
                            call.enqueue(new Callback<JsonObject>() {
                                @Override
                                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                    UpdateUserDetails(response.body().toString());
                                }

                                @Override
                                public void onFailure(Call<JsonObject> call, Throwable t) {

                                }
                            });
                        } else {
                            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                        }
                    } else {
                        toastDialog.ShowToastMessage(message);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        if (preferencesUtility.getstatus().equalsIgnoreCase("-1")) {
            _txt_resend_email.setVisibility(View.VISIBLE);
        } else {
            _txt_resend_email.setVisibility(View.GONE);
        }

        _txt_resend_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject objResendEmail = new JSONObject();
                    objResendEmail.put("user_id", preferencesUtility.getuser_id().toString());
                    if (isInternetOn(getContext())) {
                        Call<JsonObject> call = apiService.resend_email_signup(preferencesUtility.getuser_id().toString());
                        call.enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                try {
                                    JSONObject obj = new JSONObject(response.body().toString());
                                    if (obj.getString("status").equalsIgnoreCase("1")) {
                                        toastDialog.ShowToastMessage(obj.getString("message"));
                                    } else {
                                        toastDialog.ShowToastMessage(obj.getString("message"));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {

                            }
                        });
                    } else {
                        toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        ArrayList<ZoneList_Model> al_countryList = new ArrayList<>();

        hashMaps_zoneList = mHelper.GetZoneListFromDBForProfile();
        ZoneSpinnerAdapter teamSpinnerAdapter = new ZoneSpinnerAdapter(getActivity(),
                R.layout.custom_spinner_items, hashMaps_zoneList);

        spCity.setAdapter(teamSpinnerAdapter);

        for (int j = 0; j < hashMaps_zoneList.size(); j++) {
            String mZoneId = hashMaps_zoneList.get(j).get("zone_id").toString();
            if (mZoneId.equals(preferencesUtility.getZoneId())) {
                spCity.setSelection(j);
            }
        }

        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Zone_Id = hashMaps_zoneList.get(i).get("zone_id").toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void showPopupMenu(View view) {
        PopupMenu popup = new PopupMenu(getActivity(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.main, popup.getMenu());
        Menu m = popup.getMenu();
        for (int i = 0; i < m.size(); i++) {
            applyFontToMenuItem(m.getItem(i));
        }
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
        popup.show();
    }

    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {

        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_change_password:
                    Intent i_change_pass = new Intent(getActivity(), ChangePasswordActivity.class);
                    getActivity().startActivity(i_change_pass);
                    return true;

                case R.id.action_user_support:
                    Intent i_user_support = new Intent(getActivity(), UserSupportActivity.class);
                    getActivity().startActivity(i_user_support);
                    return true;

                case R.id.action_suggestions:
                    Intent i_suggestion = new Intent(getActivity(), SuggestionActivity.class);
                    getActivity().startActivity(i_suggestion);
                    return true;

                case R.id.action_logout:
                    LogoutUser();
                    return true;
                default:
            }
            return false;
        }
    }


    //load image from gallery
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.RESULT_LOAD_IMG && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {

                Glide.with(getActivity())
                        .load(uri)
                        .asBitmap()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                _img_edit_profile.setImageBitmap(resource);
                                Gallery_imageBitmap = resource;

                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                resource.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                                byte[] byteArray = byteArrayOutputStream.toByteArray();
                                String encodedString = Base64.encodeToString(byteArray, Base64.DEFAULT);
                                saveImage(byteArrayOutputStream.toByteArray());

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == Constant.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            try {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                String encodedString = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);

                Glide.with(this)
                        .load(stream.toByteArray())
                        .asBitmap()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(_img_edit_profile);
                saveImage(stream.toByteArray());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Update User Details response
     *
     * @param result
     */
    private void UpdateUserDetails(String result) {

        try {
            final JSONObject objUserDetails = new JSONObject(result);
            if (objUserDetails.getString("status").equalsIgnoreCase("1")) {
                toastDialog.ShowToastMessage(getResources().getString(R.string.st_profile_updated));

                JSONObject jarrayLogin = objUserDetails.getJSONObject("payload");
                preferencesUtility.setuser_id(jarrayLogin.getString("user_id"));
                preferencesUtility.setfirstname(jarrayLogin.getString("firstname"));
                preferencesUtility.setlastname(jarrayLogin.getString("lastname"));
                preferencesUtility.setemail(jarrayLogin.getString("email"));
                preferencesUtility.setgender(jarrayLogin.getString("gender"));
                preferencesUtility.setmobile(jarrayLogin.getString("mobile"));
                preferencesUtility.setauth_token(jarrayLogin.getString("auth_token"));
                preferencesUtility.setimage(jarrayLogin.getString("image"));
                preferencesUtility.setrefer_code(jarrayLogin.getString("refer_code"));
                preferencesUtility.setstatus(jarrayLogin.getString("status"));
                preferencesUtility.setZoneId(jarrayLogin.getString("zone_id"));

            } else {
                toastDialog.ShowToastMessage(getResources().getString(R.string.st_try_again));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * User Logout from app
     */
    private void LogoutUser() {
        if (isInternetOn(getContext())) {
            try {

                Call<JsonObject> call = apiService.logout(preferencesUtility.getuser_id(), preferencesUtility.getauth_token());
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try {
                            JSONObject objBookingData = new JSONObject(response.body().toString());
                            if (objBookingData.getString("success").equalsIgnoreCase("true") && objBookingData.getString("status").equalsIgnoreCase("1")) {
                                preferencesUtility.ClearPrefrenceKey("login_type");
                                preferencesUtility.ClearPreferences();

                                Intent i = new Intent(getActivity(), LoginActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                getActivity().finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }

    }

    /**
     * Sign Up Form Validation
     *
     * @return
     */
    private String IsValidate() {
        if (_edt_firstname.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_name);
        } else if (_edt_lastname.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_last_name);
        } else if (_rgroup_gender.getCheckedRadioButtonId() == -1) {
            return getResources().getString(R.string.st_no_gender);
        } else if (_edt_mobile_no.getText().toString().trim().equalsIgnoreCase("")) {
            return getResources().getString(R.string.st_no_mobile);
        } else {
            return "true";
        }
    }


    public void saveImage(final byte[] byteArray) {
        progressDialog.show();
        try {

            String url = Global_ServiceApi.API_HOST + "api/image_upload";

            VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse response) {
                    String resultResponse = new String(response.data);
                    progressDialog.dismiss();
                    UpdateUserDetails(resultResponse);
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    NetworkResponse networkResponse = error.networkResponse;
                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";
                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";
                        }
                    }
                    Log.i("Error", errorMessage);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", preferencesUtility.getuser_id());
                    params.put("user_type", "1");

                    return params;
                }

                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    // file name could found file base or direct access from real path
                    // for now just get bitmap data from ImageView
                    params.put("image0", new DataPart("file_avatar.jpg", byteArray, "image/jpeg"));
                    return params;
                }
            };
            MultipartVolleySingleton.getInstance(getContext()).addToRequestQueue(multipartRequest);

        } catch (Exception e) {
            Log.e("ERORR", e.toString());
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = global_typeface.Sansation_Regular();
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }
}
