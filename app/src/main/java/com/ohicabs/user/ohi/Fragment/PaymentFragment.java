package com.ohicabs.user.ohi.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontEditText;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentFragment extends Fragment {


    public PaymentFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.edt_promo_code)
    CustomRegularFontEditText _edt_promo_code;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_payment, container, false);
        ButterKnife.bind(this,view);

        return view;
    }

}
