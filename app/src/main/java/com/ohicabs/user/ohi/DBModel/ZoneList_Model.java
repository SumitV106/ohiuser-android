package com.ohicabs.user.ohi.DBModel;

/**
 * Created by sumit on 11/04/17.
 */

public class ZoneList_Model {

    String created_date;
    String city;
    String status;
    String zone_name;
    String zone_id;
    String tax;
    String modify_date;
    String google_zone_name;
    String gst_tax;
    String paytm_term_html;

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getZone_name() {
        return zone_name;
    }

    public void setZone_name(String zone_name) {
        this.zone_name = zone_name;
    }

    public String getZone_id() {
        return zone_id;
    }

    public void setZone_id(String zone_id) {
        this.zone_id = zone_id;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getModify_date() {
        return modify_date;
    }

    public void setModify_date(String modify_date) {
        this.modify_date = modify_date;
    }

    public String getGoogle_zone_name() {
        return google_zone_name;
    }

    public void setGoogle_zone_name(String google_zone_name) {
        this.google_zone_name = google_zone_name;
    }

    public String getGst_tax() {
        return gst_tax;
    }

    public void setGst_tax(String gst_tax) {
        this.gst_tax = gst_tax;
    }

    public String getPaytm_term_html() {
        return paytm_term_html;
    }

    public void setPaytm_term_html(String paytm_term_html) {
        this.paytm_term_html = paytm_term_html;
    }

    //to display object as a string in spinner
    @Override
    public String toString() {
        return zone_name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ZoneList_Model) {
            ZoneList_Model c = (ZoneList_Model) obj;
            if (c.getZone_name().equals(zone_name) && c.getZone_id() == zone_id) return true;
        }

        return false;
    }

}
