package com.ohicabs.user.ohi.Adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.Global_Typeface;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by sumit on 19/04/17.
 */

public class RentalPriceAdapter extends BaseAdapter {

    private Context activity;
    int ResourceId;
    ArrayList<HashMap> _arr_CarModels;
    private Global_Typeface global_typeface;
    private BtnClickListener mClickListener = null;

    private int selected_position = -1;

    public RentalPriceAdapter(Context act, int resId, ArrayList<HashMap> _arrCars, BtnClickListener listener) {
        this.activity = act;
        this.ResourceId = resId;
        this._arr_CarModels = _arrCars;
        mClickListener = listener;
        global_typeface = new Global_Typeface(activity);
    }

    @Override
    public int getCount() {
        return _arr_CarModels.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    View mLastView;

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View itemView = convertView;
        final ViewHolder viewHolder;
        if (itemView == null) {

            viewHolder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(ResourceId, parent, false);
            viewHolder.txt_time = (TextView) itemView.findViewById(R.id.txt_time);
            viewHolder.txt_km = (TextView) itemView.findViewById(R.id.txt_km);
            viewHolder.txt_price = (TextView) itemView.findViewById(R.id.txt_price);

            viewHolder.radioButton = (RadioButton) itemView.findViewById(R.id.radioButton);

            itemView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) itemView.getTag();
        }
        
        int rental_time = Integer.parseInt(_arr_CarModels.get(position).get("rental_time").toString());
        viewHolder.txt_time.setText(rideTimeDifference(rental_time));

        viewHolder.txt_km.setText(_arr_CarModels.get(position).get("minimum_km").toString() + " KM");
        viewHolder.txt_price.setText(activity.getResources().getString(R.string.st_rupee) + " " + _arr_CarModels.get(position).get("minimum_fair").toString());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mClickListener!=null){
                    mClickListener.onBtnClick(position, String.valueOf(_arr_CarModels.get(position).get("service_id")),
                          "",viewHolder.radioButton);
                }
            }
        });

        viewHolder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mClickListener!=null){
                    mClickListener.onBtnClick(position, String.valueOf(_arr_CarModels.get(position).get("service_id")),
                            "",viewHolder.radioButton);
                }
            }
        });


        return itemView;
    }

    public class ViewHolder {
        TextView txt_time, txt_km, txt_price;
        RadioButton  radioButton;
    }

    private String rideTimeDifference(int min) {

        //1 minute = 60 seconds
        //1 hour = 60 x 60 = 3600
        //1 day = 3600 x 24 = 86400

        try {
            long differentTime = min * 60 * 1000;
            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = differentTime / daysInMilli;
            differentTime = differentTime % daysInMilli;

            long elapsedHours = differentTime / hoursInMilli;
            differentTime = differentTime % hoursInMilli;

            long elapsedMinutes = differentTime / minutesInMilli;
            differentTime = differentTime % minutesInMilli;

            long elapsedSeconds = differentTime / secondsInMilli;

            System.out.printf(
                    "%d days, %d hours, %d minutes, %d seconds%n",
                    elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

            String formattedTime = (String.format("%02dhr:%02dm", elapsedHours, elapsedMinutes));

            return formattedTime;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public interface BtnClickListener {
        void onBtnClick(int position, String service_id, String IsTaxi,View view);
    }

}
