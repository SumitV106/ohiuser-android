package com.ohicabs.user.ohi.Fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ohicabs.user.ohi.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import im.delight.android.webview.AdvancedWebView;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllPolicyFragment extends Fragment implements AdvancedWebView.Listener {

    @BindView(R.id.webview)
    AdvancedWebView _webview_terms_condition;
    String url_type = "", url = "";

    ProgressDialog progressDialog;

    public AllPolicyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_policy, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.st_please_wait));
        progressDialog.setCancelable(false);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String url_type = bundle.getString("link");

            if (url_type.equals("terms")) {
                url = "https://ohicabs.com/app_terms_and_conditions.html";

            } else if (url_type.equals("privacy")) {
                url = "https://ohicabs.com/Privacy_Policy.html";

            } else if (url_type.equals("cancellation")) {
                url = "https://ohicabs.com/cancellation_of_rides_policy.html";

            } else if (url_type.equals("refund")) {
                url = "https://ohicabs.com/Refund_Policy.html";
            }
        }

        _webview_terms_condition.setListener(getActivity(), this);
        _webview_terms_condition.setGeolocationEnabled(false);
        _webview_terms_condition.setMixedContentAllowed(true);
        _webview_terms_condition.setCookiesEnabled(true);
        _webview_terms_condition.setThirdPartyCookiesEnabled(true);

        _webview_terms_condition.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {

            }
        });

        _webview_terms_condition.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
            }
        });

        _webview_terms_condition.loadUrl(url);

    }


    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();
        _webview_terms_condition.onResume();
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        _webview_terms_condition.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        _webview_terms_condition.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        _webview_terms_condition.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        progressDialog.show();
    }

    @Override
    public void onPageFinished(String url) {
        progressDialog.dismiss();
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
    }

    @Override
    public void onExternalPageRequest(String url) {
    }
}
