package com.ohicabs.user.ohi.ui.subscription;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.Activity.BaseActivity;
import com.ohicabs.user.ohi.Adpater.SubscriptionListAdapter;
import com.ohicabs.user.ohi.PojoModel.SubscriptionModel;
import com.ohicabs.user.ohi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class SubscriptionActivity extends BaseActivity {

    @BindView(R.id.viewPager) ViewPager viewPager;
    @BindView(R.id.img_close) AppCompatImageView imgClose;

    private ArrayList<SubscriptionModel> arrSubscriptions = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        ButterKnife.bind(this);

        setViews();
    }

    private void setViews() {

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getSubscriptionList();
    }

    private void getSubscriptionList() {

        if (isInternetOn(getApplicationContext())) {
            try {
                progressDialog.show();
                Call<JsonObject> call = apiService.subscriptionPlanList(preferencesUtility.getuser_id(), "1");

                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try {
                            progressDialog.dismiss();

                            JSONObject objBookingList = new JSONObject(response.body().toString());
                            if (objBookingList.optString("status").equalsIgnoreCase("1")) {

                                JSONArray jsonArrayPayload = objBookingList.getJSONArray("payload");
                                if (jsonArrayPayload.length() > 0) {

                                    for (int i = 0; i < jsonArrayPayload.length(); i++) {

                                        JSONObject jsonObjectData = jsonArrayPayload.getJSONObject(i);
                                        SubscriptionModel subscriptionModel = new SubscriptionModel();

                                        subscriptionModel.setPlan_id(jsonObjectData.getString("plan_id"));
                                        subscriptionModel.setPlan_name(jsonObjectData.getString("plan_name"));
                                        subscriptionModel.setDetails(jsonObjectData.getString("details"));
                                        subscriptionModel.setPlan_days(jsonObjectData.getString("days"));
                                        subscriptionModel.setAmount(jsonObjectData.getString("amount"));
                                        subscriptionModel.setIs_offer(jsonObjectData.getString("is_offer"));
                                        subscriptionModel.setMax_ride(jsonObjectData.getString("max_ride"));
                                        subscriptionModel.setMax_discount(jsonObjectData.getString("max_discount"));
                                        subscriptionModel.setDiscount_perm(jsonObjectData.getString("discount_per"));
                                        subscriptionModel.setPlan_image(jsonObjectData.getString("image"));
                                        subscriptionModel.setTerm_and_condition(jsonObjectData.getString("term_and_condition"));
                                        subscriptionModel.setStart_date(jsonObjectData.getString("start_date"));
                                        subscriptionModel.setEnd_date(jsonObjectData.getString("end_date"));
                                        subscriptionModel.setIs_buy(jsonObjectData.getString("is_buy"));
                                        subscriptionModel.setService_name(jsonObjectData.getString("service_name"));

                                        subscriptionModel.setSubscriptionDetails(jsonObjectData.toString());

                                        arrSubscriptions.add(subscriptionModel);
                                    }

                                    SubscriptionListAdapter subscriptionListAdapter = new SubscriptionListAdapter(SubscriptionActivity.this, 6.0f,
                                            arrSubscriptions, new SubscriptionListAdapter.BtnClickListener() {
                                        @Override
                                        public void onBtnSubscribeClick(int position, String subscription_details, String isOffer, String isBuy) {
                                            Intent intent = new Intent(SubscriptionActivity.this, SubscriptionDetailsActivity.class);
                                            intent.putExtra("subscription_details", subscription_details);
                                            startActivity(intent);
                                        }

                                    }, new SubscriptionListAdapter.BtnOKClickListener() {
                                        @Override
                                        public void onBtnOKClick(String status) {
                                            finish();
                                        }
                                    });
                                    viewPager.setAdapter(subscriptionListAdapter);
                                } else {
                                    toastDialog.ShowToastMessage("Currently no any subscription plan list!");
                                    finish();
                                }
                            } else {
                                toastDialog.ShowToastMessage("Currently no any subscription plan list!");
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        progressDialog.dismiss();
                    }
                });
            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
            }
        }
    }

}
