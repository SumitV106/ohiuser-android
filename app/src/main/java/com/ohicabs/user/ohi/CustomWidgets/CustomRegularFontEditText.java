package com.ohicabs.user.ohi.CustomWidgets;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

public class CustomRegularFontEditText extends AppCompatEditText {
    public CustomRegularFontEditText(Context context) {
        super(context);
        init();
    }

    public CustomRegularFontEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomRegularFontEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        if (!isInEditMode()) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(),"fonts/Sansation_Regular.ttf"));
        }
    }
}
