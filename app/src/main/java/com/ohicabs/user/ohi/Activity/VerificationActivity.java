package com.ohicabs.user.ohi.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.PinEntry.PinEntryView;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

/**
 * Created by sumit on 10/04/17.
 */

public class VerificationActivity extends BaseActivity {

    Bundle b_data;

    @BindView(R.id.btn_verify) Button _btnVerify;
    @BindView(R.id.btn_resend) Button btn_resend;
    @BindView(R.id.edt_pin_entry) PinEntryView _edtPinentry;
    @BindView(R.id.img_back) ImageView _img_back;
    @BindView(R.id.txt_varification) TextView _txt_varification;
    @BindView(R.id.txt_resend_code) AppCompatTextView txtResendCode;
    @BindView(R.id.txt_resend_timer) AppCompatTextView txtResendTimer;

    private String mVerifyPhoneNumber, mEmailID, user_id;
    private boolean mShouldFallback = true;
    CountDownTimer mCountDownTimer;

    private static final String TAG = "VerificationActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_verification);
        ButterKnife.bind(this);

        setContent();
    }

    private void setContent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorBar
            ));
        }
        _btnVerify.setTypeface(global_typeface.Sansation_Bold());

        b_data = getIntent().getExtras();
        mVerifyPhoneNumber = b_data.getString("verify_mobile_no");
        mEmailID = b_data.getString("email");
        user_id = b_data.getString("user_id");

        _txt_varification.setTypeface(global_typeface.Sansation_Bold());

        _btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = _edtPinentry.getText().toString();
                if (!code.isEmpty()) {
                    if (isInternetOn(getApplicationContext())) {
                        progressDialog.show();
                        Call<JsonObject> call = apiService.otp_verify(user_id, code, "1");
                        call.enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                Log.e("response", response.body().toString());
                                progressDialog.dismiss();
                                SetVerificationData(response.body().toString());
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                progressDialog.dismiss();
                                Log.e("onFailure", t.toString());

                            }
                        });
                    } else {
                        toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                    }
                } else {
                    toastDialog.ShowToastMessage(getResources().getString(R.string.verification_validate));
                }
            }
        });

        btn_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                createVerification();
            }
        });

        _img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountDownTimer.cancel();
                finish();
            }
        });

        txtResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendOTPApiCall();
                reSendOTP();
            }
        });
    }

    private void reSendOTP() {
        txtResendCode.setEnabled(false);
        txtResendTimer.setVisibility(View.VISIBLE);

        mCountDownTimer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                String formattedTime = String.format("(%02d:%02d)",  00, millisUntilFinished / 1000);
                txtResendTimer.setText(formattedTime);
            }

            public void onFinish() {
                txtResendCode.setEnabled(true);
                txtResendTimer.setVisibility(View.GONE);
            }
        }.start();
    }

    private void resendOTPApiCall() {

        if (isInternetOn(getApplicationContext())) {
            progressDialog.show();
            Call<JsonObject> call = apiService.otp_resend(user_id,"1");
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }


    private void SetVerificationData(String ActivationResult) {
        try {
            JSONObject objData = new JSONObject(ActivationResult);
            if (objData.getString("status").equalsIgnoreCase("1")) {
                JSONObject objPayload = objData.getJSONObject("payload");
                preferencesUtility.setuser_id(objPayload.getString("user_id"));
                preferencesUtility.setfirstname(objPayload.getString("firstname"));
                preferencesUtility.setlastname(objPayload.getString("lastname"));
                preferencesUtility.setemail(objPayload.getString("email"));
                preferencesUtility.setgender(objPayload.getString("gender"));
                preferencesUtility.setmobile(objPayload.getString("mobile"));
                preferencesUtility.setauth_token(objPayload.getString("auth_token"));
                preferencesUtility.setimage(objPayload.getString("image"));
                preferencesUtility.setrefer_code(objPayload.getString("refer_code"));
                preferencesUtility.setstatus(objPayload.getString("status"));
                preferencesUtility.setZoneId(objPayload.optString("zone_id"));

                preferencesUtility.setLogedin(true);
                preferencesUtility.setLoginType("true");

                Intent intent = new Intent(VerificationActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                toastDialog.ShowToastMessage(objData.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mCountDownTimer != null)
            mCountDownTimer.cancel();
    }

    /**
     * Validate  Form
     *
     * @return true or false
     */
    private String IsValidate() {

        String code = _edtPinentry.getText().toString();
        if (code.trim().equals("")) {
            return "Enter Otp number";
        } else if (code.trim().length() != 4) {
            return "Enter valid Otp number";

        } else {
            return "true";
        }
    }
}
