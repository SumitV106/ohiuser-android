package com.ohicabs.user.ohi.Fragment;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Activity.MonthlySpendingTermsActivity;
import com.ohicabs.user.ohi.Utils.Global_Typeface;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;
import com.squareup.timessquare.CalendarPickerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;
import static com.squareup.timessquare.CalendarPickerView.SelectionMode.RANGE;

/**
 * A simple {@link Fragment} subclass.
 */
public class MonthlySpendFragment extends Fragment {

    private ToastDialog toastDialog;
    private Global_Typeface global_typeface;
    private SharedPreferencesUtility sharedPreferencesUtility;
    private ProgressDialog progressDialog;

    ApiInterface apiService;

    @BindView(R.id.txt_trip_count)
    TextView _txt_trip_count;
    @BindView(R.id.txt_cash_ride)
    TextView _txt_cash_ride;
    @BindView(R.id.txt_paytm_ride)
    TextView _txt_paytm_ride;
    @BindView(R.id.txt_total)
    TextView _txt_total;

    @BindView(R.id.txt_terms)
    TextView _txt_terms;

    @BindView(R.id.txt_date)
    TextView _txt_date;

    ImageView img_cal;

    String cal_startdate = "", cal_enddate = "";

    public MonthlySpendFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_monthly_spend, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toastDialog = new ToastDialog(getActivity());
        global_typeface = new Global_Typeface(getActivity());
        sharedPreferencesUtility = new SharedPreferencesUtility(getActivity());
        apiService = ApiClient.getClient().create(ApiInterface.class);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setView();

    }

    private void setView() {

        GetMonthlySpend();

        img_cal = (ImageView) getActivity().findViewById(R.id.img_cal);
        img_cal.setVisibility(View.VISIBLE);

        img_cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog d = new Dialog(getActivity());
                d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                d.setContentView(R.layout.layout_calenderdialog);
                d.setCancelable(false);
                Calendar lastYear = Calendar.getInstance();
                lastYear.add(Calendar.YEAR, -1);

                final CalendarPickerView calendar = (CalendarPickerView) d.findViewById(R.id.calendar_view);
                final Button btn_done = (Button) d.findViewById(R.id.btn_done);

                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// HH:mm:ss");
                c.add(Calendar.DATE, 1);  // number of days to add
                String end_date = df.format(c.getTime());
                try {
                    Date todays = df.parse(end_date);
//                    Date today = new Date();
                    calendar.init(lastYear.getTime(), todays)
                            .inMode(RANGE);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                btn_done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        List<Date> dateList = calendar.getSelectedDates();
                        if (dateList.size() == 0) {
                            toastDialog.ShowToastMessage("Please select date");
                        } else {
                            d.dismiss();
                            cal_startdate = new SimpleDateFormat("yyyy-MM-dd").format(new Date(calendar.getSelectedDates().get(0).getTime()));
                            cal_enddate = new SimpleDateFormat("yyyy-MM-dd").format(new Date(calendar.getSelectedDates().get(dateList.size() - 1).getTime()));
//                            Intent i = new Intent(getActivity(), EarningHistoryActivity.class);
//                            i.putExtra("startdate", cal_startdate);
//                            i.putExtra("enddate", cal_enddate);
//                            startActivity(i);


                            GetMonthlySpend();
                        }
                    }
                });
                d.show();
            }
        });

        _txt_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getContext(), MonthlySpendingTermsActivity.class);
                intent.putExtra("term_type","2");
                startActivity(intent);

            }
        });

    }

    private void GetMonthlySpend() {

        if (isInternetOn(getContext())) {
            try {
                Call<JsonObject> call = apiService.user_monthly_spend(sharedPreferencesUtility.getuser_id(), cal_startdate, cal_enddate);

                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try {
                            Log.e("RES_monthly_spend", response.body().toString());
                            JSONObject objBookingList = new JSONObject(response.body().toString());
                            if (objBookingList.getString("status").equalsIgnoreCase("1")) {

                                JSONArray payload = objBookingList.getJSONArray("payload");

                                for (int i = 0; i < payload.length(); i++) {
                                    JSONObject object = payload.getJSONObject(i);

                                    _txt_paytm_ride.setText(getResources().getString(R.string.st_rupee) + object.getString("paytm_total"));
                                    _txt_cash_ride.setText(getResources().getString(R.string.st_rupee) + object.getString("cash_total"));
                                    _txt_total.setText(getResources().getString(R.string.st_rupee) + object.getString("total"));
                                    _txt_trip_count.setText(object.getString("tip_count"));

                                }

                                _txt_date.setText(objBookingList.getString("start_date") + " TO " + objBookingList.getString("end_date"));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
