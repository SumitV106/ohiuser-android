package com.ohicabs.user.ohi.DBModel;

/**
 * Created by sumit on 11/04/17.
 */

public class CareList_Model {
    String care_id;
    String care_name;
    String care_number;
    String status;
    String created_date;
    String modify_date;
    String address, care_time, languages, care_driver_number, care_driver_time;

    public String getCare_id() {
        return care_id;
    }

    public void setCare_id(String care_id) {
        this.care_id = care_id;
    }

    public String getCare_name() {
        return care_name;
    }

    public void setCare_name(String care_name) {
        this.care_name = care_name;
    }

    public String getCare_number() {
        return care_number;
    }

    public void setCare_number(String care_number) {
        this.care_number = care_number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getModify_date() {
        return modify_date;
    }

    public void setModify_date(String modify_date) {
        this.modify_date = modify_date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCare_time() {
        return care_time;
    }

    public void setCare_time(String care_time) {
        this.care_time = care_time;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public String getCare_driver_number() {
        return care_driver_number;
    }

    public void setCare_driver_number(String care_driver_number) {
        this.care_driver_number = care_driver_number;
    }

    public String getCare_driver_time() {
        return care_driver_time;
    }

    public void setCare_driver_time(String care_driver_time) {
        this.care_driver_time = care_driver_time;
    }
}
