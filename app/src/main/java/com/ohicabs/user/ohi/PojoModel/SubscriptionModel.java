package com.ohicabs.user.ohi.PojoModel;

public class SubscriptionModel {

    String plan_id, plan_name, details, plan_days, amount, is_offer, max_ride, max_discount,
            discount_perm, plan_image, term_and_condition, start_date, end_date, is_buy, service_name;

    String subscriptionDetails;

    // is_offer = 0 hoi tyare buy nu button nai ave
    // is_buy = 1 hoi tyare display playtm button

    public String getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(String plan_id) {
        this.plan_id = plan_id;
    }

    public String getPlan_name() {
        return plan_name;
    }

    public void setPlan_name(String plan_name) {
        this.plan_name = plan_name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getPlan_days() {
        return plan_days;
    }

    public void setPlan_days(String plan_days) {
        this.plan_days = plan_days;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getIs_offer() {
        return is_offer;
    }

    public void setIs_offer(String is_offer) {
        this.is_offer = is_offer;
    }

    public String getMax_ride() {
        return max_ride;
    }

    public void setMax_ride(String max_ride) {
        this.max_ride = max_ride;
    }

    public String getMax_discount() {
        return max_discount;
    }

    public void setMax_discount(String max_discount) {
        this.max_discount = max_discount;
    }

    public String getDiscount_perm() {
        return discount_perm;
    }

    public void setDiscount_perm(String discount_perm) {
        this.discount_perm = discount_perm;
    }

    public String getPlan_image() {
        return plan_image;
    }

    public void setPlan_image(String plan_image) {
        this.plan_image = plan_image;
    }

    public String getTerm_and_condition() {
        return term_and_condition;
    }

    public void setTerm_and_condition(String term_and_condition) {
        this.term_and_condition = term_and_condition;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getIs_buy() {
        return is_buy;
    }

    public void setIs_buy(String is_buy) {
        this.is_buy = is_buy;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getSubscriptionDetails() {
        return subscriptionDetails;
    }

    public void setSubscriptionDetails(String subscriptionDetails) {
        this.subscriptionDetails = subscriptionDetails;
    }
}
