package com.ohicabs.user.ohi.DBModel;

/**
 * Created by sumit on 11/04/17.
 */

public class ServicePrice_Model {

    String price_id;
    String zone_id;
    String service_id;
    String base_charge;
    String per_km_charge;
    String per_minute_charge;
    String booking_charge;
    String minimum_fair;
    String cancel_charge;
    String created_at;
    String modify_date;
    String status;
    String setsec_ride_per;
    String minimum_km;
    String paytm_offer;
    String max_offer_amount;
    String rental_time, allowance, tokenPer, maxToken, gst, maxTax;

    public String getsec_ride_per() {
        return setsec_ride_per;
    }

    public void setSetsec_ride_per(String setsec_ride_per) {
        this.setsec_ride_per = setsec_ride_per;
    }

    public String getMinimum_km() {
        return minimum_km;
    }

    public void setMinimum_km(String minimum_km) {
        this.minimum_km = minimum_km;
    }



    public String getPrice_id() {
        return price_id;
    }

    public void setPrice_id(String price_id) {
        this.price_id = price_id;
    }

    public String getZone_id() {
        return zone_id;
    }

    public void setZone_id(String zone_id) {
        this.zone_id = zone_id;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getBase_charge() {
        return base_charge;
    }

    public void setBase_charge(String base_charge) {
        this.base_charge = base_charge;
    }

    public String getPer_km_charge() {
        return per_km_charge;
    }

    public void setPer_km_charge(String per_km_charge) {
        this.per_km_charge = per_km_charge;
    }

    public String getPer_minute_charge() {
        return per_minute_charge;
    }

    public void setPer_minute_charge(String per_minute_charge) {
        this.per_minute_charge = per_minute_charge;
    }

    public String getBooking_charge() {
        return booking_charge;
    }

    public void setBooking_charge(String booking_charge) {
        this.booking_charge = booking_charge;
    }

    public String getMinimum_fair() {
        return minimum_fair;
    }

    public void setMinimum_fair(String minimum_fair) {
        this.minimum_fair = minimum_fair;
    }

    public String getCancel_charge() {
        return cancel_charge;
    }

    public void setCancel_charge(String cancel_charge) {
        this.cancel_charge = cancel_charge;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getModify_date() {
        return modify_date;
    }

    public void setModify_date(String modify_date) {
        this.modify_date = modify_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



    public String getPaytm_offer() {
        return paytm_offer;
    }

    public void setPaytm_offer(String paytm_offer) {
        this.paytm_offer = paytm_offer;
    }

    public String getMax_offer_amount() {
        return max_offer_amount;
    }

    public void setMax_offer_amount(String max_offer_amount) {
        this.max_offer_amount = max_offer_amount;
    }

    public String getRental_time() {
        return rental_time;
    }

    public void setRental_time(String rental_time) {
        this.rental_time = rental_time;
    }


    public String getAllowance() {
        return allowance;
    }

    public void setAllowance(String allowance) {
        this.allowance = allowance;
    }

    public String getTokenPer() {
        return tokenPer;
    }

    public void setTokenPer(String tokenPer) {
        this.tokenPer = tokenPer;
    }

    public String getMaxToken() {
        return maxToken;
    }

    public void setMaxToken(String maxToken) {
        this.maxToken = maxToken;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getMaxTax() {
        return maxTax;
    }

    public void setMaxTax(String maxTax) {
        this.maxTax = maxTax;
    }
}
