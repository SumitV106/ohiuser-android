package com.ohicabs.user.ohi.Rental;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.Activity.NewBookingActivity;
import com.ohicabs.user.ohi.Activity.SavedLocationActivity;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Activity.AddCardActivity;
import com.ohicabs.user.ohi.Activity.BaseActivity;
import com.ohicabs.user.ohi.Adpater.CarAdapter;
import com.ohicabs.user.ohi.Adpater.UserCardListAdapter;
import com.ohicabs.user.ohi.CustomWidgets.CustomBoldFontButton;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontTextView;
import com.ohicabs.user.ohi.PojoModel.CardModel;
import com.ohicabs.user.ohi.StorageManager.DatabaseHelper;
import com.ohicabs.user.ohi.Utils.Constant;
import com.ohicabs.user.ohi.Utils.DataParser;
import com.ohicabs.user.ohi.Utils.PermissionUtils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;
import static com.ohicabs.user.ohi.Utils.Constant.BACK_TO_ACTIVITY;
import static com.ohicabs.user.ohi.Utils.Constant.CONNECTION_FAILURE_RESOLUTION_REQUEST;
import static com.ohicabs.user.ohi.Utils.Constant.LOCATION_PERMISSION_REQUEST_CODE;
import static com.ohicabs.user.ohi.Utils.Constant.PLACE_AUTOCOMPLETE_REQUEST_CODE_DROPTO;
import static com.ohicabs.user.ohi.Utils.Constant.PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP;
import static com.ohicabs.user.ohi.Utils.Constant.REQUEST_CODE_DROP_ADDRESS;
import static com.ohicabs.user.ohi.Utils.Constant.REQUEST_CODE_PICKUP_ADDRESS;


public class RentalBookingActivity extends BaseActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, View.OnClickListener,
        GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnMyLocationButtonClickListener {

    public static final String TAG = RentalBookingActivity.class.getSimpleName();

    // Request code for add cards
    private static final int ADD_CARDS = 1993;

    double price;
    String twoseatprice;

    @BindView(R.id.imgfab) ImageButton imgFabLocation;
    @BindView(R.id.txt_pickup) CustomRegularFontTextView txtPickupAddress;
    @BindView(R.id.txt_dropto) CustomRegularFontTextView txtDropAddress;
    @BindView(R.id.btnbooking) CustomBoldFontButton _btnbooking;
    @BindView(R.id.lv_carItems) TwoWayView listCarItems;
    @BindView(R.id.lypayment) LinearLayout llPayments;
    @BindView(R.id.lydatetime) LinearLayout llDateTime;
    @BindView(R.id.imgback) ImageView imgBack;
    @BindView(R.id.img_pickup_marker) ImageView imgPickupMarker;
    @BindView(R.id.img_drop_marker) ImageView imgDropMarker;
    @BindView(R.id.cardview_est_price) CardView cardViewEstPrice;
    @BindView(R.id.cardview_noservice) CardView cardViewNoService;
    @BindView(R.id.cardview_taxi) CardView cardViewTaxi;
    @BindView(R.id.txt_estimated_price) AppCompatTextView txtEstimatedPrice;
    @BindView(R.id.txt_pickup_time) AppCompatTextView txtPickupTime;
    @BindView(R.id.txt_set_payment) AppCompatTextView txtSetPayment;
    @BindView(R.id.txt_duration) AppCompatTextView txtDuration;
    @BindView(R.id.txt_estimated_time) CustomRegularFontTextView txtEstimatedTime;
    @BindView(R.id.lydropto) RelativeLayout rlDropToLayout;

    @BindView(R.id.img_search_drop_address) ImageView imgSearchDropAddress;
    @BindView(R.id.img_search_pickup_address) ImageView imgSearchPickupAddress;
    @BindView(R.id.img_save_drop_address) ImageView imgSaveDropAddress;
    @BindView(R.id.img_save_pickup_address) ImageView imgSavePickupAddress;
    @BindView(R.id.img_saved_drop_address) ImageView imgSavedDropAddress;
    @BindView(R.id.img_saved_pickup_address) ImageView imgSavedPickupAddress;

    private View mLastView;

    private GoogleMap mGoogleMap;
    private BitmapDescriptor icon;
    private MarkerOptions markerOptions;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private SupportMapFragment mSupportMapFragment;

    private CarAdapter carAdapter;

    public double mPickUpLat = 0.0, mPickUpLng = 0.0, mDropLat = 0.0, mDropLng = 0.0,
            mCurrentLat, mCurrentLng, mUserCurrentLat, mUserCurrentLng;

    private String stLocationTypeSelected, stZoneID, stDropZoneID, stPreviousZoneID = "0", mCardResult, stPaymentType = "",
            stPaymentStatus, stServiceID = "", stRideType, stCardID = "", stPriceID, stSelectDatTime, isTexiService = "", stServiceName = "";

    String pickupLatitude = "", pickupLongitude = "", pickupAddress = "", dropLatitude = "", dropLongitude = "", dropAddress = "";

    private Marker mMarkerUser, mMakerPickup, mMarkerDrop;

    private ArrayList<HashMap> arrDefaultHashMapsZoneList = new ArrayList<>();
    private ArrayList<LatLng> arrZoneCoordinatesList = new ArrayList<>();
    private ArrayList<Marker> arrMarkersPickup = new ArrayList<>();
    private ArrayList<Marker> arrMarkersDrop = new ArrayList<>();
    private ArrayList<HashMap> arrHashMapCarList = new ArrayList<>();
    private ArrayList<CardModel> arrCardList = new ArrayList<>();

    private ArrayList<Marker> arrCarMarkers = new ArrayList<>();
    private ArrayList<HashMap> arrHashMapPriceList = new ArrayList<>();
    private ArrayList<HashMap> arrHashMapZoneList = new ArrayList<>();
    private ArrayList<LatLng> arrPoints = new ArrayList<>();
    private List<Polyline> arrPolyLines = new ArrayList<Polyline>();

    HashMap<String, Double> getTotalKmPrice = new HashMap<String, Double>();

    private boolean isClickedOnCar = false;

    // used for differentiate b/w search address and saved address
    private String stLocationType = "";

    // used for differentiate b/w search address and saved address
    private boolean isMapTouched = false;

    // use only when screen load first time
    private boolean isFirstLaunch = true;

    String stMaxOfferAmount = "", stPaytmOffer = "", stEsrTotalDistance = "", stEstDuration = "", stTotalAmt = "", stSecondSeatAmt = "", stPaytmTerm = "";

    double GSTTax = 0.0;

    DatabaseHelper mHelper;
    /**
     * User Card List Receiver
     */
    private BroadcastReceiver mMessageReceiver_UserCardList = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mCardResult = intent.getStringExtra("user_card_list");
            Log.e("mCardResult", mCardResult);
        }
    };

    /**
     * Near By Car list - diff with User Current Lat Lng
     */
    private BroadcastReceiver mMessageReceiver_NearByCarList = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mCarResult = intent.getStringExtra("nearest_car_list");
            SetDriverOnMap(mCarResult);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        ButterKnife.bind(this);

        setViews();
    }

    /*
     * Set defaults for views and fill default data to views.
     */
    private void setViews() {

        rlDropToLayout.setVisibility(View.GONE);

        /* This call is for near by car list  */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_NearByCarList,
                new IntentFilter("NearBy_Car_List"));

        mSupportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map);
        mSupportMapFragment.getMapAsync(this);

        mHelper = new DatabaseHelper(getApplicationContext());

        // initializing location listener and other related stuffs.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            mGoogleApiClient.connect();
        }

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(Constant.UPDATE_INTERVAL)
                .setFastestInterval(Constant.FASTEST_INTERVAL)
                .setSmallestDisplacement(10);

        markerOptions = new MarkerOptions();
        MapsInitializer.initialize(this);

        imgBack.setOnClickListener(this);
        imgFabLocation.setOnClickListener(this);
        txtPickupAddress.setOnClickListener(this);
        txtDropAddress.setOnClickListener(this);
        _btnbooking.setOnClickListener(this);

        imgSearchDropAddress.setOnClickListener(this);
        imgSearchPickupAddress.setOnClickListener(this);
        imgSavedDropAddress.setOnClickListener(this);
        imgSavedPickupAddress.setOnClickListener(this);
        imgSaveDropAddress.setOnClickListener(this);
        imgSavePickupAddress.setOnClickListener(this);

        llPayments.setOnClickListener(this);
        llDateTime.setOnClickListener(this);

        stLocationTypeSelected = "pickup_location";

        String rideType = getIntent().getStringExtra("ride_type");
        if (rideType.equalsIgnoreCase("ride_now")) {
            stRideType = "1";
            llDateTime.setVisibility(View.GONE);
        } else {
            stRideType = "0";
            llDateTime.setVisibility(View.VISIBLE);
        }

        listCarItems.setChoiceMode(TwoWayView.ChoiceMode.SINGLE);

        listCarItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                try {
                    if (mLastView != null)
                        deselectListItem(mLastView);

                    selectedListItem(view.findViewById(R.id.lyvehicle));
                    mLastView = view.findViewById(R.id.lyvehicle);

                    for (int i = 0; i < arrHashMapCarList.size(); i++) {
                        if (position == i) {
                            stServiceID = arrHashMapCarList.get(i).get("service_id").toString();
                            isTexiService = arrHashMapCarList.get(i).get("is_taxi").toString();
                            stServiceName = arrHashMapCarList.get(i).get("service_name").toString();


                            arrHashMapPriceList = mHelper.GetPriceList(stZoneID, stServiceID);
                            if (arrHashMapPriceList.size() > 0) {
                                stPriceID = arrHashMapPriceList.get(0).get("price_id").toString();
                                CalculationByDistance(mPickUpLat, mPickUpLng, mDropLat, mDropLng);

                                updateDriverOnMap(String.valueOf(mPickUpLat), String.valueOf(mPickUpLng), stServiceID, stZoneID); // Changed by sumit

                                isClickedOnCar = true;
                            }

                            if (mDropLat != 0.0 ) {
                                if (isTexiService.equalsIgnoreCase("1")) {
                                    cardViewEstPrice.setVisibility(View.GONE);
                                    cardViewTaxi.setVisibility(View.VISIBLE);
                                } else {
                                    cardViewEstPrice.setVisibility(View.VISIBLE);
                                    cardViewTaxi.setVisibility(View.GONE);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        listCarItems.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
                Log.e("long clicked", "pos: " + position);

                for (int i = 0; i < arrHashMapCarList.size(); i++) {
                    if (position == i) {

                        stServiceID = arrHashMapCarList.get(i).get("service_id").toString();

                        ArrayList<HashMap> _arr_hashMap_service = mHelper.GetServiceDetail(stServiceID);

                        if (_arr_hashMap_service.size() > 0) {


                            // This is for Get get height and width of Screen
                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            try {
                                display.getRealSize(size);
                            } catch (NoSuchMethodError err) {
                                display.getSize(size);
                            }
                            final int width = size.x;
                            int height = size.y;


                            final Dialog dlg = new Dialog(RentalBookingActivity.this);
                            dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dlg.setContentView(R.layout.layout_service_details_dailog);
                            dlg.setCancelable(false);
                            dlg.show();

                            LinearLayout layout_main = (LinearLayout) dlg.findViewById(R.id.layout_main);
                            layout_main.setMinimumWidth((int) (width / 0.7));
                            layout_main.setMinimumHeight((int) (height / 1.5));

                            ImageView img_cancel = (ImageView) dlg.findViewById(R.id.img_cancel);
                            ImageView img_icon = (ImageView) dlg.findViewById(R.id.img_icon);
                            ImageView img_background = (ImageView) dlg.findViewById(R.id.img_background);

                            TextView txt_name = (TextView) dlg.findViewById(R.id.txt_name);
                            TextView txt_seat = (TextView) dlg.findViewById(R.id.txt_seat);
                            TextView txt_desc = (TextView) dlg.findViewById(R.id.txt_desc);
                            SimpleDraweeView draweeView = (SimpleDraweeView) dlg.findViewById(R.id.img_task_image);


                            txt_name.setText(_arr_hashMap_service.get(0).get("service_name").toString());
                            txt_seat.setText(_arr_hashMap_service.get(0).get("seat").toString());
                            txt_desc.setText(Html.fromHtml(_arr_hashMap_service.get(0).get("description").toString().replaceAll("\n", "<br/>")));

                            img_cancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dlg.dismiss();
                                }
                            });

                            try {
                                Uri imageUri = Uri.parse(Global_ServiceApi.API_IMAGE_HOST + _arr_hashMap_service.get(0).get("icon").toString());
                                draweeView.setImageURI(imageUri);

                                Glide.with(RentalBookingActivity.this).load(Global_ServiceApi.API_IMAGE_HOST + _arr_hashMap_service.get(0).get("image_details").toString())
                                        .centerCrop()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(img_background);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
        mGoogleMap.getUiSettings().setCompassEnabled(false);
        mGoogleMap.getUiSettings().setTiltGesturesEnabled(false);
        mGoogleMap.setBuildingsEnabled(false);
        mGoogleMap.setOnCameraIdleListener(this);
        mGoogleMap.setOnCameraMoveStartedListener(this);

        //Disable Marker click event
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });

        mGoogleMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int reason) {
                if (reason == REASON_GESTURE) {
                    isMapTouched = true;

                } else if (reason == REASON_API_ANIMATION) {
                    isMapTouched = false;

                } else if (reason == REASON_DEVELOPER_ANIMATION) {
                    isMapTouched = false;
                }
            }
        });

        mGoogleMap.setOnMyLocationButtonClickListener(this);
        enableMyLocation();
    }

    /*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
                return;
            }
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            handleNewLocation(location);
        }
    }

    /*
    * Called by Location Services if the connection to the
    * location client drops because of an error.
    */
    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            ShowToast(getResources().getString(R.string.network_disconnect));
        } else if (i == CAUSE_NETWORK_LOST) {
            ShowToast(getResources().getString(R.string.network_lost));
        }
    }

    /*
    * Called by Location Services if the attempt to
    * Location Services fails.
    */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            if (stLocationTypeSelected.equalsIgnoreCase("pickup_location")) {
                mPickUpLat = location.getLatitude();
                mPickUpLng = location.getLongitude();
            } else {
                mDropLat = location.getLatitude();
                mDropLng = location.getLongitude();
            }
            animateMarker(new LatLng(location.getLatitude(), location.getLongitude()), false);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCameraIdle() {

        if (isFirstLaunch) {
            getRelatedCabService();

        } else {

            if (isMapTouched || stLocationType.equalsIgnoreCase("search_address")) {

                LatLng mPosition = mGoogleMap.getCameraPosition().target;
                mCurrentLat = mPosition.latitude;
                mCurrentLng = mPosition.longitude;

                if (stLocationTypeSelected.equalsIgnoreCase("pickup_location")) {
                    mPickUpLat = mPosition.latitude;
                    mPickUpLng = mPosition.longitude;
                    imgPickupMarker.setVisibility(View.VISIBLE);
                    imgDropMarker.setVisibility(View.GONE);

                    if (CheckZoneArea(mPickUpLat, mPickUpLng)) {
                        HashMap<String, String> hashMap_ZoneDetails = GetZoneID(mPickUpLat, mPickUpLng, "AllZone");

                        stZoneID = hashMap_ZoneDetails.get("ZoneId");
                        if (!stZoneID.equalsIgnoreCase(stPreviousZoneID)) {
                            stPreviousZoneID = stZoneID;
                            /* Get CarList diff with Zone Area */
                            GetCarList();
                        }
                    } else {
                        HashMap<String, String> hashMap_ZoneDetails = GetZoneID(mPickUpLat, mPickUpLng, "DefaultZone");
                        stZoneID = hashMap_ZoneDetails.get("ZoneId");
                        if (!stZoneID.equalsIgnoreCase(stPreviousZoneID)) {
                            stPreviousZoneID = stZoneID;
                            /* Get CarList diff with Zone Area */
                            GetCarList();
                        }
                    }

                    //Get Address from Client current lat-lng
                    GetAddressFromLatLng(mPickUpLat, mPickUpLng);

                } else {
                    mDropLat = mPosition.latitude;
                    mDropLng = mPosition.longitude;
                    imgPickupMarker.setVisibility(View.GONE);
                    txtDuration.setVisibility(View.GONE);
                    txtDuration.setText("");
                    imgDropMarker.setVisibility(View.VISIBLE);

                    if (CheckZoneArea(mDropLat, mDropLng)) {
                        HashMap<String, String> hashMap_ZoneDetails = GetZoneID(mDropLat, mDropLng, "AllZone");
                        stDropZoneID = hashMap_ZoneDetails.get("ZoneId");
                    } else {
                        HashMap<String, String> hashMap_ZoneDetails = GetZoneID(mDropLat, mDropLng, "DefaultZone");
                        stDropZoneID = hashMap_ZoneDetails.get("ZoneId");
                    }

                    //Get Address from Client current lat-lng
                    GetAddressFromLatLng(mDropLat, mDropLng);
                }

                if (mDropLat != 0.0 || mDropLng != 0.0) {
                    try {
                        if (stZoneID.equals(stDropZoneID)) {
                            if (arrHashMapCarList.size() == 0) {
                                cardViewNoService.setVisibility(View.VISIBLE);
                            } else {
                                cardViewNoService.setVisibility(View.GONE);
                                if (isClickedOnCar) {
                                    cardViewEstPrice.setVisibility(View.VISIBLE);
                                }
                            }
                        } else {
                            cardViewNoService.setVisibility(View.VISIBLE);
                            cardViewEstPrice.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                CalculationByDistance(mPickUpLat, mPickUpLng, mDropLat, mDropLng);

                updateDriverOnMap(String.valueOf(mPickUpLat), String.valueOf(mPickUpLng), stServiceID, stZoneID); // Changes By Sumit
            }
        }
    }

    @Override
    public void onCameraMoveStarted(int i) {

    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        return false;
    }

    /**
     * this function call only once when Activity launch
     *
     * -To get related ride services
     *
     */
    private void getRelatedCabService() {
        isFirstLaunch = false;

        LatLng mPosition = mGoogleMap.getCameraPosition().target;
        mCurrentLat = mPosition.latitude;
        mCurrentLng = mPosition.longitude;

        if (stLocationTypeSelected.equalsIgnoreCase("pickup_location")) {
            mPickUpLat = mPosition.latitude;
            mPickUpLng = mPosition.longitude;
            imgPickupMarker.setVisibility(View.VISIBLE);
            imgDropMarker.setVisibility(View.GONE);

            if (CheckZoneArea(mPickUpLat, mPickUpLng)) {
                HashMap<String, String> hashMap_ZoneDetails = GetZoneID(mPickUpLat, mPickUpLng, "AllZone");

                stZoneID = hashMap_ZoneDetails.get("ZoneId");
                if (!stZoneID.equalsIgnoreCase(stPreviousZoneID)) {
                    stPreviousZoneID = stZoneID;
                    /* Get CarList diff with Zone Area */
                    GetCarList();
                }
            } else {
                HashMap<String, String> hashMap_ZoneDetails = GetZoneID(mPickUpLat, mPickUpLng, "DefaultZone");
                stZoneID = hashMap_ZoneDetails.get("ZoneId");
                if (!stZoneID.equalsIgnoreCase(stPreviousZoneID)) {
                    stPreviousZoneID = stZoneID;
                    /* Get CarList diff with Zone Area */
                    GetCarList();
                }
            }

            //Get Address from Client current lat-lng
            GetAddressFromLatLng(mPickUpLat, mPickUpLng);

        } else {
            mDropLat = mPosition.latitude;
            mDropLng = mPosition.longitude;
            imgPickupMarker.setVisibility(View.GONE);
            txtDuration.setVisibility(View.GONE);
            txtDuration.setText("");
            imgDropMarker.setVisibility(View.VISIBLE);

            if (CheckZoneArea(mDropLat, mDropLng)) {
                HashMap<String, String> hashMap_ZoneDetails = GetZoneID(mDropLat, mDropLng, "AllZone");
                stDropZoneID = hashMap_ZoneDetails.get("ZoneId");
            } else {
                HashMap<String, String> hashMap_ZoneDetails = GetZoneID(mDropLat, mDropLng, "DefaultZone");
                stDropZoneID = hashMap_ZoneDetails.get("ZoneId");
            }

            //Get Address from Client current lat-lng
            GetAddressFromLatLng(mDropLat, mDropLng);
        }

        if (mDropLat != 0.0 || mDropLng != 0.0) {
            try {
                if (stZoneID.equals(stDropZoneID)) {
                    if (arrHashMapCarList.size() == 0) {
                        cardViewNoService.setVisibility(View.VISIBLE);
                    } else {
                        cardViewNoService.setVisibility(View.GONE);
                        if (isClickedOnCar) {
                            cardViewEstPrice.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    cardViewNoService.setVisibility(View.VISIBLE);
                    cardViewEstPrice.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        CalculationByDistance(mPickUpLat, mPickUpLng, mDropLat, mDropLng);

        updateDriverOnMap(String.valueOf(mPickUpLat), String.valueOf(mPickUpLng), stServiceID, stZoneID); // Changes By Sumit
    }


    /**
     *
     * Check pickup and drop location inside zone area
     *
     * update driver on map
     *
     * calculate distance b/w pickup address & drop address
     *
     */
    private void checkRideAvailable() {

        if (stLocationTypeSelected.equalsIgnoreCase("pickup_location")) {

            imgPickupMarker.setVisibility(View.VISIBLE);
            imgDropMarker.setVisibility(View.GONE);

            if (CheckZoneArea(mPickUpLat, mPickUpLng)) {
                HashMap<String, String> hashMap_ZoneDetails = GetZoneID(mPickUpLat, mPickUpLng, "AllZone");

                stZoneID = hashMap_ZoneDetails.get("ZoneId");
                if (!stZoneID.equalsIgnoreCase(stPreviousZoneID)) {
                    stPreviousZoneID = stZoneID;
                    /* Get CarList diff with Zone Area */
                    GetCarList();
                }
            } else {
                HashMap<String, String> hashMap_ZoneDetails = GetZoneID(mPickUpLat, mPickUpLng, "DefaultZone");
                stZoneID = hashMap_ZoneDetails.get("ZoneId");
                if (!stZoneID.equalsIgnoreCase(stPreviousZoneID)) {
                    stPreviousZoneID = stZoneID;
                    /* Get CarList diff with Zone Area */
                    GetCarList();
                }
            }

            //Get Address from Client current lat-lng
            GetAddressFromLatLng(mPickUpLat, mPickUpLng);

        } else {

            imgPickupMarker.setVisibility(View.GONE);
            txtDuration.setVisibility(View.GONE);
            txtDuration.setText("");
            imgDropMarker.setVisibility(View.VISIBLE);

            if (CheckZoneArea(mDropLat, mDropLng)) {
                HashMap<String, String> hashMap_ZoneDetails = GetZoneID(mDropLat, mDropLng, "AllZone");
                stDropZoneID = hashMap_ZoneDetails.get("ZoneId");
            } else {
                HashMap<String, String> hashMap_ZoneDetails = GetZoneID(mDropLat, mDropLng, "DefaultZone");
                stDropZoneID = hashMap_ZoneDetails.get("ZoneId");
            }

            //Get Address from Client current lat-lng
            GetAddressFromLatLng(mDropLat, mDropLng);
        }

        if (mDropLat != 0.0 || mDropLng != 0.0) {
            try {
                if (stZoneID.equals(stDropZoneID)) {
                    if (arrHashMapCarList.size() == 0) {
                        cardViewNoService.setVisibility(View.VISIBLE);
                    } else {
                        cardViewNoService.setVisibility(View.GONE);
                        if (isClickedOnCar) {
                            cardViewEstPrice.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    cardViewNoService.setVisibility(View.VISIBLE);
                    cardViewEstPrice.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*
         * Draw polyline on map and calculate estimate ride time and km
         */
        CalculationByDistance(mPickUpLat, mPickUpLng, mDropLat, mDropLng);

        /*
         * Update driver on map according to zone selection
         */
        updateDriverOnMap(String.valueOf(mPickUpLat), String.valueOf(mPickUpLng), stServiceID, stZoneID); // Changes By Sumit
    }

    /**
     * Calculate distnace between pickup location and drop location
     *
     * @param pickupLat
     * @param pickupLong
     * @param dropLat
     * @param dropLong
     */
    private void CalculationByDistance(double pickupLat, double pickupLong, double dropLat, double dropLong) {
        if (pickupLat != 0.0 && pickupLong != 0.0 && dropLat != 0.0 && dropLong != 0.0) {
            try {
                float[] results = new float[1];
                Location.distanceBetween(pickupLat, pickupLong, dropLat, dropLong, results);
                float distance = results[0];

                double TotalKm = 0, TotalDuration = 0;

                if (getTotalKmPrice.size() > 0) {

                    if (getTotalKmPrice.containsKey("totalKM")) {

                        TotalKm = getTotalKmPrice.get("totalKM");
                    } else {
                        TotalKm = 0;
                    }

                    if (getTotalKmPrice.containsKey("totalMin")) {

                        TotalDuration = getTotalKmPrice.get("totalMin");
                    } else {
                        TotalDuration = 0;
                    }
                }

                txtEstimatedTime.setText("ETA : " + Math.round(TotalDuration) + " min");

                if (arrHashMapPriceList.size() > 0) {
                    double TotalAmnt = Double.parseDouble(arrHashMapPriceList.get(0).get("base_charge").toString()) +
                            (Double.parseDouble(arrHashMapPriceList.get(0).get("per_km_charge").toString()) * TotalKm) +
                            (Double.parseDouble(arrHashMapPriceList.get(0).get("per_minute_charge").toString()) * TotalDuration);

                    TotalAmnt = TotalAmnt + Double.parseDouble(arrHashMapPriceList.get(0).get("booking_charge").toString());

                    double minimum_km = 0.0;
                    if (!arrHashMapPriceList.get(0).get("minimum_km").equals("")) {
                        minimum_km = Double.parseDouble(arrHashMapPriceList.get(0).get("minimum_km").toString());
                    }

                    if (minimum_km >= TotalKm) {
                        TotalAmnt = Double.parseDouble(arrHashMapPriceList.get(0).get("minimum_fair").toString());
                    }

                    if (Double.parseDouble(arrHashMapPriceList.get(0).get("minimum_fair").toString()) > TotalAmnt) {
                        TotalAmnt = Double.parseDouble(arrHashMapPriceList.get(0).get("minimum_fair").toString());
                    }

                    // Get GST Tax of selected Zone
                    arrHashMapZoneList = mHelper.GetGSTTax(stZoneID);
                    if (arrHashMapZoneList.size() > 0) {
                        GSTTax = Double.parseDouble(arrHashMapZoneList.get(0).get("gst_tax").toString());
                    }

                    TotalAmnt = TotalAmnt + (TotalAmnt * GSTTax / 100);

                    txtEstimatedPrice.setText("₹" + Math.round(TotalAmnt));

                    stTotalAmt = String.valueOf(Math.round(TotalAmnt));

                    price = TotalAmnt;

                    double onepersonalprice = price;
                    double minScePrice = 0.0;
                    if (arrHashMapPriceList.get(0).get("sec_ride_per").equals("")) {
                        minScePrice = price + (price * 0.0 / 100);
                    } else {
                        minScePrice = price + (price * Double.parseDouble(arrHashMapPriceList.get(0).get("sec_ride_per").toString()) / 100);
                    }

                    minScePrice = minScePrice + (minScePrice * GSTTax / 100);

                    stMaxOfferAmount = arrHashMapPriceList.get(0).get("max_offer_amount").toString();
                    stPaytmOffer = arrHashMapPriceList.get(0).get("paytm_offer").toString();

                    Double hours = (TotalDuration) / 60; //since both are ints, you get an int
                    Double minutes = (TotalDuration) % 60;

                    String str_hours = String.valueOf(Math.round(hours));
                    if (str_hours.length() == 1) {
                        str_hours = "0" + str_hours;
                    }
                    String str_minutes = String.valueOf(Math.round(minutes));
                    if (str_minutes.length() == 1) {
                        str_minutes = "0" + str_minutes;
                    }

                    stEstDuration = str_hours + ":" + str_minutes;
                    stEsrTotalDistance = String.valueOf(roundTwoDecimals(TotalKm));

                    Log.e("onepersonalprice", onepersonalprice + "..");
                    Log.e("twoseatprice", minScePrice + "..");
                    Log.e("stEstDuration", stEstDuration + "..");

                    twoseatprice = "₹" + Math.round((minScePrice));

                    stSecondSeatAmt = String.valueOf(Math.round(minScePrice));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (Polyline line : arrPolyLines) {
            line.remove();
        }
        arrPolyLines.clear();

        LatLng pickup = new LatLng(pickupLat, pickupLong);
        LatLng dest = new LatLng(dropLat, dropLong);
        if (pickupLat != 0.0 && pickupLong != 0.0 && dropLat != 0.0 && dropLong != 0.0) {

            String url = getUrl(pickup, dest);
            FetchUrl FetchUrl = new FetchUrl();
            // Start downloading json data from Google Directions API
            FetchUrl.execute(url);

        } else {
            Log.d("Navigation", "Latlng not found");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("TAG_ON_RESUME", "onResume");
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_UserCardList,
                new IntentFilter("User_Card_List"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("TAG_ON_PAUSE", "Pause");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mSupportMapFragment.onLowMemory();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mGoogleMap != null) {
            // Access to the location has been granted to the app.
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP) {
            if (resultCode == RESULT_OK) {
                isMapTouched = true;
                stLocationType = "search_address";

                Place place = PlaceAutocomplete.getPlace(this, data);
                pickupAddress = place.getName().toString();
                pickupLatitude = place.getLatLng().latitude + "";
                pickupLongitude = place.getLatLng().longitude + "";
                mPickUpLat = place.getLatLng().latitude;
                mPickUpLng = place.getLatLng().longitude;

                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mPickUpLat, mPickUpLng)));

                GetAddressFromLatLng(mPickUpLat, mPickUpLng);

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
            }

        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_DROPTO) {
            if (resultCode == RESULT_OK) {
                isMapTouched = true;
                stLocationType = "search_address";

                Place place = PlaceAutocomplete.getPlace(this, data);

                dropAddress = place.getName().toString();
                dropLatitude = place.getLatLng().latitude + "";
                dropLongitude = place.getLatLng().longitude + "";
                mDropLat = place.getLatLng().latitude;
                mDropLng = place.getLatLng().longitude;

                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mDropLat, mDropLng)));
                GetAddressFromLatLng(mDropLat, mDropLng);

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
            }

            // Get Address from saved address book
        } else if (requestCode == REQUEST_CODE_PICKUP_ADDRESS) {
            if (resultCode == RESULT_OK) {
                stLocationType = "saved_address";

                pickupAddress = data.getStringExtra("address");
                mPickUpLat = data.getDoubleExtra("latitude", 0.0);
                mPickUpLng = data.getDoubleExtra("longitude", 0.0);
                pickupLatitude = data.getDoubleExtra("latitude", 0.0) + "";
                pickupLongitude = data.getDoubleExtra("longitude", 0.0) + "";
                txtPickupAddress.setText(pickupAddress);

                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mPickUpLat, mPickUpLng)));

                checkRideAvailable();

                setButtonVisibility();
            }

            // Get Address from saved address book
        } else if (requestCode == REQUEST_CODE_DROP_ADDRESS) {
            if (resultCode == RESULT_OK) {
                stLocationType = "saved_address";

                dropAddress = data.getStringExtra("address");
                mDropLat = data.getDoubleExtra("latitude", 0.0);
                mDropLng = data.getDoubleExtra("longitude", 0.0);
                dropLatitude = data.getDoubleExtra("latitude", 0.0) + "";
                dropLongitude = data.getDoubleExtra("longitude", 0.0) + "";
                txtDropAddress.setText(dropAddress);

                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mDropLat, mDropLng)));

                checkRideAvailable();

                setButtonVisibility();
            }

        } else if (resultCode == ADD_CARDS) {
            String message = data.getStringExtra("ADD_CARD");
            if (message.equalsIgnoreCase("Card_Added")) {

            } else if (message.equalsIgnoreCase("Finish")) {
                showPaymentSelectionDialog();
            }
        } else if (resultCode == BACK_TO_ACTIVITY) {
            String message = data.getStringExtra("MESSAGE");
            if (data.getStringExtra("MESSAGE").toString().equals("finish")) {
                finish();
            }
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnbooking:
                UserBookingRide();
                break;

            case R.id.imgfab:
                try {
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mUserCurrentLat, mUserCurrentLng)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.imgback:
                finish();
                break;

            case R.id.txt_pickup:
            case R.id.img_search_pickup_address:
                stLocationType = "search_address";
                searchLocation("pickup_location", PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP);

                break;

            case R.id.txt_dropto:
            case R.id.img_search_drop_address:
                stLocationType = "search_address";
                searchLocation("drop_location", PLACE_AUTOCOMPLETE_REQUEST_CODE_DROPTO);

                break;

            //Saved Address
            case R.id.img_saved_pickup_address:
                stLocationType = "saved_address";
                stLocationTypeSelected = "pickup_location";
                Intent intentPickup = new Intent(RentalBookingActivity.this, SavedLocationActivity.class);
                startActivityForResult(intentPickup, REQUEST_CODE_PICKUP_ADDRESS);
                break;

            //Saved Address
            case R.id.img_saved_drop_address:
                stLocationType = "saved_address";
                stLocationTypeSelected = "drop_location";
                Intent intentDrop = new Intent(RentalBookingActivity.this, SavedLocationActivity.class);
                startActivityForResult(intentDrop, REQUEST_CODE_DROP_ADDRESS);
                break;

            case R.id.img_save_pickup_address:
                saveNewAddress("Pickup");
                break;

            case R.id.img_save_drop_address:
                saveNewAddress("Drop");
                break;

            case R.id.lypayment:
                showPaymentSelectionDialog();
                break;

            case R.id.lydatetime:
                try {
                    ShowDateTimePickerDialog();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
        }
    }

    /**
     *
     * Search Pickup or Drop Location
     *
     * @param type
     * @param requestCode
     */
    private void searchLocation(String type, int requestCode) {

        try {
            stLocationTypeSelected = type;

            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    .build(); //.setCountry("IN")

            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(typeFilter).build(this);
            startActivityForResult(intent, requestCode);

        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show - hide search address button
     */
    private void setButtonVisibility() {
        if (txtPickupAddress.getText().toString().equalsIgnoreCase(getResources().getString(R.string.st_pickup_location))) {
            imgSearchPickupAddress.setVisibility(View.VISIBLE);
            imgSavePickupAddress.setVisibility(View.GONE);

        } else {
            imgSearchPickupAddress.setVisibility(View.GONE);
            imgSavePickupAddress.setVisibility(View.VISIBLE);
        }

        if (txtDropAddress.getText().toString().equalsIgnoreCase(getResources().getString(R.string.st_drop_location))) {
            imgSearchDropAddress.setVisibility(View.VISIBLE);
            imgSaveDropAddress.setVisibility(View.GONE);
        } else {
            imgSearchDropAddress.setVisibility(View.GONE);
            imgSaveDropAddress.setVisibility(View.VISIBLE);
        }
    }

    /**
     *
     * Add new address dialog
     *
     *
     * @return
     */
    public Dialog saveNewAddress(String type) {

        final Dialog dialog = new Dialog(RentalBookingActivity.this);
        dialog.setContentView(R.layout.dialog_save_address);
        dialog.setTitle("");
        dialog.setCancelable(false);

        AppCompatEditText edtTagName = dialog.findViewById(R.id.edt_tag_name);
        AppCompatButton btnSave = dialog.findViewById(R.id.btn_save);
        AppCompatButton btnClose = dialog.findViewById(R.id.btn_close);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!txtPickupAddress.getText().toString().equalsIgnoreCase(getResources().getString(R.string.st_pickup_location)) ||
                        !txtDropAddress.getText().toString().equalsIgnoreCase(getResources().getString(R.string.st_drop_location))) {

                    if (!edtTagName.getText().toString().equalsIgnoreCase("")) {

                        if (type.equalsIgnoreCase("Pickup")) {
                            addNewAddress(edtTagName.getText().toString(), txtPickupAddress.getText().toString(), mPickUpLat, mPickUpLng);

                        } else if (type.equalsIgnoreCase("Drop")) {
                            addNewAddress(edtTagName.getText().toString(), txtDropAddress.getText().toString(), mDropLat, mDropLng);
                        }

                    } else {
                        toastDialog.ShowToastMessage("Please enter tag name");
                    }
                } else {
                    toastDialog.ShowToastMessage("Please select your location!");
                }
                dialog.dismiss();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        return null;
    }

    /**
     * add new address
     */
    private void addNewAddress(String tagName, String address, double latitude, double longitude) {
        if (isInternetOn(getApplicationContext())) {
            Call<JsonObject> call = apiService.user_location_add(preferencesUtility.getuser_id(),preferencesUtility.getauth_token(), tagName, address, latitude, longitude);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        JSONObject jsonObjectDetails = new JSONObject(response.body().toString());
                        if (jsonObjectDetails.optString("status").equalsIgnoreCase("1") &&
                                jsonObjectDetails.optString("success").equalsIgnoreCase("true")) {

                            toastDialog.ShowToastMessage(jsonObjectDetails.optString("message"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    toastDialog.ShowToastMessage("Sorry, something went wrong. Please try again.");

                }
            });
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }

    /**
     * User Booking Ride
     */
    private void UserBookingRide() {


        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final String stCurrentDate = df.format(c.getTime());

        String stMessage = IsValidate();
        if (stMessage.equalsIgnoreCase("true")) {
            String pick_date = "";
            if (stRideType.equalsIgnoreCase("1")) {
                pick_date = stCurrentDate;
            } else {
                pick_date = stSelectDatTime;
            }

//            Intent intent = new Intent(this, BookingActivityStep2Activity.class);
            Intent intent = new Intent(this, RentCarActivity.class);
            intent.putExtra("stServiceID", stServiceID);
            intent.putExtra("stServiceName", stServiceName);
            intent.putExtra("stPriceID", stPriceID);
            intent.putExtra("mPickUpLat", String.valueOf(mPickUpLat));
            intent.putExtra("mPickUpLng", String.valueOf(mPickUpLng));
            intent.putExtra("pickup", txtPickupAddress.getText().toString());
            intent.putExtra("mDropLat", String.valueOf(mDropLat));
            intent.putExtra("mDropLng", String.valueOf(mDropLng));
            intent.putExtra("dropto", txtDropAddress.getText().toString());
            intent.putExtra("pick_date", pick_date);
            intent.putExtra("stPaymentStatus", stPaymentStatus);
            intent.putExtra("stRideType", stRideType);
            intent.putExtra("stCardID", stCardID);
            intent.putExtra("time", txtDuration.getText().toString());
            intent.putExtra("isTexiService", isTexiService);

            intent.putExtra("2seatprice", stSecondSeatAmt);
            intent.putExtra("price", stTotalAmt);

            intent.putExtra("est_duration", stEstDuration);
            intent.putExtra("est_total_distance", stEsrTotalDistance);
            intent.putExtra("max_offer_amount", stMaxOfferAmount);
            intent.putExtra("paytm_offer", stPaytmOffer);

            intent.putExtra("stZoneID", stZoneID);

            startActivityForResult(intent, BACK_TO_ACTIVITY);// Activity is started with requestCode
//            finish();


        } else {
            toastDialog.ShowToastMessage(stMessage);
        }
    }

    public void animateMarker(final LatLng toPosition, final boolean hideMarke) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mGoogleMap.getProjection();
        Point startPoint = proj.toScreenLocation(mMarkerUser.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 5000;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                mMarkerUser.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarke) {
                        mMarkerUser.setVisible(false);
                    } else {
                        mMarkerUser.setVisible(true);
                    }
                }
            }
        });
    }

    /*
     for select item from listview
     */
    private void selectedListItem(View v) {
        LinearLayout parent = (LinearLayout) (v.getParent());
        CustomRegularFontTextView tv = (CustomRegularFontTextView) parent.findViewById(R.id.tvname);
//        tv.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorBlue));
        tv.setTextColor(Color.parseColor("#ffffff"));
        parent.findViewById(R.id.imgvehicle).setBackgroundResource(R.drawable.vehicle_background_withcolor);
        parent.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
    }

    /*
        for deselect item from listview
    */
    private void deselectListItem(View v) {
        LinearLayout parent = (LinearLayout) (v.getParent());
        CustomRegularFontTextView tv = (CustomRegularFontTextView) parent.findViewById(R.id.tvname);
        tv.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colortextNormal));
        parent.findViewById(R.id.imgvehicle).setBackgroundResource(R.drawable.vehicle_background);
        parent.setBackgroundColor(Color.TRANSPARENT);

    }

    private void handleNewLocation(Location location) {
        Log.d(TAG, location.toString());
        if (mLocationRequest != null) {
            try {
                mPickUpLat = location.getLatitude();
                mPickUpLng = location.getLongitude();
                location.setAccuracy(10);

                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(mPickUpLat, mPickUpLng)).zoom(14.0f).build();
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                mUserCurrentLat = mPickUpLat;
                mUserCurrentLng = mPickUpLng;
                GetAddressFromLatLng(mPickUpLat, mPickUpLng);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        /* Add Zone Area on Google Map */
        try {
//            SetZoneBoundary();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void GetAddressFromLatLng(final double lat, final double lng) {
        try {
            String stLatLng = lat + "," + lng;
            RequestQueue requestQueue;
            StringRequest stringRequest;

            requestQueue = Volley.newRequestQueue(this);
            String GetAddress_URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng="
                    + stLatLng + "&sensor=false&key=" + getResources().getString(R.string.google_maps_key);//AIzaSyCYS3GNlw4NJITjFTH1BeORQffASIPnVrk";

            stringRequest = new StringRequest(Request.Method.GET, GetAddress_URL,
                    new com.android.volley.Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                if (response != null) {
                                    JSONObject objAddress = new JSONObject(response);

                                    JSONArray jarrayAddress = objAddress.getJSONArray("results");
                                    if (jarrayAddress.length() > 0) {
                                        String stFormattedAddress = jarrayAddress.getJSONObject(0).getString("formatted_address");
                                        JSONObject jsonObjectGeoMetry = jarrayAddress.getJSONObject(0).getJSONObject("geometry");

                                        JSONObject jsonObjectLocation = jsonObjectGeoMetry.getJSONObject("location");
                                        String stLatitude = jsonObjectLocation.getString("lat");
                                        String stLongitude = jsonObjectLocation.getString("lng");

                                        if (stLocationTypeSelected.equalsIgnoreCase("drop_location")) {
                                            txtDropAddress.setText(stFormattedAddress);
                                            mDropLat = Double.parseDouble(stLatitude);
                                            mDropLng = Double.parseDouble(stLongitude);

                                        } else if (stLocationTypeSelected.equalsIgnoreCase("pickup_location")) {
                                            txtPickupAddress.setText(stFormattedAddress);
                                            mPickUpLat = Double.parseDouble(stLatitude);
                                            mPickUpLng = Double.parseDouble(stLongitude);
                                        }

                                        setButtonVisibility();
                                    } else {
                                        setButtonVisibility();
                                        imgSearchDropAddress.setVisibility(View.VISIBLE);
                                        imgSearchPickupAddress.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    ShowToast(getResources().getString(R.string.no_address_found));

                                    setButtonVisibility();
                                    imgSearchDropAddress.setVisibility(View.VISIBLE);
                                    imgSearchPickupAddress.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        //TODO
                    } else if (error instanceof AuthFailureError) {
                        //TODO
                    } else if (error instanceof ServerError) {
                        //TODO
                    } else if (error instanceof NetworkError) {
                        //TODO
                    } else if (error instanceof ParseError) {
                        //TODO
                    }
                }
            });
            requestQueue.add(stringRequest);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Get Car List Service emit
     */
    private void updateDriverOnMap(String stLat, String stLng, String stServiceID, String zoneID) {

        try {
            JSONObject objCarList = new JSONObject();
            objCarList.put("latitude", stLat);
            objCarList.put("longitude", stLng);
            objCarList.put("service_id", stServiceID);
            objCarList.put("zone_id", zoneID);
            objCarList.put("Sumit", zoneID);
            objCarList.put("is_taxi", "3");

            appDelegate.emit("near_by_driver", objCarList.toString(), mSocket.id());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Zone Area On Map
     * <p>
     * Get Data From Database
     * <p>
     * Table: zone_list
     * <p>
     * Draw Polygon on Map: Area and Zone
     */
    private void SetZoneBoundary() {
        arrDefaultHashMapsZoneList = mHelper.GetDefaultZoneFromDB();
        for (int j = 0; j < arrDefaultHashMapsZoneList.size(); j++) {
            String mZoneList = arrDefaultHashMapsZoneList.get(j).get("zone_id").toString();
        }

        try {

            ArrayList<HashMap> hashMaps_zoneList = mHelper.GetZoneListFromDB();
            for (int j = 0; j < hashMaps_zoneList.size(); j++) {
                String mZoneList = hashMaps_zoneList.get(j).get("google_zone_name").toString();
                try {
                    JSONArray itemArray = new JSONArray(mZoneList);
                    for (int i = 0; i < itemArray.length(); i++) {
                        JSONObject objLatLng = itemArray.getJSONObject(i);
                        arrZoneCoordinatesList.add(new LatLng(Double.parseDouble(objLatLng.getString("lat")), Double.parseDouble(objLatLng.getString("lng"))));
                    }

                    PolygonOptions polygonOptions_Zone = new PolygonOptions();
                    polygonOptions_Zone.addAll(arrZoneCoordinatesList)
                            .strokeColor(Color.parseColor("#ef5350"))
                            .fillColor(Color.parseColor("#8066bb6a"));

                    if (mGoogleMap != null) {
                        Polygon polygon_Zone = mGoogleMap.addPolygon(polygonOptions_Zone);
                    }

                    arrZoneCoordinatesList.clear();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get Car List diff with Zone Area- Selected by User
     */
    private void GetCarList() {
        try {
            arrHashMapCarList = mHelper.GetCarListForRental(preferencesUtility.getgender(), stZoneID);

            if (arrHashMapCarList.size() > 0) {
                carAdapter = new CarAdapter(getApplicationContext(), R.layout.vehical_item, arrHashMapCarList, new CarAdapter.BtnClickListener() {
                    @Override
                    public void onBtnClick(int position, String service_id, String IsTaxi) {

                    }
                });
                listCarItems.setAdapter(carAdapter);
                txtDuration.setVisibility(View.VISIBLE);

            } else {

                progressDialog.dismiss();
                txtDuration.setVisibility(View.INVISIBLE);
                ShowToast("Ohi-Cab service not available in this area!");
                arrHashMapCarList.clear();

                carAdapter = new CarAdapter(getApplicationContext(), R.layout.vehical_item, arrHashMapCarList, new CarAdapter.BtnClickListener() {
                    @Override
                    public void onBtnClick(int position, String service_id, String IsTaxi) {

                    }
                });
                listCarItems.setAdapter(carAdapter);
            }

            if (listCarItems.getAdapter() != null) {
                progressDialog.dismiss();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get User CardList
     */
    private void GetCardList() {
        if (isInternetOn(getApplicationContext())) {
            Call<JsonObject> call = apiService.user_save_card_list(preferencesUtility.getuser_id());
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        Log.e("response", response.body().toString());
                        mCardResult = response.body().toString();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                Intent intent = new Intent("User_Card_List");
//                intent.putExtra("user_card_list", response.body().toString());
//                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }

    /**
     * Set Driver location On Map
     * <p>
     * set Driver car location and calculate approx time duration between driver and pickup location
     *
     * @param stResult
     */
    private void SetDriverOnMap(String stResult) {

        try {
            ArrayList<HashMap> markersArray = new ArrayList<>();

            JSONObject objCarList = new JSONObject(stResult);
            if (objCarList.getString("status").equalsIgnoreCase("1")) {

                JSONArray jArry_CarDetail = objCarList.getJSONArray("payload");
                if (jArry_CarDetail.length() > 0) {
                    for (int i = 0; i < jArry_CarDetail.length(); i++) {
                        JSONObject objVale = jArry_CarDetail.getJSONObject(i);

                        HashMap<String, String> map_MarkerList = new HashMap<>();
                        map_MarkerList.put("DriverName", objVale.getString("user_id"));
                        map_MarkerList.put("vale_lat", objVale.getString("lati"));
                        map_MarkerList.put("vale_lng", objVale.getString("longi"));
                        map_MarkerList.put("top_icon", objVale.getString("top_icon"));
                        map_MarkerList.put("service_id", objVale.getString("service_id"));

                        markersArray.add(map_MarkerList);
                    }
                }
            }
            removeMarkers();
            for (int i = 0; i < markersArray.size(); i++) {
                createMarker(Double.parseDouble(markersArray.get(i).get("vale_lat").toString()),
                        Double.parseDouble(markersArray.get(i).get("vale_lng").toString()),
                        markersArray.get(i).get("DriverName").toString(),
                        markersArray.get(i).get("service_id").toString()
                );
            }

            if (markersArray.size() > 0) {
                float[] results = new float[1];
                Location.distanceBetween(mPickUpLat, mPickUpLng, Double.parseDouble(markersArray.get(0).get("vale_lat").toString()),
                        Double.parseDouble(markersArray.get(0).get("vale_lng").toString()), results);
                float distance = results[0];
                double TotalKm = distance / 1000;
                double TotalDuration = TotalKm * 2.5;

                if (Math.round(TotalDuration) == 0) {

                    txtDuration.setText("1 min");

                } else {
                    txtDuration.setText(Math.round(TotalDuration) + " min");
                }


                txtDuration.setVisibility(View.VISIBLE);

            } else {
                txtDuration.setVisibility(View.GONE);
                txtDuration.setText("");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /*
     First Remove All Marker from Map
      */
    private void removeMarkers() {
        for (Marker marker : arrCarMarkers) {
            marker.remove();
        }
        arrCarMarkers.clear();
    }

    /**
     * Add marker on google map
     *
     * @param latitude
     * @param longitude
     * @param title
     */
    protected void createMarker(double latitude, double longitude, String title, String service_id) {
//        URL img_url= null;
//        try {
//            img_url = new URL(Global_ServiceApi.API_IMAGE_HOST+icon);
//            Bitmap bmp = BitmapFactory.decodeStream(img_url.openConnection().getInputStream());
//            Marker markerName = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
//                    .icon(BitmapDescriptorFactory.fromBitmap(bmp)));
//            arrCarMarkers.add(markerName);
//
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        Marker markerName = null;
        if (service_id != null) {
            if (service_id.equals("1")) {
                markerName = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_1)));
            } else if (service_id.equals("2") || service_id.equals("13") || service_id.equals("18")) {
                markerName = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_2)));
            } else if (service_id.equals("3") || service_id.equals("19")) {
                markerName = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_3)));
            } else if (service_id.equals("11")) {
                markerName = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_11)));
            } else if (service_id.equals("12")) {
                markerName = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_12)));
            } else if (service_id.equals("13")) {
                markerName = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_13)));
            } else if (service_id.equals("14")) {
                markerName = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_14)));
            } else if (service_id.equals("15")) {
                markerName = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_15)));
            } else if (service_id.equals("16") || service_id.equals("17")) {
                markerName = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_16)));
            } else {
                markerName = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_taxi)));
            }
        } else {
            markerName = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).draggable(false).title(title)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_taxi)));
        }
        arrCarMarkers.add(markerName);
    }


    /**
     * Select Payment Option Dialog
     *
     * @return
     */
    public Dialog showPaymentSelectionDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_payment_option_dialog);
        dialog.setTitle("");
        dialog.setCancelable(false);

        ListView list_card = (ListView) dialog.findViewById(R.id.listview_cards);
        list_card.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        Button btn_add_card = (Button) dialog.findViewById(R.id.btn_add_card);
        AppCompatTextView txt_done = (AppCompatTextView) dialog.findViewById(R.id.txt_done);
        AppCompatTextView txt_select_payment = (AppCompatTextView) dialog.findViewById(R.id.txt_select_payment);
        final AppCompatTextView txt_pay_by_paypal = (AppCompatTextView) dialog.findViewById(R.id.txt_pay_by_paypal);
        final AppCompatTextView txt_pay_by_paytm = (AppCompatTextView) dialog.findViewById(R.id.txt_pay_by_paytm);
        final AppCompatTextView txt_pay_by_cash = (AppCompatTextView) dialog.findViewById(R.id.txt_pay_by_cash);
        final AppCompatCheckBox chkbox_pay_by_reward = (AppCompatCheckBox) dialog.findViewById(R.id.chkbox_pay_by_reward);
        final AppCompatCheckBox chkbox_pay_by_Paytm = (AppCompatCheckBox) dialog.findViewById(R.id.chkbox_pay_by_Paytm);
        final AppCompatCheckBox chkbox_pay_by_OhiPayment = (AppCompatCheckBox) dialog.findViewById(R.id.chkbox_pay_by_OhiPayment);
        final AppCompatCheckBox chkbox_pay_by_PayUmoneyWithRewards = (AppCompatCheckBox) dialog.findViewById(R.id.chkbox_pay_by_PayUmoneyWithRewards);
        final AppCompatCheckBox chkbox_pay_by_Crash = (AppCompatCheckBox) dialog.findViewById(R.id.chkbox_pay_by_Crash);


        chkbox_pay_by_OhiPayment.setTypeface(global_typeface.Sansation_Regular());
        chkbox_pay_by_reward.setTypeface(global_typeface.Sansation_Regular());
        txt_select_payment.setTypeface(global_typeface.Sansation_Regular());
        txt_done.setTypeface(global_typeface.Sansation_Regular());
        btn_add_card.setTypeface(global_typeface.Sansation_Regular());

        btn_add_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent i = new Intent(RentalBookingActivity.this, AddCardActivity.class);
                startActivityForResult(i, ADD_CARDS);

            }
        });

        txt_pay_by_paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stPaymentType = "PayPal";
                ShowToast(getResources().getString(R.string.st_coming_soon));
            }
        });

        txt_pay_by_paytm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stPaymentType = "PayUmoney";
                stPaymentStatus = "0";
                txtSetPayment.setText(stPaymentType);
                txt_pay_by_paytm.setBackgroundColor(getResources().getColor(R.color.colorEditBackground));
                txt_pay_by_cash.setBackgroundColor(getResources().getColor(R.color.colorwhite));


            }
        });

        txt_pay_by_cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stPaymentType = "Cash payment";
                stPaymentStatus = "4";
                txtSetPayment.setText(stPaymentType);
                txt_pay_by_cash.setBackgroundColor(getResources().getColor(R.color.colorEditBackground));
                txt_pay_by_paytm.setBackgroundColor(getResources().getColor(R.color.colorwhite));

            }
        });

        chkbox_pay_by_Paytm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.e("checkedvalue1", b + "");
                chkbox_pay_by_Paytm.setChecked(b);
                chkbox_pay_by_Crash.setChecked(false);
            }
        });
        chkbox_pay_by_Crash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                chkbox_pay_by_Crash.setChecked(b);
                chkbox_pay_by_Paytm.setChecked(false);
            }
        });
        txt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chkbox_pay_by_OhiPayment.isChecked()) {
                    if (stPaymentType.equalsIgnoreCase("Cash payment")) {
                        txtSetPayment.setText(stPaymentType);

                    } else {
                        txtSetPayment.setText("wallet & " + stPaymentType);
                    }
                }
//                else {
//                    if (stPaymentType.equalsIgnoreCase("PayPal")) {
//                        stPaymentStatus = "1";
//                    } else {
//                        stPaymentStatus = "0";
//                    }
//                    txtSetPayment.setText("Only " + stPaymentType);
//                }


//                if (chkbox_pay_by_Paytm.isChecked()) {
//                    stPaymentType="PaymentTypePayUmoney";
//                    stPaymentStatus="0";
//                    txtSetPayment.setText(stPaymentType);
//
//
//
//                }
//                if (chkbox_pay_by_Crash.isChecked()) {
//                    stPaymentType="PaymentTypeCash";
//                    stPaymentStatus="4";
//                    txtSetPayment.setText(stPaymentType);
//                }

                Log.e("stPaymentStatus", stPaymentStatus);
                if (!stPaymentType.equalsIgnoreCase("")) {
                    dialog.dismiss();
                } else {
                    toastDialog.ShowToastMessage(getResources().getString(R.string.st_select_payment_option));
                }
            }
        });

        chkbox_pay_by_reward.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                } else {

                }
            }
        });

        try {
            JSONObject objCardList = new JSONObject(mCardResult);
            arrCardList.clear();
            if (objCardList.getString("status").equalsIgnoreCase("1")) {
                JSONArray jarrPayload = objCardList.getJSONArray("payload");
                if (jarrPayload.length() > 0) {

                    arrCardList.clear();

                    for (int i = 0; i < jarrPayload.length(); i++) {
                        JSONObject objCardData = jarrPayload.getJSONObject(i);

                        CardModel cardModel = new CardModel();
                        cardModel.setCard_id(objCardData.getString("card_id"));
                        cardModel.setUser_id(objCardData.getString("user_id"));
                        cardModel.setCard_type(objCardData.getString("card_type"));
                        cardModel.setCreated_date(objCardData.getString("created_date"));
                        cardModel.setStatus(objCardData.getString("status"));
                        cardModel.setCard_expiry_month(objCardData.getString("card_expiry_month"));
                        cardModel.setCard_expiry_year(objCardData.getString("card_expiry_year"));
                        cardModel.setCard_name(objCardData.getString("card_name"));
                        cardModel.setCard_number(objCardData.getString("last_digit"));
                        cardModel.setStripeCardID(objCardData.getString("stripe_card_id"));

                        arrCardList.add(cardModel);
                    }
                }
            }

            list_card.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    for (int i = 0; i < arrCardList.size(); i++) {
                        if (position == i) {
                            stPaymentType = "Card";
                            stCardID = arrCardList.get(i).getCard_id();
                            Log.e("TAG_CARD_ID = ", "==- " + stCardID);
                        }
                    }
                }
            });


            UserCardListAdapter userCardListAdapter = new UserCardListAdapter(this, R.layout.layout_card_row, arrCardList, new UserCardListAdapter.BtnClickListener() {
                @Override
                public void onBtnClick(int position, String card_id, String opration_type) {
                    stPaymentType = "Card";
                    stCardID = card_id;
                }
            });

            list_card.setAdapter(userCardListAdapter);
            userCardListAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog.show();
        return null;
    }

    public Dialog showAlertDialog(String title, String message, final String status) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_popup);
        dialog.setTitle("");
        dialog.setCancelable(false);
        TextView txt_popup_header = (TextView) dialog.findViewById(R.id.txt_popup_header);
        TextView txt_popup_message = (TextView) dialog.findViewById(R.id.txt_popup_message);
        TextView btn_ok = (TextView) dialog.findViewById(R.id.btn_ok);

        txt_popup_header.setText(title);
        txt_popup_message.setText(message);


        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("0")) {
                    dialog.dismiss();
                } else if (status.equalsIgnoreCase("1")) {
                    dialog.dismiss();
                    finish();
                } else if (status.equalsIgnoreCase("2")) {
                    dialog.dismiss();
                    finish();
                }
            }
        });

        dialog.show();
        return null;
    }

    /**
     * Set Date-Time Picker Dialog for Schedule Ride
     *
     * @return
     */
    public Dialog ShowDateTimePickerDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_date_time_picker);
        dialog.setTitle("");
        dialog.setCancelable(false);

        final AppCompatButton btn_next = (AppCompatButton) dialog.findViewById(R.id.btn_next);
        final AppCompatButton btn_done = (AppCompatButton) dialog.findViewById(R.id.btn_done);
        final TimePicker timePicker = (TimePicker) dialog.findViewById(R.id.timePicker);
        final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.datePicker);
        timePicker.setIs24HourView(true);

        btn_done.setTypeface(global_typeface.Sansation_Regular());
        btn_next.setTypeface(global_typeface.Sansation_Regular());

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_next.setVisibility(View.GONE);
                btn_done.setVisibility(View.VISIBLE);
                timePicker.setVisibility(View.VISIBLE);
                datePicker.setVisibility(View.GONE);
            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder builder = new StringBuilder();
                builder.append((datePicker.getMonth() + 1) + "/");//month is 0 based
                builder.append(datePicker.getDayOfMonth() + "/");
                builder.append(datePicker.getYear());

                String selectedTime;
                int hourOfDay = timePicker.getCurrentHour();
                int minute = timePicker.getCurrentMinute();

                if (hourOfDay >= 0 && hourOfDay < 12) {
                    selectedTime = hourOfDay + ":" + minute + " AM";
                } else {
                    if (hourOfDay == 12) {
                        selectedTime = hourOfDay + ":" + minute + " PM";
                    } else {
                        hourOfDay = hourOfDay - 12;
                        selectedTime = hourOfDay + ":" + minute + " PM";
                    }
                }

                stSelectDatTime = datePicker.getYear() + "-" + (datePicker.getMonth() + 1) + "-" + datePicker.getDayOfMonth() + " " + hourOfDay + ":" + minute + ":00";
                txtPickupTime.setText(datePicker.getYear() + "-" + (datePicker.getMonth() + 1) + "-" + datePicker.getDayOfMonth() + " " + selectedTime);
                dialog.dismiss();
            }
        });
        dialog.show();
        return null;
    }

    /**
     * Validation
     *
     * @return
     */
    private String IsValidate() {
        Log.e("stDropZoneID", stDropZoneID + "-");
        Log.e("stZoneID", stZoneID + "-");
        if (txtPickupAddress.getText().toString().equalsIgnoreCase(getResources().getString(R.string.st_pickup_location))) {
            return getResources().getString(R.string.st_select_pickup_location);
        } else if (stServiceID.equalsIgnoreCase("")) {
            return getResources().getString(R.string.st_select_car_type);
        } else {
            return "true";
        }

        //        else if (txtSetPayment.getText().toString().equalsIgnoreCase("SET PAYMENT")) {
//            return getResources().getString(R.string.st_select_payment_option);
//        }
    }

    private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        String mode = "mode=driving";

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode + "&key=AIzaSyAfye39JZYcYdS196GZrvxRBfMShOCaA3w";

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,
            // Parsing the data in non-ui thread
            String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                getTotalKmPrice = parser.parseTotal(jObject);
                Log.e("TotalAmount", getTotalKmPrice.toString());
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
//            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;
            arrPoints.clear();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the Points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    arrPoints.add(position);
                }

                // Adding all the Points in the route to LineOptions
                lineOptions.addAll(arrPoints);
                lineOptions.width(10);
                lineOptions.color(Color.BLUE);

                Log.d("onPostExecute", "onPostExecute lineoptions decoded");

            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
//                mGoogleMap.addPolyline(lineOptions);
                arrPolyLines.add(mGoogleMap.addPolyline(lineOptions));
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }
    }

}
