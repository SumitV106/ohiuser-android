package com.ohicabs.user.ohi.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.CustomWidgets.CustomBoldFontButton;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontEditText;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontTextView;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.ForceUpdateChecker;
import com.ohicabs.user.ohi.Utils.Global_Typeface;
import com.ohicabs.user.ohi.Utils.TypefaceSpan;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.gson.JsonObject;
import com.truecaller.android.sdk.ITrueCallback;
import com.truecaller.android.sdk.TrueError;
import com.truecaller.android.sdk.TrueProfile;
import com.truecaller.android.sdk.TrueSDK;
import com.truecaller.android.sdk.TrueSdkScope;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;
import static com.ohicabs.user.ohi.Utils.Constant.BACK_TO_ACTIVITY;

public class LoginActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener , ForceUpdateChecker.OnUpdateNeededListener {

    private static final int RC_SIGN_IN = 9001;
    public static final int REQUEST_STORAGE_PERMISSION = 5001;

    @BindView(R.id.tvfb) TextView _tvfb;
    @BindView(R.id.tvgplus) TextView _tvgplus;
    @BindView(R.id.btnlogin) CustomBoldFontButton _btnlogin;
    @BindView(R.id.passwordWrapper) TextInputLayout passwordWrapper;
    @BindView(R.id.emailWrapper) TextInputLayout emailWrapper;
    @BindView(R.id.tvregister) CustomRegularFontTextView _tvregister;
    @BindView(R.id.tvfpwd) CustomRegularFontTextView _tvfpwd;
    @BindView(R.id.edemail) CustomRegularFontEditText _edemail;
    @BindView(R.id.edpwd) CustomRegularFontEditText _edpwd;

    ApiInterface apiService;
    Global_Typeface global_typeface;

    private GoogleApiClient mGoogleApiClient;
    CallbackManager mFacebookCallbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        setContent();
    }

    private void setContent() {

        global_typeface = new Global_Typeface(getApplicationContext());
        apiService = ApiClient.getClient().create(ApiInterface.class);

        //GoogleApiClient instance
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PLUS_ME), new Scope(Scopes.PLUS_LOGIN), new Scope(Scopes.PROFILE), new Scope(Scopes.EMAIL))
                .requestEmail()
                .requestId()
                .requestProfile()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addOnConnectionFailedListener(this)
                .build();

        _edemail.addTextChangedListener(new MyTextWatcher(_edemail));
        _edpwd.addTextChangedListener(new MyTextWatcher(_edpwd));

        _tvfb.setTypeface(global_typeface.Sansation_Regular());
        _tvgplus.setTypeface(global_typeface.Sansation_Regular());
        passwordWrapper.setTypeface(global_typeface.Sansation_Regular());

        _tvfb.setOnClickListener(this);
        _tvgplus.setOnClickListener(this);
        _btnlogin.setOnClickListener(this);
        _tvregister.setOnClickListener(this);
        _tvfpwd.setOnClickListener(this);

        ForceUpdateChecker.with(this).onUpdateNeeded(this).check();

        requestPermission();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvfb:
                facebookLogin();
                break;

            case R.id.tvgplus:
                gPlussignIn();
                break;

            case R.id.btnlogin:
                if (submitForm()) {
                    if (isInternetOn(getApplicationContext())) {
                        if (preferencesUtility.gettoken().equalsIgnoreCase("")) {
                            preferencesUtility.settoken(appDelegate.getOneSignalToken());
                            userLogin();

                        } else {
                            userLogin();
                        }

                    } else {
                        toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                    }
                }
                break;

            case R.id.tvregister:
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
//                finish();
                break;

            case R.id.tvfpwd:
                Intent i = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                startActivity(i);
//                finish();
                break;
        }
    }

    private void facebookLogin() {
        mFacebookCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "user_photos", "public_profile"));
        LoginManager.getInstance().registerCallback(mFacebookCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        System.out.println("Success");
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject json, GraphResponse response) {
                                        if (response.getError() != null) {
                                            // handle error
                                            System.out.println("ERROR");
                                        } else {
                                            System.out.println("Success");
                                            try {
                                                Log.e("json", json.toString());
                                                socialMediaLoginServiceCall(json.getString("email"), json.getString("id"), "fb", json.getString("name"));

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }

                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Log.d("TAG_CANCEL", "On cancel");
                        toastDialog.ShowToastMessage("cancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.d("TAG_ERROR", error.toString());
                    }
                });
    }

    private void gPlussignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    // [START signOut]
    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]

                        // [END_EXCLUDE]
                    }
                });
    }
    // [END signOut]

    // [START revokeAccess]
    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]

                        // [END_EXCLUDE]
                    }
                });
    }
    // [END revokeAccess]

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        TrueSDK.getInstance().onActivityResultObtained( this,resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    // [START handleSignInResult]
    private void handleSignInResult(GoogleSignInResult result) {
        //   Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            socialMediaLoginServiceCall(acct.getEmail(), acct.getId(), "google", acct.getDisplayName());

        } else {
            // Signed out, show unauthenticated UI.
        }
    }
    // [END handleSignInResult]


    /* google plus method */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * User Login via Email ID
     */
    private void userLogin() {
        progressDialog.show();

        Call<JsonObject> call = apiService.user_login(preferencesUtility.gettoken(), "Android", _edemail.getText().toString(),
                _edpwd.getText().toString(), "1", mSocket.id());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressDialog.dismiss();

                if (response.isSuccessful()) {
                    JsonObject object = response.body();

                    if (object.get("success").getAsString().equalsIgnoreCase("true")
                            && object.get("status").getAsString().equalsIgnoreCase("1")) {

                        JsonObject jarrayLogin = object.getAsJsonObject("payload");

                        preferencesUtility.setuser_id(jarrayLogin.get("user_id").getAsString());
                        preferencesUtility.setfirstname(jarrayLogin.get("firstname").getAsString());
                        preferencesUtility.setemail(jarrayLogin.get("email").getAsString());
                        preferencesUtility.setgender(jarrayLogin.get("gender").getAsString());
                        preferencesUtility.setmobile(jarrayLogin.get("mobile").getAsString());
                        preferencesUtility.setauth_token(jarrayLogin.get("auth_token").getAsString());
                        preferencesUtility.setimage(jarrayLogin.get("image").getAsString());
                        preferencesUtility.setrefer_code(jarrayLogin.get("refer_code").getAsString());
                        preferencesUtility.setstatus(jarrayLogin.get("status").getAsString());
                        preferencesUtility.setZoneId(jarrayLogin.get("zone_id").getAsString());

                        preferencesUtility.setLogedin(true);
                        preferencesUtility.setLoginType("true");
                        toastDialog.ShowToastMessage(getResources().getString(R.string.st_login_success));

                        // for close previous activity
                        Intent intent1=new Intent();
                        intent1.putExtra("MESSAGE","finish");
                        setResult(BACK_TO_ACTIVITY,intent1);

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();

                    } else if (object.get("status").getAsString().equalsIgnoreCase("2")) {
                        JsonObject jarrayLogin = object.getAsJsonObject("payload");

                        // for close previous activity
                        Intent intent1=new Intent();
                        intent1.putExtra("MESSAGE","finish");
                        setResult(BACK_TO_ACTIVITY,intent1);

                        Intent intent = new Intent(LoginActivity.this, VerificationActivity.class);
                        Bundle b_data = new Bundle();
                        b_data.putString("email", jarrayLogin.get("email").getAsString());
                        b_data.putString("verify_mobile_no", jarrayLogin.get("mobile").getAsString());
                        b_data.putString("user_id", jarrayLogin.get("user_id").getAsString());
                        intent.putExtras(b_data);
                        startActivity(intent);
                        finish();

                    } else if (object.get("status").getAsString().equalsIgnoreCase("3")) {
                        JsonObject jarrayLogin = object.getAsJsonObject("payload");

                        // for close previous activity
                        Intent intent1=new Intent();
                        intent1.putExtra("MESSAGE","finish");
                        setResult(BACK_TO_ACTIVITY,intent1);

                        Intent intent = new Intent(LoginActivity.this, BlockUserActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        toastDialog.ShowToastMessage(object.get("message").getAsString());
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("error", t.toString());
            }
        });
    }

    /* For Login using Social Media */
    private void socialMediaLoginServiceCall(final String email, final String social_id, final String social_type, final String name) {
        final String token = FirebaseInstanceId.getInstance().getToken();

        if (isInternetOn(getApplicationContext())) {

            progressDialog.show();
            Call<JsonObject> call = apiService.social_login(token, "Android", email, social_id, social_type, name, mSocket.id());
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    progressDialog.dismiss();
                    if (response.isSuccessful()) {
                        JsonObject object = response.body();
                        if (object.get("status").getAsString().equalsIgnoreCase("1")) {
                            JsonObject jarrayLogin = object.getAsJsonObject("payload");
                            preferencesUtility.setuser_id(jarrayLogin.get("user_id").getAsString());
                            preferencesUtility.setfirstname(jarrayLogin.get("firstname").getAsString());
                            preferencesUtility.setemail(jarrayLogin.get("email").getAsString());
                            preferencesUtility.setgender(jarrayLogin.get("gender").getAsString());
                            preferencesUtility.setmobile(jarrayLogin.get("mobile").getAsString());
                            preferencesUtility.setauth_token(jarrayLogin.get("auth_token").getAsString());
                            preferencesUtility.setimage(jarrayLogin.get("image").getAsString());
                            preferencesUtility.setrefer_code(jarrayLogin.get("refer_code").getAsString());
                            preferencesUtility.setis_block(jarrayLogin.get("is_block").getAsString());
                            preferencesUtility.setstatus(jarrayLogin.get("status").getAsString());

                            preferencesUtility.setLogedin(true);
                            preferencesUtility.setLoginType("true");
                            toastDialog.ShowToastMessage(getResources().getString(R.string.st_login_success));

                            // for close previous activity
                            Intent intent1=new Intent();
                            intent1.putExtra("MESSAGE","finish");
                            setResult(BACK_TO_ACTIVITY,intent1);

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else if (object.get("status").getAsString().equalsIgnoreCase("2")) {

                            // for close previous activity
                            Intent intent1=new Intent();
                            intent1.putExtra("MESSAGE","finish");
                            setResult(BACK_TO_ACTIVITY,intent1);

                            Intent intent = new Intent(LoginActivity.this, MobileNumberActivity.class);
                            Bundle b_data = new Bundle();
                            b_data.putString("email", email);
                            b_data.putString("social_id", social_id);
                            b_data.putString("social_type", social_type);
                            b_data.putString("name", name);
                            intent.putExtras(b_data);
                            startActivity(intent);
                            finish();
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                }
            });
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean validatePassword() {
        if (_edpwd.getText().toString().trim().isEmpty()) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_password_blank));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            passwordWrapper.setError(ssbuilder);
            requestFocus(_edpwd);
            return false;
        } else if (_edpwd.getText().toString().trim().length() < 6) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_password_minimum));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            passwordWrapper.setError(ssbuilder);
            requestFocus(_edpwd);
            return false;
        } else {
            passwordWrapper.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail() {
        if (_edemail.getText().toString().trim().equals("")) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_email_blank));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            emailWrapper.setError(ssbuilder);
            requestFocus(_edemail);
            return false;
        } else if (!isValidEmail(_edemail.getText().toString())) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_email_no_valid));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            emailWrapper.setError(ssbuilder);
            requestFocus(_edemail);
            return false;
        } else {
            emailWrapper.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean submitForm() {
        boolean result = true;
        if (!validateEmail()) {
            result = false;
        }
        if (!validatePassword()) {
            result = false;
        }
        return result;
    }

    @Override
    public void onUpdateNeeded(final String updateUrl) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please update your application to continue.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).create();
        dialog.show();
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {

                case R.id.edemail:
                    validateEmail();
                    break;
                case R.id.edpwd:
                    validatePassword();
                    break;

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_STORAGE_PERMISSION) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                // Finish Activity - when permission not accepted
                finish();

//                requestPermission();
            } else {

            }
        }
    }

    /**
     * Check Permission
     */
    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSION);
        }
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
