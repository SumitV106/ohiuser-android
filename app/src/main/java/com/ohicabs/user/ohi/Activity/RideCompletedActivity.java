package com.ohicabs.user.ohi.Activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.Utils.CircularImageView;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.Constant;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.Utils.Constant.CONNECTION_FAILURE_RESOLUTION_REQUEST;

public class RideCompletedActivity extends BaseActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    @BindView(R.id.img_profile)
    CircularImageView _img_profile;
    @BindView(R.id.img_ride_status)
    AppCompatImageView _img_ride_status;
    @BindView(R.id.img_car_type)
    AppCompatImageView _img_car_type;

    @BindView(R.id.txt_ride_status)
    AppCompatTextView _txt_ride_status;
    @BindView(R.id.txt_ride_date_time)
    AppCompatTextView _txt_ride_date_time;
    @BindView(R.id.txt_ride_id)
    AppCompatTextView _txt_ride_id;

    @BindView(R.id.txt_car_type)
    AppCompatTextView _txt_car_type;
    @BindView(R.id.txt_driver_name)
    AppCompatTextView _txt_driver_name;
    @BindView(R.id.txt_driver_mobile_no)
    AppCompatTextView _txt_driver_mobile_no;
    @BindView(R.id.txt_driver_car_number)
    AppCompatTextView _txt_driver_car_number;


    @BindView(R.id.txt_ride_distance)
    AppCompatTextView _txt_ride_distance;
    @BindView(R.id.txt_ride_duration)
    AppCompatTextView _txt_ride_duration;

    @BindView(R.id.txt_from_address)
    AppCompatTextView _txt_from_address;
    @BindView(R.id.txt_to_address)
    AppCompatTextView _txt_to_address;



    @BindView(R.id.layout_call)
    LinearLayout _layout_call;


    @BindView(R.id.img_call)
    AppCompatImageView _img_call;


    @BindView(R.id.layout_address)
    RelativeLayout _layout_address;




    @BindView(R.id.layout_driver_name)
    LinearLayout _layout_driver_name;
    @BindView(R.id.layout_driver_call)
    LinearLayout _layout_driver_call;
    @BindView(R.id.layout_driver_vehical_no)
    LinearLayout _layout_driver_vehical_no;




    @BindView(R.id.tv_pickup_location)
    AppCompatTextView _tv_pickup_location;
    @BindView(R.id.tv_drop_location)
    AppCompatTextView _tv_drop_location;
    @BindView(R.id.txt_call)
    AppCompatTextView _txt_call;


    private SlidingUpPanelLayout mLayout;

    SimpleDateFormat date_format;
    String stDriverId, stBooking_Status, stBookingID, stDriverContactNumber, stBookingId, isTaxiRide;

    private SupportMapFragment mSupportMapFragment;
    private GoogleMap mGoogleMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    public double mPickUpLat = 0.0, mPickUpLng = 0.0, mDropLat = 0.0, mDropLng = 0.0, mDriverLat = 0.0, mDriverLng = 0.0;
    private Marker mMarker_user, mMarker_pickup, mMarker_drop, mMarker_driver;
    Location prevLoc, newLoc;

    private ArrayList<Marker> _arr_Markers_Pickup = new ArrayList<>();
    public ArrayList<LatLng> routeArray = new ArrayList<LatLng>();
    ArrayList<HashMap> _arrPriceList;
    public PolylineOptions lineOptions = null;
    @BindView(R.id.txt_header)
    AppCompatTextView _txt_header;
    @BindView(R.id.img_back)
    AppCompatImageView _img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_completed);
        ButterKnife.bind(this);

        stBookingId = getIntent().getStringExtra("booking_id");
        Log.e("stBookingId",stBookingId);
        BookingDetails();
        setContent();
    }

    private void BookingDetails() {
        Log.e("stBookingId--",stBookingId);
        Call<JsonObject> call = apiService.booking_detail(stBookingId,"1");
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e("response",response.body().toString());
                GetRideBookingDetails(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }


    private void setContent() {
        _txt_header.setTypeface(global_typeface.Sansation_Regular());
        _txt_header.setText(getResources().getString(R.string.ride_details));
        _img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_DetailsFinishActivity,
                new IntentFilter("Details_Activity_finish"));

        lineOptions = new PolylineOptions();
        BookingDetails();

        if (getIntent().hasExtra("new_request_accepted")) {
            toastDialog.showAlertDialog(getResources().getString(R.string.st_request_accepted_title), getResources().getString(R.string.st_request_accepted_msg));
        }

        mSupportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.google_map);
        mSupportMapFragment.getMapAsync(this);


        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }

        String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        date_format = new SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.getDefault());

        _txt_ride_status.setTypeface(global_typeface.Sansation_Bold());
        _txt_ride_date_time.setTypeface(global_typeface.Sansation_Regular());
        _txt_ride_id.setTypeface(global_typeface.Sansation_Regular());

        _txt_car_type.setTypeface(global_typeface.Sansation_Bold());
        _txt_driver_name.setTypeface(global_typeface.Sansation_Regular());
        _txt_driver_mobile_no.setTypeface(global_typeface.Sansation_Regular());
        _txt_driver_car_number.setTypeface(global_typeface.Sansation_Regular());

//        _txt_ride_price.setTypeface(global_typeface.AvenirLTStd_Medium());
        _txt_ride_distance.setTypeface(global_typeface.Sansation_Regular());
        _txt_ride_duration.setTypeface(global_typeface.Sansation_Regular());

        _txt_from_address.setTypeface(global_typeface.Sansation_Regular());
        _txt_to_address.setTypeface(global_typeface.Sansation_Regular());

        _txt_call.setTypeface(global_typeface.Sansation_Regular());
//        _txt_cancel_ride.setTypeface(global_typeface.AvenirLTStd_Medium());
//        _txt_support.setTypeface(global_typeface.AvenirLTStd_Medium());
//
//        _txt_ride_toll_price.setTypeface(global_typeface.AvenirLTStd_Medium());
//        _txt_ride_tip_price.setTypeface(global_typeface.AvenirLTStd_Medium());
//
//        _txt_ride_message.setTypeface(global_typeface.AvenirLTStd_Medium());
//        _txt_driver_delay_time.setTypeface(global_typeface.AvenirLTStd_Medium());



        _layout_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //CallToDriver(stDriverContactNumber);
            }
        });



        //Drop Address
        _txt_to_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mDropLat + "," + mDropLng + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //Pickup Address
        _txt_from_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mPickUpLat + "," + mPickUpLng + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        _tv_pickup_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mPickUpLat + "," + mPickUpLng + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        _tv_drop_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mDropLat + "," + mDropLng + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    /**
     * Booking Details Parse JSON
     *
     * @param mResult
     */
    private void GetRideBookingDetails(String mResult) {
        try {

            Log.e("result",mResult);
            JSONObject objBookingList = new JSONObject(mResult);

            if (objBookingList.getString("status").equalsIgnoreCase("1")) {
                JSONArray jarrPayload = objBookingList.getJSONArray("payload");
                if (jarrPayload.length() > 0) {
                    for (int i = 0; i < jarrPayload.length(); i++) {
                        JSONObject objBookingData = jarrPayload.getJSONObject(i);

                        stDriverId = objBookingData.getString("driver_id");
                        stBooking_Status = objBookingData.getString("booking_status");
                        stBookingID = objBookingData.getString("booking_id");
                        isTaxiRide = objBookingData.getString("is_taxi");

                        _txt_ride_id.setText("#" + objBookingData.getString("booking_id"));
                        _txt_from_address.setText(objBookingData.getString("pickup_address"));
                        _txt_to_address.setText(objBookingData.getString("drop_address"));
                        _txt_car_type.setText(objBookingData.getString("service_name"));

                        _txt_driver_name.setText(objBookingData.getString("firstname") + " " + objBookingData.getString("lastname"));
                        _txt_driver_mobile_no.setText(objBookingData.getString("mobile"));
                        Log.e("number",objBookingData.getString("mobile")+"--");
                        _txt_driver_car_number.setText(objBookingData.getString("car_number"));
                        stDriverContactNumber = objBookingData.getString("mobile");

                        /* Check Is Taxi Ride or Normal Ride */
                        if (isTaxiRide.equalsIgnoreCase("0")) {
//                            _txt_ride_price.setText("$ " + objBookingData.getString("amount"));
//                            _txt_ride_price.setVisibility(View.VISIBLE);
                        } else {
//                            _txt_ride_price.setVisibility(View.INVISIBLE);
                        }

                        _txt_ride_duration.setText(objBookingData.getString("duration") + " " + getResources().getString(R.string.st_minute));
                        _txt_ride_distance.setText(roundTwoDecimals(Double.parseDouble(objBookingData.getString("total_distance"))) + " " + getResources().getString(R.string.st_km));

                        if (objBookingData.getString("toll_tax").equalsIgnoreCase("")) {
//                            _txt_ride_toll_price.setText(getResources().getString(R.string.st_toll) + ": $0.00");
                        } else {
//                            _txt_ride_toll_price.setText(getResources().getString(R.string.st_toll) + ": $" + objBookingData.getString("toll_tax"));
                        }

                        if (objBookingData.getString("tip_amount").equalsIgnoreCase("")) {
//                            _txt_ride_tip_price.setText(getResources().getString(R.string.st_tip) + ": $0.00");
                        } else {
//                            _txt_ride_tip_price.setText(getResources().getString(R.string.st_tip) + ": $" + objBookingData.getString("tip_amount"));
                        }

                        Glide.with(this).load(Global_ServiceApi.API_IMAGE_HOST + objBookingData.getString("icon"))
                                .crossFade()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(_img_car_type);

                        Glide.with(this).load(Global_ServiceApi.API_IMAGE_HOST+ objBookingData.getString("image"))
                                .crossFade()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(_img_profile);

                        /* Price Data */
                        _arrPriceList = mHelper.GetPriceData(objBookingData.getString("price_id"));

                         if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_COMPLETE_TRIP)) {
                            //Ride Complete
                            //_img_cancel.setEnabled(false);
                            _img_call.setEnabled(false);
//                            _txt_call.setEnabled(false);
//                            _txt_cancel_ride.setEnabled(false);

//                            _layout_cancel_ride.setEnabled(false);
                            _layout_call.setEnabled(false);
//                            _layout_support.setEnabled(false);

//                            _img_cancel.setAlpha(0.2f);
//                            _img_support.setAlpha(0.2f);
                            _img_call.setAlpha(0.2f);

//                            _txt_cancel_ride.setAlpha(0.2f);
//                            _txt_call.setAlpha(0.2f);
//                            _txt_support.setAlpha(0.2f);

//                            _txt_ride_message.setVisibility(View.GONE);
//                            _layout_ride_price.setVisibility(View.VISIBLE);
//                            view_toll_tip.setVisibility(View.VISIBLE);

                            _img_ride_status.setImageResource(R.drawable.ic_completed);
                            _txt_ride_status.setText(getResources().getString(R.string.st_ride_complete));

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("stop_time"));
                            df.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat(" HH:mm a  dd-MM-yyyy", Locale.getDefault());
                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString().toLowerCase());

                            /* Draw Path b/w pickup and drop location */
                            JSONArray _arrRouteLocation = objBookingData.getJSONArray("location");
                            if (_arrRouteLocation.length() > 0) {
                                for (int k = 0; k < _arrRouteLocation.length(); k++) {
                                    JSONObject objRoute = _arrRouteLocation.getJSONObject(k);
                                    routeArray.add(new LatLng(objRoute.getDouble("latitude"), objRoute.getDouble("longitude")));
                                }

                                lineOptions.addAll(routeArray);
                                lineOptions.color(Color.parseColor("#168E5A"));
                                lineOptions.width(7.0f);
                                mGoogleMap.addPolyline(lineOptions);
                            }

                        }
//                        _txt_car_type.setTextColor(Color.parseColor("#" + objBookingData.getString("color")));
                        mPickUpLat = Double.parseDouble(objBookingData.getString("pickup_lati"));
                        mPickUpLng = Double.parseDouble(objBookingData.getString("pickup_longi"));

                        mDropLat = Double.parseDouble(objBookingData.getString("drop_lati"));
                        mDropLng = Double.parseDouble(objBookingData.getString("drop_longi"));

                        mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(mPickUpLat, mPickUpLng))
                                .title(getResources().getString(R.string.st_pickup_location))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin)));
                        _arr_Markers_Pickup.add(mMarker_pickup);

                        mMarker_drop = mGoogleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(mDropLat, mDropLng))
                                .title(getResources().getString(R.string.st_drop_location))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin)));
                        _arr_Markers_Pickup.add(mMarker_drop);

                        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(mPickUpLat, mPickUpLng)).zoom(14.0f).build();
                        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                }

                 /* Get Ride Tracking Details */
                if (stBooking_Status.equalsIgnoreCase("2") || stBooking_Status.equalsIgnoreCase("3")) {

                    Call<JsonObject> call = apiService.tracking(preferencesUtility.getuser_id(),stDriverId,stBookingID,stBooking_Status,"1");

                    call.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            Intent intent = new Intent("User_Ride_Tracking");
                            intent.putExtra("user_ride_track_data", response.body().toString());
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {

                        }
                    });
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }



    @Override
    public void onMapReady(GoogleMap map) {
        mGoogleMap = map;

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
        mGoogleMap.getUiSettings().setCompassEnabled(false);
        mGoogleMap.getUiSettings().setTiltGesturesEnabled(false);
        mGoogleMap.setBuildingsEnabled(false);

        //Disable Marker click event
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
                return;
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            ShowToast(getResources().getString(R.string.network_disconnect));
        } else if (i == CAUSE_NETWORK_LOST) {
            ShowToast(getResources().getString(R.string.network_lost));
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("TAG", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }




    private void CallToDriver(String ValeMobile) {
        try {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {

                Intent intent_call = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ValeMobile));
                startActivity(intent_call);
            }
        } catch (Exception ex) {
            toastDialog.ShowToastMessage(getResources().getString(R.string.call_error));
            ex.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        /* Ride Tracking */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_UserRideTracking,
                new IntentFilter("User_Ride_Tracking"));

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_UserWaitForDriver,
                new IntentFilter("User_Wait_For_Driver_Data"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_DriverAcceptUserRequest,
                new IntentFilter("User_Ride_Request"));

          /* This Call Receive After User Give a Review to Driver For Finish This Activity */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_ReviewFinishActivity,
                new IntentFilter("Review_Activity_finish"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /* Ride Tracking */
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_UserRideTracking);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_UserWaitForDriver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_DriverAcceptUserRequest);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_DetailsFinishActivity);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_ReviewFinishActivity);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Finish This Activity After Giving Review & Tips
     */
    private BroadcastReceiver mMessageReceiver_ReviewFinishActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };

    /**
     * Receiver Call From Dialog Activity
     * <p>
     * Finish Detail Activity on Cancel Request
     */
    private BroadcastReceiver mMessageReceiver_DetailsFinishActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };

    /**
     * User Wait Timer for Driver
     */
    private BroadcastReceiver mMessageReceiver_UserWaitForDriver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_UserWaitDriver = intent.getStringExtra("user_wait_driver_data");
            try {
                JSONObject objResult = new JSONObject(mResult_UserWaitDriver);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                if (objResult.getString("status").equalsIgnoreCase("1")) {
                    JSONArray arrPayload = objResult.getJSONArray("payload");
                    if (arrPayload.length() > 0) {
                        JSONObject objData = arrPayload.getJSONObject(0);
                        preferencesUtility.setBookingIdOnRide(objData.getString("booking_id"));
                        preferencesUtility.setBookingStatusOnRide(objData.getString("booking_status"));
                        int wait_time = objData.getInt("waiting");

                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.SECOND, wait_time);
                        String formattedDate = df.format(calendar.getTime());
                        preferencesUtility.setDriverWaitTime(formattedDate);
                    }
                    // Start Time Tracker
                    //DriverWaitCountDown();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    /**
     * Request Accept Timer
     */
    private BroadcastReceiver mMessageReceiver_DriverAcceptUserRequest = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_UserWaitDriver = intent.getStringExtra("user_wait_driver_data");
            try {
                JSONObject objResult = new JSONObject(mResult_UserWaitDriver);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                if (objResult.getString("status").equalsIgnoreCase("1")) {
                    JSONArray arrPayload = objResult.getJSONArray("payload");
                    if (arrPayload.length() > 0) {
                        JSONObject objData = arrPayload.getJSONObject(0);
                        preferencesUtility.setBookingIdOnRide(objData.getString("booking_id"));
                        preferencesUtility.setBookingStatusOnRide(objData.getString("booking_status"));
                        int wait_time = objData.getInt("waiting");

                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.SECOND, wait_time);
                        String formattedDate = df.format(calendar.getTime());
                        preferencesUtility.setDriverWaitTime(formattedDate);
                    }
                    // Start Time Tracker
                    //DriverWaitCountDown();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    /**
     * User Ride Booking Tracking Receiver
     */
    private BroadcastReceiver mMessageReceiver_UserRideTracking = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_RideTracking = intent.getStringExtra("user_ride_track_data");
            Log.e("receiver_user_tracking", "Got message: " + mResult_RideTracking);
            prevLoc = new Location(LocationManager.GPS_PROVIDER);
            newLoc = new Location(LocationManager.GPS_PROVIDER);

            try {
                JSONObject objTracking = new JSONObject(mResult_RideTracking);
                if (objTracking.getString("status").equalsIgnoreCase("1")) {

                } else if (objTracking.getString("status").equalsIgnoreCase("2")) {
                    JSONObject objPayload = objTracking.getJSONObject("payload");
                    if (objPayload.getString("booking_id").equalsIgnoreCase(stBookingID)) {

                        if (objPayload.getString("booking_status").equalsIgnoreCase(Constant.STATUS_START_TRIP)) {

                            //_txt_ride_distance.setText(roundTwoDecimals(Double.parseDouble(objPayload.getString("total_distance"))) + " " + getResources().getString(R.string.st_km));
                            _txt_ride_duration.setText(objPayload.getString("duration") + " min");

                            if (_arrPriceList.size() > 0) {
                                double TotalAmnt = Double.parseDouble(_arrPriceList.get(0).get("base_charge").toString()) +
                                        (Double.parseDouble(_arrPriceList.get(0).get("base_charge").toString()) * Double.parseDouble(objPayload.getString("total_distance").toString())) +
                                        (Double.parseDouble(_arrPriceList.get(0).get("per_minute_charge").toString()) * Double.parseDouble(objPayload.getString("total_minutes").toString()));

                                if (Double.parseDouble(_arrPriceList.get(0).get("minimum_fair").toString()) > TotalAmnt) {
                                    TotalAmnt = Double.parseDouble(_arrPriceList.get(0).get("minimum_fair").toString());
                                }

                                TotalAmnt = TotalAmnt + Double.parseDouble(_arrPriceList.get(0).get("booking_charge").toString());

                               // _txt_ride_price.setText(String.valueOf(roundTwoDecimals(TotalAmnt)) + " $");

                                if (isTaxiRide.equalsIgnoreCase("0")) {
//                                    _txt_ride_price.setText(String.valueOf(roundTwoDecimals(TotalAmnt)) + " $");
//                                    _txt_ride_price.setVisibility(View.VISIBLE);
                                } else {
//                                    _txt_ride_price.setVisibility(View.INVISIBLE);
                                }
                            }
                        }

                        mDriverLat = Double.parseDouble(objPayload.getString("latitude"));
                        mDriverLng = Double.parseDouble(objPayload.getString("longitude"));

                        if (mMarker_driver == null) {
                            mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(mDriverLat, mDriverLng))
                                    .title(getResources().getString(R.string.st_driver_location))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin)));
                        } else {
                            prevLoc.setLatitude(mMarker_driver.getPosition().latitude);
                            prevLoc.setLongitude(mMarker_driver.getPosition().longitude);

                            mMarker_driver.setPosition(new LatLng(mDriverLat, mDriverLng));

                            newLoc.setLatitude(mMarker_driver.getPosition().latitude);
                            newLoc.setLongitude(mMarker_driver.getPosition().longitude);

                            float bearing = prevLoc.bearingTo(newLoc);
                            mMarker_driver.setRotation(bearing);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


}


