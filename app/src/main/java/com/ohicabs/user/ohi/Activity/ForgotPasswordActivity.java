package com.ohicabs.user.ohi.Activity;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.CustomWidgets.CustomBoldFontButton;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontEditText;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.Global_Typeface;
import com.ohicabs.user.ohi.Utils.TypefaceSpan;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class ForgotPasswordActivity extends BaseActivity {

    @BindView(R.id.tvheader)
    TextView _tvheader;

    Global_Typeface global_typeface;

    @BindView(R.id.btnforgotpwd)
    CustomBoldFontButton _btnforgotpwd;

    @BindView(R.id.imgcancle)
    ImageView _imgcancle;

    @BindView(R.id.edemail)
    CustomRegularFontEditText _edemail;
    ApiInterface apiService;

    @BindView(R.id.emailWrapper)
    TextInputLayout emailWrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);

        global_typeface = new Global_Typeface(getApplicationContext());
        _tvheader.setTypeface(global_typeface.Sansation_Bold());
        apiService = ApiClient.getClient().create(ApiInterface.class);


        _btnforgotpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (submitForm()) {
                    forgotPassword();

                }

            }
        });

        _imgcancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(ForgotPasswordActivity.this);
                finish();
            }
        });
    }

    private void forgotPassword() {
        if (isInternetOn(getApplicationContext())) {

            progressDialog.show();
            Call<JsonObject> call = apiService.user_forgot_password("1",
                    _edemail.getText().toString());

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    progressDialog.dismiss();
                    Log.e("response", response.body().toString());
                    try {
                        JSONObject object = new JSONObject(response.body().toString());
                        if (object.get("success").toString().equalsIgnoreCase("true") && object.get("status").toString().equalsIgnoreCase("1")) {
                            toastDialog.ShowToastMessage(object.get("message").toString());

                            JSONArray jsonArrayplayload = object.getJSONArray("payload");
                            if(jsonArrayplayload.length() > 0){
                                JSONObject jsonObject = jsonArrayplayload.getJSONObject(0);
                                Intent i = new Intent(ForgotPasswordActivity.this,ForgotcodeVerificationActivity.class);
                                i.putExtra("forgot_code",jsonObject.getString("forgot_code"));
                                i.putExtra("user_id",jsonObject.getString("user_id"));
                                startActivity(i);
                                finish();
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("error", t.toString());
                }
            });
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean submitForm() {
        boolean result = true;
        if (!validateEmail()) {
            result = false;
        }
        return result;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean validateEmail() {
        if (_edemail.getText().toString().trim().equals("")) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_email_blank));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            emailWrapper.setError(ssbuilder);
            requestFocus(_edemail);
            return false;
        } else if (!isValidEmail(_edemail.getText().toString())) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_email_no_valid));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            emailWrapper.setError(ssbuilder);
            requestFocus(_edemail);
            return false;
        } else {
            emailWrapper.setErrorEnabled(false);
        }

        return true;
    }

}
