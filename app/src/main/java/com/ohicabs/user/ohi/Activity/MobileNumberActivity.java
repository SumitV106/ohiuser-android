package com.ohicabs.user.ohi.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontEditText;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class MobileNumberActivity extends BaseActivity {

    @BindView(R.id.edt_mobile_no)
    CustomRegularFontEditText _edt_mobile_no;

    @BindView(R.id.txt_done)
    CustomRegularFontTextView _txt_done;

    Bundle b_data;
    private String email, social_id,social_type,name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_number);
        ButterKnife.bind(this);

        setContent();

    }

    private void setContent() {

        b_data = getIntent().getExtras();
        email= b_data.getString("email");
        social_id= b_data.getString("social_id");
        social_type= b_data.getString("social_type");
        name= b_data.getString("name");
        final String token = FirebaseInstanceId.getInstance().getToken();

        _txt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (_edt_mobile_no.getText().toString().equals("")) {
                    toastDialog.ShowToastMessage(getResources().getString(R.string.st_no_mobile));
                }else if (_edt_mobile_no.getText().toString().toString().trim().length() != 10) {
                    toastDialog.ShowToastMessage(getResources().getString(R.string.st_no_valid_mobile));
                }
                else{
                    if (isInternetOn(getApplicationContext())) {
                        Call<JsonObject> call = apiService.social_new_user(token, "Android", email, social_id, social_type, name, _edt_mobile_no.getText().toString(), mSocket.id());
                        call.enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                Log.e("new_user_login_Response", response.body().toString());
                                JsonObject object = response.body();
                                if (object.get("status").getAsString().equalsIgnoreCase("1")) {
                                    JsonObject jarrayLogin = object.getAsJsonObject("payload");
                                    preferencesUtility.setuser_id(jarrayLogin.get("user_id").getAsString());
                                    preferencesUtility.setfirstname(jarrayLogin.get("firstname").getAsString());
                                    preferencesUtility.setlastname(jarrayLogin.get("lastname").getAsString());
                                    preferencesUtility.setemail(jarrayLogin.get("email").getAsString());
                                    preferencesUtility.setgender(jarrayLogin.get("gender").getAsString());
                                    preferencesUtility.setmobile(jarrayLogin.get("mobile").getAsString());
                                    preferencesUtility.setauth_token(jarrayLogin.get("auth_token").getAsString());
                                    preferencesUtility.setimage(jarrayLogin.get("image").getAsString());
                                    preferencesUtility.setrefer_code(jarrayLogin.get("refer_code").getAsString());
                                    preferencesUtility.setis_block(jarrayLogin.get("is_block").getAsString());
                                    preferencesUtility.setstatus(jarrayLogin.get("status").getAsString());

                                    preferencesUtility.setLogedin(true);
                                    preferencesUtility.setLoginType("true");
                                    toastDialog.ShowToastMessage(getResources().getString(R.string.st_login_success));

                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else if (object.get("status").getAsString().equalsIgnoreCase("0")) {
                                    _edt_mobile_no.setText("");
                                    _edt_mobile_no.setError(object.get("message").getAsString());
                                    toastDialog.ShowToastMessage("Please re enter Mobile Number");
                                }
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {

                            }
                        });
                    }else {
                        toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                    }
                }

            }
        });

    }
}
