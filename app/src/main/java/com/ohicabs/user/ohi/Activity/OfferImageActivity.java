package com.ohicabs.user.ohi.Activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OfferImageActivity extends BaseActivity {

    @BindView(R.id.webView) WebView webView;
    @BindView(R.id.img_back) ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_image);
        ButterKnife.bind(this);

        setViews();
    }

    private void setViews() {

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if (getIntent().hasExtra("image")) {

            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);

            webView.getSettings().setBuiltInZoomControls(true);
            webView.loadUrl(Global_ServiceApi.API_IMAGE_HOST + getIntent().getStringExtra("image"));

        }
    }
}
