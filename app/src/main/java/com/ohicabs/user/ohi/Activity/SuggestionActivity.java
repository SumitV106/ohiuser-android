package com.ohicabs.user.ohi.Activity;

import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.R;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SuggestionActivity extends BaseActivity {
    @BindView(R.id.edt_email)
    EditText _edt_email;

    @BindView(R.id.edt_title)
    EditText _edt_title;

    @BindView(R.id.edt_message)
    EditText _edt_message;

    @BindView(R.id.btn_submit)
    Button _btn_submit;

    @BindView(R.id.img_back)
    ImageView _img_back;

    @BindView(R.id.txt_header)
    TextView _txt_header;

    @BindView(R.id.emailWrapper)
    TextInputLayout _emailWrapper;

    @BindView(R.id.tilteWrapper)
    TextInputLayout _tilteWrapper;

    @BindView(R.id.messageWrapper)
    TextInputLayout _messageWrapper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestion);
        ButterKnife.bind(this);
        setContent();
    }
    private void setContent() {



        _txt_header.setTypeface(global_typeface.Sansation_Bold());
        _edt_email.setTypeface(global_typeface.Sansation_Regular());
        _edt_title.setTypeface(global_typeface.Sansation_Regular());
        _edt_message.setTypeface(global_typeface.Sansation_Regular());
        _btn_submit.setTypeface(global_typeface.Sansation_Bold());

        _emailWrapper.setTypeface(global_typeface.Sansation_Regular());
        _tilteWrapper.setTypeface(global_typeface.Sansation_Regular());
        _messageWrapper.setTypeface(global_typeface.Sansation_Regular());

        _btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stMessage = IsValidate();

                if (stMessage.equalsIgnoreCase("true")) {
                    try {

                        Call<JsonObject> call = apiService.suggestion( _edt_email.getText().toString(),
                                _edt_title.getText().toString(),
                                _edt_message.getText().toString());
                        call.enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                                try {

                                    Log.e("response",response.body().toString()+"-");
                                    JSONObject obj = new JSONObject(response.body().toString());
                                    if (obj.getString("status").equalsIgnoreCase("1")) {
                                        toastDialog.ShowToastMessage(obj.getString("message"));
                                    } else {
                                        toastDialog.ShowToastMessage(obj.getString("message"));
                                    }
                                    _edt_message.setText("");
                                    _edt_title.setText("");
                                    _edt_email.setText("");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {

                            }
                        });

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    toastDialog.ShowToastMessage(stMessage);
                }
            }
        });

        _img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(SuggestionActivity.this);
                finish();
            }
        });
    }



    /**
     * Form Validation
     *
     * @return
     */
    private String IsValidate() {
        if (_edt_email.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_email_blank);

        } else if (!Global_ServiceApi.isValidEmail(_edt_email.getText().toString())) {
            return getResources().getString(R.string.st_email_no_valid);

        } else if (_edt_title.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_enter_title);

        } else if (_edt_message.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_enter_suggestion_msg);

        } else {
            return "true";
        }
    }
}
