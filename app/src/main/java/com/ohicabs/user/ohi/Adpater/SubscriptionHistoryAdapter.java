package com.ohicabs.user.ohi.Adpater;

import android.app.Activity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.ohicabs.user.ohi.PojoModel.OfferModel;
import com.ohicabs.user.ohi.PojoModel.SubscriptionHistoryModel;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.Global_Typeface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class SubscriptionHistoryAdapter extends RecyclerView.Adapter<SubscriptionHistoryAdapter.MyViewHolder> {

    private Activity activity;
    int ResourceId;
    private ArrayList<SubscriptionHistoryModel> arrSubscriptionHistoryList;
    private BtnClickListener mClickListener = null;

    public SubscriptionHistoryAdapter(Activity act, ArrayList<SubscriptionHistoryModel> subscriptionHistoryList, BtnClickListener listener) {
        activity = act;
        arrSubscriptionHistoryList = subscriptionHistoryList;
        mClickListener = listener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView txtSubscriptionName, txtSubscriptionId, txtSubscriptionStatus, txtAmount, txtValidity,
                txtDiscount, txtMaximumDiscount, txtPayType, txtMaximumRide, txtActiveOn, txtExpiryOn;

        CardView cardViewHistory;

        public MyViewHolder(View view) {
            super(view);
            txtSubscriptionName = (AppCompatTextView) view.findViewById(R.id.txt_offer);
            txtSubscriptionId = (AppCompatTextView) view.findViewById(R.id.txt_subscription_id);
            txtSubscriptionStatus = (AppCompatTextView) view.findViewById(R.id.txt_subscription_status);
            txtAmount = (AppCompatTextView) view.findViewById(R.id.txt_amount);
            txtValidity = (AppCompatTextView) view.findViewById(R.id.txt_validity);
            txtDiscount = (AppCompatTextView) view.findViewById(R.id.txt_discount);
            txtMaximumDiscount = (AppCompatTextView) view.findViewById(R.id.txt_maximum_discount);
            txtPayType = (AppCompatTextView) view.findViewById(R.id.txt_pay_type);
            txtMaximumRide = (AppCompatTextView) view.findViewById(R.id.txt_maximum_ride);
            txtActiveOn = (AppCompatTextView) view.findViewById(R.id.txt_active_on);
            txtExpiryOn = (AppCompatTextView) view.findViewById(R.id.txt_expiry_on);
            cardViewHistory = (CardView) view.findViewById(R.id.cardView_history);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_subscription_history, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.txtSubscriptionName.setText(arrSubscriptionHistoryList.get(position).getPlan_name());
        holder.txtSubscriptionId.setText("#" + arrSubscriptionHistoryList.get(position).getSub_buy_id());
        holder.txtAmount.setText("Rs. " + arrSubscriptionHistoryList.get(position).getAmount());
        holder.txtValidity.setText(arrSubscriptionHistoryList.get(position).getDays() + "(s)");
        holder.txtDiscount.setText(arrSubscriptionHistoryList.get(position).getDiscount_per() + "%");
        holder.txtMaximumDiscount.setText("Rs. " + arrSubscriptionHistoryList.get(position).getMax_discount());
        holder.txtMaximumRide.setText(arrSubscriptionHistoryList.get(position).getMax_ride());
        holder.txtActiveOn.setText(convertDate(arrSubscriptionHistoryList.get(position).getActive_date()));
        holder.txtExpiryOn.setText(convertDate(arrSubscriptionHistoryList.get(position).getExpiry_date()));

        switch (arrSubscriptionHistoryList.get(position).getStatus()) {
            case "1":
                holder.txtSubscriptionStatus.setText("Active");
                holder.txtSubscriptionStatus.setTextColor(activity.getResources().getColor(R.color.colorGreen));
                break;

            case "2":
                holder.txtSubscriptionStatus.setText("Payment Fail");
                holder.txtSubscriptionStatus.setTextColor(activity.getResources().getColor(R.color.colorRed));
                break;

            default:
                holder.txtSubscriptionStatus.setText("End");
                holder.txtSubscriptionStatus.setTextColor(activity.getResources().getColor(R.color.colorGrey));
                break;
        }

        switch (arrSubscriptionHistoryList.get(position).getPayment_type()) {
            case "0":
                holder.txtPayType.setText("Paytm");
                break;

            case "1":
                holder.txtPayType.setText("Cash");
                break;

            case "2":
                holder.txtPayType.setText("Exclusive Offer");
                break;

            case "3":
                holder.txtPayType.setText("Referral Points Redeem");
                break;
        }

        holder.cardViewHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mClickListener != null) {
                    mClickListener.onBtnClick(position, arrSubscriptionHistoryList.get(position).getSubscriptionDetails());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrSubscriptionHistoryList.size();
    }

    public interface BtnClickListener {
        void onBtnClick(int position, String details);
    }

    private String convertDate(String stDate) {
        try {
            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = df.parse(stDate);
            df.setTimeZone(TimeZone.getDefault());
            SimpleDateFormat date_fmt = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aa", Locale.getDefault());
            String _date = date_fmt.format(date);

            return _date;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}
