package com.ohicabs.user.ohi.Service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by crayon on 09/07/18.
 */

public class TimeService extends Service {

    public static final String TAG = "TimeService";
    private final IBinder binder = new MyBinder();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }
    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    public class MyBinder extends Binder {
        TimeService getService() {
            return TimeService.this;
        }
    }
    public static Context context;

    @Override
    public void onCreate() {
        context = getApplicationContext();

    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        Toast.makeText(context, "..Start", Toast.LENGTH_SHORT).show();
        startTimer();
        return START_STICKY;
    }

    private Timer timer;
    private TimerTask timerTask;

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 1000, 1000); //
    }
    public int counter = 0;

    public void initializeTimerTask() {

        timerTask = new TimerTask() {

            public void run() {
                Log.e("in timer", "in timer ++++  " + (counter++));

                Intent intent = new Intent("MIN_data");
                intent.putExtra("min_data", counter);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }
}
