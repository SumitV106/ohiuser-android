package com.ohicabs.user.ohi.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.R;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class MonthlySpendingTermsActivity extends BaseActivity {

    @BindView(R.id.txt_terms)
    TextView _txt_terms;

    @BindView(R.id.img_back)
    ImageView _img_back;

    String term_type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monthly_spending);
        ButterKnife.bind(this);

        setContent();
    }

    private void setContent() {

        if (getIntent().hasExtra("term_type")) {
            term_type = getIntent().getStringExtra("term_type");
        }

        TermsServiceCall();

        _img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void TermsServiceCall() {

        if (isInternetOn(this)) {
            try {
                progressDialog.show();
                Call<JsonObject> call = apiService.term_and_condition(term_type);
                progressDialog.dismiss();
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try {
                            JSONObject objBookingList = new JSONObject(response.body().toString());
                            if (objBookingList.getString("status").equalsIgnoreCase("1")) {

                                JSONObject objPayload = objBookingList.getJSONObject("payload");
                                _txt_terms.setText(objPayload.getString("term_text"));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
