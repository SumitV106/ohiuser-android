package com.ohicabs.user.ohi.Adpater;

import android.app.Activity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.PojoModel.RewardsModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class RewardsListAdapter extends RecyclerView.Adapter<RewardsListAdapter.MyViewHolder> {

    private Activity mActivity;
    ArrayList<RewardsModel> arrRewardsList;

    public RewardsListAdapter(Activity act, ArrayList<RewardsModel> arrRewards) {
        this.mActivity = act;
        arrRewardsList = arrRewards;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_rewards_cell, parent, false);

        return new MyViewHolder(itemView);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public AppCompatTextView txtRewardDate, txtRewardTitle, txtAmount, txtInitiatedDate, txtReferenceNo, txtTermCondition;
        public LinearLayout layout_car_info;
        public CardView card_view;
        public RelativeLayout layout_count;
        public View view_divider, view_divider_bottom;

        public MyViewHolder(View view) {
            super(view);

            txtRewardDate = (AppCompatTextView) view.findViewById(R.id.txt_rewards_date);
            txtRewardTitle = (AppCompatTextView) view.findViewById(R.id.txt_rewards_title);
            txtAmount = (AppCompatTextView) view.findViewById(R.id.txt_amount);
            txtInitiatedDate = (AppCompatTextView) view.findViewById(R.id.txt_initial_date);
            txtReferenceNo = (AppCompatTextView) view.findViewById(R.id.txt_reference_no);
            txtTermCondition = (AppCompatTextView) view.findViewById(R.id.txt_terms_condition);
        }
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        holder.txtRewardDate.setText(convertDate(arrRewardsList.get(position).getPeriod_start()) + " To " + convertDate(arrRewardsList.get(position).getPeriod_end()));
        holder.txtRewardTitle.setText(arrRewardsList.get(position).getReason());
        holder.txtAmount.setText("₹ " + arrRewardsList.get(position).getAmount());
        holder.txtInitiatedDate.setText(convertDate(arrRewardsList.get(position).getInitated_date()));
        holder.txtReferenceNo.setText(arrRewardsList.get(position).getReference_no());

        holder.txtTermCondition.setTag(arrRewardsList.get(position).getTerm_and_condition());

    }

    @Override
    public int getItemCount() {
        return arrRewardsList.size();
    }

    /**
     *
     * Convert Server date to user date
     *
     * @param stDate
     * @return
     */
    private String convertDate(String stDate) {
        try {
            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
            df.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = df.parse(stDate);
            df.setTimeZone(TimeZone.getDefault());
            SimpleDateFormat date_fmt = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            String _date = date_fmt.format(date);

            return _date;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }
}
