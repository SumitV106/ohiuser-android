package com.ohicabs.user.ohi.Fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.Application.OhiApplication;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.Global_Typeface;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.socket.client.Socket;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReferralFragment extends Fragment {


    public ReferralFragment() {
        // Required empty public constructor
    }

    private ToastDialog toastDialog;
    private Global_Typeface global_typeface;
    private SharedPreferencesUtility sharedPreferencesUtility;
    private Socket mSocket;
    OhiApplication appDelegate;

    @BindView(R.id.txt_d_ref_invite_message)
    TextView _txt_d_ref_invite_message;

    @BindView(R.id.edt_frnd_name)
    AppCompatEditText _edt_frnd_name;

    @BindView(R.id.edt_frnd_email)
    AppCompatEditText _edt_frnd_email;

    @BindView(R.id.edt_promo_code)
    AppCompatEditText _edt_promo_code;

    @BindView(R.id.btn_invite)
    AppCompatButton _btn_invite;

    @BindView(R.id.btn_redeem)
    AppCompatButton _btn_redeem;

    @BindView(R.id.txt_invite_code)
    TextView _txt_invite_code;

    @BindView(R.id.txt_reward_amount)
    TextView _txt_reward_amount;

    @BindView(R.id.txt_reward)
    TextView _txt_reward;

    @BindView(R.id.tv_invite_code)
    TextView _tv_invite_code;

    @BindView(R.id.txt_message_1)
    TextView _txt_message_1;

    ApiInterface apiService;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toastDialog = new ToastDialog(getActivity());
        global_typeface = new Global_Typeface(getActivity());
        sharedPreferencesUtility = new SharedPreferencesUtility(getActivity());
        apiService= ApiClient.getClient().create(ApiInterface.class);

        appDelegate = (OhiApplication) getActivity().getApplication();
        mSocket = appDelegate.getSocket();
        mSocket.connect();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_referral, container, false);
        ButterKnife.bind(this, view);
        return view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setContent();
    }

    private void setContent() {
        String text = "<font color=#2f363c>Once you\'ve invited friends, you can </font> <font color=#0c66de>view the status of your referrals</font>";
        _txt_d_ref_invite_message.setText(Html.fromHtml(text));

        _txt_reward.setTypeface(global_typeface.Sansation_Regular());
        _tv_invite_code.setTypeface(global_typeface.Sansation_Regular());
        _txt_message_1.setTypeface(global_typeface.Sansation_Regular());
        _btn_invite.setTypeface(global_typeface.Sansation_Regular());
        _btn_redeem.setTypeface(global_typeface.Sansation_Regular());
        _edt_frnd_email.setTypeface(global_typeface.Sansation_Bold());
        _edt_frnd_name.setTypeface(global_typeface.Sansation_Bold());
        _edt_promo_code.setTypeface(global_typeface.Sansation_Bold());
        _txt_invite_code.setTypeface(global_typeface.Sansation_Bold());
        _txt_reward_amount.setTypeface(global_typeface.Sansation_Bold());

        _txt_invite_code.setText(sharedPreferencesUtility.getrefer_code());

        _txt_d_ref_invite_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://doyengo.com"));
                startActivity(browserIntent);
            }
        });

        try {
//            JSONObject objWalletAmt = new JSONObject();
//            objWalletAmt.put("user_id", sharedPreferencesUtility.getuser_id());
//            appDelegate.emit("wallet", objWalletAmt.toString(), mSocket.id());

            Call<JsonObject> call = apiService.wallet(sharedPreferencesUtility.getuser_id());
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        String mResult =response.body().toString();
                        Log.e("TAG_Wallet_Data", mResult);
                        JSONObject objWallet = new JSONObject(mResult);
                        if (objWallet.getString("status").equalsIgnoreCase("1")) {
                            JSONObject objPayload = objWallet.getJSONObject("payload");
                            _txt_reward_amount.setText("$ " + objPayload.getString("wallet_amount"));
                            _edt_promo_code.setText(objPayload.getString("refer_code"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        _btn_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stMessage = IsValidate();
                if (stMessage.equalsIgnoreCase("true")) {
                    try {
//                        JSONObject objInvite = new JSONObject();
//                        objInvite.put("email", _edt_frnd_email.getText().toString());
//                        objInvite.put("name", _edt_frnd_name.getText().toString());
//                        objInvite.put("user_id", sharedPreferencesUtility.getuser_id());
//
//                        appDelegate.emit("invite", objInvite.toString(), mSocket.id());
                        Call<JsonObject> call = apiService.invite(_edt_frnd_email.getText().toString(),
                                _edt_frnd_name.getText().toString(),sharedPreferencesUtility.getuser_id());

                        call.enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                try {
                                    String mResult =response.body().toString();
                                    Log.e("TAG_Invite_Data", mResult);
                                    JSONObject objWallet = new JSONObject(mResult);
                                    if (objWallet.getString("status").equalsIgnoreCase("1")) {
                                        toastDialog.ShowToastMessage(objWallet.getString("message"));
                                    } else {
                                        toastDialog.ShowToastMessage(objWallet.getString("message"));
                                    }
                                    _edt_frnd_email.setText("");
                                    _edt_frnd_name.setText("");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {

                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    toastDialog.ShowToastMessage(stMessage);
                }
            }
        });

        _btn_redeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_edt_promo_code.getText().toString().trim().equals("")) {
                    toastDialog.ShowToastMessage(getResources().getString(R.string.st_no_rcode));
                } else {
                    try {
//                        JSONObject objRefr = new JSONObject();
//                        objRefr.put("user_id", sharedPreferencesUtility.getuser_id());
//                        objRefr.put("refer_code", _edt_promo_code.getText().toString());
//
//                        appDelegate.emit("referral", objRefr.toString(), mSocket.id());
                        Call<JsonObject> call = apiService.referral(sharedPreferencesUtility.getuser_id(), _edt_promo_code.getText().toString());
                        call.enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                try {
                                    String mResult = response.body().toString();
                                    Log.e("TAG_Referral_Data", mResult);
                                    JSONObject objWallet = new JSONObject(mResult);

                                    if (objWallet.getString("status").equalsIgnoreCase("1")) {
                                        toastDialog.ShowToastMessage(objWallet.getString("Message"));
                                        _edt_promo_code.setText("");

                                    } else {
                                        toastDialog.ShowToastMessage(objWallet.getString("Message"));
                                        _edt_promo_code.setText("");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {

                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }






    /**
     * Invite Validation
     *
     * @return
     */
    private String IsValidate() {
        if (_edt_frnd_email.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_email_blank);

        } else if (!Global_ServiceApi.isValidEmail(_edt_frnd_email.getText().toString())) {
            return getResources().getString(R.string.st_email_no_valid);

        } else if (_edt_frnd_name.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_name);

        } else {
            return "true";
        }
    }
}

