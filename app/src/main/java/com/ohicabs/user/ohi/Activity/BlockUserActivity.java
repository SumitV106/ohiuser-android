package com.ohicabs.user.ohi.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.R;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class BlockUserActivity extends BaseActivity {

    @BindView(R.id.toolbar_title) TextView _toolbar_title;
    @BindView(R.id.txt_msg) TextView _txt_msg;
    @BindView(R.id.txt_logout) TextView _txt_logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block_user);
        ButterKnife.bind(this);

        setContent();
    }

    private void setContent() {
        _toolbar_title.setTypeface(global_typeface.Sansation_Bold());
        _txt_msg.setTypeface(global_typeface.Sansation_Regular());
        _txt_logout.setTypeface(global_typeface.Sansation_Regular());

        _txt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogoutUser();
            }
        });
    }

    /**
     * User Logout from app
     */
    private void LogoutUser() {
        try {

            if(isInternetOn(getApplicationContext())) {
                Call<JsonObject> call = apiService.logout(preferencesUtility.getuser_id(), preferencesUtility.getauth_token());
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        Log.e("LOGOUT_response", response.body().toString());
                        try {
                            JSONObject objBookingData = new JSONObject(response.body().toString());
                            if (objBookingData.getString("success").equalsIgnoreCase("true") && objBookingData.getString("status").equalsIgnoreCase("1")) {
                                preferencesUtility.ClearPrefrenceKey("login_type");
                                preferencesUtility.ClearPreferences();

                                Intent i = new Intent(BlockUserActivity.this, LoginActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
            else{
                toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
