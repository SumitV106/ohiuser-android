package com.ohicabs.user.ohi.DBModel;

/**
 * Created by sumit on 11/04/17.
 */

public class ServiceDetail_Model {
    String service_id;
    String service_name;
    String seat;
    String color;
    String icon;
    String gender;
    String created_date;
    String modify_date;
    String status;
    String top_icon;
    String is_taxi;
    String image_details;
    String description;



    public String getTop_icon() {
        return top_icon;
    }

    public void setTop_icon(String top_icon) {
        this.top_icon = top_icon;
    }

    public String getIs_taxi() {
        return is_taxi;
    }

    public void setIs_taxi(String is_taxi) {
        this.is_taxi = is_taxi;
    }


    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getModify_date() {
        return modify_date;
    }

    public void setModify_date(String modify_date) {
        this.modify_date = modify_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage_details() {
        return image_details;
    }

    public void setImage_details(String image_details) {
        this.image_details = image_details;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
