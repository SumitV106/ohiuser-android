package com.ohicabs.user.ohi.Client;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.API_HOST;

/**
 * Created by crayon on 15/09/17.
 */

public class ApiClient {

    public static final String BASE_URL = API_HOST;
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
