package com.ohicabs.user.ohi.CustomWidgets;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class CustomBoldFontTextView extends AppCompatTextView {
    public CustomBoldFontTextView(Context context) {
        super(context);
        init();
    }

    public CustomBoldFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomBoldFontTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        if (!isInEditMode()) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Sansation_Bold.ttf"));
        }
    }
}
