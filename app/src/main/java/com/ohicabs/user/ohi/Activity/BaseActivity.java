package com.ohicabs.user.ohi.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;
import com.ohicabs.user.ohi.Application.OhiApplication;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontTextView;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.StorageManager.DatabaseHelper;
import com.ohicabs.user.ohi.Utils.Global_Typeface;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import io.socket.client.Socket;

/**
 * Created by crayon on 22/08/17.
 */

public class BaseActivity extends AppCompatActivity {

    public ProgressDialog progressDialog;
    public Global_Typeface global_typeface;

    public Socket mSocket;
    public OhiApplication appDelegate;

    public ToastDialog toastDialog;
    public SharedPreferencesUtility preferencesUtility;
    public DatabaseHelper mHelper;
    public ApiInterface apiService;
    Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appDelegate = (OhiApplication) getApplication();
        mSocket = appDelegate.getSocket();
        mSocket.connect();
        mHelper = new DatabaseHelper(getApplicationContext());

        global_typeface = new Global_Typeface(this);
        progressDialog = new ProgressDialog(BaseActivity.this);
        progressDialog.setMessage(getResources().getString(R.string.st_please_wait));
        progressDialog.setCancelable(false);

        apiService = ApiClient.getClient().create(ApiInterface.class);
        toastDialog = new ToastDialog(this);

        preferencesUtility = new SharedPreferencesUtility(this);
    }

    /**
     * Show Toast Message - Short Length
     *
     * @param s
     */
    public void ShowToast(String s) {

        if (s.length() > 0) {
            Toast.makeText(BaseActivity.this, s, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Check Customer inside Zone or not
     *
     * @return
     */
    public boolean CheckZoneArea(double mCurrentLat, double mCurrentLng) {
        ArrayList<LatLng> mZoneCoordList = new ArrayList<>();
        ArrayList<HashMap> hashMaps_zoneList = mHelper.GetZoneListFromDB();

        for (int j = 0; j < hashMaps_zoneList.size(); j++) {
            String mZoneList = hashMaps_zoneList.get(j).get("google_zone_name").toString();
            try {
                JSONArray itemArray = new JSONArray(mZoneList);
                for (int i = 0; i < itemArray.length(); i++) {
                    JSONObject objLatLng = itemArray.getJSONObject(i);
                    mZoneCoordList.add(new LatLng(Double.parseDouble(objLatLng.getString("lat")), Double.parseDouble(objLatLng.getString("lng"))));
                }
                boolean isInsideZone = PolyUtil.containsLocation(new LatLng(mCurrentLat, mCurrentLng), mZoneCoordList, false);
                if (isInsideZone) {
                    return true;
                }
                mZoneCoordList.clear();

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return false;
    }

    /**
     * Get Zone Id where user is located
     *
     * @param mCurrentLat
     * @param mCurrentLng
     * @return
     */
    public HashMap<String, String> GetZoneID(double mCurrentLat, double mCurrentLng, String ZoneType) {
        ArrayList<LatLng> mZoneCoordList = new ArrayList<>();
        ArrayList<HashMap> hashMaps_zoneList;

        if (ZoneType.equalsIgnoreCase("AllZone")) {
            hashMaps_zoneList = mHelper.GetZoneListFromDB();
        } else {
            hashMaps_zoneList = mHelper.GetDefaultZoneFromDB();
        }

        for (int j = 0; j < hashMaps_zoneList.size(); j++) {
            String mZoneList = hashMaps_zoneList.get(j).get("google_zone_name").toString();
            try {
                JSONArray itemArray = new JSONArray(mZoneList);
                for (int i = 0; i < itemArray.length(); i++) {
                    JSONObject objLatLng = itemArray.getJSONObject(i);
                    mZoneCoordList.add(new LatLng(Double.parseDouble(objLatLng.getString("lat")), Double.parseDouble(objLatLng.getString("lng"))));
                }
                boolean isInsideZone = PolyUtil.containsLocation(new LatLng(mCurrentLat, mCurrentLng), mZoneCoordList, false);
                if (isInsideZone) {
                    HashMap<String, String> hashMap_zone = new HashMap<>();
                    hashMap_zone.put("ZoneId", hashMaps_zoneList.get(j).get("zone_id").toString());
                    return hashMap_zone;
                }
                mZoneCoordList.clear();

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        HashMap<String, String> hashMap_zone = new HashMap<>();
        hashMap_zone.put("ZoneId", hashMaps_zoneList.get(0).get("zone_id").toString());
        return hashMap_zone;
    }

    /**
     * Double Round
     */
    public double roundTwoDecimals(double d) {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return Double.valueOf(twoDForm.format(d));
    }

    public long dateDifference(Date startDate, Date endDate) {

        //milliseconds
        long different = startDate.getTime() - endDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

//        long elapsedDays = different / daysInMilli;
//        different = different % daysInMilli;
//
//        long elapsedHours = different / hoursInMilli;
//        different = different % hoursInMilli;

//        long elapsedMinutes = different / minutesInMilli;
//        different = different % minutesInMilli;
//
//        long elapsedSeconds = different / secondsInMilli;
//
//        TimeUnit.MINUTES.toMillis(different);

        return different;
    }

    /**
     * Hide system keyboard on button click or on Activity load
     *
     * @param activity
     */
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public Dialog NoInternetDialog(String title, String message, Context context) {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.layout_dialog_popup);
        dialog.setTitle("");
        dialog.setCancelable(false);
        AppCompatTextView txt_popup_header = (AppCompatTextView) dialog.findViewById(R.id.txt_popup_header);
        AppCompatTextView txt_popup_message = (AppCompatTextView) dialog.findViewById(R.id.txt_popup_message);
        AppCompatButton btn_ok = (AppCompatButton) dialog.findViewById(R.id.btn_ok);

        txt_popup_header.setText(title);
        txt_popup_message.setText(message);


        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {

            }
        });
        dialog.show();
        return null;
    }

    /**
     * for select item from listview
     *
     * @param v
     */
    public void selectedServiceTypeListItem(View v) {
        LinearLayout parent = (LinearLayout) (v.getParent());
        CustomRegularFontTextView tv = (CustomRegularFontTextView) parent.findViewById(R.id.tvname);
        tv.setTextColor(Color.parseColor("#ffffff"));
        parent.findViewById(R.id.imgvehicle).setBackgroundResource(R.drawable.vehicle_background_withcolor);
        parent.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
    }

    /**
     * for deselect item from listview
     *
     * @param v
     */
    public void deSelectedServiceTypeListItem(View v) {
        LinearLayout parent = (LinearLayout) (v.getParent());
        CustomRegularFontTextView tv = (CustomRegularFontTextView) parent.findViewById(R.id.tvname);
        tv.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colortextNormal));
        parent.findViewById(R.id.imgvehicle).setBackgroundResource(R.drawable.vehicle_background);
        parent.setBackgroundColor(Color.TRANSPARENT);

    }

    /**
     * get Time diff in minute
     *
     * @return
     */
    public long getTimeDiff(String pickupDate) {

        //1 minute = 60 seconds
        //1 hour = 60 x 60 = 3600
        //1 day = 3600 x 24 = 86400

        try {
            // Date Formatter
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd h:mm a");

            // Get Current Date
            Calendar calendar = Calendar.getInstance();
            String nowDate = simpleDateFormat.format(calendar.getTime());

            Date Date1 = simpleDateFormat.parse(nowDate); // Now Date
            Date Date2 = simpleDateFormat.parse(pickupDate); // Pickup Date

            //milliseconds
            long differentMilliSec = Date2.getTime() - Date1.getTime();

            long differentTime = Date2.getTime() - Date1.getTime();
            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = differentTime / daysInMilli;
            differentTime = differentTime % daysInMilli;

            long elapsedHours = differentTime / hoursInMilli;
            differentTime = differentTime % hoursInMilli;

            long elapsedMinutes = differentTime / minutesInMilli;
            differentTime = differentTime % minutesInMilli;

            long elapsedSeconds = differentTime / secondsInMilli;

            return elapsedMinutes;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }


}
