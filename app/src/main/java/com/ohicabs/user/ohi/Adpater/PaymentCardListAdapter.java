package com.ohicabs.user.ohi.Adpater;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.PojoModel.CardModel;
import com.ohicabs.user.ohi.Utils.Global_Typeface;

import java.util.ArrayList;


public class PaymentCardListAdapter extends BaseAdapter {

    private Activity activity;
    int ResourceId;
    ArrayList<CardModel> _arr_CardModels;
    private Global_Typeface global_typeface;
    private BtnClickListener mClickListener = null;

    public PaymentCardListAdapter(Activity act, int resId, ArrayList<CardModel> cardModels, BtnClickListener listener) {
        this.activity = act;
        this.ResourceId = resId;
        this._arr_CardModels = cardModels;
        mClickListener = listener;

        global_typeface = new Global_Typeface(activity);
    }

    @Override
    public int getCount() {
        return _arr_CardModels.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        final ViewHolder viewHolder;
        if (itemView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(ResourceId, parent, false);

            viewHolder.txt_card_owner_name = (AppCompatTextView) itemView.findViewById(R.id.txt_card_owner_name);
            viewHolder.txt_cardnumber = (AppCompatTextView) itemView.findViewById(R.id.txt_cardnumber);
            viewHolder.txt_card_type = (AppCompatTextView) itemView.findViewById(R.id.txt_card_type);
            viewHolder.layout_payment_card = (RelativeLayout) itemView.findViewById(R.id.layout_payment_card);

            itemView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) itemView.getTag();
        }

        viewHolder.txt_card_owner_name.setText(_arr_CardModels.get(position).getCard_name());
        viewHolder.txt_cardnumber.setText("● ● ● ●  " + _arr_CardModels.get(position).getCard_number().toString());

        if (_arr_CardModels.get(position).getCard_type().equalsIgnoreCase("0")) {
            viewHolder.txt_card_type.setText(activity.getResources().getString(R.string.st_personal_card));
        } else {
            viewHolder.txt_card_type.setText(activity.getResources().getString(R.string.st_work_card));
        }

        viewHolder.txt_card_owner_name.setTypeface(global_typeface.Sansation_Bold());
        viewHolder.txt_cardnumber.setTypeface(global_typeface.Sansation_Regular());
        viewHolder.txt_card_type.setTypeface(global_typeface.Sansation_Regular());

        viewHolder.layout_payment_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null)
                    mClickListener.onBtnClick(position, _arr_CardModels.get(position).getCard_id(), "Delete");
            }
        });

        return itemView;
    }

    public class ViewHolder {
        AppCompatTextView txt_card_owner_name, txt_cardnumber, txt_card_type;
        RelativeLayout layout_payment_card;
    }

    public interface BtnClickListener {
        void onBtnClick(int position, String card_id, String opration_type);
    }
}
