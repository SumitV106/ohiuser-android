package com.ohicabs.user.ohi.Utils;

/**
 * Created by crayon on 19/09/17.
 */

public class Constant {
    public static final String DB_PATH = "/data/data/com.example.crayon.ohi/databases/";
    public static final String DB_NAME = "ohi_user.sqlite";

    public static final int REQUEST_PERMISSION = 5001;

    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP = 1995;
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE_DROPTO = 1996;

    // For default saved address
    public static final int REQUEST_CODE_PICKUP_ADDRESS = 1997;
    public static final int REQUEST_CODE_DROP_ADDRESS = 1998;


    //Request code for back to activity in Booking Activity
    public static final int BACK_TO_ACTIVITY = 1994;
    public static final int ADD_CARDS = 1993;


    public static final int UPDATE_INTERVAL = 20 * 1000;
    public static final int FASTEST_INTERVAL = 10 * 1000;

    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1990;
    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 1991;

    final public static String STATUS_PENDING = "0";
    final public static String STATUS_ACCEPT = "1";
    final public static String STATUS_GOTO_USER = "2";
    final public static String STATUS_USER_WAIT = "3";
    final public static String STATUS_START_TRIP = "4";
    final public static String STATUS_COMPLETE_TRIP = "5";
    final public static String STATUS_CANCEL_TRIP = "6";
    final public static String STATUS_AUTO_REJECT = "7";

    public static final int REQUEST_LOCATION_PERMISSION = 5001;
    public static final int RESULT_LOAD_IMG = 786;
    public static final int REQUEST_IMAGE_CAPTURE = 787;


    // Sinch SMS Verification Key
    public static final String APPLICATION_KEY = "3970ffe1-adba-424d-91e6-402b1b6f05b2";



    public static final int SUCCESS_RESULT = 0;

    public static final int FAILURE_RESULT = 1;

    public static final String PACKAGE_NAME =
            "com.ohicabs.user.ohi";

    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";

    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";

    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";

    /**
     * Booking Status constants
     */
    public class BOOKING_STATUS {
        public static final String BOOKING_PENDING = "0";
        public static final String BOOKING_ACCEPT = "1";
        public static final String BOOKING_USER_WAIT = "3";
        public static final String BOOKING_START = "4";
        public static final String BOOKING_COMPLETE = "5";
        public static final String BOOKING_CANCEL = "6";
        public static final String BOOKING_AUTO_REJECT = "7";
    }


    /**
     * Ride Type constants
     */
    public class RIDE_TYPE {
        public static final int REGULAR_RIDE = 1;
        public static final int RENTAL_RIDE = 2;
        public static final int OUTSTATION_RIDE = 3;
    }
}
