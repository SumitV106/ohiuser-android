package com.ohicabs.user.ohi.Activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.Utils.CircularImageView;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class RideAcceptActivity extends BaseActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "RideAcceptActivity";

    @BindView(R.id.img_profile)
    CircularImageView _img_profile;
    @BindView(R.id.img_ride_status)
    AppCompatImageView _img_ride_status;
    @BindView(R.id.img_car_type)
    AppCompatImageView _img_car_type;

    @BindView(R.id.txt_ride_status)
    AppCompatTextView _txt_ride_status;
    @BindView(R.id.txt_ride_date_time)
    AppCompatTextView _txt_ride_date_time;
    @BindView(R.id.txt_ride_id)
    AppCompatTextView _txt_ride_id;

    @BindView(R.id.txt_car_type)
    AppCompatTextView _txt_car_type;
    @BindView(R.id.txt_driver_name)
    AppCompatTextView _txt_driver_name;
    @BindView(R.id.txt_driver_mobile_no)
    AppCompatTextView _txt_driver_mobile_no;
    @BindView(R.id.txt_driver_car_number)
    AppCompatTextView _txt_driver_car_number;

    @BindView(R.id.txt_ride_price)
    AppCompatTextView _txt_ride_price;
    @BindView(R.id.txt_ride_distance)
    AppCompatTextView _txt_ride_distance;
    @BindView(R.id.txt_ride_duration)
    AppCompatTextView _txt_ride_duration;


    @BindView(R.id.txt_from_address)
    AppCompatTextView _txt_from_address;
    @BindView(R.id.txt_to_address)
    AppCompatTextView _txt_to_address;

    @BindView(R.id.txt_call)
    AppCompatTextView _txt_call;
    @BindView(R.id.txt_cancel_ride)
    AppCompatTextView _txt_cancel_ride;
    @BindView(R.id.txt_support)
    AppCompatTextView _txt_support;

    @BindView(R.id.layout_cancel_ride)
    LinearLayout _layout_cancel_ride;
    @BindView(R.id.layout_call)
    LinearLayout _layout_call;
    @BindView(R.id.layout_support)
    LinearLayout _layout_support;

    @BindView(R.id.img_call)
    AppCompatImageView _img_call;
    @BindView(R.id.img_cancel)
    AppCompatImageView _img_cancel;
    @BindView(R.id.img_support)
    AppCompatImageView _img_support;

    @BindView(R.id.txt_ride_message)
    AppCompatTextView _txt_ride_message;

    @BindView(R.id.layout_address)
    RelativeLayout _layout_address;
    @BindView(R.id.layout_driver_info)
    RelativeLayout _layout_driver_info;
    @BindView(R.id.layout_ride_time_details)
    RelativeLayout _layout_ride_time_details;
    @BindView(R.id.layout_ride_price)
    RelativeLayout _layout_ride_price;
    @BindView(R.id.view_toll_tip)
    View view_toll_tip;

    @BindView(R.id.view_ride_details)
    View view_ride_details;

    @BindView(R.id.txt_driver_delay_time)
    AppCompatTextView _txt_driver_delay_time;
    @BindView(R.id.layout_driver_name)
    LinearLayout _layout_driver_name;
    @BindView(R.id.layout_driver_call)
    LinearLayout _layout_driver_call;
    @BindView(R.id.layout_driver_vehical_no)
    LinearLayout _layout_driver_vehical_no;

    @BindView(R.id.txt_ride_toll_price)
    AppCompatTextView _txt_ride_toll_price;
    @BindView(R.id.txt_ride_tip_price)
    AppCompatTextView _txt_ride_tip_price;

    @BindView(R.id.tv_pickup_location)
    AppCompatTextView _tv_pickup_location;
    @BindView(R.id.tv_drop_location)
    AppCompatTextView _tv_drop_location;

    @BindView(R.id.txt_header)
    AppCompatTextView _txt_header;
    @BindView(R.id.img_back)
    AppCompatImageView _img_back;

    SimpleDateFormat date_format;
    String stDriverId, stBooking_Status, stBookingID, stDriverContactNumber, stBookingId, isTaxiRide;

    private SupportMapFragment mSupportMapFragment;
    private GoogleMap mGoogleMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    public double mPickUpLat = 0.0, mPickUpLng = 0.0, mDropLat = 0.0, mDropLng = 0.0, mDriverLat = 0.0, mDriverLng = 0.0;
    private Marker mMarker_user, mMarker_pickup, mMarker_drop, mMarker_driver;
    Location prevLoc, newLoc;

    private ArrayList<Marker> _arr_Markers_Pickup = new ArrayList<>();
    public ArrayList<LatLng> routeArray = new ArrayList<LatLng>();
    ArrayList<HashMap> _arrPriceList;
    public PolylineOptions lineOptions = null;
    private static final String FORMAT = "%02d:%02d";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_accept);
        ButterKnife.bind(this);

        stBookingId = getIntent().getStringExtra("booking_id");
        // Log.e("stBookingId",stBookingId);
        //BookingDetails();
        setContent();
    }


    private void BookingDetails() {
        Log.e("stBookingId--", stBookingId);
        if (isInternetOn(getApplicationContext())) {
            Call<JsonObject> call = apiService.booking_detail(stBookingId, "1");
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    Log.e("response", response.body().toString());
                    GetRideBookingDetails(response.body().toString());
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }

    private void setContent() {

        _txt_header.setTypeface(global_typeface.Sansation_Regular());
        _txt_header.setText(getResources().getString(R.string.ride_details));
        _img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    /* Receiver Call From Dialog Activity */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_DetailsFinishActivity,
                new IntentFilter("Details_Activity_finish"));

        lineOptions = new PolylineOptions();
        BookingDetails();

        if (getIntent().hasExtra("new_request_accepted")) {
            toastDialog.showAlertDialog(getResources().getString(R.string.st_request_accepted_title), getResources().getString(R.string.st_request_accepted_msg));
        }

        mSupportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.google_map);
        mSupportMapFragment.getMapAsync(this);


        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }

        String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        date_format = new SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.getDefault());

        _txt_ride_status.setTypeface(global_typeface.Sansation_Bold());
        _txt_ride_date_time.setTypeface(global_typeface.Sansation_Regular());
        _txt_ride_id.setTypeface(global_typeface.Sansation_Regular());

        _txt_car_type.setTypeface(global_typeface.Sansation_Regular());
        _txt_driver_name.setTypeface(global_typeface.Sansation_Regular());
        _txt_driver_mobile_no.setTypeface(global_typeface.Sansation_Regular());
        _txt_driver_car_number.setTypeface(global_typeface.Sansation_Regular());

        _txt_ride_price.setTypeface(global_typeface.Sansation_Regular());
        _txt_ride_distance.setTypeface(global_typeface.Sansation_Regular());
        _txt_ride_duration.setTypeface(global_typeface.Sansation_Regular());

        _txt_from_address.setTypeface(global_typeface.Sansation_Regular());
        _txt_to_address.setTypeface(global_typeface.Sansation_Regular());

        _txt_call.setTypeface(global_typeface.Sansation_Regular());
        _txt_cancel_ride.setTypeface(global_typeface.Sansation_Regular());
        _txt_support.setTypeface(global_typeface.Sansation_Regular());

        _txt_ride_toll_price.setTypeface(global_typeface.Sansation_Regular());
        _txt_ride_tip_price.setTypeface(global_typeface.Sansation_Regular());

        _txt_ride_message.setTypeface(global_typeface.Sansation_Regular());
        _txt_driver_delay_time.setTypeface(global_typeface.Sansation_Regular());


        _layout_cancel_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetOn(getApplicationContext())) {

                    try {
                        Call<JsonObject> call = apiService.user_ride_cancel(stBookingID, preferencesUtility.getuser_id(), stBooking_Status);
                        call.enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                String mResult = response.body().toString();
                                Log.e("TAG_Cancel_Booking_Data", mResult);
                                try {
                                    JSONObject objCancelData = new JSONObject(mResult);
                                    if (objCancelData.getString("status").equalsIgnoreCase("1")) {
                                        toastDialog.ShowToastMessage(objCancelData.getString("message"));
                                        finish();
                                    } else {
                                        toastDialog.ShowToastMessage(objCancelData.getString("message"));
                                    }
                                } catch (Exception e) {

                                }

                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {

                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                }
            }
        });

        _layout_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallToDriver(stDriverContactNumber);
            }
        });

        _layout_support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent _intent = new Intent(RideAcceptActivity.this, UserDriverSupportActivity.class);
                _intent.putExtra("driverID", stDriverId);
                startActivity(_intent);
            }
        });

        //Drop Address
        _txt_to_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mDropLat + "," + mDropLng + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //Pickup Address
        _txt_from_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mPickUpLat + "," + mPickUpLng + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        _tv_pickup_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mPickUpLat + "," + mPickUpLng + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        _tv_drop_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mDropLat + "," + mDropLng + "&mode=d");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mGoogleMap = map;

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
        mGoogleMap.getUiSettings().setCompassEnabled(false);
        mGoogleMap.getUiSettings().setTiltGesturesEnabled(false);
        mGoogleMap.setBuildingsEnabled(false);

        //Disable Marker click event
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
                return;
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            ShowToast(getResources().getString(R.string.network_disconnect));
        } else if (i == CAUSE_NETWORK_LOST) {
            ShowToast(getResources().getString(R.string.network_lost));
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("TAG", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    private void CallToDriver(String ValeMobile) {
        try {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {

                Intent intent_call = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ValeMobile));
                startActivity(intent_call);
            }
        } catch (Exception ex) {
            toastDialog.ShowToastMessage(getResources().getString(R.string.call_error));
            ex.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    /* Ride Tracking */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_UserRideTracking,
                new IntentFilter("User_Ride_Tracking"));

    /* User Wait for Driver on method */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_UserWaitForDriver,
                new IntentFilter("User_Wait_For_Driver_Data"));

    /*This Call Receive  when Ride Request Acceptted*/
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_DriverAcceptUserRequest,
                new IntentFilter("User_Ride_Request"));

      /* This Call Receive After User Give a Review to Driver For Finish This Activity */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_ReviewFinishActivity,
                new IntentFilter("Review_Activity_finish"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    /* Ride Tracking */
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_UserRideTracking);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_UserWaitForDriver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_DriverAcceptUserRequest);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_DetailsFinishActivity);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_ReviewFinishActivity);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Finish This Activity After Giving Review & Tips
     */
    private BroadcastReceiver mMessageReceiver_ReviewFinishActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };

    /**
     * Receiver Call From Dialog Activity
     * <p>
     * Finish Detail Activity on Cancel Request
     */
    private BroadcastReceiver mMessageReceiver_DetailsFinishActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };

    /**
     * User Wait Timer for Driver
     */
    private BroadcastReceiver mMessageReceiver_UserWaitForDriver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_UserWaitDriver = intent.getStringExtra("user_wait_driver_data");
            try {
                JSONObject objResult = new JSONObject(mResult_UserWaitDriver);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                if (objResult.getString("status").equalsIgnoreCase("1")) {
                    JSONArray arrPayload = objResult.getJSONArray("payload");
                    if (arrPayload.length() > 0) {
                        JSONObject objData = arrPayload.getJSONObject(0);
                        preferencesUtility.setBookingIdOnRide(objData.getString("booking_id"));
                        preferencesUtility.setBookingStatusOnRide(objData.getString("booking_status"));
                        int wait_time = objData.getInt("waiting");

                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.SECOND, wait_time);
                        String formattedDate = df.format(calendar.getTime());
                        preferencesUtility.setDriverWaitTime(formattedDate);
                    }
                    // Start Time Tracker
                    DriverWaitCountDown();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    /**
     * Request Accept Timer
     */
    private BroadcastReceiver mMessageReceiver_DriverAcceptUserRequest = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_UserWaitDriver = intent.getStringExtra("user_wait_driver_data");
            try {
                JSONObject objResult = new JSONObject(mResult_UserWaitDriver);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                if (objResult.getString("status").equalsIgnoreCase("1")) {
                    JSONArray arrPayload = objResult.getJSONArray("payload");
                    if (arrPayload.length() > 0) {
                        JSONObject objData = arrPayload.getJSONObject(0);
                        preferencesUtility.setBookingIdOnRide(objData.getString("booking_id"));
                        preferencesUtility.setBookingStatusOnRide(objData.getString("booking_status"));
                        int wait_time = objData.getInt("waiting");

                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.SECOND, wait_time);
                        String formattedDate = df.format(calendar.getTime());
                        preferencesUtility.setDriverWaitTime(formattedDate);
                    }
                    // Start Time Tracker
                    DriverWaitCountDown();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    /**
     * User Ride Booking Tracking Receiver
     */
    private BroadcastReceiver mMessageReceiver_UserRideTracking = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_RideTracking = intent.getStringExtra("user_ride_track_data");
            Log.e("receiver_user_tracking", "Got message: " + mResult_RideTracking);
            prevLoc = new Location(LocationManager.GPS_PROVIDER);
            newLoc = new Location(LocationManager.GPS_PROVIDER);

            try {
                JSONObject objTracking = new JSONObject(mResult_RideTracking);
                if (objTracking.getString("status").equalsIgnoreCase("1")) {

                } else if (objTracking.getString("status").equalsIgnoreCase("2")) {
                    JSONObject objPayload = objTracking.getJSONObject("payload");
                    if (objPayload.getString("booking_id").equalsIgnoreCase(stBookingID)) {

                        if (objPayload.getString("booking_status").equalsIgnoreCase(Constant.STATUS_START_TRIP)) {

                            _txt_ride_distance.setText(roundTwoDecimals(Double.parseDouble(objPayload.getString("total_distance"))) + " " + getResources().getString(R.string.st_km));
                            _txt_ride_duration.setText(objPayload.getString("duration") + " min");

                            if (_arrPriceList.size() > 0) {
                                double TotalAmnt = Double.parseDouble(_arrPriceList.get(0).get("base_charge").toString()) +
                                        (Double.parseDouble(_arrPriceList.get(0).get("base_charge").toString()) * Double.parseDouble(objPayload.getString("total_distance").toString())) +
                                        (Double.parseDouble(_arrPriceList.get(0).get("per_minute_charge").toString()) * Double.parseDouble(objPayload.getString("total_minutes").toString()));

                                if (Double.parseDouble(_arrPriceList.get(0).get("minimum_fair").toString()) > TotalAmnt) {
                                    TotalAmnt = Double.parseDouble(_arrPriceList.get(0).get("minimum_fair").toString());
                                }

                                TotalAmnt = TotalAmnt + Double.parseDouble(_arrPriceList.get(0).get("booking_charge").toString());

                                _txt_ride_price.setText(String.valueOf(roundTwoDecimals(TotalAmnt)) + " ₹");

                                if (isTaxiRide.equalsIgnoreCase("0")) {
                                    _txt_ride_price.setText(String.valueOf(roundTwoDecimals(TotalAmnt)) + " ₹");
                                    _txt_ride_price.setVisibility(View.VISIBLE);
                                } else {
                                    _txt_ride_price.setVisibility(View.INVISIBLE);
                                }
                            }
                        }

                        mDriverLat = Double.parseDouble(objPayload.getString("latitude"));
                        mDriverLng = Double.parseDouble(objPayload.getString("longitude"));

                        if (mMarker_driver == null) {
                            mMarker_driver = mGoogleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(mDriverLat, mDriverLng))
                                    .title(getResources().getString(R.string.st_driver_location))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_point)));
                        } else {
                            prevLoc.setLatitude(mMarker_driver.getPosition().latitude);
                            prevLoc.setLongitude(mMarker_driver.getPosition().longitude);

                            mMarker_driver.setPosition(new LatLng(mDriverLat, mDriverLng));

                            newLoc.setLatitude(mMarker_driver.getPosition().latitude);
                            newLoc.setLongitude(mMarker_driver.getPosition().longitude);

                            float bearing = prevLoc.bearingTo(newLoc);
                            mMarker_driver.setRotation(bearing);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Booking Details Parse JSON
     *
     * @param mResult
     */
    private void GetRideBookingDetails(String mResult) {
        Log.e("mResult", mResult);
        try {
            JSONObject objBookingList = new JSONObject(mResult);
            if (objBookingList.getString("status").equalsIgnoreCase("1")) {
                JSONArray jarrPayload = objBookingList.getJSONArray("payload");
                if (jarrPayload.length() > 0) {
                    for (int i = 0; i < jarrPayload.length(); i++) {
                        JSONObject objBookingData = jarrPayload.getJSONObject(i);

                        stDriverId = objBookingData.getString("driver_id");
                        stBooking_Status = objBookingData.getString("booking_status");
                        stBookingID = objBookingData.getString("booking_id");
                        isTaxiRide = objBookingData.getString("is_taxi");

                        _txt_ride_id.setText("#" + objBookingData.getString("booking_id"));
                        _txt_from_address.setText(objBookingData.getString("pickup_address"));
                        _txt_to_address.setText(objBookingData.getString("drop_address"));
                        _txt_car_type.setText(objBookingData.getString("service_name"));

                        _txt_driver_name.setText(objBookingData.getString("firstname") + " " + objBookingData.getString("lastname"));
                        _txt_driver_mobile_no.setText(objBookingData.getString("mobile"));
                        _txt_driver_car_number.setText(objBookingData.getString("car_number"));
                        stDriverContactNumber = objBookingData.getString("mobile");

                    /* Check Is Taxi Ride or Normal Ride */
                        if (isTaxiRide.equalsIgnoreCase("0")) {
                            _txt_ride_price.setText("₹ " + objBookingData.getString("amount"));
                            _txt_ride_price.setVisibility(View.VISIBLE);
                        } else {
                            _txt_ride_price.setVisibility(View.INVISIBLE);
                        }

                        _txt_ride_duration.setText(objBookingData.getString("duration") + " " + getResources().getString(R.string.st_minute));
                        _txt_ride_distance.setText(roundTwoDecimals(Double.parseDouble(objBookingData.getString("total_distance"))) + " " + getResources().getString(R.string.st_km));

                        if (objBookingData.getString("toll_tax").equalsIgnoreCase("")) {
                            _txt_ride_toll_price.setText(getResources().getString(R.string.st_toll) + ": ₹0.00");
                        } else {
                            _txt_ride_toll_price.setText(getResources().getString(R.string.st_toll) + ": ₹" + objBookingData.getString("toll_tax"));
                        }

                        if (objBookingData.getString("tip_amount").equalsIgnoreCase("")) {
                            _txt_ride_tip_price.setText(getResources().getString(R.string.st_tip) + ": ₹0.00");
                        } else {
                            _txt_ride_tip_price.setText(getResources().getString(R.string.st_tip) + ": ₹" + objBookingData.getString("tip_amount"));
                        }

                        Glide.with(this).load(Global_ServiceApi.API_IMAGE_HOST + objBookingData.getString("icon"))
                                .crossFade()
                                .placeholder(R.drawable.ic_car_ride)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(_img_car_type);

                        // Log.e("User Image",Global_ServiceApi.API_IMAGE_HOST+ objBookingData.getString("image"));
                        Glide.with(this).load(Global_ServiceApi.API_IMAGE_HOST + objBookingData.getString("image"))
                                .crossFade()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(_img_profile);

                    /* Price Data */
                        _arrPriceList = mHelper.GetPriceData(objBookingData.getString("price_id"));

                        if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_PENDING)) {
                            //Ride  Pending
                            _img_call.setEnabled(false);
                            _txt_call.setEnabled(false);
                            _img_call.setAlpha(0.2f);
                            _txt_call.setAlpha(0.2f);
                            _layout_call.setEnabled(true);

                            _img_support.setEnabled(false);
                            _img_support.setAlpha(0.2f);
                            _txt_support.setAlpha(0.2f);
                            _layout_support.setEnabled(false);

                            _img_cancel.setEnabled(true);
                            _txt_cancel_ride.setEnabled(true);
                            _layout_cancel_ride.setEnabled(false);

                            _txt_ride_message.setVisibility(View.VISIBLE);
                            _layout_driver_name.setVisibility(View.GONE);
                            _layout_driver_call.setVisibility(View.GONE);
                            _layout_driver_vehical_no.setVisibility(View.GONE);
                            view_ride_details.setVisibility(View.GONE);
                            _layout_ride_time_details.setVisibility(View.GONE);
                            _layout_ride_price.setVisibility(View.GONE);
                            view_toll_tip.setVisibility(View.GONE);

                            _img_ride_status.setImageResource(R.drawable.ic_pending);
                            _txt_ride_status.setText(getResources().getString(R.string.st_ride_pending));

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("pickup_date"));
                            df.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat(" HH:mm a  dd-MM-yyyy", Locale.getDefault());

                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString().toLowerCase());

                        } else if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_ACCEPT)) {
                            //Ride Accepted
                            _txt_ride_message.setVisibility(View.GONE);
                            _layout_ride_time_details.setVisibility(View.GONE);
                            view_ride_details.setVisibility(View.GONE);
                            _layout_ride_price.setVisibility(View.GONE);
                            view_toll_tip.setVisibility(View.GONE);

                            _img_cancel.setEnabled(true);
                            _txt_cancel_ride.setEnabled(true);
                            _img_call.setEnabled(false);
                            _txt_call.setEnabled(false);
                            _layout_call.setEnabled(false);
                            _img_call.setAlpha(0.2f);
                            _txt_call.setAlpha(0.2f);

                            _img_ride_status.setImageResource(R.drawable.ic_accepted);
                            _txt_ride_status.setText(getResources().getString(R.string.st_ride_accept));

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("accept_time"));
                            df.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat(" HH:mm a  dd-MM-yyyy", Locale.getDefault());

                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString().toLowerCase());
                            // Start Time Tracker
                            DriverWaitCountDown();

                        } else if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_GOTO_USER)) {
                            // Go to User
                            _img_call.setEnabled(true);
                            _img_cancel.setEnabled(true);
                            _txt_call.setEnabled(true);
                            _txt_cancel_ride.setEnabled(true);
                            _txt_ride_message.setVisibility(View.GONE);
                            _layout_cancel_ride.setEnabled(true);

                            _layout_ride_time_details.setVisibility(View.GONE);
                            view_ride_details.setVisibility(View.GONE);
                            _layout_ride_price.setVisibility(View.GONE);
                            view_toll_tip.setVisibility(View.GONE);

                            _img_ride_status.setImageResource(R.drawable.ic_go_user);
                            _txt_ride_status.setText(getResources().getString(R.string.st_go_to_user));

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("start_time"));
                            df.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat(" HH:mm a  dd-MM-yyyy", Locale.getDefault());

                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString().toLowerCase());
                            // Start Time Tracker
                            DriverWaitCountDown();

                        } else if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_USER_WAIT)) {
                            // Go to User Wait
                            _img_call.setEnabled(true);
                            _img_cancel.setEnabled(true);
                            _txt_call.setEnabled(true);
                            _txt_cancel_ride.setEnabled(true);
                            _txt_ride_message.setVisibility(View.GONE);
                            _layout_cancel_ride.setEnabled(true);

                            _layout_ride_time_details.setVisibility(View.GONE);
                            view_ride_details.setVisibility(View.GONE);
                            _layout_ride_price.setVisibility(View.GONE);
                            view_toll_tip.setVisibility(View.GONE);

                            _img_ride_status.setImageResource(R.drawable.ic_go_user);
                            _txt_ride_status.setText(getResources().getString(R.string.st_driver_has_arrived));

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("start_time"));
                            df.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat(" HH:mm a  dd-MM-yyyy", Locale.getDefault());

                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString().toLowerCase());

                            DriverWaitCountDown();

                        } else if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_START_TRIP)) {
                            //Ride Start
                            _img_call.setEnabled(true);
                            _txt_call.setEnabled(true);
                            _txt_ride_message.setVisibility(View.GONE);

                            _layout_cancel_ride.setEnabled(false);
                            _img_cancel.setAlpha(0.2f);
                            _txt_cancel_ride.setAlpha(0.2f);
                            _img_cancel.setEnabled(false);
                            _txt_cancel_ride.setEnabled(false);

                            _layout_ride_price.setVisibility(View.GONE);
                            view_toll_tip.setVisibility(View.GONE);

                            _img_ride_status.setImageResource(R.drawable.ic_started);
                            _txt_ride_status.setText(getResources().getString(R.string.st_ride_start));

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("start_time"));
                            df.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat(" HH:mm a  dd-MM-yyyy", Locale.getDefault());

                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString().toLowerCase());


                        } else if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_COMPLETE_TRIP)) {
                            //Ride Complete
                            _img_cancel.setEnabled(false);
                            _img_call.setEnabled(false);
                            _txt_call.setEnabled(false);
                            _txt_cancel_ride.setEnabled(false);

                            _layout_cancel_ride.setEnabled(false);
                            _layout_call.setEnabled(false);
                            _layout_support.setEnabled(false);

                            _img_cancel.setAlpha(0.2f);
                            _img_support.setAlpha(0.2f);
                            _img_call.setAlpha(0.2f);

                            _txt_cancel_ride.setAlpha(0.2f);
                            _txt_call.setAlpha(0.2f);
                            _txt_support.setAlpha(0.2f);

                            _txt_ride_message.setVisibility(View.GONE);
                            _layout_ride_price.setVisibility(View.VISIBLE);
                            view_toll_tip.setVisibility(View.VISIBLE);

                            _img_ride_status.setImageResource(R.drawable.ic_completed);
                            _txt_ride_status.setText(getResources().getString(R.string.st_ride_complete));

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("stop_time"));
                            df.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString());

                        /* Draw Path b/w pickup and drop location */
                            JSONArray _arrRouteLocation = objBookingData.getJSONArray("location");
                            if (_arrRouteLocation.length() > 0) {
                                for (int k = 0; k < _arrRouteLocation.length(); k++) {
                                    JSONObject objRoute = _arrRouteLocation.getJSONObject(k);
                                    routeArray.add(new LatLng(objRoute.getDouble("latitude"), objRoute.getDouble("longitude")));
                                }

                                lineOptions.addAll(routeArray);
                                lineOptions.color(Color.parseColor("#168E5A"));
                                lineOptions.width(7.0f);
                                mGoogleMap.addPolyline(lineOptions);
                            }

                        } else if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_CANCEL_TRIP)) {
                            //Ride Canceled
                            _img_cancel.setEnabled(false);
                            _img_call.setEnabled(false);
                            _txt_call.setEnabled(false);
                            _txt_cancel_ride.setEnabled(false);

                            _layout_cancel_ride.setEnabled(false);
                            _layout_call.setEnabled(false);
                            _layout_support.setEnabled(false);

                            _img_cancel.setAlpha(0.2f);
                            _img_support.setAlpha(0.2f);
                            _img_call.setAlpha(0.2f);

                            _txt_cancel_ride.setAlpha(0.2f);
                            _txt_call.setAlpha(0.2f);
                            _txt_support.setAlpha(0.2f);

                            _txt_ride_message.setVisibility(View.GONE);
                            _layout_driver_name.setVisibility(View.GONE);
                            _layout_driver_call.setVisibility(View.GONE);
                            _layout_driver_vehical_no.setVisibility(View.GONE);
                            _layout_ride_price.setVisibility(View.GONE);
                            view_toll_tip.setVisibility(View.GONE);

                            _txt_ride_price.setText("₹ 0");
                            _txt_ride_distance.setText("0 " + getResources().getString(R.string.st_km));
                            _txt_ride_duration.setText("0 " + getResources().getString(R.string.st_minute));

                            _img_ride_status.setImageResource(R.drawable.ic_cancelled);
                            _txt_ride_status.setText(getResources().getString(R.string.st_ride_cancel));

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("stop_time"));
                            df.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat(" HH:mm a  dd-MM-yyyy", Locale.getDefault());

                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString().toLowerCase());

                        } else if (objBookingData.getString("booking_status").equalsIgnoreCase(Constant.STATUS_AUTO_REJECT)) {
                            //Ride Canceled Auto Reject- Driver not found
                            _img_cancel.setEnabled(false);
                            _img_call.setEnabled(false);
                            _txt_call.setEnabled(false);
                            _txt_cancel_ride.setEnabled(false);

                            _layout_cancel_ride.setEnabled(false);
                            _layout_call.setEnabled(false);
                            _layout_support.setEnabled(false);

                            _img_cancel.setAlpha(0.2f);
                            _img_support.setAlpha(0.2f);
                            _img_call.setAlpha(0.2f);

                            _txt_cancel_ride.setAlpha(0.2f);
                            _txt_call.setAlpha(0.2f);
                            _txt_support.setAlpha(0.2f);

                            _txt_ride_message.setVisibility(View.GONE);
                            _layout_driver_name.setVisibility(View.GONE);
                            _layout_driver_call.setVisibility(View.GONE);
                            _layout_driver_vehical_no.setVisibility(View.GONE);
                            _layout_ride_price.setVisibility(View.GONE);
                            view_toll_tip.setVisibility(View.GONE);

                            _txt_ride_price.setText("₹ 0");
                            _txt_ride_distance.setText("0 " + getResources().getString(R.string.st_km));
                            _txt_ride_duration.setText("0 " + getResources().getString(R.string.st_minute));

                            _img_ride_status.setImageResource(R.drawable.ic_auto_rejected);
                            _txt_ride_status.setText("Driver Not Available");

                            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = df.parse(objBookingData.getString("stop_time"));
                            df.setTimeZone(TimeZone.getDefault());
                            //  SimpleDateFormat date_fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
                            SimpleDateFormat date_fmt = new SimpleDateFormat(" HH:mm a  dd-MM-yyyy", Locale.getDefault());

                            String _date = date_fmt.format(date);
                            _txt_ride_date_time.setText(_date.toString().toLowerCase());
                        }

                        // _txt_car_type.setTextColor(Color.parseColor("#" + objBookingData.getString("color")));
                        mPickUpLat = Double.parseDouble(objBookingData.getString("pickup_lati"));
                        mPickUpLng = Double.parseDouble(objBookingData.getString("pickup_longi"));

                        mDropLat = Double.parseDouble(objBookingData.getString("drop_lati"));
                        mDropLng = Double.parseDouble(objBookingData.getString("drop_longi"));

                        mMarker_pickup = mGoogleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(mPickUpLat, mPickUpLng))
                                .title(getResources().getString(R.string.st_pickup_location))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_point)));
                        _arr_Markers_Pickup.add(mMarker_pickup);

                        mMarker_drop = mGoogleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(mDropLat, mDropLng))
                                .title(getResources().getString(R.string.st_drop_location))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_point)));
                        _arr_Markers_Pickup.add(mMarker_drop);

                        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(mPickUpLat, mPickUpLng)).zoom(14.0f).build();
                        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                }

             /* Get Ride Tracking Details */
                if (stBooking_Status.equalsIgnoreCase("2") || stBooking_Status.equalsIgnoreCase("3")) {

                    Call<JsonObject> call = apiService.tracking(preferencesUtility.getuser_id(), stDriverId, stBookingID, stBooking_Status, "1");

                    call.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            Log.e("TAG_TRACK_POST", response.body().toString());

                            Intent intent = new Intent("User_Ride_Tracking");
                            intent.putExtra("user_ride_track_data", response.body().toString());
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {

                        }
                    });
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Driver Wait Time CountDown
     */
    private void DriverWaitCountDown() {
        try {
            Log.e("TAG_FINISH", preferencesUtility.getDriverWaitTime() + "==");

            if (!preferencesUtility.getDriverWaitTime().equalsIgnoreCase("")) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date ServerDateTime = df.parse(preferencesUtility.getDriverWaitTime());

                Calendar c = Calendar.getInstance();
                String currentDate = df.format(c.getTime());
                Date currentDateTime = df.parse(currentDate);
                long totalTime = dateDifference(ServerDateTime, currentDateTime);

                if (totalTime > 0) {
                    _txt_driver_delay_time.setVisibility(View.VISIBLE);

                    CountDownTimer cT = new CountDownTimer(totalTime, 1000) {
                        public void onTick(long millisUntilFinished) {

                            _txt_driver_delay_time.setText("" + String.format(FORMAT,
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                            TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),

                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                        }

                        public void onFinish() {
                            Log.e("TAG_FINISH", "finish");
                            _txt_driver_delay_time.setVisibility(View.GONE);
                        }
                    };
                    cT.start();

                } else {
                    Log.e("TAG_FINISH", "else");
                    _txt_driver_delay_time.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


