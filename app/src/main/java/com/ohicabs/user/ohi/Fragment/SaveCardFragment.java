package com.ohicabs.user.ohi.Fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.Application.OhiApplication;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.Global_Typeface;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.socket.client.Socket;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SaveCardFragment extends Fragment {


    public SaveCardFragment() {
        // Required empty public constructor
    }

    private Socket mSocket;
    OhiApplication appDelegate;
    private Animation fabOpenAnimation;
    private Animation fabCloseAnimation;
    private boolean isFabMenuOpen = false;
    private FloatingActionButton floatingActionButton;

    private ToastDialog toastDialog;
    private Global_Typeface global_typeface;
    private SharedPreferencesUtility sharedPreferencesUtility;

    private String selected_card_type, stCard;
//    @BindView(R.id.btn_save_card)
//    Button _btn_save_card;

    @BindView(R.id.edt_card_number)
    EditText _edt_card_number;

    @BindView(R.id.edt_card_owner)
    EditText _edt_card_owner;

    @BindView(R.id.edt_month_year)
    EditText _edt_month_year;

    @BindView(R.id.edt_card_cvv)
    EditText _edt_card_cvv;

    @BindView(R.id.rgroup_card_type)
    RadioGroup _rgroup_card_type;

    @BindView(R.id.rbtn_personal_card)
    RadioButton _rbtn_personal_card;

    @BindView(R.id.rbtn_work_card)
    RadioButton _rbtn_work_card;

    @BindView(R.id.btn_save_card)
    Button _btn_save_card;
    ApiInterface apiService;

    ProgressDialog progressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toastDialog = new ToastDialog(getActivity());
        global_typeface = new Global_Typeface(getActivity());
        sharedPreferencesUtility = new SharedPreferencesUtility(getActivity());
        apiService= ApiClient.getClient().create(ApiInterface.class);

//        appDelegate = (OhiApplication) getActivity().getApplication();
//        mSocket = appDelegate.getSocket();
//        mSocket.connect();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_save_card, container, false);
        ButterKnife.bind(this, view);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        View _view = getActivity().getCurrentFocus();
        if (_view == null) {
            _view = new View(getActivity());
        }
        imm.hideSoftInputFromWindow(_view.getWindowToken(), 0);

        return view;
    }
    public void  setContent(){
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        _rbtn_personal_card.setTypeface(global_typeface.Sansation_Regular());
        _rbtn_work_card.setTypeface(global_typeface.Sansation_Regular());
        _rgroup_card_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbtn_personal_card:
                        selected_card_type = "0";
                        break;
                    case R.id.rbtn_work_card:
                        selected_card_type = "1";
                        break;
                }
            }
        });

        _btn_save_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String message = IsValidate();
                    if (message.equalsIgnoreCase("true")) {
                        progressDialog.show();
                        JSONObject objCardData = new JSONObject();
                        objCardData.put("card_number", _edt_card_number.getText());
                        objCardData.put("card_name", _edt_card_owner.getText().toString());
                        objCardData.put("card_expiry_month", _edt_month_year.getText());
                        objCardData.put("card_expiry_year", _edt_month_year.getText());
                        objCardData.put("cvc", _edt_card_cvv.getText().toString());

                        Call<JsonObject> call = apiService.user_save_card(sharedPreferencesUtility.getuser_id(),
                                Global_ServiceApi.EncryptCard(objCardData.toString(),"user_save_card"),selected_card_type);
                        call.enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                progressDialog.dismiss();
                                try {
                                    String mResult = response.body().toString();
                                    Log.e("TAG_CArd", mResult);

                                    JSONObject objCardData = new JSONObject(mResult);
                                    if (objCardData.getString("status").equalsIgnoreCase("1")) {
                                        toastDialog.ShowToastMessage(objCardData.getString("message"));
                                        progressDialog.dismiss();
                                        _edt_card_owner.setText("");
                                        _edt_card_number.setText("");
                                        _edt_card_cvv.setText("");
                                        _edt_month_year.setText("");
                                        _rbtn_personal_card.setChecked(false);
                                        _rbtn_work_card.setChecked(false);

                                    } else {

                                        toastDialog.ShowToastMessage(objCardData.getString("message"));
                                    }
                                    //GetCardList();
                                } catch (Exception e) {
                                    progressDialog.dismiss();
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {

                            }
                        });

                        // }
                    } else {
                        toastDialog.ShowToastMessage(message);
                        progressDialog.dismiss();
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                }
            }
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

       setContent();

    }

    /**
     * Card Blank Field Validation
     *
     * @return
     */
    private String IsValidate() {
        if (_edt_card_owner.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_card_name);
        } else if (_edt_card_number.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_card_number);
        } else if (_edt_month_year.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_expiry_month);
        } else if (_edt_card_cvv.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_card_cvc);
        } else if (_rgroup_card_type.getCheckedRadioButtonId() == -1) {
            return getResources().getString(R.string.st_select_card);
        } else {
            return "true";
        }
    }
}
