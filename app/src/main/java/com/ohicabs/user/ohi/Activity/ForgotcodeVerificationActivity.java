package com.ohicabs.user.ohi.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.PinEntry.PinEntryView;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sumit on 10/04/17.
 */

public class ForgotcodeVerificationActivity extends BaseActivity {

    Bundle b_data;

    @BindView(R.id.btn_verify)
    Button _btnVerify;

    @BindView(R.id.btn_resend)
    Button btn_resend;

    @BindView(R.id.edt_pin_entry)
    PinEntryView _edtPinentry;

    @BindView(R.id.img_back)
    ImageView _img_back;

    @BindView(R.id.txt_varification)
    TextView _txt_varification;

    private String forgot_code, user_id;
    private boolean mShouldFallback = true;

    private static final String TAG = "VerificationActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_verification);
        ButterKnife.bind(this);

        setContent();
    }

    private void setContent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorBar
            ));
        }
        _btnVerify.setTypeface(global_typeface.Sansation_Bold());

        b_data = getIntent().getExtras();
        forgot_code = b_data.getString("forgot_code");
        user_id = b_data.getString("user_id");

        _txt_varification.setTypeface(global_typeface.Sansation_Bold());

        _btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = _edtPinentry.getText().toString();

                if (!code.isEmpty()) {

                    if (code.equals(forgot_code)) {
                        Intent i = new Intent(ForgotcodeVerificationActivity.this, NewPasswordActivity.class);
                        i.putExtra("forgot_code", forgot_code);
                        i.putExtra("user_id", user_id);
                        startActivity(i);
                        finish();
                    } else {
                        toastDialog.ShowToastMessage("Enter valid code");
                    }

                } else {
                    toastDialog.ShowToastMessage(getResources().getString(R.string.verification_validate));
                }
            }
        });

        btn_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                createVerification();
            }
        });

        _img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    private void SetVerificationData(String ActivationResult) {
        try {
            JSONObject objData = new JSONObject(ActivationResult);
            if (objData.getString("status").equalsIgnoreCase("1")) {
                JSONObject objPayload = objData.getJSONObject("payload");
                preferencesUtility.setuser_id(objPayload.getString("user_id"));
                preferencesUtility.setfirstname(objPayload.getString("firstname"));
                preferencesUtility.setlastname(objPayload.getString("lastname"));
                preferencesUtility.setemail(objPayload.getString("email"));
                preferencesUtility.setgender(objPayload.getString("gender"));
                preferencesUtility.setmobile(objPayload.getString("mobile"));
                preferencesUtility.setauth_token(objPayload.getString("auth_token"));
                preferencesUtility.setimage(objPayload.getString("image"));
                preferencesUtility.setrefer_code(objPayload.getString("refer_code"));
                preferencesUtility.setstatus(objPayload.getString("status"));

                preferencesUtility.setLogedin(true);
                preferencesUtility.setLoginType("true");

                Intent intent = new Intent(ForgotcodeVerificationActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                toastDialog.ShowToastMessage(objData.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Validate  Form
     *
     * @return true or false
     */
    private String IsValidate() {

        String code = _edtPinentry.getText().toString();
        if (code.trim().equals("")) {
            return "Enter Otp number";
        } else if (code.trim().length() != 4) {
            return "Enter valid Otp number";

        } else {
            return "true";
        }
    }
}
