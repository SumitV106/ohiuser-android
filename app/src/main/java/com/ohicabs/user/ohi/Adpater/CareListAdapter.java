package com.ohicabs.user.ohi.Adpater;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.Global_Typeface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;


public class CareListAdapter extends BaseAdapter {

    private Activity activity;
    int ResourceId;
    private ArrayList<HashMap> _arr_userBookedRides;
    private Global_Typeface global_typeface;
    private BtnClickListener mClickListener = null;

    public CareListAdapter(Activity act, int resId, ArrayList<HashMap> hashMapArrayList, BtnClickListener listener) {
        this.activity = act;
        this.ResourceId = resId;
        this._arr_userBookedRides = hashMapArrayList;
        mClickListener = listener;
        global_typeface = new Global_Typeface(activity);
    }

    @Override
    public int getCount() {
        return _arr_userBookedRides.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        final ViewHolder viewHolder;
        if (itemView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(ResourceId, parent, false);

            viewHolder.txt_mobile = (AppCompatTextView) itemView.findViewById(R.id.txt_mobile);
            viewHolder.txt_location = (AppCompatTextView) itemView.findViewById(R.id.txt_location);
            viewHolder.txtTime = (AppCompatTextView) itemView.findViewById(R.id.txt_time);
            viewHolder.txtLanguages = (AppCompatTextView) itemView.findViewById(R.id.txt_languages);

            itemView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) itemView.getTag();
        }

        viewHolder.txt_location.setText(_arr_userBookedRides.get(position).get("care_name").toString());
        viewHolder.txt_mobile.setText(_arr_userBookedRides.get(position).get("care_number").toString());
        viewHolder.txtTime.setText(_arr_userBookedRides.get(position).get("care_time").toString());
        viewHolder.txtLanguages.setText(_arr_userBookedRides.get(position).get("languages").toString());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return itemView;
    }

    public class ViewHolder {
        AppCompatTextView txt_mobile, txt_location, txtTime, txtLanguages;;
    }

    public interface BtnClickListener {
        void onBtnClick(int position, String ride_id, String ride_status);
    }

    public String convertDate(Date d) {
        SimpleDateFormat sdfAmerica = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");
        String actualTime = sdfAmerica.format(d);
        //Changed timezone
        TimeZone tzInAmerica = TimeZone.getTimeZone("GMT+10");
        sdfAmerica.setTimeZone(tzInAmerica);

        String convertedTime = sdfAmerica.format(d);
        System.out.println("actual : " + actualTime + "  converted " + convertedTime);

        return convertedTime;
    }
}
