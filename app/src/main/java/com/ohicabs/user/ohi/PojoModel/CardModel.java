package com.ohicabs.user.ohi.PojoModel;

/**
 * Created by sumit on 14/04/17.
 */

public class CardModel {

    String card_id, user_id, card_info, card_type, created_date, status, card_number, stripe_card_id, card_expiry_month, card_name, card_expiry_year;


    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCard_info() {
        return card_info;
    }

    public void setCard_info(String card_info) {
        this.card_info = card_info;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String geStripeCardID() {
        return stripe_card_id;
    }

    public void setStripeCardID(String stripe_card_id) {
        this.stripe_card_id = stripe_card_id;
    }

    public String getCard_expiry_month() {
        return card_expiry_month;
    }

    public void setCard_expiry_month(String card_expiry_month) {
        this.card_expiry_month = card_expiry_month;
    }

    public String getCard_name() {
        return card_name;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public String getCard_expiry_year() {
        return card_expiry_year;
    }

    public void setCard_expiry_year(String card_expiry_year) {
        this.card_expiry_year = card_expiry_year;
    }
}
