package com.ohicabs.user.ohi.Adpater;

import android.app.Activity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ohicabs.user.ohi.PojoModel.SavedAddressModel;
import com.ohicabs.user.ohi.R;

import java.util.ArrayList;
import java.util.HashMap;

public class SavedAddressAdapter extends RecyclerView.Adapter<SavedAddressAdapter.MyViewHolder>{

    private Activity mActivity;
    ArrayList<SavedAddressModel> arrSavedAddress;

    private BtnClickListener mClickListener = null;
    private DeleteLocationListener mDeleteLocationListener = null;

    public SavedAddressAdapter(Activity act, ArrayList<SavedAddressModel> listAddress, BtnClickListener btnClickListener, DeleteLocationListener deleteLocationListener) {
        this.mActivity = act;
        arrSavedAddress = listAddress;
        mClickListener = btnClickListener;
        mDeleteLocationListener = deleteLocationListener;
    }

    @Override
    public SavedAddressAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_address_book, parent, false);

        return new SavedAddressAdapter.MyViewHolder(itemView);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public AppCompatTextView txtAddressType, txtAddress;
        public RelativeLayout rlAddressBook;
        public AppCompatImageView imgDelete;

        public MyViewHolder(View view) {
            super(view);

            rlAddressBook = view.findViewById(R.id.rl_address_book);
            txtAddressType = view.findViewById(R.id.txt_address_type);
            txtAddress = view.findViewById(R.id.txt_address);
            imgDelete = view.findViewById(R.id.img_delete);
        }
    }

    @Override
    public void onBindViewHolder(SavedAddressAdapter.MyViewHolder holder, final int position) {

        holder.txtAddressType.setText(arrSavedAddress.get(position).getTagName());
        holder.txtAddress.setText(arrSavedAddress.get(position).getAddress());

        holder.rlAddressBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mClickListener != null) {
                    mClickListener.onBtnClick(position, arrSavedAddress.get(position).getLocationID(),
                            Double.parseDouble(arrSavedAddress.get(position).getLatitude()),
                            Double.parseDouble(arrSavedAddress.get(position).getLongitude()),
                            arrSavedAddress.get(position).getAddress());

                }
            }
        });

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDeleteLocationListener != null) {
                    mDeleteLocationListener.onDeleteClick(position, arrSavedAddress.get(position).getLocationID());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrSavedAddress.size();
    }

    public interface BtnClickListener {
        void onBtnClick(int position, String locationID, double latitude, double longitude, String address);
    }

    public interface DeleteLocationListener {
        void onDeleteClick(int position, String locationID);
    }

}
