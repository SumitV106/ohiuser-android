package com.ohicabs.user.ohi.Adpater;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.Constant;
import com.ohicabs.user.ohi.Utils.Global_Typeface;

import java.util.ArrayList;
import java.util.HashMap;


public class RouteAdapter extends BaseAdapter {

    private Activity activity;
    int ResourceId;
    private ArrayList<HashMap<String, String>> _arr_RouteList;
    private Global_Typeface global_typeface;
    private BtnClickListener mClickListener = null;

    public RouteAdapter(Activity act, int resId, ArrayList<HashMap<String, String>> hashMapArrayList, BtnClickListener listener) {
        this.activity = act;
        this.ResourceId = resId;
        this._arr_RouteList = hashMapArrayList;
        mClickListener = listener;

        global_typeface = new Global_Typeface(activity);
    }

    @Override
    public int getCount() {
        return _arr_RouteList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        final ViewHolder viewHolder;
        if (itemView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(ResourceId, parent, false);

            viewHolder.tv_msg = (AppCompatTextView) itemView.findViewById(R.id.tv_msg);
            viewHolder.img_line = (View) itemView.findViewById(R.id.img_line);
            viewHolder.img_circle = (ImageView) itemView.findViewById(R.id.img_circle);

            itemView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) itemView.getTag();
        }

        viewHolder.tv_msg.setText(_arr_RouteList.get(position).get("display_message").toString());

        if(position==0){
            viewHolder.img_line.setBackground(activity.getResources().getDrawable(R.drawable.rectangle_verticle_first));
        }
        else{
            viewHolder.img_line.setBackground(activity.getResources().getDrawable(R.drawable.rectangle_verticle));
        }

        if(_arr_RouteList.get(position).get("booking_status").equals(Constant.STATUS_START_TRIP)){
            viewHolder.img_circle.setBackground(activity.getResources().getDrawable(R.drawable.circle_orange_route));
        }
        else{
            viewHolder.img_circle.setBackground(activity.getResources().getDrawable(R.drawable.circle_green_route));
        }

        return itemView;
    }

    public class ViewHolder {
        AppCompatTextView tv_msg;
        View img_line;
        ImageView img_circle;
    }

    public interface BtnClickListener {
        void onBtnClick(int position, String ride_id);
    }
}
