package com.ohicabs.user.ohi.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.Adpater.RedemptionHistoryAdapter;
import com.ohicabs.user.ohi.PojoModel.RedemptionModel;
import com.ohicabs.user.ohi.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class RedemptionListActivity extends BaseActivity {

    @BindView(R.id.recyclerView_redemption) RecyclerView recyclerViewRedemption;
    @BindView(R.id.img_back) AppCompatImageView imgBack;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.btn_total_earning) AppCompatButton btnTotalEarning;

    private ArrayList<RedemptionModel> arrRedemptionList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redemption_list);
        ButterKnife.bind(this);

        init();
    }

    private void init() {

        recyclerViewRedemption.setItemAnimator(new DefaultItemAnimator());
        recyclerViewRedemption.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewRedemption.setLayoutManager(layoutManager);

        setViews();
    }

    private void setViews() {

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getRedemptionList();

    }

    /**
     *
     * get redemption list API Call
     *
     */
    private void getRedemptionList() {
        if (isInternetOn(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);

            try {
                Call<JsonObject> call = apiService.referral_redemption(preferencesUtility.getuser_id(), preferencesUtility.getauth_token());

                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        progressBar.setVisibility(View.GONE);
                        GetRedemptionList(response.body().toString());
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        progressBar.setVisibility(View.GONE);
                    }
                });
            } catch (Exception e) {
                progressBar.setVisibility(View.GONE);
                e.printStackTrace();
            }
        } else {
            progressBar.setVisibility(View.GONE);
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }

    /**
     * set redemption data list
     *
     * @param result
     */
    private void GetRedemptionList(String result) {

        try {

            JSONObject jsonObjectDetails = new JSONObject(result);

            if (jsonObjectDetails.optString("status").equalsIgnoreCase("1") &&
                    jsonObjectDetails.optString("success").equalsIgnoreCase("true")) {

                JSONObject jsonObjectPayload = jsonObjectDetails.getJSONObject("payload");
                JSONArray jsonArrayRedemption = jsonObjectPayload.getJSONArray("redemption_list");

                if (jsonArrayRedemption.length() > 0) {

                    for (int i = 0; i <jsonArrayRedemption.length(); i++) {
                        JSONObject jsonObjectData = jsonArrayRedemption.getJSONObject(i);

                        RedemptionModel redemptionModel = new RedemptionModel();

                        redemptionModel.setWalletId(jsonObjectData.optString("wallet_id"));
                        redemptionModel.setRedeemAmount(jsonObjectData.optString("point"));
                        redemptionModel.setExpiryTime(jsonObjectData.optString("date"));
                        redemptionModel.setIsExpired(jsonObjectData.optString("is_expired"));
                        redemptionModel.setInitiateDate(jsonObjectData.optString("initiate_date"));
                        redemptionModel.setPlanName(jsonObjectData.optString("plan_name"));
                        redemptionModel.setZoneName(jsonObjectData.optString("zone_name"));

                        arrRedemptionList.add(redemptionModel);
                    }


                    RedemptionHistoryAdapter redemptionHistoryAdapter = new RedemptionHistoryAdapter(RedemptionListActivity.this, arrRedemptionList, new RedemptionHistoryAdapter.BtnClickListener() {
                        @Override
                        public void onBtnClick(int position) {

                        }
                    });

                    recyclerViewRedemption.setAdapter(redemptionHistoryAdapter);

                } else {
                    toastDialog.ShowToastMessage("No Records Found!");
                }

                btnTotalEarning.setText(jsonObjectPayload.optString("redeemed_expired_point") + " Points");

            } else {

                toastDialog.ShowToastMessage(jsonObjectDetails.optString("message"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
