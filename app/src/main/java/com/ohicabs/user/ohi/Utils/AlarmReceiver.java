package com.ohicabs.user.ohi.Utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Activity.SplashActivity;

import java.util.Random;



public class AlarmReceiver extends BroadcastReceiver {

    int MID = 0;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub;
        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;

        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notificationIntent = new Intent(context, SplashActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, intent.getIntExtra(("OBJECT_ID"), 0), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(
                context).setSmallIcon(R.drawable.ohicabs_icon)
                .setContentTitle(intent.getStringExtra("title"))
                .setContentText(intent.getStringExtra("message")).setSound(alarmSound)
                .setAutoCancel(true).setWhen(when)
                .setContentIntent(pendingIntent)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});


        notificationManager.notify(m, mNotifyBuilder.build());
        MID++;
    }
}
//6 10, 6 12