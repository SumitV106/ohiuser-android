package com.ohicabs.user.ohi.Adpater;

import android.app.Activity;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.PojoModel.CreditHistoryModel;
import com.ohicabs.user.ohi.PojoModel.RedemptionModel;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.CircularImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class RedemptionHistoryAdapter extends RecyclerView.Adapter<RedemptionHistoryAdapter.MyViewHolder>{

    private Activity mActivity;
    private ArrayList<RedemptionModel> arrRedemptionList;

    private BtnClickListener mClickListener = null;

    public RedemptionHistoryAdapter(Activity act, ArrayList<RedemptionModel> redemptionModels, BtnClickListener btnClickListener) {
        this.mActivity = act;
        arrRedemptionList = redemptionModels;
        mClickListener = btnClickListener;
    }

    @Override
    public RedemptionHistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_redemption_list, parent, false);

        return new RedemptionHistoryAdapter.MyViewHolder(itemView);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public AppCompatTextView txtRedeemPoints, txtExpiryDate, txtPlanName, txtCity, txtInitiatedDate, txtExpireOn;

        public MyViewHolder(View view) {
            super(view);

            txtRedeemPoints = view.findViewById(R.id.txt_redeem_points);
            txtExpiryDate = view.findViewById(R.id.txt_expiry_date);
            txtPlanName = view.findViewById(R.id.txt_plan_name);
            txtCity = view.findViewById(R.id.txt_city);
            txtInitiatedDate = view.findViewById(R.id.txt_initiated_date);
            txtExpireOn = view.findViewById(R.id.txt_expire_on);

        }
    }

    @Override
    public void onBindViewHolder(RedemptionHistoryAdapter.MyViewHolder holder, final int position) {
        if (arrRedemptionList.get(position).getIsExpired().equalsIgnoreCase("0")) {
            holder.txtRedeemPoints.setText("Redeemed Points: "+ arrRedemptionList.get(position).getRedeemAmount());
            holder.txtInitiatedDate.setVisibility(View.GONE);
            holder.txtExpireOn.setVisibility(View.GONE);
            holder.txtCity.setVisibility(View.VISIBLE);
            holder.txtPlanName.setVisibility(View.VISIBLE);

        } else {
            holder.txtRedeemPoints.setText("Expired Points: "+arrRedemptionList.get(position).getRedeemAmount());
            holder.txtInitiatedDate.setVisibility(View.VISIBLE);
            holder.txtExpireOn.setVisibility(View.VISIBLE);
            holder.txtCity.setVisibility(View.GONE);
            holder.txtPlanName.setVisibility(View.GONE);
        }

        holder.txtExpiryDate.setText("Date: " + convertDate(arrRedemptionList.get(position).getExpiryTime()));

        // if is_expired = 1 then visible it
        holder.txtInitiatedDate.setText("Initiated On: " + convertDate(arrRedemptionList.get(position).getInitiateDate()));
        holder.txtExpireOn.setText("Expires On: " + convertDate(arrRedemptionList.get(position).getExpiryTime()));


        // if is_expired = 0 then visible it
        holder.txtPlanName.setText("Plan: " + arrRedemptionList.get(position).getPlanName());
        holder.txtCity.setText("City: " + arrRedemptionList.get(position).getZoneName());
    }

    @Override
    public int getItemCount() {
        return arrRedemptionList.size();
    }

    public interface BtnClickListener {
        void onBtnClick(int position);
    }

    private String convertDate(String stDate) {
        try {
            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = df.parse(stDate);
            df.setTimeZone(TimeZone.getDefault());
            SimpleDateFormat date_fmt = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            String _date = date_fmt.format(date);

            return _date;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
