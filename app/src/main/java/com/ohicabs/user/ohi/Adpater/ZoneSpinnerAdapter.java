package com.ohicabs.user.ohi.Adpater;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ohicabs.user.ohi.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by crayon on 31/01/18.
 */

public class ZoneSpinnerAdapter extends BaseAdapter {
    Activity activity;
    int ResouceID;
    ArrayList<HashMap> _arrPlanName;

    public ZoneSpinnerAdapter(Activity applicationContext, int resID, ArrayList<HashMap> planModelArrayList) {
        this.activity = applicationContext;
        this.ResouceID = resID;
        this._arrPlanName = planModelArrayList;
    }

    @Override
    public int getCount() {
        return _arrPlanName.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        final ViewHolder viewHolder;
        if (itemView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(ResouceID, parent, false);

            viewHolder.txt_zone_name = (TextView) itemView.findViewById(R.id.txt_cartypename);

            itemView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) itemView.getTag();
        }
        viewHolder.txt_zone_name.setText(_arrPlanName.get(position).get("zone_name").toString());
        viewHolder.txt_zone_name.setTag(_arrPlanName.get(position).get("zone_id").toString());
        return itemView;
    }

    public class ViewHolder {
        TextView txt_zone_name;
    }
}

