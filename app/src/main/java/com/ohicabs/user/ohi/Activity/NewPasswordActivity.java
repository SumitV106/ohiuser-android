package com.ohicabs.user.ohi.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.R;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class NewPasswordActivity extends BaseActivity {

    @BindView(R.id.edt_new_password) AppCompatEditText edt_new_password;

    @BindView(R.id.edt_confirm_password) AppCompatEditText edt_confirm_password;
    @BindView(R.id.btn_change_password) AppCompatButton btn_change_password;
    @BindView(R.id.img_back) ImageView _img_back;
    @BindView(R.id.txt_header) TextView _txt_header;
    @BindView(R.id.newpwdWrapper) TextInputLayout _newpwdWrapper;
    @BindView(R.id.confirmpwdWrapper) TextInputLayout _confirmpwdWrapper;

    Bundle b_data;
    private String forgot_code, user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);
        ButterKnife.bind(this);
        setContent();

    }

    private void setContent() {

        _txt_header.setTypeface(global_typeface.Sansation_Bold());
        edt_new_password.setTypeface(global_typeface.Sansation_Regular());
        edt_confirm_password.setTypeface(global_typeface.Sansation_Regular());

        _confirmpwdWrapper.setTypeface(global_typeface.Sansation_Regular());
        _newpwdWrapper.setTypeface(global_typeface.Sansation_Regular());

        b_data = getIntent().getExtras();
        forgot_code = b_data.getString("forgot_code");
        user_id = b_data.getString("user_id");

        btn_change_password.setTypeface(global_typeface.Sansation_Bold());

        btn_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(NewPasswordActivity.this);

                String stMessage = IsValidate();
                if (stMessage.equalsIgnoreCase("true")) {

                    if (isInternetOn(getApplicationContext())) {
                        progressDialog.show();
                        try {
                            Call<JsonObject> call = apiService.forgot_new_password(user_id, forgot_code,
                                    edt_new_password.getText().toString());
                            call.enqueue(new Callback<JsonObject>() {
                                @Override
                                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                    Log.e("new pwd response", response.body().toString());
                                    progressDialog.dismiss();
                                    try {
                                        JSONObject objResponse = new JSONObject(response.body().toString());
                                        if (objResponse.get("success").toString().equalsIgnoreCase("true")) {

                                            toastDialog.ShowToastMessage(objResponse.get("message").toString());
                                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            toastDialog.ShowToastMessage(objResponse.get("message").toString());
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(Call<JsonObject> call, Throwable t) {
                                    t.printStackTrace();
                                    progressDialog.dismiss();
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                    }
                } else {
                    toastDialog.ShowToastMessage(stMessage);
                }
            }
        });

        _img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(NewPasswordActivity.this);
                finish();
            }
        });

    }

    /**
     * Form Validation
     *
     * @return
     */
    private String IsValidate() {
        if (edt_new_password.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_enter_new_pass);

        } else if (edt_new_password.getText().toString().trim().length() < 6) {
            return getResources().getString(R.string.st_password_minimum);

        } else if (edt_confirm_password.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_confirm_pass);

        } else if (!checkPassWordAndConfirmPassword(edt_new_password.getText().toString(), edt_confirm_password.getText().toString())) {
            return getResources().getString(R.string.st_pass_no_match);

        } else {
            return "true";
        }
    }

    /**
     * Check Password and Confirm Password
     *
     * @param password
     * @param confirmPassword
     * @return
     */
    public boolean checkPassWordAndConfirmPassword(String password, String confirmPassword) {
        boolean pstatus = false;
        if (confirmPassword != null && password != null) {
            if (password.equals(confirmPassword)) {
                pstatus = true;
            }
        }
        return pstatus;
    }
}

