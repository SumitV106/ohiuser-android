package com.ohicabs.user.ohi.Utils;

import android.content.Context;
import android.graphics.Typeface;

public class Global_Typeface {

    public static Typeface Sansation_Bold,Sansation_Bold_Italic,Sansation_Italic,Sansation_Light,Sansation_Light_Italic,Sansation_Regular;
    public static Typeface Roboto_Light,Roboto_Medium;


    Context context;

    public Global_Typeface(Context cntx) {

        this.context = cntx;

        Sansation_Bold = Typeface.createFromAsset(context.getAssets(),
                "fonts/Sansation_Bold.ttf");

        Sansation_Bold_Italic = Typeface.createFromAsset(context.getAssets(),
                "fonts/Sansation_Bold_Italic.ttf");

        Sansation_Italic = Typeface.createFromAsset(context.getAssets(),
                "fonts/Sansation_Italic.ttf");

        Sansation_Light = Typeface.createFromAsset(context.getAssets(),
                "fonts/Sansation_Light.ttf");

        Sansation_Light_Italic = Typeface.createFromAsset(context.getAssets(),
                "fonts/Sansation_Light_Italic.ttf");

        Sansation_Regular = Typeface.createFromAsset(context.getAssets(),
                "fonts/Sansation_Regular.ttf");

        Roboto_Light=Typeface.createFromAsset(context.getAssets(),"fonts/Roboto-Light.ttf");

        Roboto_Medium=Typeface.createFromAsset(context.getAssets(),"fonts/Roboto-Medium.ttf");

    }

    public Typeface Sansation_Bold() {
        return Sansation_Bold;
    }

    public Typeface Sansation_Bold_Italic() {
        return Sansation_Bold_Italic;
    }

    public Typeface Sansation_Italic() {
        return Sansation_Italic;
    }

    public Typeface Sansation_Light() {
        return Sansation_Light;
    }

    public Typeface Sansation_Light_Italic() {
        return Sansation_Light_Italic;
    }

    public Typeface Sansation_Regular() {
        return Sansation_Regular;
    }

    public Typeface Roboto_Light() {
        return Roboto_Light;
    }


    public Typeface Roboto_Medium() {
        return Roboto_Medium;
    }
}
