package com.ohicabs.user.ohi.Fragment;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.OvershootInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Adpater.PaymentCardListAdapter;
import com.ohicabs.user.ohi.PojoModel.CardModel;
import com.ohicabs.user.ohi.Utils.ExpandableHeightListView;
import com.ohicabs.user.ohi.Utils.Global_Typeface;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddCardFragment extends Fragment {


    public AddCardFragment() {
        // Required empty public constructor
    }

    private boolean isFabMenuOpen = false;
    private FloatingActionButton floatingActionButton;

    private ToastDialog toastDialog;
    private Global_Typeface global_typeface;
    private SharedPreferencesUtility sharedPreferencesUtility;

    private String selected_card_type, stCard;

    private ArrayList<CardModel> _arr_CardList = new ArrayList<>();

    @BindView(R.id.layout_add_card)
    LinearLayout _layout_add_card;

    @BindView(R.id.txt_new_card)
    TextView _txt_new_card;

    @BindView(R.id.txt_save_card)
    TextView _txt_save_card;

    @BindView(R.id.edt_card_number)
    EditText _edt_card_number;

    @BindView(R.id.edt_card_owner)
    EditText _edt_card_owner;

    @BindView(R.id.edt_month_year)
    EditText _edt_month_year;

    @BindView(R.id.edt_card_cvv)
    EditText _edt_card_cvv;

    @BindView(R.id.rgroup_card_type)
    RadioGroup _rgroup_card_type;

    @BindView(R.id.rbtn_personal_card)
    RadioButton _rbtn_personal_card;

    @BindView(R.id.rbtn_work_card)
    RadioButton _rbtn_work_card;

    @BindView(R.id.btn_save_card)
    Button _btn_save_card;

    @BindView(R.id.list_card)
    ExpandableHeightListView _list_card;

    @BindView(R.id.txt_no_data)
    AppCompatTextView _txt_no_data;

    ProgressDialog progressDialog;
    ApiInterface apiService;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toastDialog = new ToastDialog(getActivity());
        global_typeface = new Global_Typeface(getActivity());
        sharedPreferencesUtility = new SharedPreferencesUtility(getActivity());
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_card, container, false);

        ButterKnife.bind(this, view);

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        View _view = getActivity().getCurrentFocus();
        if (_view == null) {
            _view = new View(getActivity());
        }
        imm.hideSoftInputFromWindow(_view.getWindowToken(), 0);
        _list_card.setExpanded(true);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        GetCardList();
        _txt_new_card.setTypeface(global_typeface.Sansation_Bold());
        _txt_save_card.setTypeface(global_typeface.Sansation_Bold());
        _edt_card_number.setTypeface(global_typeface.Sansation_Regular());
        _edt_card_owner.setTypeface(global_typeface.Sansation_Regular());
        _edt_month_year.setTypeface(global_typeface.Sansation_Regular());
        _edt_card_cvv.setTypeface(global_typeface.Sansation_Regular());
        _rbtn_personal_card.setTypeface(global_typeface.Sansation_Regular());
        _rbtn_work_card.setTypeface(global_typeface.Sansation_Regular());
        _btn_save_card.setTypeface(global_typeface.Sansation_Bold());

        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.baseFloatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFabMenuOpen) {
                    collapseFabMenu();
                    if (_arr_CardList.size() > 0) {
                        _txt_no_data.setVisibility(View.GONE);
                    } else {
                        _txt_no_data.setVisibility(View.VISIBLE);
                    }
                } else {
                    expandFabMenu();
                    _txt_no_data.setVisibility(View.GONE);
                }
            }
        });

        _edt_month_year.addTextChangedListener(new MaskWatcher("##/##"));


        _rgroup_card_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbtn_personal_card:
                        selected_card_type = "0";
                        break;
                    case R.id.rbtn_work_card:
                        selected_card_type = "1";
                        break;
                }
            }
        });

        _btn_save_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String message = IsValidate();
                    if (message.equalsIgnoreCase("true")) {

                        JSONObject objCardData = new JSONObject();
                        objCardData.put("card_number", _edt_card_number.getText());
                        objCardData.put("card_name", _edt_card_owner.getText().toString());
                        objCardData.put("card_expiry_month", getValidDateFields(_edt_month_year.getText().toString())[0] + "");
                        objCardData.put("card_expiry_year", getValidDateFields(_edt_month_year.getText().toString())[1] + "");
                        objCardData.put("cvc", _edt_card_cvv.getText().toString());

                        if (isInternetOn(getContext())) {
                            progressDialog.show();
                            Call<JsonObject> call = apiService.user_save_card(sharedPreferencesUtility.getuser_id(),
                                    Global_ServiceApi.EncryptCard(objCardData.toString(), "user_save_card"), selected_card_type);
                            call.enqueue(new Callback<JsonObject>() {
                                @Override
                                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                    progressDialog.dismiss();
                                    try {
                                        String mResult = response.body().toString();
                                        Log.e("TAG_CArd", mResult);

                                        JSONObject objCardData = new JSONObject(mResult);
                                        if (objCardData.getString("status").equalsIgnoreCase("1")) {
                                            toastDialog.ShowToastMessage(objCardData.getString("message"));
                                            progressDialog.dismiss();
                                            _edt_card_owner.setText("");
                                            _edt_card_number.setText("");
                                            _edt_card_cvv.setText("");
                                            _edt_month_year.setText("");
                                            _rbtn_personal_card.setChecked(false);
                                            _rbtn_work_card.setChecked(false);

                                        } else {

                                            toastDialog.ShowToastMessage(objCardData.getString("message"));
                                        }
                                        GetCardList();
                                    } catch (Exception e) {
                                        progressDialog.dismiss();
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(Call<JsonObject> call, Throwable t) {

                                }
                            });
                        } else {
                            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                        }
                    } else {
                        toastDialog.ShowToastMessage(message);
                        progressDialog.dismiss();
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                }
            }
        });

        //getAnimations();
    }

    /**
     * Get User CardList
     */
    private void GetCardList() {

        if (isInternetOn(getContext())) {

            try {
                Call<JsonObject> call = apiService.user_save_card_list(sharedPreferencesUtility.getuser_id());

                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        Intent intent = new Intent("User_Card_List");
                        intent.putExtra("user_card_list", response.body().toString());
                        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver_UserCardList,
                new IntentFilter("User_Card_List"));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver_UserCardList);
    }

    /**
     * User Card List Receiver
     */
    private BroadcastReceiver mMessageReceiver_UserCardList = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_CardList = intent.getStringExtra("user_card_list");
            GetCardList(mResult_CardList);
        }
    };

    /**
     * User Card List Response Parse
     *
     * @param mResult
     */
    private void GetCardList(String mResult) {
        try {
            JSONObject objCardList = new JSONObject(mResult);
            if (objCardList.getString("status").equalsIgnoreCase("1")) {
                _list_card.setVisibility(View.VISIBLE);
                _txt_no_data.setVisibility(View.GONE);
                JSONArray jarrPayload = objCardList.getJSONArray("payload");
                if (jarrPayload.length() > 0) {

                    _arr_CardList.clear();

                    for (int i = 0; i < jarrPayload.length(); i++) {
                        JSONObject objCardData = jarrPayload.getJSONObject(i);

                        CardModel cardModel = new CardModel();
                        cardModel.setCard_id(objCardData.getString("card_id"));
                        cardModel.setUser_id(objCardData.getString("user_id"));
                        cardModel.setCard_type(objCardData.getString("card_type"));
                        cardModel.setCreated_date(objCardData.getString("created_date"));
                        cardModel.setStatus(objCardData.getString("status"));
                        cardModel.setCard_expiry_month(objCardData.getString("card_expiry_month"));
                        cardModel.setCard_expiry_year(objCardData.getString("card_expiry_year"));
                        cardModel.setCard_name(objCardData.getString("card_name"));
                        cardModel.setCard_number(objCardData.getString("last_digit"));
                        cardModel.setStripeCardID(objCardData.getString("stripe_card_id"));

                        _arr_CardList.add(cardModel);
                    }
                }
            } else {
                _list_card.setVisibility(View.GONE);
                if (_layout_add_card.getVisibility() == View.VISIBLE) {
                    _txt_no_data.setVisibility(View.GONE);
                } else {
                    _txt_no_data.setVisibility(View.VISIBLE);
                }
                _txt_no_data.setText(objCardList.getString("Message"));
            }

            PaymentCardListAdapter userCardListAdapter = new PaymentCardListAdapter(getActivity(), R.layout.layout_card_row, _arr_CardList, new PaymentCardListAdapter.BtnClickListener() {
                @Override
                public void onBtnClick(int position, String card_id, String opration_type) {
                    if (opration_type.equalsIgnoreCase("Delete")) {
                        showAlertDialog(card_id);
                    }
                }
            });
            _list_card.setAdapter(userCardListAdapter);
            userCardListAdapter.notifyDataSetChanged();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Card Blank Field Validation
     *
     * @return
     */
    private String IsValidate() {
        if (_edt_card_owner.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_card_name);
        } else if (_edt_card_number.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_card_number);
        } else if (_edt_month_year.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_expiry_month);
        } else if (_edt_card_cvv.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_card_cvc);
        } else if (_rgroup_card_type.getCheckedRadioButtonId() == -1) {
            return getResources().getString(R.string.st_select_card);
        } else {
            return "true";
        }

    }


    /**
     * Delete Card Dialog
     *
     * @return
     */
    public Dialog showAlertDialog(final String card_id) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_popup);
        dialog.setTitle("");
        dialog.setCancelable(true);

        TextView txt_popup_header = (TextView) dialog.findViewById(R.id.txt_popup_header);
        TextView txt_popup_message = (TextView) dialog.findViewById(R.id.txt_popup_message);
        TextView btn_delete = (TextView) dialog.findViewById(R.id.btn_delete);

        txt_popup_header.setTypeface(global_typeface.Sansation_Bold());
        txt_popup_message.setTypeface(global_typeface.Sansation_Regular());
        btn_delete.setTypeface(global_typeface.Sansation_Regular());

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Call<JsonObject> call = apiService.user_save_card_delete(sharedPreferencesUtility.getuser_id(), card_id);
                    call.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            Log.e("response", response.body().toString());
                            try {
                                JSONObject obj = new JSONObject(response.body().toString());
                                if (obj.getString("success").equals("true") && obj.getString("status").equals("1")) {


                                }
                            } catch (Exception e) {

                            }

                            GetCardList();
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {

                        }
                    });


                    GetCardList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });

        dialog.show();
        return null;
    }

    private void getAnimations() {
//        fabOpenAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.collapse_fabmenu);
//        fabCloseAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.expand_fabmenu);
    }

    private void expandFabMenu() {
        ViewCompat.animate(floatingActionButton).rotation(45.0F).withLayer().setDuration(300).setInterpolator(new OvershootInterpolator(10.0F)).start();
        ViewCompat.animate(_layout_add_card).rotation(0.0F).withLayer().setDuration(500).setInterpolator(new OvershootInterpolator(10.0F)).start();
        _layout_add_card.setVisibility(View.VISIBLE);
        isFabMenuOpen = true;
    }

    private void collapseFabMenu() {
        ViewCompat.animate(floatingActionButton).rotation(0.0F).withLayer().setDuration(300).setInterpolator(new OvershootInterpolator(10.0F)).start();
        ViewCompat.animate(_layout_add_card).rotation(0.0F).withLayer().setDuration(500).setInterpolator(new OvershootInterpolator(10.0F)).start();
        _layout_add_card.setVisibility(View.GONE);
        isFabMenuOpen = false;
    }

    /* for masking on month year feild */
    private class MaskWatcher implements TextWatcher {
        private boolean isRunning = false;
        private boolean isDeleting = false;
        private final String mask;

        public MaskWatcher(String mask) {
            this.mask = mask;
        }

        MaskWatcher buildCpf() {
            return new MaskWatcher("##/##");
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
            isDeleting = count > after;
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (isRunning || isDeleting) {
                return;
            }
            isRunning = true;

            int editableLength = editable.length();
            if (editableLength < mask.length()) {
                if (mask.charAt(editableLength) != '#') {
                    editable.append(mask.charAt(editableLength));
                } else if (mask.charAt(editableLength - 1) != '#') {
                    editable.insert(editableLength - 1, mask, editableLength - 1, editableLength);
                }
            }

            isRunning = false;
        }
    }

    public int[] getValidDateFields(String monthyear) {
        int[] monthYearPair = new int[2];
        String rawNumericInput = monthyear.replaceAll("/", "");
        String[] dateFields = separateDateStringParts(rawNumericInput);

        try {
            monthYearPair[0] = Integer.parseInt(dateFields[0]);
            monthYearPair[1] = convertTwoDigitYearToFour(Integer.parseInt(dateFields[1]), Calendar.getInstance());
        } catch (NumberFormatException numEx) {
            // Given that the date should already be valid when getting to this method, we should
            // not his this exception. Returning null to indicate error if we do.
            return null;
        }

        return monthYearPair;
    }

    public static String[] separateDateStringParts(String expiryInput) {
        String[] parts = new String[2];
        if (expiryInput.length() >= 2) {
            parts[0] = expiryInput.substring(0, 2);
            parts[1] = expiryInput.substring(2);
        } else {
            parts[0] = expiryInput;
            parts[1] = "";
        }
        return parts;
    }

    static int convertTwoDigitYearToFour(
            @IntRange(from = 0, to = 99) int inputYear,
            @NonNull Calendar calendar) {
        int year = calendar.get(Calendar.YEAR);
        // Intentional integer division
        int centuryBase = year / 100;
        if (year % 100 > 80 && inputYear < 20) {
            centuryBase++;
        } else if (year % 100 < 20 && inputYear > 80) {
            centuryBase--;
        }
        return centuryBase * 100 + inputYear;
    }

}

