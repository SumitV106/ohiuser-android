package com.ohicabs.user.ohi.ohiOutStation;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.Activity.BaseActivity;
import com.ohicabs.user.ohi.Activity.BookingDetailsActivity;
import com.ohicabs.user.ohi.Activity.MainActivity;
import com.ohicabs.user.ohi.Activity.MonthlySpendingTermsActivity;
import com.ohicabs.user.ohi.Activity.PaytmTermsActivity;
import com.ohicabs.user.ohi.CustomWidgets.CustomBoldFontButton;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.Constant;
import com.ohicabs.user.ohi.Utils.DataParser;
import com.ohicabs.user.ohi.Utils.PermissionUtils;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;
import static com.ohicabs.user.ohi.Utils.Constant.BACK_TO_ACTIVITY;
import static com.ohicabs.user.ohi.Utils.Constant.LOCATION_PERMISSION_REQUEST_CODE;

public class OutStationConfirmRideActivity extends BaseActivity implements View.OnClickListener, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, LocationListener, GoogleApiClient.OnConnectionFailedListener, PaytmPaymentDialog.DialogListener {

    @BindView(R.id.txt_estimated_price) AppCompatTextView txtEstimatedPrice;
    @BindView(R.id.txt_dis_estimated_price) AppCompatTextView txtDiscountEstimatedPrice;
    @BindView(R.id.txt_set_payment) AppCompatTextView txtSetPayment;
    @BindView(R.id.txt_pickup_address) AppCompatTextView txtPickupAddress;
    @BindView(R.id.txt_drop_address) AppCompatTextView txtDropAddress;
    @BindView(R.id.btn_ride_book) CustomBoldFontButton btnRideBook;
    @BindView(R.id.img_back) ImageView imgBack;
    @BindView(R.id.fab_current_location) ImageButton fabCurrentLocation;
    @BindView(R.id.ll_payment_mode) LinearLayout llPaymentMode;
    @BindView(R.id.ll_date_time) LinearLayout llDateTime;
    @BindView(R.id.txt_date_time) AppCompatTextView txtDateTime;

    @BindView(R.id.txt_ride_term) TextView txtRideTNC;
    @BindView(R.id.rl_drop_address) RelativeLayout rlDropAddress;

    @BindView(R.id.txt_ride_amount) AppCompatTextView txRideAmount;
    @BindView(R.id.txt_paytm_discount) AppCompatTextView txtPaytmDiscount;
    @BindView(R.id.txt_paytm_discount_amount) AppCompatTextView txtPaytmDiscountAmount;

    @BindView(R.id.txt_subs_discount_total) AppCompatTextView txtSubscriptionDiscountTotal;
    @BindView(R.id.txt_subscription_discount) AppCompatTextView txtSubscriptionDiscount;
    @BindView(R.id.txt_subscription_discount_amount) AppCompatTextView txtSubscriptionDiscountAmount;

    @BindView(R.id.txt_final_total) AppCompatTextView txtFinalTotal;

    Double mPickUpLat = 0.0, mPickUpLng = 0.0, mDropLat = 0.0, mDropLng = 0.0, mUserCurrentLat, mUserCurrentLng;

    String isTaxiService = "", stPaymentType = "Cash", stPaymentStatus = "4", stRideType, stServiceID, stPriceID, stPickupDate, stCardID = "", stZoneID = "",
            stEstDuration = "", stEstTotalDistance = "", stMaxOfferAmount = "", stPaytmOffer = "";

    int price;
    Double Amount = 0.0, Discount_Amount = 0.0;

    String book_seat = "1", stSubscription;
    private ArrayList<LatLng> points = new ArrayList<>();
    private List<Polyline> polylines = new ArrayList<Polyline>();

    private GoogleMap mGoogleMap;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private SupportMapFragment mSupportMapFragment;

    Dialog dialogFindDriver;
    CountDownTimer cT;

    private HashMap<String, Object> hashMapRideParam = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out_station_ride_book);
        ButterKnife.bind(this);

        setViews();
    }

    private void setViews() {
        hashMapRideParam = (HashMap<String, Object>) getIntent().getSerializableExtra("hashMap_BookingParam");

        mSupportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap_OutStation);
        mSupportMapFragment.getMapAsync(this);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            mGoogleApiClient.connect();
        }

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(Constant.UPDATE_INTERVAL)
                .setFastestInterval(Constant.FASTEST_INTERVAL)
                .setSmallestDisplacement(10);

        txtDropAddress.setText(hashMapRideParam.get("dropto").toString());
        txtPickupAddress.setText(hashMapRideParam.get("pickup").toString());

        txtEstimatedPrice.setText("₹ " + hashMapRideParam.get("price"));
        txtDiscountEstimatedPrice.setText("");

        mDropLat = (double) hashMapRideParam.get("mDropLat");
        mDropLng = (double) hashMapRideParam.get("mDropLng");

        mPickUpLat = (double) hashMapRideParam.get("mPickUpLat");
        mPickUpLng = (double) hashMapRideParam.get("mPickUpLng");

        isTaxiService = hashMapRideParam.get("isTexiService").toString();
        stRideType = hashMapRideParam.get("stRideType").toString();
        stServiceID = hashMapRideParam.get("stServiceID").toString();
        stPriceID = hashMapRideParam.get("stPriceID").toString();
        price = Integer.parseInt(hashMapRideParam.get("price").toString());

        stEstDuration = hashMapRideParam.get("est_duration").toString();
        stEstTotalDistance = hashMapRideParam.get("est_total_distance").toString();
        stMaxOfferAmount = hashMapRideParam.get("max_offer_amount").toString();
        stPaytmOffer = hashMapRideParam.get("paytm_offer").toString();
        stZoneID = hashMapRideParam.get("stZoneID").toString();

        llDateTime.setVisibility(View.VISIBLE);

        imgBack.setOnClickListener(this);
        btnRideBook.setOnClickListener(this);
        llPaymentMode.setOnClickListener(this);
        llDateTime.setOnClickListener(this);
        fabCurrentLocation.setOnClickListener(this);
        txtRideTNC.setOnClickListener(this);

        txtSetPayment.setText(stPaymentType); // Default Payment type is Cash
        setPrice();
    }

    @Override
    protected void onResume() {
        super.onResume();

        /* This call Receive after user_request_accept */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_UserRideRequest,
                new IntentFilter("User_Ride_Request"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_UserRideRequest);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    /**
     * Refresh User Ride List Service
     */
    private BroadcastReceiver mMessageReceiver_UserRideRequest = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (cT != null)
                    cT.cancel();
                if (dialogFindDriver != null) {
                    dialogFindDriver.dismiss();
                }
                FinishActivty();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void FinishActivty() {
        Intent intent1 = new Intent();
        intent1.putExtra("MESSAGE", "finish");
        setResult(BACK_TO_ACTIVITY, intent1);
        finish();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_ride_book:
                if (!txtDateTime.getText().toString().equalsIgnoreCase("Select Date-Time")) {
                    UserBookingRide();
                } else {
                    toastDialog.ShowToastMessage("Please select date and time");
                }
                break;

            case R.id.img_back:
                Intent intent = new Intent();
                intent.putExtra("MESSAGE", "");
                setResult(BACK_TO_ACTIVITY, intent);
                finish();
                break;

            case R.id.ll_payment_mode:
                showPaymentSelectionDialog();
                break;

            case R.id.ll_date_time:
                showDateTimePicker();
                break;

            case R.id.fab_current_location:
                try {
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mUserCurrentLat, mUserCurrentLng)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.txt_ride_term:

                Intent intent_term = new Intent(getApplicationContext(), MonthlySpendingTermsActivity.class);
                intent_term.putExtra("term_type", "6");
                startActivity(intent_term);

                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
        mGoogleMap.getUiSettings().setCompassEnabled(false);
        mGoogleMap.getUiSettings().setTiltGesturesEnabled(false);
        mGoogleMap.setBuildingsEnabled(false);

        //Disable Marker click event
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });

        enableMyLocation();
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mGoogleMap != null) {
            // Access to the location has been granted to the app.
            mGoogleMap.setMyLocationEnabled(true);
        }

        // set pick up and drop to marker
        setMarkerOnMap();

    }

    // set pick up and drop to marker
    private void setMarkerOnMap() {

        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mPickUpLat, mPickUpLng)).draggable(false).title("pick up")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin)));

        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mDropLat, mDropLng)).draggable(false).title("drop to")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin)));

        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(mPickUpLat, mPickUpLng)).zoom(14.0f).build();
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        try {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(new LatLng(mPickUpLat, mPickUpLng));
            builder.include(new LatLng(mDropLat, mDropLng));
            LatLngBounds bounds = builder.build();
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 150));


            LatLng pickup = new LatLng(mPickUpLat, mPickUpLng);
            LatLng dest = new LatLng(mDropLat, mDropLng);

            String url = getUrl(pickup, dest);
            FetchUrl FetchUrl = new FetchUrl();
            // Start downloading json data from Google Directions API
            FetchUrl.execute(url);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*
    * Called by Location Services when the request to connect the
    * client finishes successfully. At this point, you can
    * request the current location or start periodic updates
    */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
                return;
            }
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            handleNewLocation(location);
        }
    }

    private void handleNewLocation(Location location) {
        if (mLocationRequest != null) {
            try {
                mUserCurrentLat = location.getLatitude();
                mUserCurrentLng = location.getLongitude();
                location.setAccuracy(10);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            ShowToast(getResources().getString(R.string.network_disconnect));
        } else if (i == CAUSE_NETWORK_LOST) {
            ShowToast(getResources().getString(R.string.network_lost));
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    /**
     * Show Date-Time Picker dialog
     */
    int mYear, mMonth, mDay, mHour, mMin;
    private void showDateTimePicker() {

        final Calendar currentDate = Calendar.getInstance();
        Calendar calendarDate = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(OutStationConfirmRideActivity.this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

                mYear = year;
                mMonth = monthOfYear + 1;
                mDay = dayOfMonth;

                calendarDate.set(year, monthOfYear, dayOfMonth);

                new TimePickerDialog(OutStationConfirmRideActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        try {
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd h:mm a");


                            /* Min Time Start */

                            SimpleDateFormat sdfMinTime = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
                            String tempMinTime = sdfMinTime.format(new Date());

                            Date date = sdfMinTime.parse(tempMinTime);
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(date);
                            calendar.add(Calendar.HOUR, 1);

                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd h:mm a");
                            String minTime = sdf.format(calendar.getTime());

                            /* Min Time End */


                            /* Selected Time Start */
                            mHour = hourOfDay;
                            mMin = minute;

                            Calendar datetime = Calendar.getInstance();
                            datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            datetime.set(Calendar.MINUTE, minute);

                            String am_pm = getTime(mHour, mMin);
                            String stSelectedDate = mYear + "-" + mMonth + "-" + mDay + " " + am_pm;

                            /* Selected Time End */


                            Date minDate = format.parse(minTime);
                            Date selectedDate = format.parse(stSelectedDate);

                            if (selectedDate.after(minDate)) {
                                calendarDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                calendarDate.set(Calendar.MINUTE, minute);
                                txtDateTime.setText(new SimpleDateFormat("yyyy-MM-dd h:mm a").format(calendarDate.getTime()));
                            } else {
                                ShowToast("Please select time after 1 hour from current time");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE));

        datePickerDialog.getDatePicker().setMinDate(currentDate.getTimeInMillis());
        datePickerDialog.show();
    }

    public String getTime(int hr, int min) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY,hr);
        cal.set(Calendar.MINUTE,min);
        Format formatter;
        formatter = new SimpleDateFormat("h:mm a");
        return formatter.format(cal.getTime());
    }


    /**
     * User Booking Ride
     */
    private void UserBookingRide() {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd h:mm a");
        try {
            Date date = format.parse(txtDateTime.getText().toString());

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stPickupDate = dateFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (txtSetPayment.getText().toString().equalsIgnoreCase("SET PAYMENT")) {
            ShowToast(getResources().getString(R.string.st_select_payment_option));
        } else {

            if (isInternetOn(getApplicationContext())) {

                progressDialog.show();
                Call<JsonObject> call = apiService.booking_outstation(preferencesUtility.getuser_id(), stServiceID, preferencesUtility.getauth_token(),
                        String.valueOf(mPickUpLat), String.valueOf(mPickUpLng), txtPickupAddress.getText().toString(),
                        String.valueOf(mDropLat), String.valueOf(mDropLng), txtDropAddress.getText().toString(),
                        stPickupDate, stPaymentStatus, stPriceID, stEstTotalDistance, stEstDuration,
                        String.valueOf(price), stSubscription);

                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject objBookingData = new JSONObject(response.body().toString());

                            if (objBookingData.optString("success").equalsIgnoreCase("true") &&
                                    objBookingData.optString("status").equalsIgnoreCase("1")) {

                                JSONObject jsonObjectPayload = objBookingData.optJSONObject("payload");

                                PaytmPaymentDialog paytmPaymentDialog = new PaytmPaymentDialog();

                                Bundle bundle = new Bundle();
                                bundle.putBoolean("notAlertDialog", true);
                                bundle.putString("booking_id", jsonObjectPayload.optString("booking_id"));
                                bundle.putString("upfront_amount", jsonObjectPayload.optString("upfront_amount"));
                                paytmPaymentDialog.setArguments(bundle);

                                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                                Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
                                if (prev != null) {
                                    ft.remove(prev);
                                }
                                ft.addToBackStack(null);

                                paytmPaymentDialog.show(ft, "dialog");

                            } else {
                                ShowToast(objBookingData.optString("message"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        progressDialog.dismiss();
                    }
                });
            } else {
                toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
            }
        }
    }

    public Dialog showAlertDialog(String title, String message, final String status) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_popup);
        dialog.setTitle("");
        dialog.setCancelable(false);
        TextView txt_popup_header = (TextView) dialog.findViewById(R.id.txt_popup_header);
        TextView txt_popup_message = (TextView) dialog.findViewById(R.id.txt_popup_message);
        TextView btn_ok = (TextView) dialog.findViewById(R.id.btn_ok);

        txt_popup_header.setText(title);
        txt_popup_message.setText(message);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("0")) {
                    dialog.dismiss();
                } else if (status.equalsIgnoreCase("1")) {
                    dialog.dismiss();
                    Intent intent = new Intent();
                    intent.putExtra("MESSAGE", "finish");
                    setResult(BACK_TO_ACTIVITY, intent);
                    finish();
                } else if (status.equalsIgnoreCase("2")) {
                    dialog.dismiss();
                    Intent intent = new Intent();
                    intent.putExtra("MESSAGE", "finish");
                    setResult(BACK_TO_ACTIVITY, intent);
                    finish();
                }
            }
        });

        dialog.show();
        return null;
    }

    public String checkDigit(int number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }

    public Dialog showAlertDialogForFindingDriver(String title, String message, final String status, final String stBookingID, final String stBooking_Status) {

        // This is for Get get height and width of Screen
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        try {
            display.getRealSize(size);
        } catch (NoSuchMethodError err) {
            display.getSize(size);
        }
        final int width = size.x;
        int height = size.y;

        dialogFindDriver = new Dialog(this);
        dialogFindDriver.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogFindDriver.setContentView(R.layout.layout_dialog_popup_finding_driver);
        dialogFindDriver.setTitle("");
        dialogFindDriver.setCancelable(false);
        if (dialogFindDriver != null) {
            try {
                dialogFindDriver.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        final TextView txt_counter = (TextView) dialogFindDriver.findViewById(R.id.txt_counter);
        final TextView txt_popup_message = (TextView) dialogFindDriver.findViewById(R.id.txt_popup_message);
        TextView txt_cancel = (TextView) dialogFindDriver.findViewById(R.id.txt_cancel);

        LinearLayout mainLayout = (LinearLayout) dialogFindDriver.findViewById(R.id.mainLayout);
        mainLayout.setMinimumWidth((int) (width / 0.7));

        ProgressBar progressBar = (ProgressBar) dialogFindDriver.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        final int[] time = {90};

        cT = new CountDownTimer(90000, 1000) {
            public void onTick(long millisUntilFinished) {
                txt_popup_message.setText(getResources().getString(R.string.st_finding_driver_msg) + " " + checkDigit(time[0]));
                time[0]--;
            }

            public void onFinish() {

                try {

                    final Dialog onfinishDialog = new Dialog(OutStationConfirmRideActivity.this);
                    onfinishDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    onfinishDialog.setContentView(R.layout.layout_dialog_ontimer_finish);
                    onfinishDialog.show();

                    final Button btn_ok = (Button) onfinishDialog.findViewById(R.id.btn_ok);

                    btn_ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            onfinishDialog.dismiss();

                            if (dialogFindDriver != null) {
                                dialogFindDriver.dismiss();
                            }
                            FinishActivty();
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        cT.start();

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog onRideCancelDialog = new Dialog(OutStationConfirmRideActivity.this);
                onRideCancelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                onRideCancelDialog.setContentView(R.layout.layout_dialog_ride_cancel);

                onRideCancelDialog.show();
                final Button btn_yes = (Button) onRideCancelDialog.findViewById(R.id.btn_yes);
                final Button btn_no = (Button) onRideCancelDialog.findViewById(R.id.btn_no);

                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isInternetOn(getApplicationContext())) {

                            try {
                                progressDialog.show();
                                Call<JsonObject> call = apiService.user_ride_force_cancel(stBookingID, preferencesUtility.getuser_id());
                                call.enqueue(new Callback<JsonObject>() {
                                    @Override
                                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                        String mResult = response.body().toString();
                                        progressDialog.dismiss();
                                        try {
                                            JSONObject objCancelData = new JSONObject(mResult);
                                            if (objCancelData.getString("status").equalsIgnoreCase("1")) {
                                                toastDialog.ShowToastMessage(objCancelData.getString("message"));

                                                onRideCancelDialog.dismiss();

                                                if (dialogFindDriver != null) {
                                                    dialogFindDriver.dismiss();
                                                }
                                                if (cT != null) {
                                                    cT.cancel();
                                                }

                                                FinishActivty();
                                            } else {
                                                toastDialog.ShowToastMessage(objCancelData.getString("message"));
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<JsonObject> call, Throwable t) {
                                        t.printStackTrace();
                                        progressDialog.dismiss();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                        }
                    }
                });

                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onRideCancelDialog.dismiss();
                    }
                });
            }
        });
        return null;
    }

    /**
     * Select Payment Option Dialog
     *
     * @return
     */
    public Dialog showPaymentSelectionDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_payment_option_dialog);
        dialog.setTitle("");
        dialog.setCancelable(false);

        ListView list_card = (ListView) dialog.findViewById(R.id.listview_cards);
        list_card.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        Button btn_add_card = (Button) dialog.findViewById(R.id.btn_add_card);
        AppCompatTextView txt_done = (AppCompatTextView) dialog.findViewById(R.id.txt_done);
        AppCompatTextView txt_select_payment = (AppCompatTextView) dialog.findViewById(R.id.txt_select_payment);
        final AppCompatTextView txt_pay_by_paypal = (AppCompatTextView) dialog.findViewById(R.id.txt_pay_by_paypal);
        final AppCompatTextView txt_pay_by_paytm = (AppCompatTextView) dialog.findViewById(R.id.txt_pay_by_paytm);
        final AppCompatTextView txt_pay_by_cash = (AppCompatTextView) dialog.findViewById(R.id.txt_pay_by_cash);
        final AppCompatCheckBox chkbox_pay_by_reward = (AppCompatCheckBox) dialog.findViewById(R.id.chkbox_pay_by_reward);
        final AppCompatCheckBox chkbox_pay_by_Paytm = (AppCompatCheckBox) dialog.findViewById(R.id.chkbox_pay_by_Paytm);
        final AppCompatCheckBox chkbox_pay_by_OhiPayment = (AppCompatCheckBox) dialog.findViewById(R.id.chkbox_pay_by_OhiPayment);
        final AppCompatCheckBox chkbox_pay_by_PayUmoneyWithRewards = (AppCompatCheckBox) dialog.findViewById(R.id.chkbox_pay_by_PayUmoneyWithRewards);
        final AppCompatCheckBox chkbox_pay_by_Crash = (AppCompatCheckBox) dialog.findViewById(R.id.chkbox_pay_by_Crash);

        final AppCompatTextView txt_msg = (AppCompatTextView) dialog.findViewById(R.id.txt_msg);
        final AppCompatTextView txt_terms = (AppCompatTextView) dialog.findViewById(R.id.txt_terms);
        final LinearLayout layout_paytm = (LinearLayout) dialog.findViewById(R.id.layout_paytm);

        chkbox_pay_by_OhiPayment.setTypeface(global_typeface.Sansation_Regular());
        chkbox_pay_by_reward.setTypeface(global_typeface.Sansation_Regular());
        txt_select_payment.setTypeface(global_typeface.Sansation_Regular());
        txt_done.setTypeface(global_typeface.Sansation_Regular());
        btn_add_card.setTypeface(global_typeface.Sansation_Regular());
        txt_terms.setTypeface(global_typeface.Sansation_Regular());

        txt_msg.setText("Get " + stPaytmOffer + "% instant discount upto Rs. " + stMaxOfferAmount + " per ride");
        txt_msg.setVisibility(View.INVISIBLE);

        txt_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(OutStationConfirmRideActivity.this, PaytmTermsActivity.class);
                intent.putExtra("stZoneID", stZoneID);
                startActivity(intent);

            }
        });

        layout_paytm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stPaymentType = "Paytm";
                stPaymentStatus = "0";

                setPrice();

                txtSetPayment.setText(stPaymentType);
                layout_paytm.setBackgroundColor(getResources().getColor(R.color.colorEditBackground));
                txt_pay_by_cash.setBackgroundColor(getResources().getColor(R.color.colorwhite));

                dialog.dismiss();
            }
        });

        txt_pay_by_cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stPaymentType = "Cash";
                stPaymentStatus = "4";

                setPrice();

                txtSetPayment.setText(stPaymentType);
                txt_pay_by_cash.setBackgroundColor(getResources().getColor(R.color.colorEditBackground));
                layout_paytm.setBackgroundColor(getResources().getColor(R.color.colorwhite));

                dialog.dismiss();
            }
        });

        chkbox_pay_by_Paytm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                chkbox_pay_by_Paytm.setChecked(b);
                chkbox_pay_by_Crash.setChecked(false);
            }
        });

        chkbox_pay_by_Crash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                chkbox_pay_by_Crash.setChecked(b);
                chkbox_pay_by_Paytm.setChecked(false);
            }
        });

        txt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chkbox_pay_by_OhiPayment.isChecked()) {
                    if (stPaymentType.equalsIgnoreCase("Cash payment")) {
                        txtSetPayment.setText(stPaymentType);

                    } else {
                        txtSetPayment.setText("wallet & " + stPaymentType);
                    }
                }

                if (!stPaymentType.equalsIgnoreCase("")) {
                    dialog.dismiss();
                } else {
                    toastDialog.ShowToastMessage(getResources().getString(R.string.st_select_payment_option));
                }
            }
        });

        chkbox_pay_by_reward.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                } else {

                }
            }
        });


        dialog.show();
        return null;
    }

    private void setPrice() {

        if (isInternetOn(getApplicationContext())) {
            checkSubscriptionEligible();
        }
    }

    /**
     * To check user is eligible for subscription - OutStation Ride
     *
     */
    private void checkSubscriptionEligible() {

        try {
            progressDialog.show();
            Call<JsonObject> call = apiService.check_subscription_outstation_eligible(preferencesUtility.getuser_id(),
                    preferencesUtility.getauth_token(), stPriceID, price + "", stPaymentStatus);

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        progressDialog.dismiss();

                        JSONObject jsonObjectDetails = new JSONObject(response.body().toString());

                        if (jsonObjectDetails.optString("status").equalsIgnoreCase("1")) {

                            stSubscription = "1";
                            JSONObject jsonObjectPayload = jsonObjectDetails.getJSONObject("payload");


                            txRideAmount.setText("Rs. " + jsonObjectPayload.optString("booking_amount")); // Amount

                            txtSubscriptionDiscount.setText("Subscription Discount(" + jsonObjectPayload.optString("plan_discount_per") + "%):"); // Subscription Discount
                            txtSubscriptionDiscountAmount.setText("Rs. " + jsonObjectPayload.optString("subscription_discount_amount")); // Subscription Amount

                            Amount = (Double.parseDouble(jsonObjectPayload.optString("booking_amount")) - Double.parseDouble(jsonObjectPayload.optString("subscription_discount_amount")));
                            txtSubscriptionDiscountTotal.setText("Rs. " + Math.round(Amount) + ""); //Subscription Total Amount after discount

                            txtPaytmDiscount.setText("Paytm Discount(" + stPaytmOffer + "%):"); //Paytm Discount
                            txtPaytmDiscountAmount.setText("Rs. " + jsonObjectPayload.optString("paytm_discount_amount")); //Paytm Discount Amount

                            txtFinalTotal.setText("Rs. " + jsonObjectPayload.optString("new_user_payable_amount")); // Final Amount
                            txtEstimatedPrice.setText("Rs. " + jsonObjectPayload.optString("new_user_payable_amount")); // Price after discount

                        } else {

                            stSubscription = "0";
                            JSONObject jsonObjectPayload = jsonObjectDetails.getJSONObject("payload");

                            txRideAmount.setText("Rs. " + jsonObjectPayload.optString("booking_amount")); // Amount

                            txtSubscriptionDiscount.setText("Subscription Discount(0%):"); // Subscription Discount
                            txtSubscriptionDiscountAmount.setText("Rs. 0"); // Subscription Amount


                            Amount = (Double.parseDouble(jsonObjectPayload.optString("booking_amount")) - Double.parseDouble(jsonObjectPayload.optString("subscription_discount_amount")));
                            txtSubscriptionDiscountTotal.setText("Rs. " + Math.round(Amount) + ""); //Subscription Total Amount after discount

                            txtPaytmDiscount.setText("Paytm Discount(" + stPaytmOffer + "%):"); //Paytm Discount
                            txtPaytmDiscountAmount.setText("Rs. " + Math.round(Discount_Amount) + ""); //Paytm Discount Amount

                            txtFinalTotal.setText("Rs. " + jsonObjectPayload.optString("new_user_payable_amount")); // Final Amount
                            txtEstimatedPrice.setText("Rs. " + jsonObjectPayload.optString("new_user_payable_amount")); // Price after discount
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            progressDialog.dismiss();
        }
    }

    @Override
    public void onFinishEditDialog() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        startActivity(intent);
    }

    private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        String mode = "mode=driving";

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode + "&key=AIzaSyAfye39JZYcYdS196GZrvxRBfMShOCaA3w";

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
//            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;
            points.clear();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.BLUE);

                Log.d("onPostExecute", "onPostExecute lineoptions decoded");

            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                polylines.add(mGoogleMap.addPolyline(lineOptions));
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }
    }
}
