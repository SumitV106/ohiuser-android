package com.ohicabs.user.ohi.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtility {

    SharedPreferences sharedpref;

    public SharedPreferencesUtility(Context context) {

        sharedpref = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
    }

    public void ClearPrefrenceKey(String KeyName) {
        sharedpref.edit().remove(KeyName).commit();
    }

    public void ClearPreferences() {
        sharedpref.edit().clear().commit();
    }

    public boolean CheckPreferences(String KeyName) {
        if (sharedpref.contains(KeyName))
            return true;
        return false;
    }

    public void setLogedin(boolean logedin) {
        sharedpref.edit().putBoolean("logedin", logedin).commit();
    }

    public boolean getLogedin() {
        return sharedpref.getBoolean("logedin", false);
    }


    public String getLoginType() {
        return sharedpref.getString("login_type", "");
    }

    public void setLoginType(String login_type) {
        sharedpref.edit().putString("login_type", login_type).commit();
    }

    public String getuser_id() {
        return sharedpref.getString("user_id", "");
    }

    public void setuser_id(String user_id) {
        sharedpref.edit().putString("user_id", user_id).commit();
    }

    public String getfirstname() {
        return sharedpref.getString("firstname", "");
    }

    public void setfirstname(String firstname) {
        sharedpref.edit().putString("firstname", firstname).commit();
    }

    public String getlastname() {
        return sharedpref.getString("firstname", "");
    }

    public void setlastname(String firstname) {
        sharedpref.edit().putString("lastname", firstname).commit();
    }

    public String getemail() {
        return sharedpref.getString("email", "");
    }

    public void setemail(String email) {
        sharedpref.edit().putString("email", email).commit();
    }

    public String getgender() {
        return sharedpref.getString("gender", "");
    }

    public void setgender(String gender) {
        sharedpref.edit().putString("gender", gender).commit();
    }

    public String getmobile() {
        return sharedpref.getString("mobile", "");
    }

    public void setmobile(String mobile) {
        sharedpref.edit().putString("mobile", mobile).commit();
    }

    public String getauth_token() {
        return sharedpref.getString("auth_token", "");
    }

    public void setauth_token(String auth_token) {
        sharedpref.edit().putString("auth_token", auth_token).commit();
    }


    public String getimage() {
        return sharedpref.getString("image", "");
    }

    public void setimage(String image) {
        sharedpref.edit().putString("image", image).commit();
    }

    public String getrefer_code() {
        return sharedpref.getString("refer_code", "");
    }

    public void setrefer_code(String refer_code) {
        sharedpref.edit().putString("refer_code", refer_code).commit();
    }

    public String getstatus() {
        return sharedpref.getString("status", "");
    }

    public void setstatus(String status) {
        sharedpref.edit().putString("status", status).commit();
    }

    public String getis_block() {
        return sharedpref.getString("is_block", "");
    }

    public void setis_block(String is_block) {
        sharedpref.edit().putString("is_block", is_block).commit();
    }

    /* Data Last Updated Date  */
    public String getLastUpdateDate() {
        return sharedpref.getString("data_last_update_date", "");
    }

    public void setLastUpdateDate(String data_last_update_date) {
        sharedpref.edit().putString("data_last_update_date", data_last_update_date).commit();
    }

    /* Driver wait time */
    public String getDriverWaitTime() {
        return sharedpref.getString("wait_time", "");
    }

    public void setDriverWaitTime(String wait_time) {
        sharedpref.edit().putString("wait_time", wait_time).commit();
    }

    /* Ride Start or not */
    public String getIsRideStarted() {
        return sharedpref.getString("ride_started", "");
    }

    public void setIsRideStarted(String ride_started) {
        sharedpref.edit().putString("ride_started", ride_started).commit();
    }

    /* Current Ride Start time */
    public String getCurrentTimeRideStart() {
        return sharedpref.getString("currenttimeridestart", "");
    }

    public void setCurrentTimeRideStart(String currenttimeridestart) {
        sharedpref.edit().putString("currenttimeridestart", currenttimeridestart).commit();
    }

    /* Ride Over Time */
    public String getRideOverTime() {
        return sharedpref.getString("rideovertime", "");
    }

    public void setRideOverTime(String rideovertime) {
        sharedpref.edit().putString("rideovertime", rideovertime).commit();
    }

    /* Running Ride Bokking Id */
    public String getStartRideBookindId() {
        return sharedpref.getString("start_ride_booking_id", "");
    }

    public void setStartRideBookindId(String start_ride_booking_id) {
        sharedpref.edit().putString("start_ride_booking_id", start_ride_booking_id).commit();
    }

    /* Booking Status on Start Ride */
    public String getBookingStatusOnRide() {
        return sharedpref.getString("Booking_Id", "");
    }

    public void setBookingStatusOnRide(String Booking_Id) {
        sharedpref.edit().putString("Booking_Id", Booking_Id).commit();
    }

    /* Booking Id on Start Ride */
    public String getBookingIdOnRide() {
        return sharedpref.getString("Booking_Status", "");
    }

    public void setBookingIdOnRide(String Booking_Status) {
        sharedpref.edit().putString("Booking_Status", Booking_Status).commit();
    }

    /**
     * FCM Token Key
     */

    public String gettoken() {
        return sharedpref.getString("token_id", "");
    }

    public void settoken(String token) {
        sharedpref.edit().putString("token_id", token).commit();
    }


    /**
     * Disclaimer Key
     */
    public boolean getDisclaimer() {
        return sharedpref.getBoolean("is_disclaimer", false);
    }

    public void setDisclaimer(boolean is_disclaimer) {
        sharedpref.edit().putBoolean("is_disclaimer", is_disclaimer).commit();
    }
    /**
     * driver id
     */

    public String getDriverid() {
        return sharedpref.getString("driver_id", "");
    }

    public void setDriverid(String token) {
        sharedpref.edit().putString("driver_id", token).commit();
    }


    /**
     * set Zone ID
     * @return
     */
    public String getZoneId() {
        return sharedpref.getString("zone_id", "");
    }

    public void setZoneId(String zone_id) {
        sharedpref.edit().putString("zone_id", zone_id).commit();
    }

    /**
     * set Refer Code
     *
     * @return
     */
    public String getReferCode() {
        return sharedpref.getString("refer_code", "");
    }

    public void setReferCode(String refer_code) {
        sharedpref.edit().putString("refer_code", refer_code).commit();
    }

}
