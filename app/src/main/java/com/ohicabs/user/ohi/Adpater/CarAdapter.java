package com.ohicabs.user.ohi.Adpater;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.facebook.drawee.view.SimpleDraweeView;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontTextView;
import com.ohicabs.user.ohi.Utils.Global_Typeface;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by sumit on 19/04/17.
 */

public class CarAdapter extends BaseAdapter {

    private Context activity;
    int ResourceId;
    ArrayList<HashMap> _arr_CarModels;
    private Global_Typeface global_typeface;
    private BtnClickListener mClickListener = null;

    private int selected_position = -1;

    public CarAdapter(Context act, int resId, ArrayList<HashMap> _arrCars, BtnClickListener listener) {
        this.activity = act;
        this.ResourceId = resId;
        this._arr_CarModels = _arrCars;
        mClickListener = listener;
        global_typeface = new Global_Typeface(activity);
    }

    @Override
    public int getCount() {
        return _arr_CarModels.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    View mLastView;
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View itemView = convertView;
        final ViewHolder viewHolder;
        if (itemView == null) {

            viewHolder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(ResourceId, parent, false);
            viewHolder.lyvehicle=(LinearLayout)itemView.findViewById(R.id.lyvehicle);
            viewHolder.txt_car_service_type = (CustomRegularFontTextView) itemView.findViewById(R.id.tvname);
            viewHolder.img_car_type = (ImageView) itemView.findViewById(R.id.imgvehicle);
            viewHolder.draweeView = (SimpleDraweeView)itemView.findViewById(R.id.img_task_image);

            itemView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) itemView.getTag();
        }

        viewHolder.txt_car_service_type.setText(_arr_CarModels.get(position).get("service_name").toString());
//        Log.e("vehicle",Global_ServiceApi.API_IMAGE_HOST+ _arr_CarModels.get(position).get("icon"));
//        Glide.with(activity).load(Global_ServiceApi.API_IMAGE_HOST+ _arr_CarModels.get(position).get("icon")).
//                crossFade().
//                diskCacheStrategy(DiskCacheStrategy.ALL).
//                into(viewHolder.img_car_type);
        /*loag image using frasco lib*/
        try {
            Uri imageUri = Uri.parse(Global_ServiceApi.API_IMAGE_HOST+ _arr_CarModels.get(position).get("icon"));
            viewHolder.draweeView.setImageURI(imageUri);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return itemView;
    }

    public class ViewHolder {
        CustomRegularFontTextView txt_car_service_type;
        ImageView img_car_type;
        LinearLayout lyvehicle;
        SimpleDraweeView draweeView;
    }

    public interface BtnClickListener {
        void onBtnClick(int position, String service_id, String IsTaxi);
    }

}
