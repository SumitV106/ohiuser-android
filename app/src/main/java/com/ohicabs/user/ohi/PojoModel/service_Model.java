package com.ohicabs.user.ohi.PojoModel;

/**
 * Created by crayon on 20/09/17.
 */

public class service_Model {
    String service_id;
    String service_name;

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    String icon;
}
