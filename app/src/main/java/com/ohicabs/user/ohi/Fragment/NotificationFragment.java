package com.ohicabs.user.ohi.Fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Adpater.NotificationAdapter;
import com.ohicabs.user.ohi.StorageManager.DatabaseHelper;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {



    private DatabaseHelper mDbHelper;
    NotificationAdapter notificationAdapter;

    @BindView(R.id.layout_no_notification)
    LinearLayout _layout_no_notification;

    @BindView(R.id.list_notification)
    ListView _list_notification;

    private ArrayList<HashMap> _arr_notificationData = new ArrayList<>();

    public NotificationFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDbHelper = new DatabaseHelper(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, view);

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        View _view = getActivity().getCurrentFocus();
        if (_view == null) {
            _view = new View(getActivity());
        }
        imm.hideSoftInputFromWindow(_view.getWindowToken(), 0);
        return view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        _arr_notificationData = mDbHelper.GetPushNotificationFromDB();
        if (_arr_notificationData.size() > 0) {
            notificationAdapter = new NotificationAdapter(getActivity(), R.layout.layout_notification_row, _arr_notificationData);
            _list_notification.setAdapter(notificationAdapter);
            _layout_no_notification.setVisibility(View.GONE);
        } else {
            _layout_no_notification.setVisibility(View.VISIBLE);
        }
    }
}
