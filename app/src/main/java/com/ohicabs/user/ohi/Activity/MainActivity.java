package com.ohicabs.user.ohi.Activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ohicabs.user.ohi.DBModel.CareList_Model;
import com.ohicabs.user.ohi.Fragment.AboutUsFragment;
import com.ohicabs.user.ohi.Fragment.AllPolicyFragment;
import com.ohicabs.user.ohi.Fragment.CustomerCareListFragment;
import com.ohicabs.user.ohi.Fragment.InviteFragment;
import com.ohicabs.user.ohi.Fragment.MonthlySpendFragment;
import com.ohicabs.user.ohi.Fragment.OHICreditsFragment;
import com.ohicabs.user.ohi.Fragment.OfferFragment;
import com.ohicabs.user.ohi.Fragment.ReferEarnFragment;
import com.ohicabs.user.ohi.Fragment.RewardFragment;
import com.ohicabs.user.ohi.Fragment.UserPromoFragment;
import com.ohicabs.user.ohi.ServiceManager.VolleyResponseListener;
import com.ohicabs.user.ohi.ServiceManager.VolleyUtils;
import com.ohicabs.user.ohi.Utils.CircularImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontTextView;
import com.ohicabs.user.ohi.DBModel.ServiceDetail_Model;
import com.ohicabs.user.ohi.DBModel.ServicePrice_Model;
import com.ohicabs.user.ohi.DBModel.ZoneList_Model;
import com.ohicabs.user.ohi.Fragment.DashBoardFragment;
import com.ohicabs.user.ohi.Fragment.NotificationFragment;
import com.ohicabs.user.ohi.Fragment.ProfileFragment;
import com.ohicabs.user.ohi.Fragment.RideHistoryFragment;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Fragment.SupportFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.Utils.ForceUpdateChecker;
import com.ohicabs.user.ohi.notificationbadge.NotificationBadge;
import com.ohicabs.user.ohi.ui.subscription.ExclusiveHistoryFragment;
import com.ohicabs.user.ohi.ui.subscription.SubscriptionActivity;
import com.ohicabs.user.ohi.ui.subscription.SubscriptionHistoryFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.UPDATE_ONE_SIGNAL_ID;
import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class MainActivity extends BaseActivity implements View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener, ForceUpdateChecker.OnUpdateNeededListener {

    LinearLayout _layout_dashboard, _layout_book_ride, _layout_payment, _layout_notification,
            _layout_support, _layout_saved_card, _layout_logout, _layout_terms_condition,
            _layout_privacy_policy, _layout_cancellation_policy, _layout_refund_policy, _layout_about_us,
            _layout_invite, _layout_customer_care, _layout_offer, _layout_reward, _layout_monthly_spend,
            llPromoCode, llSubscription, llSubscriptionHistory, llExclusiveOffers, llReferEarn, llOhiCredits;

    RelativeLayout layout_serach;
    ImageView img_cal;
    CircularImageView _img_profile;
    TextView _toolbar_title, _txt_msg_clear, _txt_sos;
    AppCompatTextView _txt_username, _txt_edit_profile, txtReferEarn, txtRedemption;
    ImageView _img_more_menu;
    FloatingActionButton _floatingActionButton;
    private RelativeLayout _layout_notification_badge;
    private NotificationBadge _notificationBadge;

    public ImageLoader mImageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mImageLoader = ImageLoader.getInstance();
        mImageLoader.init(ImageLoaderConfiguration.createDefault(getApplicationContext()));

        /* All Service On Method Call*/
        appDelegate.SetActivity(MainActivity.this);

        setContent();
    }

    private void setContent() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerview = navigationView.getHeaderView(0);

        _toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        _floatingActionButton = (FloatingActionButton) toolbar.findViewById(R.id.baseFloatingActionButton);
        _txt_msg_clear = (TextView) toolbar.findViewById(R.id.txt_msg_clear);
        _txt_sos = (TextView) toolbar.findViewById(R.id.txt_sos);
        layout_serach = (RelativeLayout) toolbar.findViewById(R.id.layout_serach);
        img_cal = (ImageView) toolbar.findViewById(R.id.img_cal);
        txtReferEarn = toolbar.findViewById(R.id.txt_refer_earn);
        txtRedemption = toolbar.findViewById(R.id.txt_redemption);

        _txt_username = (CustomRegularFontTextView) headerview.findViewById(R.id.nav_username);
        _txt_edit_profile = (CustomRegularFontTextView) headerview.findViewById(R.id.nav_edit_profile);
        _img_profile = (CircularImageView) headerview.findViewById(R.id.img_user);

        _layout_dashboard = (LinearLayout) headerview.findViewById(R.id.layout_dashboard);
        _layout_book_ride = (LinearLayout) headerview.findViewById(R.id.layout_book_ride);
        _layout_payment = (LinearLayout) headerview.findViewById(R.id.layout_payment);
        _layout_notification = (LinearLayout) headerview.findViewById(R.id.layout_notification);
        _layout_support = (LinearLayout) headerview.findViewById(R.id.layout_support);
        _layout_saved_card = (LinearLayout) headerview.findViewById(R.id.layout_saved_card);
        _layout_terms_condition = (LinearLayout) headerview.findViewById(R.id.layout_terms_condition);
        _layout_privacy_policy = (LinearLayout) headerview.findViewById(R.id.layout_privacy_policy);
        _layout_cancellation_policy = (LinearLayout) headerview.findViewById(R.id.layout_cancellation_policy);
        _layout_refund_policy = (LinearLayout) headerview.findViewById(R.id.layout_refund_policy);
        _layout_about_us = (LinearLayout) headerview.findViewById(R.id.layout_about_us);
        _layout_logout = (LinearLayout) headerview.findViewById(R.id.layout_logout);
        _layout_invite = (LinearLayout) headerview.findViewById(R.id.layout_invite);
        _layout_customer_care = (LinearLayout) headerview.findViewById(R.id.layout_customer_care);
        _layout_offer = (LinearLayout) headerview.findViewById(R.id.layout_offer);
        _layout_reward = (LinearLayout) headerview.findViewById(R.id.layout_reward);
        _layout_monthly_spend = (LinearLayout) headerview.findViewById(R.id.layout_monthly_spend);
        llPromoCode = headerview.findViewById(R.id.layout_promo);
        llSubscription = headerview.findViewById(R.id.layout_subscription);
        llSubscriptionHistory = headerview.findViewById(R.id.layout_subscription_history);
        llExclusiveOffers = headerview.findViewById(R.id.layout_exclusive_offer);
        llReferEarn = headerview.findViewById(R.id.layout_refer_earn);
        llOhiCredits = headerview.findViewById(R.id.layout_ohi_credits);

        _img_more_menu = (ImageView) toolbar.findViewById(R.id.img_more_menu);

        _layout_notification_badge = (RelativeLayout) toolbar.findViewById(R.id.layout_notification_badge);
        _notificationBadge = (NotificationBadge) toolbar.findViewById(R.id.txt_notification_count);
        _notificationBadge.setNumber(0);

        _layout_dashboard.setOnClickListener(this);
        _layout_book_ride.setOnClickListener(this);
        _layout_payment.setOnClickListener(this);
        _layout_notification.setOnClickListener(this);
        _layout_support.setOnClickListener(this);
        _layout_saved_card.setOnClickListener(this);
        _layout_terms_condition.setOnClickListener(this);
        _layout_privacy_policy.setOnClickListener(this);
        _layout_cancellation_policy.setOnClickListener(this);
        _layout_refund_policy.setOnClickListener(this);
        _layout_about_us.setOnClickListener(this);
        _layout_logout.setOnClickListener(this);
        _layout_invite.setOnClickListener(this);
        _txt_edit_profile.setOnClickListener(this);
        _layout_offer.setOnClickListener(this);
        _layout_customer_care.setOnClickListener(this);
        _layout_monthly_spend.setOnClickListener(this);
        _layout_reward.setOnClickListener(this);
        _layout_offer.setOnClickListener(this);
        llPromoCode.setOnClickListener(this);
        llSubscription.setOnClickListener(this);
        llSubscriptionHistory.setOnClickListener(this);
        llExclusiveOffers.setOnClickListener(this);
        llReferEarn.setOnClickListener(this);
        llOhiCredits.setOnClickListener(this);

        _layout_privacy_policy.setVisibility(View.GONE);
        _layout_cancellation_policy.setVisibility(View.GONE);
        _layout_refund_policy.setVisibility(View.GONE);


        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                hideKeyboard(MainActivity.this);
                if (preferencesUtility.getLogedin()) {
                    _txt_username.setText(preferencesUtility.getfirstname());
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });

        _txt_sos.setVisibility(View.VISIBLE);
        Fragment homeFragment = new DashBoardFragment();
        setFragment(homeFragment);
        updateTitle(getResources().getString(R.string.st_dashboard));

        _layout_notification_badge.setVisibility(View.GONE);
        _layout_notification_badge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        txtReferEarn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtReferEarn.setVisibility(View.GONE);

                updateTitle(getResources().getString(R.string.st_refer_earn));
                Fragment referEarnFragment = new ReferEarnFragment();
                setFragment(referEarnFragment);
            }
        });

        txtRedemption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, RedemptionListActivity.class));
            }
        });


        /* Get App Default Data */
        GetAppData();

        // Update OneSignal token
        updateDriverToken();

        // Update socket ID
        updateSocketID();
    }

    private void updateSocketID() {

        try {
            JSONObject objectUpdateSocket = new JSONObject();
            objectUpdateSocket.put("user_id", preferencesUtility.getuser_id());
            objectUpdateSocket.put("auth_token", preferencesUtility.getauth_token());
            appDelegate.emit("UpdateSocket", objectUpdateSocket.toString(), mSocket.id());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        /* Receiver Call from @OhiApplication */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_UpdateSocket, new IntentFilter("Update_Socket_Data"));

        /* Receiver Call from @RentalBookingActivity */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("Default_App_Data"));

        /* Receiver Call from @OhiApplication */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_UpdateAppData, new IntentFilter("User_Default_App_Data"));

        ForceUpdateChecker.with(this).onUpdateNeeded(this).check();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Unregister since the activity is paused.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_UpdateSocket);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_UpdateAppData);
    }

    /**
     * Update Socket Receiver
     * <p>
     * to Update new Socket ID on Server when, disconnect Internet Connection or drop connection
     */
    private BroadcastReceiver mMessageReceiver_UpdateSocket = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_UpdateSocket = intent.getStringExtra("socket_data");
            try {
                JSONObject objUpdate = new JSONObject(mResult_UpdateSocket);
                if (objUpdate.getString("status").equalsIgnoreCase("0")) {
                    preferencesUtility.ClearPrefrenceKey("login_type");
                    preferencesUtility.ClearPreferences();

                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                } else if (objUpdate.getString("status").equalsIgnoreCase("2")) {
                    Intent intent1 = new Intent(MainActivity.this, BlockUserActivity.class);
                    startActivity(intent1);
                    finish();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onClick(View v) {
        _floatingActionButton.setVisibility(View.GONE);
        _layout_notification_badge.setVisibility(View.GONE);
        _img_more_menu.setVisibility(View.GONE);
        _txt_msg_clear.setVisibility(View.GONE);
        _txt_sos.setVisibility(View.GONE);
        layout_serach.setVisibility(View.GONE);
        img_cal.setVisibility(View.GONE);
        txtReferEarn.setVisibility(View.GONE);
        txtRedemption.setVisibility(View.GONE);

        switch (v.getId()) {
            case R.id.layout_dashboard:
                updateTitle(getResources().getString(R.string.st_dashboard));
                Fragment dashBoardFragment = new DashBoardFragment();
                setFragment(dashBoardFragment);
                _layout_notification_badge.setVisibility(View.GONE);
                txtReferEarn.setVisibility(View.VISIBLE);

                break;

            case R.id.layout_book_ride:
                updateTitle("My Rides");
                Fragment bookRideFragment = new RideHistoryFragment();
                setFragment(bookRideFragment);
                break;

            case R.id.layout_payment:
                /*
                updateTitle(getResources().getString(R.string.st_payment));
                Fragment paymentFragment = new PaymentFragment();
                setFragment(paymentFragment); */
                break;

            case R.id.layout_notification:
                updateTitle(getResources().getString(R.string.st_notification));
                Fragment notificationFragment = new NotificationFragment();
                setFragment(notificationFragment);
                break;

            case R.id.layout_support:
                updateTitle(getResources().getString(R.string.st_support));
                Fragment supportFragment = new SupportFragment();
                setFragment(supportFragment);
                break;

            case R.id.layout_saved_card:
                /*
                _floatingActionButton.setVisibility(View.VISIBLE);
                updateTitle(getResources().getString(R.string.st_save_card));
                Fragment addCardFragment = new AddCardFragment();
                setFragment(addCardFragment); */
                break;

            case R.id.layout_terms_condition:
                updateTitle(getResources().getString(R.string.st_terms_conditions));
                Fragment policyFragment = new AllPolicyFragment();
                Bundle bundle = new Bundle();
                bundle.putString("link", "terms");
                policyFragment.setArguments(bundle);
                setFragment(policyFragment);
                break;

            case R.id.layout_privacy_policy:
                updateTitle(getResources().getString(R.string.st_privancy_policy));
                Fragment policyFragment1 = new AllPolicyFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putString("link", "privacy");
                policyFragment1.setArguments(bundle1);
                setFragment(policyFragment1);
                break;

            case R.id.layout_cancellation_policy:
                updateTitle(getResources().getString(R.string.st_cancellation_policy));
                Fragment policyFragment2 = new AllPolicyFragment();
                Bundle bundle2 = new Bundle();
                bundle2.putString("link", "cancellation");
                policyFragment2.setArguments(bundle2);
                setFragment(policyFragment2);
                break;

            case R.id.layout_refund_policy:
                updateTitle(getResources().getString(R.string.st_refund_policy));
                Fragment policyFragment3 = new AllPolicyFragment();
                Bundle bundle3 = new Bundle();
                bundle3.putString("link", "refund");
                policyFragment3.setArguments(bundle3);
                setFragment(policyFragment3);
                break;

            case R.id.layout_promo:
                updateTitle(getResources().getString(R.string.st_promo));
                Fragment userPromoFragment = new UserPromoFragment();
                setFragment(userPromoFragment);
                break;

            case R.id.layout_about_us:
                updateTitle(getResources().getString(R.string.st_about_us));
                Fragment aboutUsFragment = new AboutUsFragment();
                setFragment(aboutUsFragment);
                break;

            case R.id.layout_logout:
                LogoutUser();
                break;

            case R.id.layout_invite:
                updateTitle(getResources().getString(R.string.st_invite));
                Fragment inviteFragment = new InviteFragment();
                setFragment(inviteFragment);
                break;

            case R.id.nav_edit_profile:
                updateTitle("Profile");
                Fragment profileFragment = new ProfileFragment();
                setFragment(profileFragment);
                break;

            case R.id.layout_offer:
                updateTitle("");
                Fragment offerFragment = new OfferFragment();
                setFragment(offerFragment);
                break;

            case R.id.layout_customer_care:
                updateTitle(getResources().getString(R.string.st_customer_care_list));
                Fragment customerCareListFragment = new CustomerCareListFragment();
                setFragment(customerCareListFragment);
                break;

            case R.id.layout_reward:
                updateTitle(getResources().getString(R.string.st_reward));
                Fragment rewardFragment = new RewardFragment();
                setFragment(rewardFragment);
                break;

            case R.id.layout_monthly_spend:
                updateTitle(getResources().getString(R.string.st_monthly_spend));
                Fragment monthlySpendFragment = new MonthlySpendFragment();
                setFragment(monthlySpendFragment);
                break;

            case R.id.layout_subscription:
                startActivity(new Intent(MainActivity.this, SubscriptionActivity.class));

                break;

            case R.id.layout_exclusive_offer:
                updateTitle(getResources().getString(R.string.st_exclusive_offer));
                Fragment exclusiveHistoryFragment = new ExclusiveHistoryFragment();
                setFragment(exclusiveHistoryFragment);

                break;

            case R.id.layout_subscription_history:
                updateTitle(getResources().getString(R.string.st_subscription_history));
                Fragment subscriptionHistoryFragment = new SubscriptionHistoryFragment();
                setFragment(subscriptionHistoryFragment);

                break;

            case R.id.layout_refer_earn:
                updateTitle(getResources().getString(R.string.st_refer_earn));
                Fragment referEarnFragment = new ReferEarnFragment();
                setFragment(referEarnFragment);
                break;

            case R.id.layout_ohi_credits:
                txtRedemption.setVisibility(View.VISIBLE);
                updateTitle(getResources().getString(R.string.st_ohi_credits));
                Fragment ohiCreditsFragment = new OHICreditsFragment();
                setFragment(ohiCreditsFragment);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    /**
     * Update Header Title
     *
     * @param title
     */
    private void updateTitle(String title) {
        try {
            title = title.toUpperCase();
            _toolbar_title.setText(title);
            _toolbar_title.setTypeface(global_typeface.Sansation_Bold());
        } catch (Exception ex) {
            Log.e("TAG", "Error in updateTitle.");
            ex.printStackTrace();
        }
    }

    /**
     * Replace Fragment class
     *
     * @param fragment
     */
    protected void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flContent, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * Update App Default Data- Service List, Service Details
     * <p>
     * Receiver call for server send new data - Service List, Zone List, Service Details
     */
    private BroadcastReceiver mMessageReceiver_UpdateAppData = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_UpdateSocket = intent.getStringExtra("user_default_app_data");
            try {
                JSONObject objUpdate = new JSONObject(mResult_UpdateSocket);
                if (objUpdate.getString("status").equalsIgnoreCase("1")) {
                    GetAppData();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Get App Default Data Service Call
     * <p>
     * Get All new data - Service List, Zone List, Service Details
     */
    private void GetAppData() {
        if (isInternetOn(getApplicationContext())) {

            String stLastUpdatedDate;
            if (!preferencesUtility.CheckPreferences("data_last_update_date")) {
                stLastUpdatedDate = "";
            } else {
                stLastUpdatedDate = preferencesUtility.getLastUpdateDate();
            }

            Call<JsonObject> call = apiService.server_last_data(stLastUpdatedDate);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    setDefaultAppData(response.body().toString());
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }

    /**
     * Set App Default Data
     * <p>
     * Call From Booking Activity
     */
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            String message_data = intent.getStringExtra("default_data");
            setDefaultAppData(message_data);
        }
    };

    private void setDefaultAppData(String app_data) {
        ArrayList<ZoneList_Model> list_ZoneData = new ArrayList<>();
        ArrayList<ServiceDetail_Model> list_ServiceDetail = new ArrayList<>();
        ArrayList<ServicePrice_Model> list_ServicePrice = new ArrayList<>();
        ArrayList<CareList_Model> list_CarList = new ArrayList<>();

            /* Save Current Date & Time for Next Data Call*/
         /*
        try {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String stCurrentDate = df.format(c.getTime());

            preferencesUtility.setLastUpdateDate(stCurrentDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        */

        try {
            JSONObject objAppData = new JSONObject(app_data);

            if (objAppData.getString("status").equalsIgnoreCase("1")) {

                /* Save Current Date & Time for Next Data Call*/
                preferencesUtility.setLastUpdateDate(objAppData.getString("server_time"));

                JSONObject objPayload = objAppData.getJSONObject("payload");

                /* Zone List */
                JSONArray jarryZoneList = objPayload.getJSONArray("zone_list");
                if (jarryZoneList.length() > 0) {

                    for (int i = 0; i < jarryZoneList.length(); i++) {
                        JSONObject objZoneList = jarryZoneList.getJSONObject(i);
                        ZoneList_Model zoneList_model = new ZoneList_Model();

                        zoneList_model.setZone_id(objZoneList.getString("zone_id"));
                        zoneList_model.setZone_name(objZoneList.getString("zone_name"));
                        zoneList_model.setGoogle_zone_name(objZoneList.getString("google_zone_name"));
                        zoneList_model.setCity(objZoneList.getString("city"));
                        zoneList_model.setCreated_date(objZoneList.getString("created_date"));
                        zoneList_model.setModify_date(objZoneList.getString("modify_date"));
                        zoneList_model.setStatus(objZoneList.getString("status"));
                        zoneList_model.setTax(objZoneList.getString("tax"));
                        zoneList_model.setGst_tax(objZoneList.getString("gst_tax"));
                        zoneList_model.setPaytm_term_html(objZoneList.getString("paytm_term_html"));

                        list_ZoneData.add(zoneList_model);
                    }
                    mHelper.InsertZoneListData(list_ZoneData);
                }

                    /* Service Details */
                JSONArray jarryServiceDetails = objPayload.getJSONArray("service_detail");
                if (jarryServiceDetails.length() > 0) {
                    for (int j = 0; j < jarryServiceDetails.length(); j++) {
                        JSONObject objServiceDetails = jarryServiceDetails.getJSONObject(j);
                        ServiceDetail_Model serviceDetail_model = new ServiceDetail_Model();

                        serviceDetail_model.setService_id(objServiceDetails.getString("service_id"));
                        serviceDetail_model.setService_name(objServiceDetails.getString("service_name"));
                        serviceDetail_model.setSeat(objServiceDetails.getString("seat"));
                        serviceDetail_model.setColor(objServiceDetails.getString("color"));
                        serviceDetail_model.setIcon(objServiceDetails.getString("icon"));
                        serviceDetail_model.setTop_icon(objServiceDetails.getString("top_icon"));
                        serviceDetail_model.setGender(objServiceDetails.getString("gender"));
                        serviceDetail_model.setIs_taxi(objServiceDetails.getString("is_taxi"));
                        serviceDetail_model.setCreated_date(objServiceDetails.getString("created_date"));
                        serviceDetail_model.setModify_date(objServiceDetails.getString("modify_date"));
                        serviceDetail_model.setStatus(objServiceDetails.getString("status"));
                        serviceDetail_model.setImage_details(objServiceDetails.getString("image_details"));
                        serviceDetail_model.setDescription(objServiceDetails.getString("description"));

                        list_ServiceDetail.add(serviceDetail_model);
                    }
                    boolean s = mHelper.InsertServiceDetailsData(list_ServiceDetail);
                }

                    /* Service Price - price_mnt Table*/
                JSONArray jarryServicePrice = objPayload.getJSONArray("service_price");
                if (jarryServicePrice.length() > 0) {
                    for (int k = 0; k < jarryServicePrice.length(); k++) {
                        JSONObject objServicePrice = jarryServicePrice.getJSONObject(k);
                        ServicePrice_Model servicePrice_model = new ServicePrice_Model();

                        servicePrice_model.setPrice_id(objServicePrice.getString("price_id"));
                        servicePrice_model.setZone_id(objServicePrice.getString("zone_id"));
                        servicePrice_model.setService_id(objServicePrice.getString("service_id"));
                        servicePrice_model.setBase_charge(objServicePrice.getString("base_charge"));
                        servicePrice_model.setPer_km_charge(objServicePrice.getString("per_km_charge"));
                        servicePrice_model.setPer_minute_charge(objServicePrice.getString("per_minute_charge"));
                        servicePrice_model.setBooking_charge(objServicePrice.getString("booking_charge"));
                        servicePrice_model.setMinimum_fair(objServicePrice.getString("minimum_fair"));
                        servicePrice_model.setCancel_charge(objServicePrice.getString("cancel_charge"));
                        servicePrice_model.setCreated_at(objServicePrice.getString("created_at"));
                        servicePrice_model.setModify_date(objServicePrice.getString("modify_date"));
                        servicePrice_model.setStatus(objServicePrice.getString("status"));
                        servicePrice_model.setMinimum_km(objServicePrice.getString("minimum_km"));
                        servicePrice_model.setSetsec_ride_per(objServicePrice.getString("sec_ride_per"));
                        servicePrice_model.setPaytm_offer(objServicePrice.getString("paytm_offer"));
                        servicePrice_model.setMax_offer_amount(objServicePrice.getString("max_offer_amount"));
                        servicePrice_model.setRental_time(objServicePrice.getString("rental_time"));

                        servicePrice_model.setAllowance(objServicePrice.getString("allowance"));
                        servicePrice_model.setTokenPer(objServicePrice.getString("token_per"));
                        servicePrice_model.setMaxToken(objServicePrice.getString("max_token"));
                        servicePrice_model.setGst(objServicePrice.getString("gst"));
                        servicePrice_model.setMaxTax(objServicePrice.getString("max_tax"));

                        list_ServicePrice.add(servicePrice_model);
                    }
                    mHelper.InsertServicePriceData(list_ServicePrice);
                }


                     /* Care List */
                JSONArray jarryCareList = objPayload.getJSONArray("care_list");
                if (jarryCareList.length() > 0) {
                    for (int k = 0; k < jarryCareList.length(); k++) {
                        JSONObject objCareList = jarryCareList.getJSONObject(k);
                        CareList_Model careList_model = new CareList_Model();

                        careList_model.setCare_id(objCareList.getString("care_id"));
                        careList_model.setCare_name(objCareList.getString("care_name"));
                        careList_model.setCare_number(objCareList.getString("care_number"));
                        careList_model.setStatus(objCareList.getString("status"));
                        careList_model.setCreated_date(objCareList.getString("created_date"));
                        careList_model.setModify_date(objCareList.getString("modify_date"));
                        careList_model.setAddress(objCareList.getString("address"));

                        careList_model.setCare_time(objCareList.getString("time"));
                        careList_model.setLanguages(objCareList.getString("languages"));
                        careList_model.setCare_driver_number(objCareList.getString("care_driver_number"));
                        careList_model.setCare_driver_time(objCareList.getString("care_driver_time"));

                        list_CarList.add(careList_model);
                    }
                    mHelper.InsertCareList(list_CarList);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * User Logout from app
     */
    private void LogoutUser() {
        try {

            if (isInternetOn(getApplicationContext())) {
                Call<JsonObject> call = apiService.logout(preferencesUtility.getuser_id(), preferencesUtility.getauth_token());
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try {
                            JSONObject objBookingData = new JSONObject(response.body().toString());

                            if (objBookingData.getString("success").equalsIgnoreCase("true") && objBookingData.getString("status").equalsIgnoreCase("1")) {
                                preferencesUtility.ClearPrefrenceKey("login_type");
                                preferencesUtility.ClearPreferences();

                                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();
                            } else if (objBookingData.getString("status").equalsIgnoreCase("2")) {

                                JSONArray jsonArray = objBookingData.getJSONArray("payload");

                                Intent intent_vale_request = new Intent(getApplicationContext(), ReviewRideActivity.class);
                                intent_vale_request.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent_vale_request.putExtra("panding_payment", jsonArray.toString());
                                startActivity(intent_vale_request);

                            } else {
                                ShowToast(objBookingData.getString("message"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            } else {
                toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public void onUpdateNeeded(final String updateUrl) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please update your application to continue.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).create();
        dialog.show();
    }

    /**
     * update OneSignal user device token
     */
    private void updateDriverToken() {

        try {

            Map<String, String> params = new HashMap<>();
            params.put("user_id", preferencesUtility.getuser_id());
            params.put("device_token", appDelegate.getOneSignalToken());

            VolleyUtils.POST_METHOD_URL_ENCODED(MainActivity.this, UPDATE_ONE_SIGNAL_ID, params, new VolleyResponseListener() {
                @Override
                public void onError(String message) {
                    System.out.println("Error" + message);
                }

                @Override
                public void onResponse(Object response) {
                    try {
                        JSONObject objServiceOnOff = new JSONObject(response.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


}
