package com.ohicabs.user.ohi.PushNotification;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Activity.SplashActivity;
import com.ohicabs.user.ohi.StorageManager.DatabaseHelper;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private DatabaseHelper mDBHelper;
    private String stCurrentDate = "";
    private SharedPreferencesUtility preferencesUtility;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        mDBHelper = new DatabaseHelper(getApplicationContext());
        preferencesUtility = new SharedPreferencesUtility(getApplicationContext());

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.getData());

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }
        }

        try {
            Calendar c = Calendar.getInstance();
            System.out.println("Current time => " + c.getTime());

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
            stCurrentDate = df.format(c.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            if (remoteMessage.getData().get("notification_id").equalsIgnoreCase("0")) {
                mDBHelper.InsertPushNotificationData(remoteMessage.getData().get("notification_id"), remoteMessage.getData().get("message"), stCurrentDate);
                sendNotification(remoteMessage.getNotification().getBody(), remoteMessage);

            } else if (remoteMessage.getData().get("notification_id").equalsIgnoreCase("2")) {
//                Driver Allocated
                sendNotification(remoteMessage.getNotification().getBody(), remoteMessage);

            } else if (remoteMessage.getData().get("notification_id").equalsIgnoreCase("3")) {
                sendNotification(remoteMessage.getNotification().getBody(), remoteMessage);

            } else if (remoteMessage.getData().get("notification_id").equalsIgnoreCase("5")) {

                //RIDE STARTED


                // Set Rental Ride Start Time
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                String strRideStartTime = simpleDateFormat.format(calendar.getTime());

                preferencesUtility.setIsRideStarted("true");
                preferencesUtility.setCurrentTimeRideStart(strRideStartTime);
                preferencesUtility.setStartRideBookindId(remoteMessage.getData().get("booking_id"));

                sendNotification(remoteMessage.getNotification().getBody(), remoteMessage);

            } else if (remoteMessage.getData().get("notification_id").equalsIgnoreCase("6")) {

                //RIDE CONCLUDED
                preferencesUtility.setIsRideStarted("false");
                Log.e("MyFirebaseMsgService", "end_done");
                sendNotification(remoteMessage.getNotification().getBody(), remoteMessage);

            } else if (remoteMessage.getData().get("notification_id").equalsIgnoreCase("7")) {
                sendNotification(remoteMessage.getNotification().getBody(), remoteMessage);

            } else if (remoteMessage.getData().get("notification_id").equalsIgnoreCase("8")) {
                //Driver has arrived
                sendNotification(remoteMessage.getNotification().getBody(), remoteMessage);

            } else if (remoteMessage.getData().get("notification_id").equalsIgnoreCase("9")) {
                mDBHelper.InsertPushNotificationData(remoteMessage.getData().get("notification_id"), remoteMessage.getData().get("message"), stCurrentDate);
                sendNotification(remoteMessage.getNotification().getBody(), remoteMessage);

            } else if (remoteMessage.getData().get("notification_id").equalsIgnoreCase("10")) {
                mDBHelper.InsertPushNotificationData(remoteMessage.getData().get("notification_id"), remoteMessage.getData().get("message"), stCurrentDate);
                sendNotification(remoteMessage.getNotification().getBody(), remoteMessage);

            } else if (remoteMessage.getData().get("notification_id").equalsIgnoreCase("11")) {
                sendNotification(remoteMessage.getNotification().getBody(), remoteMessage);

            } else if (remoteMessage.getData().get("notification_id").equalsIgnoreCase("12")) {
                mDBHelper.InsertPushNotificationData(remoteMessage.getData().get("notification_id"), remoteMessage.getData().get("message"), stCurrentDate);
                sendNotification(remoteMessage.getNotification().getBody(), remoteMessage);

            } else if (remoteMessage.getData().get("notification_id").equalsIgnoreCase("14")) {
                mDBHelper.InsertPushNotificationData(remoteMessage.getData().get("notification_id"), remoteMessage.getData().get("message"), stCurrentDate);
                sendNotification(remoteMessage.getNotification().getBody(), remoteMessage);
            } else if (remoteMessage.getData().get("notification_id").equalsIgnoreCase("17")) {
                mDBHelper.InsertPushNotificationData(remoteMessage.getData().get("notification_id"), remoteMessage.getData().get("message"), stCurrentDate);
                sendNotification(remoteMessage.getNotification().getBody(), remoteMessage);
            } else if (remoteMessage.getData().get("notification_id").equalsIgnoreCase("18")) {
                mDBHelper.InsertPushNotificationData(remoteMessage.getData().get("notification_id"), remoteMessage.getData().get("message"), stCurrentDate);
                sendNotification(remoteMessage.getNotification().getBody(), remoteMessage);
            }
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("custom-event-name"));
    }
    // [END receive_message]

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    private void scheduleJob() {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */

    final static String GROUP_KEY_EMAILS = "group_key_ohi_user";

    private void sendNotification(String messageBody, RemoteMessage remoteMessage) {

        //0, 9 , 10,12
        //notificationid, message

        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        if (remoteMessage.getData().containsKey("sound")) {

            if (remoteMessage.getData().get("sound").equalsIgnoreCase("CuckooSound.mpeg")) {
                Uri sound = Uri.parse("android.resource://" + this.getPackageName() + "/" + R.raw.cuckoosound);//Here is FILE_NAME is the name of file that you want to play
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "")
                        .setSmallIcon(R.drawable.ic_onesignal_large_icon_default)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setGroup(GROUP_KEY_EMAILS)
                        .setSound(sound)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setContentIntent(pendingIntent);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
            } else {
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "")
                        .setSmallIcon(R.drawable.ic_onesignal_large_icon_default)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setGroup(GROUP_KEY_EMAILS)
                        .setSound(defaultSoundUri)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setContentIntent(pendingIntent);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
            }
        } else {
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "")
                    .setSmallIcon(R.drawable.ic_onesignal_large_icon_default)
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setGroup(GROUP_KEY_EMAILS)
                    .setSound(defaultSoundUri)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            Log.d("receiver", "Got message: " + message);
        }
    };
}
