package com.ohicabs.user.ohi.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DemoActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setViews();
    }

    private void setViews() {

        String stJson = "{   \"firstName\": \"Assaf\",   \"lastName\": \"Scialom\",   \"email\": \"assaf_106@iqdesk.net\",   \"PhoneNumber\": \"015252999\",   \"socialID\": \"\",   \"password\": \"123456\",   \"passwordRepeat\": \"123456\",   \"accountType\": \"\",   \"termsAgreement\": \"\" }";
        Call<JsonObject> call = apiService.user_signup(stJson);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {

                    JsonObject object = response.body();
                    Log.e("TAG_RESULT", object.toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
