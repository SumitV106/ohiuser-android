package com.ohicabs.user.ohi.Fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.Global_Typeface;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

/**
 * A simple {@link Fragment} subclass.
 */
public class InviteFragment extends Fragment {

    @BindView(R.id.btn_invite)
    Button _btn_invite;

    private ToastDialog toastDialog;
    private Global_Typeface global_typeface;
    private SharedPreferencesUtility sharedPreferencesUtility;
    private ProgressDialog progressDialog;

    ApiInterface apiService;

    private String stImageUri = "";

    @BindView(R.id.txt_title)
    TextView _txt_title;
    @BindView(R.id.txt_details)
    TextView _txt_details;
    @BindView(R.id.txt_note)
    TextView _txt_note;
    @BindView(R.id.img_offer)
    ImageView _img_offer;

    public InviteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_invite, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toastDialog = new ToastDialog(getActivity());
        global_typeface = new Global_Typeface(getActivity());
        sharedPreferencesUtility = new SharedPreferencesUtility(getActivity());
        apiService = ApiClient.getClient().create(ApiInterface.class);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.st_please_wait));
        progressDialog.setCancelable(false);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        _btn_invite.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent sendIntent = new Intent();
//                sendIntent.setAction(Intent.ACTION_SEND);
//                sendIntent.putExtra(Intent.EXTRA_TEXT,
//                        "https://play.google.com/store/apps/details?id=" + getActivity().getPackageName());
//                sendIntent.setType("text/plain");
//                startActivity(sendIntent);
//            }
//        });

        inviteDetails();
    }

    private void inviteDetails() {

        if (isInternetOn(getContext())) {
            try {
                progressDialog.show();
                Call<JsonObject> call = apiService.invite_detail("1");

                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try {
                            progressDialog.dismiss();
                            Log.e("RES_invite", response.body().toString());
                            JSONObject objBookingList = new JSONObject(response.body().toString());
                            if (objBookingList.getString("status").equalsIgnoreCase("1")) {

                                JSONObject objPayload = objBookingList.getJSONObject("payload");

                                _txt_title.setText(objPayload.getString("title"));
                                _txt_details.setText(objPayload.getString("details"));
                                _txt_note.setText(objPayload.getString("note"));

                                stImageUri = Global_ServiceApi.API_IMAGE_HOST + objPayload.getString("image");

                                setBannerImage(Uri.parse(Global_ServiceApi.API_IMAGE_HOST + objPayload.getString("image")));


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setBannerImage(Uri uri) {

        try {

            Glide.with(getActivity()).load(uri).asBitmap().diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate().into(new BitmapImageViewTarget(_img_offer) {
                @Override
                public void onResourceReady(final Bitmap bmp, GlideAnimation anim) {
                    _img_offer.setImageBitmap(bmp);

                    _btn_invite.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareShareIntent(bmp);
                        }
                    });
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    super.onLoadFailed(e, errorDrawable);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prepareShareIntent(Bitmap bmp) {

        try {

            Uri bmpUri = getLocalBitmapUri(bmp); // see previous remote images section
            // Construct share intent as described above based on bitmap

            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);

            shareIntent.putExtra(Intent.EXTRA_TEXT, _txt_title.getText().toString()
                    + "\n\n" + _txt_details.getText().toString() + "\n\n" + _txt_note.getText().toString() + "\n\n" + "https://play.google.com/store/apps/details?id=" + getActivity().getPackageName());
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            shareIntent.setType("image/*");
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(Intent.createChooser(shareIntent, "Invite to User"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Uri getLocalBitmapUri(Bitmap bmp) {

        Uri bmpUri = null;
        File file = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            bmpUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", file);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

}
