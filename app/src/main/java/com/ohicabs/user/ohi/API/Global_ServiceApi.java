package com.ohicabs.user.ohi.API;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.ohicabs.user.ohi.Utils.CryptLib;


/**
 * Created by sumit on 10/04/17.
 */

public class Global_ServiceApi {

    /* live server */
    public static final String API_HOST = "https://ohicabs.com:3004/";
    public static final String API_IMAGE_HOST = "https://ohicabs.com:3004/img/";
    public static final String SHARE_LINK = "https://ohicabs.com/admin/#/track/";

    public static final String UPDATE_ONE_SIGNAL_ID = API_HOST + "api/update_token";

    /*testing server*/
//    public static final String API_HOST = "http://139.59.61.96:3001/";
//    public static final String API_IMAGE_HOST = "http://139.59.61.96:3001/img/";
//    public static final String SHARE_LINK = "http://ohicabs.com/dev/#/track/";

    /*testing server*/
//    public static final String API_HOST = "http://139.59.61.96:3002/";
//    public static final String API_IMAGE_HOST = "http://139.59.61.96:3002/img/";

    /* local server */
//    public static final String API_HOST = "http://10.124.203.125:3001/";
//    public static final String API_IMAGE_HOST = "http://10.124.203.125:3001/img/";

    public static final String OFFER_LIST = API_HOST + "api/offer_list";

    public static final String SUBSCRIPTION_LIST = API_HOST + "API/subscription_plan_list";

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Nullable
    public static String EncryptCard(String cardJson, String test_decrypt) {
        try {
            CryptLib _crypt = new CryptLib();
            String output = "";

            String iv = CryptLib.generateRandomIV(16); //16 bytes = 128 bit
            String newIV = iv + test_decrypt;
            String plainText = cardJson;
            String key = CryptLib.SHA256(newIV, 32); //32 bytes = 256 bit
            output = _crypt.encrypt(plainText, key, newIV); //encrypt
            output = iv + "" + output;
            return output;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Check Internet Connection connectivity
     *
     * @param context
     * @return
     */
    public final static boolean isInternetOn(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return true;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return true;
        }
        return false;
    }

}
