package com.ohicabs.user.ohi.ohiOutStation;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.ohicabs.user.ohi.Activity.BaseActivity;
import com.ohicabs.user.ohi.Adpater.PriceListAdapter;
import com.ohicabs.user.ohi.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OutStationPriceListActivity extends BaseActivity implements OnMapReadyCallback, RoutingListener {

    private static String TAG = "OutStationPrice";

    @BindView(R.id.txt_estimated_time) AppCompatTextView txtEstimatedTime;
    @BindView(R.id.recyclerViewPriceList) RecyclerView recyclerViewPriceList;
    @BindView(R.id.img_back) AppCompatImageView imgBack;
    @BindView(R.id.btn_continue) AppCompatButton btnContinue;

    private SupportMapFragment mSupportMapFragment;

    private GoogleMap mGoogleMap;
    private GoogleApiClient mGoogleApiClient;

    private HashMap<String, Object> hashMapRideParam = new HashMap<>();
    private ArrayList<HashMap> arrHashMapPriceList = new ArrayList<>();
    private ArrayList<HashMap> hashMapPriceList = new ArrayList<>();

    private static final int[] COLORS = new int[]{R.color.colorPolyLine};

    PriceListAdapter priceListAdapter;
    public double mPickUpLat = 0.0, mPickUpLng = 0.0, mDropLat = 0.0, mDropLng = 0.0;
    private String stSelectedPrice = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roundtrip_price);
        ButterKnife.bind(this);

        setViews();
        init();
    }

    private void init() {
        hashMapRideParam = (HashMap<String, Object>) getIntent().getSerializableExtra("hashMap_BookingParam");

        arrHashMapPriceList = mHelper.GetOutStationPriceList(hashMapRideParam.get("stServiceID").toString(), hashMapRideParam.get("stZoneID").toString());

        double tempTotalDistance, tempTotalDuration;
        tempTotalDistance = (double) hashMapRideParam.get("google_km") * 2;
        tempTotalDuration = (double) hashMapRideParam.get("google_duration") * 2;

        for (int i = 0; i < arrHashMapPriceList.size(); i++) {

            double tempTotalAmount = Double.parseDouble(arrHashMapPriceList.get(i).get("base_charge").toString()) +
                    (Double.parseDouble(arrHashMapPriceList.get(i).get("per_km_charge").toString()) * tempTotalDistance) +
                    (Double.parseDouble(arrHashMapPriceList.get(i).get("per_minute_charge").toString()) * tempTotalDuration) +
                    (Double.parseDouble(arrHashMapPriceList.get(i).get("allowance").toString()));

            // Get minimum km
            double minimumKM = 0.0;
            if (!arrHashMapPriceList.get(i).get("minimum_km").equals("")) {
                minimumKM = Double.parseDouble(arrHashMapPriceList.get(i).get("minimum_km").toString());
            }

            // If totalDistance is smaller then minimum km then ride amount is fixed
            if (minimumKM >= tempTotalDistance) {
                tempTotalAmount = Double.parseDouble(arrHashMapPriceList.get(i).get("minimum_fair").toString());
            }

            // If Ride amount is smaller then minimum fair amount then ride amount is fixed
            if (Double.parseDouble(arrHashMapPriceList.get(i).get("minimum_fair").toString()) > tempTotalAmount) {
                tempTotalAmount = Double.parseDouble(arrHashMapPriceList.get(i).get("minimum_fair").toString());
            }

            String rentalTime = calRoundTripFormat(Integer.parseInt(arrHashMapPriceList.get(i).get("rental_time").toString()));
            long totalAmount = Math.round(tempTotalAmount);
            String priceId = arrHashMapPriceList.get(i).get("price_id").toString();
            double tempEstimatedTime = tempTotalDuration + (Double.parseDouble(arrHashMapPriceList.get(i).get("rental_time").toString()));
            String stMaxOfferAmount = arrHashMapPriceList.get(i).get("max_offer_amount").toString();
            String stPaytmOffer = arrHashMapPriceList.get(i).get("paytm_offer").toString();

            HashMap<String, Object> hashMapPrice = new HashMap<>();
            hashMapPrice.put("rental_time", rentalTime);
            hashMapPrice.put("price", totalAmount);
            hashMapPrice.put("price_id", priceId);
            hashMapPrice.put("eta", tempEstimatedTime);
            hashMapPrice.put("paytm_offer", stPaytmOffer);
            hashMapPrice.put("max_offer_amount", stMaxOfferAmount);

            hashMapPriceList.add(hashMapPrice);
        }

        priceListAdapter = new PriceListAdapter(OutStationPriceListActivity.this, hashMapPriceList, new PriceListAdapter.BtnClickListener() {
            @Override
            public void onBtnClick(int position, double time, String priceID, long price, String paytmOfferAmt, String maxPaytmOfferAmt) {
                txtEstimatedTime.setText(timeConvert((int)time));
                stSelectedPrice = priceID;

                hashMapRideParam.put("stPriceID", priceID);
                hashMapRideParam.put("price", price);
                hashMapRideParam.put("paytm_offer", paytmOfferAmt);
                hashMapRideParam.put("max_offer_amount", maxPaytmOfferAmt);
            }
        });
        recyclerViewPriceList.setAdapter(priceListAdapter);

        mDropLat = (double) hashMapRideParam.get("mDropLat");
        mDropLng = (double) hashMapRideParam.get("mDropLng");

        mPickUpLat = (double) hashMapRideParam.get("mPickUpLat");
        mPickUpLng = (double) hashMapRideParam.get("mPickUpLng");
    }

    private void setViews() {

        recyclerViewPriceList.setItemAnimator(new DefaultItemAnimator());
        recyclerViewPriceList.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewPriceList.setLayoutManager(layoutManager);

        mSupportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map);
        mSupportMapFragment.getMapAsync(this);

        // initializing location listener and other related stuffs.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stSelectedPrice.equalsIgnoreCase("")) {
                    ShowToast("Please select any one waiting time option");
                } else {
                    Intent intent = new Intent(OutStationPriceListActivity.this, OutStationConfirmRideActivity.class);
                    intent.putExtra("hashMap_BookingParam", hashMapRideParam);
                    startActivity(intent);
                }
            }
        });
    }

    public String timeConvert(int time) {
        return time/24/60 + "day " + time/60%24 + "hr " + time%60 + "m";
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
        mGoogleMap.getUiSettings().setCompassEnabled(false);
        mGoogleMap.getUiSettings().setTiltGesturesEnabled(false);
        mGoogleMap.setBuildingsEnabled(false);

        //Disable Marker click event
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });

        Marker markerDrop = mGoogleMap.addMarker(new MarkerOptions()
                .position(new LatLng(mDropLat, mDropLng))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin)));

        Marker markerPickup = mGoogleMap.addMarker(new MarkerOptions()
                .position(new LatLng(mPickUpLat, mPickUpLng))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin)));

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        //the include method will calculate the min and max bound.
        builder.include(markerPickup.getPosition());
        builder.include(markerDrop.getPosition());

        LatLngBounds bounds = builder.build();
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,50);
        mGoogleMap.animateCamera(cu);

        getRouteToMarker(new LatLng(mPickUpLat, mPickUpLng), new LatLng(mDropLat, mDropLng));
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mSupportMapFragment.onLowMemory();
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        if(e != null) {
            Log.e("TAG_ERROR", "Error: " + e.getMessage());
        } else {
            Log.e("TAG_ERROR", "Something went wrong, Try again");
        }
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        //add route(s) to the map.
        for (int i = 0; i < route.size(); i++) {

            //In case of more than 5 alternative routes
            int colorIndex = i % COLORS.length;

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
            polyOptions.width(10 + i * 3);
            polyOptions.addAll(route.get(i).getPoints());
            Polyline polyline = mGoogleMap.addPolyline(polyOptions);

        }
    }

    @Override
    public void onRoutingCancelled() {

    }

    /**
     *
     * Get Route Polyline
     *
     * @param currentLatLng
     * @param pickupLatLng
     */
    private void getRouteToMarker(LatLng currentLatLng, LatLng pickupLatLng) {
        if (pickupLatLng != null && currentLatLng != null){
            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .alternativeRoutes(false)
                    .waypoints(currentLatLng, pickupLatLng)
                    .key("AIzaSyAfye39JZYcYdS196GZrvxRBfMShOCaA3w")
                    .build();
            routing.execute();
        }
    }

    private String calRoundTripFormat(int rentalTime) {

        switch (rentalTime) {
            case 240:
                return "0 - 4 hour";
            case 480:
                return "4 - 8 hour";
            case 720:
                return "8 - 12 hour";
            case 1440:
                return "1 day";
            case 2880:
                return "2 day";
            case 4320:
                return "3 day";
            case 5760:
                return "4 day";
            default:
                return "-";
        }
    }

}
