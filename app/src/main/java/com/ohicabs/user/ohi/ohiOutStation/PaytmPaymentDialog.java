package com.ohicabs.user.ohi.ohiOutStation;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class PaytmPaymentDialog extends DialogFragment {

    public ApiInterface apiService;
    public SharedPreferencesUtility preferencesUtility;
    String stPaytmTransactionID = "", stOrderID = "", stFailPayload = "", stSuccessPayload = "", stBookingId;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {


        if (getArguments() != null) {
            if (getArguments().getBoolean("notAlertDialog")) {
                return super.onCreateDialog(savedInstanceState);
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Alert Dialog");
        builder.setMessage("Alert Dialog inside DialogFragment");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });

        return builder.create();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_paytm_dailog, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        apiService = ApiClient.getClient().create(ApiInterface.class);
        preferencesUtility = new SharedPreferencesUtility(getActivity());

        AppCompatTextView txtBookingId = view.findViewById(R.id.txt_booking_id);
        AppCompatTextView txtUpFrontAmount = view.findViewById(R.id.txt_upfront_amount);

        AppCompatImageView imgClose = view.findViewById(R.id.img_close);
        AppCompatButton btnPay = view.findViewById(R.id.btn_pay);
        AppCompatTextView txtNote = view.findViewById(R.id.txt_note);

        if (getArguments() != null && !TextUtils.isEmpty(getArguments().getString("booking_id"))) {
            txtBookingId.setText("#" + getArguments().getString("booking_id"));
            stBookingId = getArguments().getString("booking_id");
        }

        if (getArguments() != null && !TextUtils.isEmpty(getArguments().getString("upfront_amount")))
            txtUpFrontAmount.setText("₹ " +getArguments().getString("upfront_amount"));

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        txtNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generateChecksum(getArguments().getString("booking_id"));
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean setFullScreen = false;
        if (getArguments() != null) {
            setFullScreen = getArguments().getBoolean("fullScreen");
        }

        if (setFullScreen)
            setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public interface DialogListener {
        void onFinishEditDialog();
    }

    /**
     * Generate Paytm Checksum for ride payment
     *
     * @param bookingID
     */
    private void generateChecksum(String bookingID) {
        if (isInternetOn(getActivity())) {
            final PaytmPGService paytmPGService = PaytmPGService.getProductionService(); // Production

            Call<JsonObject> call = apiService.upfront_generate_checksum(preferencesUtility.getuser_id(), preferencesUtility.getauth_token(), bookingID);

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        JSONObject objBookingData = new JSONObject(response.body().toString());

                        if (objBookingData.optString("success").equalsIgnoreCase("true") &&
                                objBookingData.optString("status").equalsIgnoreCase("1")) {

                            JSONObject objPayload = objBookingData.optJSONObject("payload");

                            // Create a HASHMAP Object holding the Order Information
                            HashMap<String, String> paramMap = new HashMap<String, String>();
                            paramMap.put("MID", objPayload.getString("MID"));
                            paramMap.put("ORDER_ID", objPayload.getString("ORDER_ID"));
                            paramMap.put("CUST_ID", objPayload.getString("CUST_ID"));
                            paramMap.put("INDUSTRY_TYPE_ID", objPayload.getString("INDUSTRY_TYPE_ID"));
                            paramMap.put("CHANNEL_ID", objPayload.getString("CHANNEL_ID"));
                            paramMap.put("TXN_AMOUNT", objPayload.getString("TXN_AMOUNT"));
                            paramMap.put("WEBSITE", objPayload.getString("WEBSITE"));
                            paramMap.put("CALLBACK_URL", objPayload.getString("CALLBACK_URL"));

                            if (objPayload.has("EMAIL")) {
                                paramMap.put("EMAIL", objPayload.getString("EMAIL"));
                            }
                            if (objPayload.has("MOBILE_NO")) {
                                paramMap.put("MOBILE_NO", objPayload.getString("MOBILE_NO"));
                            }

                            paramMap.put("CHECKSUMHASH", objPayload.getString("CHECKSUMHASH"));

                            // Create Paytm Order Object
                            PaytmOrder paytmOrder = new PaytmOrder(paramMap);

                            paytmPGService.initialize(paytmOrder, null);

                            paytmPGService.startPaymentTransaction(getActivity(), true, true,
                                    new PaytmPaymentTransactionCallback() {
                                        @Override
                                        public void onTransactionResponse(Bundle bundle) {
                                            try {

                                                if (bundle.getString("STATUS").equals("TXN_SUCCESS")) {
                                                    JSONObject json = new JSONObject();
                                                    Set<String> keys = bundle.keySet();
                                                    for (String key : keys) {
                                                        try {
                                                            // json.put(key, bundle.get(key)); see edit below
                                                            json.put(key, JSONObject.wrap(bundle.get(key)));
                                                        } catch (JSONException e) {
                                                            //Handle exception here
                                                        }
                                                    }

                                                    stSuccessPayload = json.toString();
                                                    if (bundle.containsKey("TXNID")) {
                                                        stPaytmTransactionID = bundle.getString("TXNID").toString();
                                                    }
                                                    stOrderID = bundle.getString("ORDERID").toString();

                                                    upFrontAmountPaymentServiceCall();

                                                } else {
                                                    JSONObject json = new JSONObject();
                                                    Set<String> keys = bundle.keySet();
                                                    for (String key : keys) {
                                                        try {
                                                            // json.put(key, bundle.get(key)); see edit below
                                                            json.put(key, JSONObject.wrap(bundle.get(key)));
                                                        } catch (JSONException e) {
                                                            //Handle exception here
                                                        }
                                                    }

                                                    stFailPayload = json.toString();
                                                    if (bundle.containsKey("TXNID")) {
                                                        stPaytmTransactionID = bundle.getString("TXNID").toString();
                                                    }
                                                    stOrderID = bundle.getString("ORDERID").toString();

                                                    failServiceCall();
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void networkNotAvailable() {
                                            Log.d("LOG", "UI Error Occur.");
                                            Toast.makeText(getActivity(), " UI Error Occur. ", Toast.LENGTH_LONG).show();
                                        }

                                        @Override
                                        public void clientAuthenticationFailed(String inErrorMessage) {
                                            Log.d("LOG", "clientAuthenticationFailed.");
                                            Toast.makeText(getActivity(), " Sever-side Error " + inErrorMessage, Toast.LENGTH_LONG).show();
                                        }

                                        @Override
                                        public void someUIErrorOccurred(String s) {
                                            Log.d("LOG", "UI Error Occur.");
                                        }

                                        @Override
                                        public void onErrorLoadingWebPage(int i, String s, String s1) {
                                            Log.d("LOG", "onErrorLoadingWebPage.");
                                        }

                                        @Override
                                        public void onBackPressedCancelTransaction() {
                                            Log.d("LOG", "onBackPressedCancelTransaction");
                                        }

                                        @Override
                                        public void onTransactionCancel(String s, Bundle bundle) {
                                            Log.d("LOG", "Payment_Transaction_Failed " + bundle);
                                            Toast.makeText(getActivity(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
                                        }
                                    });


                        } else {
                            ShowToast("Internal server error! Please try again later");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                }
            });
        } else {
            ShowToast(getString(R.string.st_internet_not_available));
        }
    }

    private void failServiceCall() {
        Call<JsonObject> call = apiService.paytm_payment_fail(stBookingId, stFailPayload, stPaytmTransactionID, stOrderID);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                DialogListener dialogListener = (DialogListener) getActivity();
                dialogListener.onFinishEditDialog();
                dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    /**
     * User give upfront amount to driver
     */
    private void upFrontAmountPaymentServiceCall() {
        try {
            Call<JsonObject> call = apiService.upfront_payment(preferencesUtility.getuser_id(), preferencesUtility.getauth_token(), stBookingId,
                    stPaytmTransactionID, stSuccessPayload, stOrderID);

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        String mResult = response.body().toString();

                        JSONObject objData = new JSONObject(mResult);
                        if (objData.getString("status").equalsIgnoreCase("1")) {
                            ShowToast(objData.getString("message"));

                            DialogListener dialogListener = (DialogListener) getActivity();
                            dialogListener.onFinishEditDialog();
                            dismiss();
                        }
                    } catch (Exception e) {
                        Log.e("Exception", e.toString());
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.e("onFailure", t.getMessage().toString());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Show Toast Message - Short Length
     *
     * @param s
     */
    public void ShowToast(String s) {

        if (s.length() > 0) {
            Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
        }
    }
}
