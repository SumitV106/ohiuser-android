package com.ohicabs.user.ohi.Adpater;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;


import com.ohicabs.user.ohi.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by sumit on 10/06/17.
 */

public class NotificationAdapter extends BaseAdapter {

    private Activity activity;
    int ResourceId;
    private ArrayList<HashMap> _arr_NotificationList;

    public NotificationAdapter(Activity act, int resId, ArrayList<HashMap> hashMapArrayList) {
        this.activity = act;
        this.ResourceId = resId;
        this._arr_NotificationList = hashMapArrayList;
    }

    @Override
    public int getCount() {
        return _arr_NotificationList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        final ViewHolder viewHolder;
        if (itemView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(ResourceId, parent, false);

            viewHolder.txt_notification_message = (AppCompatTextView) itemView.findViewById(R.id.txt_notification_message);
            viewHolder.txt_notification_time = (AppCompatTextView) itemView.findViewById(R.id.txt_notification_time);

            itemView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) itemView.getTag();
        }

        viewHolder.txt_notification_time.setText(_arr_NotificationList.get(position).get("date").toString());
        viewHolder.txt_notification_message.setText(_arr_NotificationList.get(position).get("message").toString());

        return itemView;
    }

    public class ViewHolder {
        AppCompatTextView txt_notification_message, txt_notification_time;
    }
}
