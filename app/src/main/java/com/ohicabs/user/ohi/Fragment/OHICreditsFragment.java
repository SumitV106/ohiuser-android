package com.ohicabs.user.ohi.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.Adpater.OhiCreditHistoryAdapter;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.PojoModel.CreditHistoryModel;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.StorageManager.DatabaseHelper;
import com.ohicabs.user.ohi.Utils.NetworkStatus;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OHICreditsFragment extends Fragment {

    @BindView(R.id.recyclerView_credits) RecyclerView recyclerViewCredits;
    @BindView(R.id.btn_total_earning) AppCompatButton btnTotalEarning;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    AppCompatTextView txtRedemption;

    private ToastDialog toastDialog;
    private SharedPreferencesUtility sharedPreferencesUtility;
    ProgressDialog progressDialog;
    ApiInterface apiService;
    DatabaseHelper mHelper;

    private ArrayList<CreditHistoryModel> arrCreditHistory = new ArrayList<>();

    OhiCreditHistoryAdapter ohiCreditHistoryAdapter;

    public OHICreditsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ohi_credits, container, false);
        ButterKnife.bind(this, view);

        txtRedemption = getActivity().findViewById(R.id.txt_redemption);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        apiService = ApiClient.getClient().create(ApiInterface.class);
        toastDialog = new ToastDialog(getActivity());
        sharedPreferencesUtility = new SharedPreferencesUtility(getActivity());
        mHelper = new DatabaseHelper(getActivity());

        init();

    }

    private void init() {
        txtRedemption.setVisibility(View.VISIBLE);

        recyclerViewCredits.setHasFixedSize(true);
        recyclerViewCredits.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.e("TAG_RESUME", "Resume");

        if (NetworkStatus.getConnectivityStatus(getActivity()))
            getCreditHistoryList();
    }

    /**
     * get credit history list
     */
    private void getCreditHistoryList() {

        try {
            progressBar.setVisibility(View.VISIBLE);

            Call<JsonObject> call = apiService.referral_history_list(sharedPreferencesUtility.getuser_id(), sharedPreferencesUtility.getauth_token());

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        progressBar.setVisibility(View.GONE);

                        String mResult = response.body().toString();

                        JSONObject jsonObjectDetails = new JSONObject(mResult);
                        if (jsonObjectDetails.optString("status").equalsIgnoreCase("1") &&
                                jsonObjectDetails.optString("success").equalsIgnoreCase("true")) {

                            JSONObject jsonObjectPayload = jsonObjectDetails.getJSONObject("payload");

                            JSONArray jsonArrayHistory = jsonObjectPayload.getJSONArray("history_list");
                            if (jsonArrayHistory.length() > 0) {

                                arrCreditHistory.clear();

                                for (int i = 0; i < jsonArrayHistory.length(); i++) {

                                    JSONObject jsonObjectData = jsonArrayHistory.getJSONObject(i);

                                    CreditHistoryModel creditHistoryModel = new CreditHistoryModel();

                                    creditHistoryModel.setWalletId(jsonObjectData.optString("wallet_id"));
                                    creditHistoryModel.setWalletStatus(jsonObjectData.optString("wallet_status"));
                                    creditHistoryModel.setStatus(jsonObjectData.optString("status"));
                                    creditHistoryModel.setAmount(jsonObjectData.optString("amount"));
                                    creditHistoryModel.setExpiryTime(jsonObjectData.optString("expiry_time"));
                                    creditHistoryModel.setCreatedDate(jsonObjectData.optString("created_date"));
                                    creditHistoryModel.setInitiateDate(jsonObjectData.optString("initiate_date"));
                                    creditHistoryModel.setRefType(jsonObjectData.optString("ref_type"));
                                    creditHistoryModel.setFirstName(jsonObjectData.optString("firstname"));
                                    creditHistoryModel.setMobile(jsonObjectData.optString("mobile"));
                                    creditHistoryModel.setImage(jsonObjectData.optString("image"));

                                    arrCreditHistory.add(creditHistoryModel);
                                }

                                ohiCreditHistoryAdapter = new OhiCreditHistoryAdapter(getActivity(), arrCreditHistory, new OhiCreditHistoryAdapter.BtnClickListener() {
                                    @Override
                                    public void onBtnClick(int position) {

                                    }
                                });

                                recyclerViewCredits.setAdapter(ohiCreditHistoryAdapter);
                            } else {
                                toastDialog.showAlertDialog("OHI Credit History", "No Credit History!");
                            }

                            btnTotalEarning.setText(jsonObjectPayload.optString("left_point") + " Points");

                        } else {
                            toastDialog.ShowToastMessage(jsonObjectDetails.optString("message"));
                        }
                    } catch (Exception e) {
                        Log.e("Exception", e.toString());
                        progressBar.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.e("onFailure", t.getMessage().toString());
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
