package com.ohicabs.user.ohi.Utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Toast;

import com.ohicabs.user.ohi.R;


public class ToastDialog {

    private final Handler handler;
    private Context mCtx;
    String stFormattedAddress;
    private Global_Typeface global_typeface;

    public ToastDialog(Context context) {
        this.mCtx = context;
        handler = new Handler(context.getMainLooper());
        global_typeface = new Global_Typeface(mCtx);
    }

    public void ShowToastMessage(final String stMessage) {
        // Do work
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ShowToast(stMessage);
            }
        });
    }

    private void runOnUiThread(Runnable r) {
        handler.post(r);
    }

    private void ShowToast(String s) {
        if (s.length() > 0) {
            Toast.makeText(mCtx, s, Toast.LENGTH_SHORT).show();
        }
    }
    public Dialog showAlertDialog(String title, String message) {

        final Dialog dialog = new Dialog(mCtx);
        dialog.setContentView(R.layout.layout_dialog_popup);
        dialog.setTitle("");
        dialog.setCancelable(false);

        AppCompatTextView txt_popup_header = (AppCompatTextView) dialog.findViewById(R.id.txt_popup_header);
        AppCompatTextView txt_popup_message = (AppCompatTextView) dialog.findViewById(R.id.txt_popup_message);
        AppCompatButton btn_ok = (AppCompatButton) dialog.findViewById(R.id.btn_ok);

        txt_popup_header.setText(title);
        txt_popup_message.setText(message);

        txt_popup_header.setTypeface(global_typeface.Sansation_Bold());
        txt_popup_message.setTypeface(global_typeface.Sansation_Regular());
        btn_ok.setTypeface(global_typeface.Sansation_Regular());

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        return null;
    }

//    public Dialog showAlertDialog(String title, String message) {
//
//        final Dialog dialog = new Dialog(mCtx);
//        dialog.setContentView(R.layout.layout_dialog_popup);
//        dialog.setTitle("");
//        dialog.setCancelable(false);
//
//        TextView txt_popup_header = (TextView) dialog.findViewById(R.id.txt_popup_header);
//        TextView txt_popup_message = (TextView) dialog.findViewById(R.id.txt_popup_message);
//        TextView btn_ok = (TextView) dialog.findViewById(R.id.btn_ok);
//
//        txt_popup_header.setText(title);
//        txt_popup_message.setText(message);
//
//        txt_popup_header.setTypeface(global_typeface.AvenirLTStd_Heavy());
//        txt_popup_message.setTypeface(global_typeface.AvenirLTStd_Medium());
//        btn_ok.setTypeface(global_typeface.AvenirLTStd_Medium());
//
//        btn_ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        dialog.show();
//        return null;
//    }
//
//    public Dialog NoInternetDialog(String title, String message) {
//
//        final Dialog dialog = new Dialog(mCtx);
//        dialog.setContentView(R.layout.layout_dialog_popup);
//        dialog.setTitle("");
//        dialog.setCancelable(false);
//
//        TextView txt_popup_header = (TextView) dialog.findViewById(R.id.txt_popup_header);
//        TextView txt_popup_message = (TextView) dialog.findViewById(R.id.txt_popup_message);
//        TextView btn_ok = (TextView) dialog.findViewById(R.id.btn_ok);
//
//        txt_popup_header.setText(title);
//        txt_popup_message.setText(message);
//
//        txt_popup_header.setTypeface(global_typeface.AvenirLTStd_Heavy());
//        txt_popup_message.setTypeface(global_typeface.AvenirLTStd_Medium());
//        btn_ok.setTypeface(global_typeface.AvenirLTStd_Medium());
//
//        btn_ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        if (!dialog.isShowing()) {
//            dialog.show();
//        }
//        return null;
//    }
}
