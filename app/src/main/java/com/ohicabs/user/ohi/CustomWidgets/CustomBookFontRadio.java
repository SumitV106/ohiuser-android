package com.ohicabs.user.ohi.CustomWidgets;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.AttributeSet;

public class CustomBookFontRadio extends AppCompatRadioButton {
    public CustomBookFontRadio(Context context) {
        super(context);
        init();
    }

    public CustomBookFontRadio(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomBookFontRadio(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        if (!isInEditMode()) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Sansation_Regular.ttf"));
        }
    }
}
