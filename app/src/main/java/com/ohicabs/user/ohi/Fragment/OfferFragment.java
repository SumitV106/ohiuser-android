package com.ohicabs.user.ohi.Fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Activity.OfferDetailActivity;
import com.ohicabs.user.ohi.Adpater.OfferListAdapter;
import com.ohicabs.user.ohi.CustomWidgets.CustomBoldFontTextView;
import com.ohicabs.user.ohi.PojoModel.OfferModel;
import com.ohicabs.user.ohi.StorageManager.DatabaseHelper;
import com.ohicabs.user.ohi.Utils.Global_Typeface;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.OFFER_LIST;
import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

/**
 * A simple {@link Fragment} subclass.
 */
public class OfferFragment extends Fragment {

    private ToastDialog toastDialog;
    private Global_Typeface global_typeface;
    private SharedPreferencesUtility sharedPreferencesUtility;
    private ProgressDialog progressDialog;

    ApiInterface apiService;

    @BindView(R.id.rv_offer)
    RecyclerView rv_offer;

    DatabaseHelper mHelper;

    @BindView(R.id.txt_no_data)
    CustomBoldFontTextView _txt_no_data;

    private ArrayList<OfferModel> _arr_BookedRideList = new ArrayList<>();

    OfferListAdapter adapter;
    RelativeLayout layout_serach;
    ImageView img_serach;
    EditText ed_search;

    public OfferFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_offer, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        toastDialog = new ToastDialog(getActivity());
        global_typeface = new Global_Typeface(getActivity());
        sharedPreferencesUtility = new SharedPreferencesUtility(getActivity());
        apiService = ApiClient.getClient().create(ApiInterface.class);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.st_please_wait));
        progressDialog.setCancelable(false);

        setView();

    }

    private void setView() {

        rv_offer.setLayoutManager(new LinearLayoutManager(getContext()));

        layout_serach = (RelativeLayout) getActivity().findViewById(R.id.layout_serach);
        img_serach = (ImageView) getActivity().findViewById(R.id.img_serach);
        ed_search = (EditText) getActivity().findViewById(R.id.ed_search);

        layout_serach.setVisibility(View.VISIBLE);

        img_serach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.getFilter().filter(ed_search.getText());
            }
        });

        GetOfferList();
    }

    private void GetOfferList() {

        if (isInternetOn(getContext())) {
            RequestQueue queue = Volley.newRequestQueue(getContext());

            StringRequest jsonObjRequest = new StringRequest(Request.Method.POST, OFFER_LIST,
                    new com.android.volley.Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                progressDialog.dismiss();
                                Log.e("offer", response.toString());
                                JSONObject objResponse = new JSONObject(response);
                                if (objResponse.getString("success").equalsIgnoreCase("true")) {


                                    JSONObject objPayload = objResponse.getJSONObject("payload");

                                    JSONArray arroffer_list = objPayload.getJSONArray("offer_list");

                                    for (int i = 0; i <= arroffer_list.length() - 1; i++) {

                                        JSONObject object = arroffer_list.getJSONObject(i);

                                        OfferModel offerModel = new OfferModel();
                                        offerModel.setOffer_id(object.getString("offer_id"));
                                        offerModel.setOffer_title(object.getString("offer_title"));
                                        offerModel.setZone_id(object.getString("zone_id"));
                                        offerModel.setDescription(object.getString("description"));
                                        offerModel.setModify_date(object.getString("modify_date"));
                                        offerModel.setZone_name(object.getString("zone_name"));
                                        offerModel.setImage(object.getString("image"));

                                        _arr_BookedRideList.add(offerModel);
                                    }

                                    adapter = new OfferListAdapter(getActivity(),
                                            R.layout.layout_offer_row, _arr_BookedRideList,
                                            new OfferListAdapter.BtnClickListener() {
                                                @Override
                                                public void onBtnClick(int position, String zonename, String offer_title, String description, String image) {

                                                    Intent _intent = new Intent(getActivity(), OfferDetailActivity.class);
                                                    _intent.putExtra("zonename", zonename);
                                                    _intent.putExtra("offer_title", offer_title);
                                                    _intent.putExtra("description", description);
                                                    _intent.putExtra("image", image);
                                                    startActivity(_intent);
                                                }
                                            });
                                    rv_offer.setAdapter(adapter);


                                } else {
                                    toastDialog.ShowToastMessage(objResponse.getString("message"));
                                }
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                                progressDialog.dismiss();
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        progressDialog.dismiss();
                        VolleyLog.d("volley", "Error: " + error.getMessage());
                        error.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/x-www-form-urlencoded; charset=UTF-8";
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();

                    params.put("user_type", "1");

                    return params;
                }

            };
            queue.add(jsonObjRequest);

        }
    }
}
