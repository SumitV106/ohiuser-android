package com.ohicabs.user.ohi.ui.subscription;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.Activity.BaseActivity;
import com.ohicabs.user.ohi.Activity.MonthlySpendingTermsActivity;
import com.ohicabs.user.ohi.Activity.PaytmTermsActivity;
import com.ohicabs.user.ohi.Activity.ReviewRideActivity;
import com.ohicabs.user.ohi.Adpater.SubscriptionListAdapter;
import com.ohicabs.user.ohi.PojoModel.SubscriptionModel;
import com.ohicabs.user.ohi.R;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class SubscriptionDetailsActivity extends BaseActivity {

    @BindView(R.id.txt_plan_validity) AppCompatTextView txtPlanValidity;
    @BindView(R.id.txt_discount_ride) AppCompatTextView txtDiscountRide;
    @BindView(R.id.txt_max_discount_ride) AppCompatTextView txtMaxDiscountRide;
    @BindView(R.id.txt_max_ride) AppCompatTextView txtMaxRide;
    @BindView(R.id.txt_offer_amount) AppCompatTextView txtOfferAmount;
    @BindView(R.id.txt_terms_condition) AppCompatTextView txtTermsCondition;
    @BindView(R.id.btn_pay_paytm) AppCompatButton btnPayPaytm;
    @BindView(R.id.img_close) AppCompatImageView imgClose;
    @BindView(R.id.img_subscription) AppCompatImageView imgSubscription;
    @BindView(R.id.txt_subscription_title) AppCompatTextView txtTitle;
    @BindView(R.id.txt_subscription_details) AppCompatTextView txtDetails;
    @BindView(R.id.txt_service_name) AppCompatTextView txtServiceName;
    @BindView(R.id.txt_city_name) AppCompatTextView txtCityName;
    @BindView(R.id.btn_referral_point) AppCompatButton btnReferralPoint;

    @BindView(R.id.txt_active_on) AppCompatTextView txtActiveOn;
    @BindView(R.id.txt_expiry_on) AppCompatTextView txtExpiryOn;
    @BindView(R.id.txt_pay_type) AppCompatTextView txtPayType;
    @BindView(R.id.txt_subscription_status) AppCompatTextView txtSubScriptionStatus;
    @BindView(R.id.ll_more_details) LinearLayout llMoreDetails;

    String stPlanId = "", stOrderID, stTxnID = "", stSubscriptionID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_details);
        ButterKnife.bind(this);

        setViews();
    }

    private void setViews() {

        String subscriptionDetails = getIntent().getStringExtra("subscription_details");
        try {

            final JSONObject jsonObjectDetails = new JSONObject(subscriptionDetails);

            txtPlanValidity.setText(jsonObjectDetails.getString("days") + " days");
            txtDiscountRide.setText(jsonObjectDetails.getString("discount_per") + " %");
            txtMaxDiscountRide.setText("Rs. " + jsonObjectDetails.getString("max_discount"));
            txtMaxRide.setText(jsonObjectDetails.getString("max_ride"));
            txtOfferAmount.setText("Rs. " + jsonObjectDetails.getString("amount"));
            txtTitle.setText(jsonObjectDetails.getString("plan_name"));
            txtDetails.setText(jsonObjectDetails.getString("details"));
            txtServiceName.setText(jsonObjectDetails.getString("service_name"));
            txtCityName.setText(jsonObjectDetails.optString("zone_name"));

            stPlanId = jsonObjectDetails.getString("plan_id");

            Glide.with(getApplicationContext()).load(Global_ServiceApi.API_IMAGE_HOST + jsonObjectDetails.getString("image").toString())
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgSubscription);

            if (getIntent().hasExtra("subscription_history")) {
                btnPayPaytm.setVisibility(View.GONE);
                llMoreDetails.setVisibility(View.VISIBLE);
                btnReferralPoint.setVisibility(View.GONE);

                switch (jsonObjectDetails.optString("payment_type")) {
                    case "0":
                        txtPayType.setText("Paytm");
                        break;

                    case "1":
                        txtPayType.setText("Cash");
                        break;

                    case "2":
                        txtPayType.setText("Exclusive Offer");
                        break;

                    case "3":
                        txtPayType.setText("Referral Points Redeem");
                        break;
                }

                switch (jsonObjectDetails.optString("status")) {
                    case "1":
                        txtSubScriptionStatus.setText("Active");
                        txtSubScriptionStatus.setTextColor(getResources().getColor(R.color.colorGreen));
                        break;

                    case "2":
                        txtSubScriptionStatus.setText("Payment Fail");
                        txtSubScriptionStatus.setTextColor(getResources().getColor(R.color.colorRed));
                        break;

                    default:
                        txtSubScriptionStatus.setText("End");
                        txtSubScriptionStatus.setTextColor(getResources().getColor(R.color.colorGrey));
                        break;
                }

                txtActiveOn.setText(convertDate(jsonObjectDetails.optString("active_date")));
                txtExpiryOn.setText(convertDate(jsonObjectDetails.optString("expiry_date")));

            } else {
                btnPayPaytm.setVisibility(View.VISIBLE);
                btnReferralPoint.setVisibility(View.VISIBLE);
                llMoreDetails.setVisibility(View.GONE);
            }

            txtTermsCondition.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent intent_term = new Intent(getApplicationContext(), PaytmTermsActivity.class);
                        intent_term.putExtra("subscribe_term_condition", jsonObjectDetails.getString("term_and_condition"));
                        startActivity(intent_term);

                    } catch (Exception e) { }
                }
            });

            btnPayPaytm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkUserSubscription();
                }
            });

            btnReferralPoint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    userReferralPoints(stPlanId);
                }
            });

            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Convert Date to Simple format
     *
     * @param stDate
     * @return
     */
    private String convertDate(String stDate) {
        try {
            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = df.parse(stDate);
            df.setTimeZone(TimeZone.getDefault());
            SimpleDateFormat date_fmt = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aa", Locale.getDefault());
            String _date = date_fmt.format(date);

            return _date;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private void checkUserSubscription() {

        if (isInternetOn(getApplicationContext())) {
            try {
                progressDialog.show();
                Call<JsonObject> call = apiService.checkUserSubscription(preferencesUtility.getuser_id(), preferencesUtility.getauth_token(), stPlanId);

                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try {
                            progressDialog.dismiss();

                            JSONObject jsonObjectDetails = new JSONObject(response.body().toString());
                            buySubscriptionPaytm(jsonObjectDetails);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        progressDialog.dismiss();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                progressDialog.dismiss();
            }
        }
    }

    private void buySubscriptionPaytm(JSONObject jsonObjectDetails) {

        try {
            if (jsonObjectDetails.optString("status").equalsIgnoreCase("1")) {

                JSONObject jsonObjectPayload = jsonObjectDetails.getJSONObject("payload");
                JSONObject jsonObjectPaytmPayload = jsonObjectPayload.getJSONObject("paytm_payload");

                stSubscriptionID = jsonObjectPayload.getString("sub_buy_id");

                // Create a HASHMAP Object holding the Order Information
                HashMap<String, String> paramMap = new HashMap<String, String>();

                paramMap.put("MID", jsonObjectPaytmPayload.getString("MID"));
                paramMap.put("ORDER_ID", jsonObjectPaytmPayload.getString("ORDER_ID"));
                paramMap.put("CUST_ID", jsonObjectPaytmPayload.getString("CUST_ID"));
                paramMap.put("INDUSTRY_TYPE_ID", jsonObjectPaytmPayload.getString("INDUSTRY_TYPE_ID"));
                paramMap.put("CHANNEL_ID", jsonObjectPaytmPayload.getString("CHANNEL_ID"));
                paramMap.put("TXN_AMOUNT", jsonObjectPaytmPayload.getString("TXN_AMOUNT"));
                paramMap.put("WEBSITE", jsonObjectPaytmPayload.getString("WEBSITE"));
                paramMap.put("CALLBACK_URL", jsonObjectPaytmPayload.getString("CALLBACK_URL"));

                if (jsonObjectPaytmPayload.has("EMAIL")) {
                    paramMap.put("EMAIL", jsonObjectPaytmPayload.getString("EMAIL"));
                }
                if (jsonObjectPaytmPayload.has("MOBILE_NO")) {
                    paramMap.put("MOBILE_NO", jsonObjectPaytmPayload.getString("MOBILE_NO"));
                }

                paramMap.put("CHECKSUMHASH", jsonObjectPaytmPayload.getString("CHECKSUMHASH"));

                stOrderID = jsonObjectPaytmPayload.getString("ORDER_ID");

                final PaytmPGService paytmPGService = PaytmPGService.getProductionService(); // Production

                // Create Paytm Order Object
                PaytmOrder paytmOrder = new PaytmOrder(paramMap);

                paytmPGService.initialize(paytmOrder, null);

                paytmPGService.startPaymentTransaction(SubscriptionDetailsActivity.this, true, true,
                        new PaytmPaymentTransactionCallback() {
                            @Override
                            public void onTransactionResponse(Bundle bundle) {
                                try {

                                    if (bundle.getString("STATUS").equals("TXN_SUCCESS")) {
                                        JSONObject json = new JSONObject();
                                        Set<String> keys = bundle.keySet();
                                        for (String key : keys) {
                                            try {
                                                // json.put(key, bundle.get(key)); see edit below
                                                json.put(key, JSONObject.wrap(bundle.get(key)));
                                            } catch (JSONException e) {
                                                //Handle exception here
                                            }
                                        }
                                        if (bundle.containsKey("TXNID")) {
                                            stTxnID = bundle.getString("TXNID").toString();
                                        }

                                        successServiceCall(json.toString());

                                    } else {
                                        JSONObject json = new JSONObject();
                                        Set<String> keys = bundle.keySet();
                                        for (String key : keys) {
                                            try {
                                                // json.put(key, bundle.get(key)); see edit below
                                                json.put(key, JSONObject.wrap(bundle.get(key)));
                                            } catch (JSONException e) {
                                                //Handle exception here
                                            }
                                        }

                                        if (bundle.containsKey("TXNID")) {
                                            stTxnID = bundle.getString("TXNID").toString();
                                        }
                                        failServiceCall(json.toString());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void networkNotAvailable() {
                                Log.d("LOG", "UI Error Occur.");
                                Toast.makeText(getApplicationContext(), " UI Error Occur. ", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void clientAuthenticationFailed(String inErrorMessage) {
                                Log.d("LOG", "clientAuthenticationFailed.");
                                Toast.makeText(getApplicationContext(), " Sever-side Error " + inErrorMessage, Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void someUIErrorOccurred(String s) {
                                Log.d("LOG", "UI Error Occur.");
                            }

                            @Override
                            public void onErrorLoadingWebPage(int i, String s, String s1) {
                                Log.d("LOG", "onErrorLoadingWebPage.");
                            }

                            @Override
                            public void onBackPressedCancelTransaction() {
                                Log.d("LOG", "onBackPressedCancelTransaction");
                            }

                            @Override
                            public void onTransactionCancel(String s, Bundle bundle) {
                                Log.d("LOG", "Payment_Transaction_Failed " + bundle);
                                Toast.makeText(getBaseContext(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
                            }
                        });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Paytm Transaction Success
     *
     * @param stSuccessPayload
     */
    private void successServiceCall(String stSuccessPayload) {
        Call<JsonObject> call = apiService.paytm_subscription_sucess(preferencesUtility.getuser_id(),
                preferencesUtility.getauth_token(), stSubscriptionID, stSuccessPayload, stTxnID, stOrderID);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                try {
                    JSONObject jsonObjectDetails = new JSONObject(response.body().toString());
                    ShowToast(jsonObjectDetails.getString("message"));
                    finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    /**
     * Paytm Tra
     * @param stFailPayload
     */
    private void failServiceCall(String stFailPayload) {
        Call<JsonObject> call = apiService.paytm_subscription_fail(preferencesUtility.getuser_id(),
                preferencesUtility.getauth_token(), stSubscriptionID, stFailPayload, stTxnID, stOrderID);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                try {
                    JSONObject jsonObjectDetails = new JSONObject(response.body().toString());
                    ShowToast(jsonObjectDetails.getString("message"));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    /**
     * buy subscription using referral points
     *
     * @param planID
     */
    private void userReferralPoints(String planID) {

        if (isInternetOn(getApplicationContext())) {
            try {
                progressDialog.show();

                Call<JsonObject> call = apiService.buy_user_subscription_on_referral_point(preferencesUtility.getuser_id(),
                        preferencesUtility.getauth_token(), planID);
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                        try {
                            JSONObject jsonObjectDetails = new JSONObject(response.body().toString());
                            ShowToast(jsonObjectDetails.getString("message"));
                            finish();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
            }
        }
    }

}
