package com.ohicabs.user.ohi.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUsFragment extends Fragment {

    @BindView(R.id.txt_title)
    CustomRegularFontTextView txt_title;

    @BindView(R.id.txt_subtitle)
    CustomRegularFontTextView txt_subtitle;

    public AboutUsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about_us, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        txt_title.setText(Html.fromHtml("<b>LUKSUS TECHNOLOGIES PRIVATE LIMITED</b>, which is a private limited company  having <b>CIN No.  U74999AS2017PTC018123.</b> "));
        txt_subtitle.setText(Html.fromHtml("The platform is operated under the brand  name <b>“OHI CABS”</b> through which we provide our services as a cab aggregator."));
    }
}
