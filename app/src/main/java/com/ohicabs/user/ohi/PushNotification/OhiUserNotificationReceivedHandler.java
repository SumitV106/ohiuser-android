package com.ohicabs.user.ohi.PushNotification;

import android.content.Context;

import com.ohicabs.user.ohi.ServiceManager.PrefManager;
import com.ohicabs.user.ohi.StorageManager.DatabaseHelper;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class OhiUserNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {

    Context mContext;
    private SharedPreferencesUtility preferencesUtility;
    private DatabaseHelper mDBHelper;
    private PrefManager prefManager;

    public OhiUserNotificationReceivedHandler(Context context) {
        mContext = context;

        prefManager = new PrefManager(mContext);
        preferencesUtility = new SharedPreferencesUtility(mContext);
        mDBHelper = new DatabaseHelper(mContext);
    }

    @Override
    public void notificationReceived(OSNotification notification) {
        JSONObject data = notification.payload.additionalData;
        if (data != null) {
            try {
                Calendar currentTime = Calendar.getInstance();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                String stCurrentPushTime = simpleDateFormat.format(currentTime.getTime());
                prefManager.setString("notification_time", stCurrentPushTime.toString());

                String stNotificationID = data.optString("notification_id");
                String stMessage = data.optString("message");

                switch (stNotificationID) {
                    case "0":
                        mDBHelper.InsertPushNotificationData(stNotificationID, stMessage, stCurrentPushTime);
                        break;

                    case "9":
                        mDBHelper.InsertPushNotificationData(stNotificationID, stMessage, stCurrentPushTime);
                        break;

                    case "10":
                        mDBHelper.InsertPushNotificationData(stNotificationID, stMessage, stCurrentPushTime);
                        break;

                    case "11":
                        mDBHelper.InsertPushNotificationData(stNotificationID, stMessage, stCurrentPushTime);
                        break;

                    case "12":
                        mDBHelper.InsertPushNotificationData(stNotificationID, stMessage, stCurrentPushTime);
                        break;

                    case "14":
                        mDBHelper.InsertPushNotificationData(stNotificationID, stMessage, stCurrentPushTime);
                        break;

                    case "17":
                        mDBHelper.InsertPushNotificationData(stNotificationID, stMessage, stCurrentPushTime);
                        break;

                    case "18":
                        mDBHelper.InsertPushNotificationData(stNotificationID, stMessage, stCurrentPushTime);
                        break;

                    case "5":
                        //RIDE STARTED

                        // Set Rental Ride Start Time
                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        String strRideStartTime = sdf.format(calendar.getTime());

                        preferencesUtility.setIsRideStarted("true");
                        preferencesUtility.setCurrentTimeRideStart(strRideStartTime);
                        preferencesUtility.setStartRideBookindId(data.optString("booking_id"));
                        break;

                    case "6":
                        //RIDE CONCLUDED
                        preferencesUtility.setIsRideStarted("false");
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}