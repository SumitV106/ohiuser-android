package com.ohicabs.user.ohi.Fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Adpater.RewardsListAdapter;
import com.ohicabs.user.ohi.PojoModel.RewardsModel;
import com.ohicabs.user.ohi.Utils.NetworkStatus;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 */
public class RewardFragment extends Fragment {

    @BindView(R.id.list_rewards)
    RecyclerView listRewards;
    @BindView(R.id.txt_nodata)
    AppCompatTextView _txtNoData;
    ProgressDialog progressDialog;

    private ArrayList<RewardsModel> arrRewardsList = new ArrayList<>();

    private SharedPreferencesUtility preferencesUtility;
    private ToastDialog toastDialog;

    ApiInterface apiService;

    public RewardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reward, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.st_please_wait));
        progressDialog.setCancelable(false);

        apiService = ApiClient.getClient().create(ApiInterface.class);
        preferencesUtility = new SharedPreferencesUtility(getActivity());

        listRewards.setItemAnimator(new DefaultItemAnimator());
        listRewards.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        listRewards.setLayoutManager(layoutManager);

        if (NetworkStatus.getConnectivityStatus(getActivity())) {
            getRewardsList();

        } else {
            toastDialog.ShowToastMessage(getResources().getString(R.string.st_internet_not_available));
        }
    }

    /**
     * GEt Rewards List
     */
    private void getRewardsList() {
        try {
            Call<JsonObject> call = apiService.user_reward_list(preferencesUtility.getuser_id());

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                    try {
                        Log.e("RES_Reward", response.body().toString());
                        setRewardsData(response.body().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * set Rewards Details
     *
     * @param rewardsResult
     */
    private void setRewardsData(String rewardsResult) {
        try {

            JSONObject objBalance = new JSONObject(rewardsResult);
            if (objBalance.getString("success").equalsIgnoreCase("true")) {

                JSONArray jsonArrayPayload = objBalance.getJSONArray("payload");

                if (jsonArrayPayload.length() > 0) {

                    for (int i = 0; i < jsonArrayPayload.length(); i++) {

                        JSONObject objPayload = jsonArrayPayload.getJSONObject(i);
                        RewardsModel rewardsModel = new RewardsModel();

                        rewardsModel.setReward_user_id(objPayload.getString("reward_user_id"));
                        rewardsModel.setInitated_date(objPayload.getString("initated_date"));
                        rewardsModel.setReference_no(objPayload.getString("reference_no"));
                        rewardsModel.setPeriod_start(objPayload.getString("period_start"));
                        rewardsModel.setPeriod_end(objPayload.getString("period_end"));
                        rewardsModel.setAmount(objPayload.getString("amount"));
                        rewardsModel.setReason(objPayload.getString("reason"));
                        rewardsModel.setTerm_and_condition(objPayload.getString("term_and_condition"));
                        rewardsModel.setCreated_date(objPayload.getString("created_date"));

                        arrRewardsList.add(rewardsModel);
                    }
                }
            } else if (objBalance.getString("status").equalsIgnoreCase("0")) {
                _txtNoData.setText(objBalance.getString("message"));
                _txtNoData.setVisibility(View.VISIBLE);
            }

            if (arrRewardsList.size() > 0) {

                RewardsListAdapter rewardsListAdapter = new RewardsListAdapter(getActivity(), arrRewardsList);
                listRewards.setAdapter(rewardsListAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
