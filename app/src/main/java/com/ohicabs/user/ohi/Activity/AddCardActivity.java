package com.ohicabs.user.ohi.Activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Adpater.PaymentCardListAdapter;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontEditText;
import com.ohicabs.user.ohi.PojoModel.CardModel;
import com.ohicabs.user.ohi.Utils.ExpandableHeightListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class AddCardActivity extends BaseActivity {

    private boolean isFabMenuOpen = false;
    private FloatingActionButton floatingActionButton;

    private String selected_card_type;

    private ArrayList<CardModel> _arr_CardList = new ArrayList<>();

    @BindView(R.id.layout_add_card)
    LinearLayout _layout_add_card;

    @BindView(R.id.txt_new_card)
    TextView _txt_new_card;

    @BindView(R.id.txt_save_card)
    TextView _txt_save_card;

    @BindView(R.id.edt_card_number)
    CustomRegularFontEditText _edt_card_number;

    @BindView(R.id.edt_card_owner)
    CustomRegularFontEditText _edt_card_owner;

    @BindView(R.id.edt_month_year)
    CustomRegularFontEditText _edt_month_year;

    @BindView(R.id.edt_card_cvv)
    CustomRegularFontEditText _edt_card_cvv;

    @BindView(R.id.rgroup_card_type)
    RadioGroup _rgroup_card_type;

    @BindView(R.id.rbtn_personal_card)
    RadioButton _rbtn_personal_card;

    @BindView(R.id.rbtn_work_card)
    RadioButton _rbtn_work_card;

    @BindView(R.id.btn_save_card)
    Button _btn_save_card;

    @BindView(R.id.list_card)
    ExpandableHeightListView _list_card;

    @BindView(R.id.layout_header)
    RelativeLayout _layout_header;

    @BindView(R.id.img_back)
    ImageView _img_back;

    @BindView(R.id.txt_header)
    TextView _txt_header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);
        ButterKnife.bind(this);

        setContent();

    }

    private void setContent() {

        _list_card.setExpanded(true);
        _layout_header.setVisibility(View.VISIBLE);

//        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_UserCardList,
//                new IntentFilter("User_Card_List"));

        GetCardList();


        _txt_header.setTypeface(global_typeface.Sansation_Bold());
        _txt_new_card.setTypeface(global_typeface.Sansation_Bold());
        _txt_save_card.setTypeface(global_typeface.Sansation_Bold());
        _rbtn_personal_card.setTypeface(global_typeface.Sansation_Regular());
        _rbtn_work_card.setTypeface(global_typeface.Sansation_Regular());
        _btn_save_card.setTypeface(global_typeface.Sansation_Regular());

        _edt_month_year.addTextChangedListener(new MaskWatcher("##/##"));
        floatingActionButton = (FloatingActionButton) findViewById(R.id.btn_FloatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFabMenuOpen) {
                    collapseFabMenu();
                } else {
                    expandFabMenu();
                }
            }
        });

        _rgroup_card_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbtn_personal_card:
                        selected_card_type = "0";
                        break;
                    case R.id.rbtn_work_card:
                        selected_card_type = "1";
                        break;
                }
            }
        });

        _btn_save_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String message = IsValidate();
                    if (message.equalsIgnoreCase("true")) {

                        JSONObject objCardData = new JSONObject();
                        objCardData.put("card_expiry_month",  getValidDateFields(_edt_month_year.getText().toString())[0]+"");
                        objCardData.put("card_expiry_year",  getValidDateFields(_edt_month_year.getText().toString())[1]+"");
                        objCardData.put("card_name", _edt_card_owner.getText().toString());
                        objCardData.put("card_number", _edt_card_number.getText());
                        objCardData.put("cvc", _edt_card_cvv.getText().toString());

                        if(isInternetOn(getApplicationContext())) {

                            Call<JsonObject> call = apiService.user_save_card(preferencesUtility.getuser_id(),
                                    Global_ServiceApi.EncryptCard(objCardData.toString(), "user_save_card"), selected_card_type);

                            call.enqueue(new Callback<JsonObject>() {
                                @Override
                                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                    try {
                                        JSONObject objCardData = new JSONObject(response.body().toString());
                                        if (objCardData.getString("status").equalsIgnoreCase("1")) {
                                            Intent intent = new Intent();
                                            intent.putExtra("ADD_CARD", "Card_Added");
                                            setResult(1993, intent);
                                            finish();
                                        }
                                    } catch (Exception e) {
                                        Log.e("Error", e.toString());
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(Call<JsonObject> call, Throwable t) {

                                }
                            });
                        }
                        else{
                            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                        }
                    } else {
                        toastDialog.ShowToastMessage(message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        _img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("ADD_CARD", "Finish");
                setResult(1993, intent);
                finish();
            }
        });


    }

    /**
     * Get User CardList
     */
    private void GetCardList() {
        if(isInternetOn(getApplicationContext())) {
            try {

                Call<JsonObject> call = apiService.user_save_card_list(preferencesUtility.getuser_id());
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                    Intent intent = new Intent("User_Card_List");
//                    intent.putExtra("user_card_list", response.body().toString());
//                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                        GetCardList(response.body().toString());
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        else{
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }


    /**
     * User Card List Receiver
     */
    private BroadcastReceiver mMessageReceiver_UserCardList = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mResult_CardList = intent.getStringExtra("user_card_list");
            GetCardList(mResult_CardList);
        }
    };

    /**
     * User Card List Response Parse
     *
     * @param mResult
     */
    private void GetCardList(String mResult) {
        try {
            JSONObject objCardList = new JSONObject(mResult);
            if (objCardList.getString("status").equalsIgnoreCase("1")) {
                JSONArray jarrPayload = objCardList.getJSONArray("payload");
                if (jarrPayload.length() > 0) {

                    _arr_CardList.clear();

                    for (int i = 0; i < jarrPayload.length(); i++) {
                        JSONObject objCardData = jarrPayload.getJSONObject(i);

                        CardModel cardModel = new CardModel();
                        cardModel.setCard_id(objCardData.getString("card_id"));
                        cardModel.setUser_id(objCardData.getString("user_id"));
                        cardModel.setCard_type(objCardData.getString("card_type"));
                        cardModel.setCreated_date(objCardData.getString("created_date"));
                        cardModel.setStatus(objCardData.getString("status"));
                        cardModel.setCard_expiry_month(objCardData.getString("card_expiry_month"));
                        cardModel.setCard_expiry_year(objCardData.getString("card_expiry_year"));
                        cardModel.setCard_name(objCardData.getString("card_name"));
                        cardModel.setCard_number(objCardData.getString("last_digit"));
                        cardModel.setStripeCardID(objCardData.getString("stripe_card_id"));

                        _arr_CardList.add(cardModel);
                    }
                }
            }

            PaymentCardListAdapter userCardListAdapter = new PaymentCardListAdapter(this, R.layout.layout_card_row, _arr_CardList, new PaymentCardListAdapter.BtnClickListener() {
                @Override
                public void onBtnClick(int position, String card_id, String opration_type) {
                    if (opration_type.equalsIgnoreCase("Delete")) {
                        showAlertDialog(card_id);
                    }
                }
            });
            _list_card.setAdapter(userCardListAdapter);
            userCardListAdapter.notifyDataSetChanged();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Card Blank Field Validation
     *
     * @return
     */
    private String IsValidate() {
        if (_edt_card_owner.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_card_name);
        } else if (_edt_card_number.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_card_number);
        } else if (_edt_month_year.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_expiry_month);
        } else if (_edt_card_cvv.getText().toString().trim().equals("")) {
            return getResources().getString(R.string.st_no_card_cvc);
        } else if (_rgroup_card_type.getCheckedRadioButtonId() == -1) {
            return getResources().getString(R.string.st_select_card);
        } else {
            return "true";
        }
    }

    /**
     * Delete Card Dialog
     *
     * @return
     */
    public Dialog showAlertDialog(final String card_id) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_popup);
        dialog.setTitle("");
        dialog.setCancelable(true);

        TextView txt_popup_header = (TextView) dialog.findViewById(R.id.txt_popup_header);
        TextView txt_popup_message = (TextView) dialog.findViewById(R.id.txt_popup_message);
        TextView btn_delete = (TextView) dialog.findViewById(R.id.btn_delete);

        txt_popup_header.setTypeface(global_typeface.Sansation_Bold());
        txt_popup_message.setTypeface(global_typeface.Sansation_Regular());
        btn_delete.setTypeface(global_typeface.Sansation_Regular());

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Call<JsonObject> call = apiService.user_save_card_delete(preferencesUtility.getuser_id(),card_id);
                    call.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            Log.e("response",response.body().toString());
                            try{
                                JSONObject obj=new JSONObject(response.body().toString());
                                if(obj.getString("success").equals("true") && obj.getString("status").equals("1")){


                                }
                            }
                            catch (Exception e){
                                e.printStackTrace();
                            }
                            Intent intent = new Intent();
                            intent.putExtra("ADD_CARD", "Finish");
                            setResult(1993, intent);
                            finish();
                            GetCardList();
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {

                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });

        dialog.show();
        return null;
    }


    private void expandFabMenu() {
        ViewCompat.animate(floatingActionButton).rotation(45.0F).withLayer().setDuration(300).setInterpolator(new OvershootInterpolator(10.0F)).start();
        ViewCompat.animate(_layout_add_card).rotation(0.0F).withLayer().setDuration(500).setInterpolator(new OvershootInterpolator(10.0F)).start();
        _layout_add_card.setVisibility(View.VISIBLE);
        isFabMenuOpen = true;
    }

    private void  collapseFabMenu() {
        ViewCompat.animate(floatingActionButton).rotation(0.0F).withLayer().setDuration(300).setInterpolator(new OvershootInterpolator(10.0F)).start();
        ViewCompat.animate(_layout_add_card).rotation(0.0F).withLayer().setDuration(500).setInterpolator(new OvershootInterpolator(10.0F)).start();
        _layout_add_card.setVisibility(View.GONE);
        isFabMenuOpen = false;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("ADD_CARD", "Finish");
        setResult(1993, intent);
        finish();
    }

    private  class MaskWatcher implements TextWatcher {
        private boolean isRunning = false;
        private boolean isDeleting = false;
        private final String mask;

        public MaskWatcher(String mask) {
            this.mask = mask;
        }

        MaskWatcher buildCpf() {
            return new MaskWatcher("##/##");
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
            isDeleting = count > after;
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (isRunning || isDeleting) {
                return;
            }
            isRunning = true;

            int editableLength = editable.length();
            if (editableLength < mask.length()) {
                if (mask.charAt(editableLength) != '#') {
                    editable.append(mask.charAt(editableLength));
                } else if (mask.charAt(editableLength-1) != '#') {
                    editable.insert(editableLength-1, mask, editableLength-1, editableLength);
                }
            }

            isRunning = false;
        }
    }

    public int [] getValidDateFields(String monthyear) {
        int [] monthYearPair = new int[2];
        String rawNumericInput = monthyear.replaceAll("/", "");
        String[] dateFields = separateDateStringParts(rawNumericInput);

        try {
            monthYearPair[0] = Integer.parseInt(dateFields[0]);
            monthYearPair[1] = convertTwoDigitYearToFour(Integer.parseInt(dateFields[1]), Calendar.getInstance());
        } catch (NumberFormatException numEx) {
            // Given that the date should already be valid when getting to this method, we should
            // not his this exception. Returning null to indicate error if we do.
            return null;
        }

        return monthYearPair;
    }
    public static String[] separateDateStringParts( String expiryInput) {
        String[] parts = new String[2];
        if (expiryInput.length() >= 2) {
            parts[0] = expiryInput.substring(0, 2);
            parts[1] = expiryInput.substring(2);
        } else {
            parts[0] = expiryInput;
            parts[1] = "";
        }
        return parts;
    }
    static int convertTwoDigitYearToFour(
            @IntRange(from = 0, to = 99) int inputYear,
            @NonNull Calendar calendar) {
        int year = calendar.get(Calendar.YEAR);
        // Intentional integer division
        int centuryBase = year / 100;
        if (year % 100 > 80 && inputYear < 20) {
            centuryBase++;
        } else if (year % 100 < 20 && inputYear > 80) {
            centuryBase--;
        }
        return centuryBase * 100 + inputYear;
    }

}
