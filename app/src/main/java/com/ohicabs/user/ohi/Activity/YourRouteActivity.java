package com.ohicabs.user.ohi.Activity;

import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Adpater.RouteAdapter;
import com.ohicabs.user.ohi.Utils.ExpandableHeightListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class YourRouteActivity extends BaseActivity {

    @BindView(R.id.img_back)
    AppCompatImageView _img_back;

    @BindView(R.id.list_route)
    ExpandableHeightListView _list_route;

    String stDriverID = "";
    private ArrayList<HashMap<String, String>> _arr_RouteList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_route);
        ButterKnife.bind(this);

        setView();
    }

    private void setView() {

        if (getIntent().hasExtra("driverID")) {
            stDriverID = getIntent().getStringExtra("driverID");
        }
        _list_route.setExpanded(true);
        servicecall();
    }

    @OnClick(R.id.img_back)
    public void imageback() {
        finish();
    }

    public void servicecall() {

        Log.e("stDriverID", stDriverID + "--");
        if (isInternetOn(getApplicationContext())) {
            progressDialog.show();
            Call<JsonObject> calls = apiService.user_ride_route(preferencesUtility.getuser_id().toString(), stDriverID);
            calls.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    progressDialog.dismiss();

                    try {
                        Log.e("responseroute", response.body().toString());

                        JSONObject objBookingList = new JSONObject(response.body().toString());
                        if (objBookingList.getString("status").equalsIgnoreCase("1")) {
                            JSONArray jarrPayload = objBookingList.getJSONArray("payload");
                            if (jarrPayload.length() > 0) {
                                for (int i = 0; i < jarrPayload.length(); i++) {
                                    JSONObject objBookingData = jarrPayload.getJSONObject(i);

                                    Iterator<String> iter = objBookingData.keys();
                                    HashMap<String, String> map_keyValue = new HashMap<>();
                                    while (iter.hasNext()) {
                                        String key = iter.next();
                                        try {
                                            Object value = objBookingData.get(key);
                                            map_keyValue.put(key, value.toString());
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    _arr_RouteList.add(map_keyValue);
                                    RouteAdapter routeAdapter = new RouteAdapter(YourRouteActivity.this, R.layout.layout_route_row, _arr_RouteList,
                                            new RouteAdapter.BtnClickListener() {
                                                @Override
                                                public void onBtnClick(int position, String ride_id) {


                                                }
                                            });
                                    _list_route.setAdapter(routeAdapter);
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("onFailure", t.toString());

                }
            });
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }
}
