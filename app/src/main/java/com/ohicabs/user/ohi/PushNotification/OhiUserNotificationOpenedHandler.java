package com.ohicabs.user.ohi.PushNotification;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.ohicabs.user.ohi.Activity.SplashActivity;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

public class OhiUserNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

    Context mContext;

    public OhiUserNotificationOpenedHandler(Context context) {

        mContext = context;
    }

    // This fires when a notification is opened by tapping on it.
    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        OSNotificationAction.ActionType actionType = result.action.type;
        JSONObject data = result.notification.payload.additionalData;

        Log.e("TAG_PUSH_DATA", " = " + data.toString());

        if (actionType == OSNotificationAction.ActionType.ActionTaken) {
            Log.e("OneSignalExample", "Button pressed with id: " + result.action.actionID);

            if (result.action.actionID.equalsIgnoreCase("clk_accpet")) {


            } else if (result.action.actionID.equalsIgnoreCase("clk_decline")) {


            } else {
                Intent intent = new Intent(mContext, SplashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        }

//        {"server_time":"2018-12-31T05:29:14.000Z","sound":"default","notification_id":"0","message":"test message"}
//        12-31 11:00:03.120 21825-21825/? E/OneSignalExample: Button pressed with id: clk_accpet
    }
}

