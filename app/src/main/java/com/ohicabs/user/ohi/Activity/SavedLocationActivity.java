package com.ohicabs.user.ohi.Activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.Adpater.SavedAddressAdapter;
import com.ohicabs.user.ohi.PojoModel.SavedAddressModel;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.NetworkStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SavedLocationActivity extends BaseActivity {

    @BindView(R.id.img_close) AppCompatImageView imgClose;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.txt_no_data) AppCompatTextView txtNoData;
    @BindView(R.id.recyclerView_SavedLocation) RecyclerView recyclerViewSavedLocation;

    private ArrayList<SavedAddressModel> arrSavedAddress = new ArrayList<>();

    SavedAddressAdapter savedAddressAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_address);
        ButterKnife.bind(this);

        setViews();
    }

    private void setViews() {

        recyclerViewSavedLocation.setHasFixedSize(true);
        recyclerViewSavedLocation.setLayoutManager(new LinearLayoutManager(this));

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });

        if (NetworkStatus.getConnectivityStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            getSavedAddressList();
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    /**
     * get user's saved address list
     */
    private void getSavedAddressList() {
        try {
            Call<JsonObject> call = apiService.user_location_list(preferencesUtility.getuser_id(), preferencesUtility.getauth_token());

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        progressBar.setVisibility(View.GONE);
                        String mResult = response.body().toString();

                        setAddressListData(mResult);
                    } catch (Exception e) {
                        Log.e("Exception", e.toString());
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.e("onFailure", t.getMessage().toString());
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * set user's address book data
     *
     * @param stResult
     */
    private void setAddressListData(String stResult) {
        try {
            JSONObject jsonObjectDetails = new JSONObject(stResult);
            if (jsonObjectDetails.optString("status").equalsIgnoreCase("1") &&
                    jsonObjectDetails.optString("success").equalsIgnoreCase("true")) {

                JSONArray jsonArrayPayload = jsonObjectDetails.optJSONArray("payload");
                if (jsonArrayPayload.length() > 0) {
                    arrSavedAddress.clear();

                    for (int i = 0; i < jsonArrayPayload.length(); i++) {

                        JSONObject jsonObjectData = jsonArrayPayload.getJSONObject(i);

                        SavedAddressModel savedAddressModel = new SavedAddressModel();
                        savedAddressModel.setLocationID(jsonObjectData.optString("location_id"));
                        savedAddressModel.setAddress(jsonObjectData.optString("address"));
                        savedAddressModel.setTagName(jsonObjectData.optString("tag_name"));
                        savedAddressModel.setLatitude(jsonObjectData.optString("lati"));
                        savedAddressModel.setLongitude(jsonObjectData.optString("longi"));

                        arrSavedAddress.add(savedAddressModel);
                    }
                }

                savedAddressAdapter = new SavedAddressAdapter(SavedLocationActivity.this, arrSavedAddress, new SavedAddressAdapter.BtnClickListener() {
                    @Override
                    public void onBtnClick(int position, String locationID, double latitude, double longitude, String address) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("latitude", latitude);
                        returnIntent.putExtra("longitude", longitude);
                        returnIntent.putExtra("address", address);
                        returnIntent.putExtra("locationID", locationID);
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                }, new SavedAddressAdapter.DeleteLocationListener() {
                    @Override
                    public void onDeleteClick(int position, String locationID) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(SavedLocationActivity.this);
                        builder.setMessage("Are you sure you want to delete this address?");
                        builder.setTitle("Delete Address");

                        // Set the action buttons
                        builder.setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                deleteSavedAddress(position, locationID);
                            }
                        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

                        builder.create();
                        builder.show();
                    }
                });

                recyclerViewSavedLocation.setAdapter(savedAddressAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * delete user's saved address from address-book
     */
    private void deleteSavedAddress(int position, String locationID) {
        try {
            Call<JsonObject> call = apiService.user_location_delete(preferencesUtility.getuser_id(), preferencesUtility.getauth_token(),locationID);

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        progressBar.setVisibility(View.GONE);
                        String mResult = response.body().toString();

                        JSONObject jsonObjectDetails = new JSONObject(mResult);
                        if (jsonObjectDetails.optString("status").equalsIgnoreCase("1") &&
                                jsonObjectDetails.optString("success").equalsIgnoreCase("true")) {

                            arrSavedAddress.remove(position);
                            savedAddressAdapter.notifyDataSetChanged();
                        }
                    } catch (Exception e) {
                        Log.e("Exception", e.toString());
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.e("onFailure", t.getMessage().toString());
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }
}
