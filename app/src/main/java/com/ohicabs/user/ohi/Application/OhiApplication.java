package com.ohicabs.user.ohi.Application;

import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.Activity.SplashActivity;
import com.ohicabs.user.ohi.BuildConfig;
import com.ohicabs.user.ohi.PushNotification.OhiUserNotificationOpenedHandler;
import com.ohicabs.user.ohi.PushNotification.OhiUserNotificationReceivedHandler;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Activity.DialogActivity;
import com.ohicabs.user.ohi.Activity.ReviewRideActivity;
import com.ohicabs.user.ohi.ServiceManager.PrefManager;
import com.ohicabs.user.ohi.StorageManager.DatabaseHelper;
import com.ohicabs.user.ohi.Utils.ForceUpdateChecker;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;

import io.fabric.sdk.android.Fabric;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by crayon on 14/09/17.
 */

public class OhiApplication extends Application {

    public Socket mSocket;
    public Activity activity;
    Dialog dialog;
    SharedPreferencesUtility preferencesUtility;
    PrefManager prefManager;
    ToastDialog toastDialog;

    private static final String TAG = OhiApplication.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private static OhiApplication mInstance;
    private DatabaseHelper mDBHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        prefManager = new PrefManager(getApplicationContext());
        preferencesUtility = new SharedPreferencesUtility(getApplicationContext());
        mDBHelper = new DatabaseHelper(getApplicationContext());

        setupOneSignalPushNotification();

        Fabric.with(this, new Crashlytics());

        // fresco config initialisation for loading larger images /
        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(getApplicationContext())
                .setDownsampleEnabled(true)
                .build();

        //fresco initialisation /
        Fresco.initialize(this, config);

        // Get Remote Config instance.
        // [START get_remote_config_instance]
        final FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        // [END get_remote_config_instance]


        // [START enable_dev_mode]
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        firebaseRemoteConfig.setConfigSettings(configSettings);
        // [END enable_dev_mode]

        // [START set_default_values]
        Map<String, Object> remoteConfigDefaults = new HashMap();
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_UPDATE_REQUIRED, firebaseRemoteConfig.getBoolean(ForceUpdateChecker.KEY_UPDATE_REQUIRED));
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_CURRENT_VERSION, firebaseRemoteConfig.getString(ForceUpdateChecker.KEY_CURRENT_VERSION));
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_UPDATE_URL,
                firebaseRemoteConfig.getString(ForceUpdateChecker.KEY_UPDATE_URL));

        firebaseRemoteConfig.setDefaults(remoteConfigDefaults);
        // [END set_default_values]


        long cacheExpiration = 3600; // 1 hour in seconds.
        // If your app is using developer mode, cacheExpiration is set to 0, so each fetch will
        // retrieve values from the service.
        if (firebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }


        firebaseRemoteConfig.fetch(cacheExpiration) // fetch every minutes
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "remote config is fetched.");
                            firebaseRemoteConfig.activateFetched();
                        }
                    }
                });

        try {
            IO.Options options = new IO.Options();
            mSocket = IO.socket(Global_ServiceApi.API_HOST, options);

        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        mInstance = this;
    }

    public Socket getSocket() {
        return mSocket;
    }

    public void emit(final String event, final Object... arg) {
        if (mSocket.connected()) {
            mSocket.emit(event, arg);
        } else {
            mSocket.once(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    mSocket.emit(event, arg);
                    Log.e("TAG_Connected_2", "emit connected");
                }
            });
        }
    }

    public void SetActivity(Activity mActivity) {
        this.activity = mActivity;
        toastDialog = new ToastDialog(activity);
        ServiceCallOnMethod();

        dialog = new Dialog(activity);
        dialog.setContentView(R.layout.layout_dialog_popup);
        dialog.setTitle("");
        dialog.setCancelable(false);
    }

    private void setupOneSignalPushNotification() {

        try {
            // OneSignal Initialization
            OneSignal.startInit(this)
                    .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.None)
                    .autoPromptLocation(true)
                    .unsubscribeWhenNotificationsAreDisabled(true)
                    .setNotificationReceivedHandler(new OhiUserNotificationReceivedHandler(getApplicationContext()))
                    .setNotificationOpenedHandler(new OhiUserNotificationOpenedHandler(getApplicationContext()))
                    .init();
            OneSignal.setSubscription(true);

            OneSignal.setInFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification);
            OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
            status.getPermissionStatus().getEnabled();

            status.getSubscriptionStatus().getSubscribed();
            status.getSubscriptionStatus().getUserSubscriptionSetting();
            status.getSubscriptionStatus().getUserId();
            status.getSubscriptionStatus().getPushToken();

            if (status.getSubscriptionStatus().getSubscribed()) {
                preferencesUtility.settoken(status.getSubscriptionStatus().getUserId().toString());
            }

            Log.e("U_ID", status.getSubscriptionStatus().getUserId().toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getOneSignalToken() {
        try {

            OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
            status.getPermissionStatus().getEnabled();

            status.getSubscriptionStatus().getSubscribed();
            status.getSubscriptionStatus().getUserSubscriptionSetting();
            status.getSubscriptionStatus().getUserId();
            status.getSubscriptionStatus().getPushToken();

            return status.getSubscriptionStatus().getUserId().toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//    class OhiUserNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {
//        @Override
//        public void notificationReceived(OSNotification notification) {
//            JSONObject data = notification.payload.additionalData;
//            if (data != null) {
//                try{
//                    Calendar currentTime = Calendar.getInstance();
//                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//                    String stCurrentPushTime = simpleDateFormat.format(currentTime.getTime());
//                    prefManager.setString("notification_time", stCurrentPushTime.toString());
//
//                    String stNotificationID = data.optString("notification_id");
//                    String stMessage = data.optString("message");
//
//                    switch (stNotificationID) {
//                        case "0":
//                            mDBHelper.InsertPushNotificationData(stNotificationID, stMessage, stCurrentPushTime);
//                            break;
//
//                        case "9":
//                            mDBHelper.InsertPushNotificationData(stNotificationID, stMessage, stCurrentPushTime);
//                            break;
//
//                        case "10":
//                            mDBHelper.InsertPushNotificationData(stNotificationID, stMessage, stCurrentPushTime);
//                            break;
//
//                        case "11":
//                            mDBHelper.InsertPushNotificationData(stNotificationID, stMessage, stCurrentPushTime);
//                            break;
//
//                        case "12":
//                            mDBHelper.InsertPushNotificationData(stNotificationID, stMessage, stCurrentPushTime);
//                            break;
//
//                        case "14":
//                            mDBHelper.InsertPushNotificationData(stNotificationID, stMessage, stCurrentPushTime);
//                            break;
//
//                        case "17":
//                            mDBHelper.InsertPushNotificationData(stNotificationID, stMessage, stCurrentPushTime);
//                            break;
//
//                        case "18":
//                            mDBHelper.InsertPushNotificationData(stNotificationID, stMessage, stCurrentPushTime);
//                            break;
//
//                        case "5":
//                            //RIDE STARTED
//
//                            // Set Rental Ride Start Time
//                            Calendar calendar = Calendar.getInstance();
//                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//                            String strRideStartTime = sdf.format(calendar.getTime());
//
//                            preferencesUtility.setIsRideStarted("true");
//                            preferencesUtility.setCurrentTimeRideStart(strRideStartTime);
//                            preferencesUtility.setStartRideBookindId(data.optString("booking_id"));
//                            break;
//
//                        case "6":
//                            //RIDE CONCLUDED
//                            preferencesUtility.setIsRideStarted("false");
//                            break;
//                    }
//
//                } catch (Exception e) { e.printStackTrace(); }
//            }
//        }
//    }

//    class OhiUserNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
//        // This fires when a notification is opened by tapping on it.
//        @Override
//        public void notificationOpened(OSNotificationOpenResult result) {
//            JSONObject data = result.notification.payload.additionalData;
//
//            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//        }
//    }


    public void ServiceCallOnMethod() {

        mSocket.off(Socket.EVENT_CONNECT);
        mSocket.on(Socket.EVENT_CONNECT, onSocketConnect);

        mSocket.off("server_change_data");
        mSocket.on("server_change_data", onServerDataChange);

        mSocket.off("UpdateSocket");
        mSocket.on("UpdateSocket", onUpdateSocket);

        mSocket.off("near_by_driver");
        mSocket.on("near_by_driver", onNearByDrivers);

        /* Driver Tracking On Method */
        mSocket.off("user_request_accept");
        mSocket.on("user_request_accept", onUserRequestAccept);

        mSocket.off("driver_ride_cancel");
        mSocket.on("driver_ride_cancel", onRideCancel);

        mSocket.off("driver_wait_user");
        mSocket.on("driver_wait_user", onDriverWait);

        mSocket.off("ride_start");
        mSocket.on("ride_start", onRideStart);

        mSocket.off("ride_stop");
        mSocket.on("ride_stop", onRideStop);

        mSocket.off("driver_not_avalable");
        mSocket.on("driver_not_avalable", onDriverNotAvailable);

        mSocket.off("driver_cancel");
        mSocket.on("driver_cancel", onDriverCancel);

        mSocket.off("tracking");
        mSocket.on("tracking", onUserRideTracking);

        mSocket.off("reminding_time");
        mSocket.on("reminding_time", onRemindTime);

        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on(Socket.EVENT_ERROR, onConnectError);
    }

    /**
     * Socket On
     */
    private Emitter.Listener onSocketConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            try {
                JSONObject objectUpdateSocket = new JSONObject();
                objectUpdateSocket.put("user_id", preferencesUtility.getuser_id());
                objectUpdateSocket.put("auth_token", preferencesUtility.getauth_token());
                Log.e("TAG_Socket_Connect", "Connected" + preferencesUtility.getuser_id() + "=" + preferencesUtility.getauth_token() + "=" + mSocket.id());
                mSocket.emit("UpdateSocket", objectUpdateSocket.toString(), mSocket.id());

                ServiceCallOnMethod();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    /**
     * App Data Change
     */
    private Emitter.Listener onUserRideTracking = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String mResult = args[0].toString();
                        Log.e("TAG_TRACK_DATA_APP", mResult);
                        Intent intent = new Intent("User_Ride_Tracking");
                        intent.putExtra("user_ride_track_data", mResult);
                        LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };


    private Emitter.Listener onRemindTime = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String mResult = args[0].toString();
                        Log.e("TAG_onRemindTime", mResult);

                        Intent intent_vale_request = new Intent(activity, DialogActivity.class);
                        intent_vale_request.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent_vale_request.putExtra("booking_status_details", mResult);
                        startActivity(intent_vale_request);


//                        Intent intent = new Intent("User_Ride_Tracking");
//                        intent.putExtra("user_ride_track_data", mResult);
//                        LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    /**
     * Driver Cancel On Method
     */
    private Emitter.Listener onDriverCancel = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String mResult = args[0].toString();
                        Log.e("TAG_DriverCancel_Data", mResult);
                        setRideStatus(mResult);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    /**
     * Driver Not Available On Method
     */
    private Emitter.Listener onDriverNotAvailable = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String mResult = args[0].toString();
                        Log.e("TAG_NoDriver_Data", mResult);
//                        setRideStatus(mResult);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };


    /**
     * Ride Stopped On Method
     */
    private Emitter.Listener onRideStop = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String mResult = args[0].toString();
                        preferencesUtility.setIsRideStarted("false");

                        JSONObject objBookingList = new JSONObject(mResult);
                        if (objBookingList.getString("status").equalsIgnoreCase("2")) {
                            Intent intent = new Intent("OutStation_Trip_One_Stop");
                            intent.putExtra("outstation_trip_one_data", mResult);
                            intent.putExtra("message", objBookingList.optString("message"));
                            LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);

                        } else {
                            RideStopAlertDialog(mResult);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    public void RideStopAlertDialog(String result) {
        try {
            Intent intent_vale_request = new Intent(activity, ReviewRideActivity.class);
            intent_vale_request.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent_vale_request.putExtra("ride_stop_details", result);
            startActivity(intent_vale_request);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ride Started On Method
     */
    private Emitter.Listener onRideStart = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String mResult = args[0].toString();
                        Log.e("TAG_RideStart_Data", mResult);
//                        setRideStatus(mResult);

                        // Set Rental Ride Start Time
                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        String strRideStartTime = simpleDateFormat.format(calendar.getTime());

                        preferencesUtility.setIsRideStarted("true");
                        preferencesUtility.setCurrentTimeRideStart(strRideStartTime);

                        JSONObject objBookingList = new JSONObject(mResult);
                        if (objBookingList.getString("status").equalsIgnoreCase("1")) {
                            JSONArray jarrPayload = objBookingList.getJSONArray("payload");

                            for (int i = 0; i <= jarrPayload.length()-1; i++) {
                                JSONObject obj = jarrPayload.getJSONObject(i);
                                preferencesUtility.setStartRideBookindId(obj.getString("booking_id"));
                            }
                        }

                        Intent intent = new Intent("User_Ride_Start");
                        intent.putExtra("user_ride_Start_data", mResult);
                        LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };


    /**
     * Ride Request Status Dialog
     *
     * @param mResult
     */
    private void setRideStatus(String mResult) {
        try {

            JSONObject objData = new JSONObject(mResult);
            if (objData.getString("status").equalsIgnoreCase("1")) {
                JSONArray jArry_ReqData = objData.getJSONArray("payload");
                JSONObject objReqData = jArry_ReqData.getJSONObject(0);
                String stBookingId = objReqData.getString("booking_id");

                CancelRequestAlertDialog(mResult);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * User Wait for Driver
     */
    private Emitter.Listener onDriverWait = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String mResult = args[0].toString();
                        Log.e("TAG_User_wait_driver", mResult);
//                        CancelRequestAlertDialog(mResult);

                        Intent intent = new Intent("User_Wait_For_Driver_Data");
                        intent.putExtra("user_wait_driver_data", mResult);
                        LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);

                        JSONObject objResult = new JSONObject(mResult);
                        if (objResult.getString("status").equalsIgnoreCase("1")) {
                            JSONArray arrPayload = objResult.getJSONArray("payload");
                            if (arrPayload.length() > 0) {
                                JSONObject objData = arrPayload.getJSONObject(0);
                                preferencesUtility.setBookingIdOnRide(objData.getString("booking_id"));
                                preferencesUtility.setBookingStatusOnRide(objData.getString("booking_status"));
                                int wait_time = objData.getInt("waiting");

                                Calendar calendar = Calendar.getInstance();
                                calendar.add(Calendar.SECOND, wait_time);
                                String formattedDate = df.format(calendar.getTime());
                                preferencesUtility.setDriverWaitTime(formattedDate);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
    /**
     * Ride Cancelled On Method
     */
    private Emitter.Listener onRideCancel = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String mResult = args[0].toString();
                        Log.e("TAG_CANCEL_REQUEST", mResult);
                        CancelRequestAlertDialog(mResult);

                        Intent intent = new Intent("Driver_Cancel_Request");
                        intent.putExtra("driver_cancel_request", mResult);
                        LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };


    /**
     * Request Accepted On Method
     */
    private Emitter.Listener onUserRequestAccept = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String mResult = args[0].toString();
                        Log.e("TAG_AcceptRequest_App", mResult);
                        setAcceptRideStatus(mResult);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    /**
     * Accept Ride Request Status Dialog
     *
     * @param mResult
     */
    private void setAcceptRideStatus(String mResult) {
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Intent intent = new Intent("User_Ride_Request");
            intent.putExtra("user_ride_request_data", mResult);
            LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);

            JSONObject objResult = new JSONObject(mResult);
            if (objResult.getString("status").equalsIgnoreCase("1")) {
                JSONArray arrPayload = objResult.getJSONArray("payload");
                if (arrPayload.length() > 0) {
                    JSONObject objData = arrPayload.getJSONObject(0);
                    preferencesUtility.setBookingIdOnRide(objData.getString("booking_id"));
                    preferencesUtility.setBookingStatusOnRide(objData.getString("booking_status"));
                    int wait_time = objData.getInt("ride_cancel");

                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.SECOND, wait_time);
                    String formattedDate = df.format(calendar.getTime());
                    preferencesUtility.setDriverWaitTime(formattedDate);
                }
//                CancelRequestAlertDialog(mResult);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void CancelRequestAlertDialog(String result) {
        try {
            Intent intent_vale_request = new Intent(activity, DialogActivity.class);
            intent_vale_request.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent_vale_request.putExtra("booking_status_details", result);
            startActivity(intent_vale_request);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * User App Default Data
     */
    private Emitter.Listener onServerDataChange = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String mResult = args[0].toString();
                        Intent intent = new Intent("User_Default_App_Data");
                        intent.putExtra("user_default_app_data", mResult);
                        LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };


    /**
     * Update Socket On Server Disconnecting
     */
    private Emitter.Listener onUpdateSocket = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String mResult = args[0].toString();
                        Intent intent = new Intent("Update_Socket_Data");
                        intent.putExtra("socket_data", mResult);
                        LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    /**
     * Get Nearest Car List according to Current Lat-Lng of User
     */
    private Emitter.Listener onNearByDrivers = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String mResult = args[0].toString();

                        Intent intent = new Intent("NearBy_Car_List");
                        intent.putExtra("nearest_car_list", mResult);
                        LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    /**
     * Socket Error listener
     */
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.e("TAG_Socket_disConnect", "DisConnected");
                        if (args.length > 0) {
                            String mResult = args[0].toString();
                            Log.e("TAG_ERROR", mResult + "===");
                            NoInternetDialog("Server not connected", getResources().getString(R.string.network_lost));

                            if (dialog != null && dialog.isShowing()) {
                                dialog.show();
                            }
                        }
                        mSocket.off(Socket.EVENT_RECONNECT);
                        mSocket.on(Socket.EVENT_RECONNECT, onReConnect);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    /**
     * Socket On
     */
    private Emitter.Listener onReConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            try {
                Log.e("TAG_Re_Socket_Connect", "ReConnected");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };


    /**
     * Network Drop or Disconnect Error
     *
     * @param title
     * @param message
     * @return
     */
    public Dialog NoInternetDialog(String title, String message) {

        AppCompatTextView txt_popup_header = (AppCompatTextView) dialog.findViewById(R.id.txt_popup_header);
        AppCompatTextView txt_popup_message = (AppCompatTextView) dialog.findViewById(R.id.txt_popup_message);
        AppCompatButton btn_ok = (AppCompatButton) dialog.findViewById(R.id.btn_ok);

        txt_popup_header.setText(title);
        txt_popup_message.setText(message);


        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {

            }
        });
        dialog.show();
        return null;
    }



    public static synchronized OhiApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
