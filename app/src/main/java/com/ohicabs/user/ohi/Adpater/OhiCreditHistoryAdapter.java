package com.ohicabs.user.ohi.Adpater;

import android.app.Activity;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.PojoModel.CreditHistoryModel;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.CircularImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

public class OhiCreditHistoryAdapter extends RecyclerView.Adapter<OhiCreditHistoryAdapter.MyViewHolder>{

    private Activity mActivity;
    ArrayList<CreditHistoryModel> arrCreditHisory;

    private static AppCompatRadioButton lastChecked = null;
    private int lastCheckedPos = 0;
    private int selectedPosition = -1;
    private BtnClickListener mClickListener = null;

    public OhiCreditHistoryAdapter(Activity act, ArrayList<CreditHistoryModel> creditHisoryList, BtnClickListener btnClickListener) {
        this.mActivity = act;
        arrCreditHisory = creditHisoryList;
        mClickListener = btnClickListener;
    }

    @Override
    public OhiCreditHistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_ohi_credit, parent, false);

        return new OhiCreditHistoryAdapter.MyViewHolder(itemView);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CircularImageView circularImageView;
        public AppCompatTextView txtUserName, txtMobileNumber, txtPoints, txtEarn, txtInitiatedDate, txtExpiryDate;
        public LinearLayout llHistoryRow;

        public MyViewHolder(View view) {
            super(view);

            circularImageView = view.findViewById(R.id.img_user);

            txtUserName = view.findViewById(R.id.txt_username);
            txtMobileNumber = view.findViewById(R.id.txt_mobile_number);
            txtPoints = view.findViewById(R.id.txt_points);
            txtEarn = view.findViewById(R.id.txt_earn);
            txtInitiatedDate = view.findViewById(R.id.txt_initiated_date);
            txtExpiryDate = view.findViewById(R.id.txt_expiry_date);
            llHistoryRow = view.findViewById(R.id.ll_history_row);
        }
    }

    @Override
    public void onBindViewHolder(OhiCreditHistoryAdapter.MyViewHolder holder, final int position) {

        Glide.with(mActivity).load(Global_ServiceApi.API_IMAGE_HOST + arrCreditHisory.get(position).getImage())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.circularImageView);

        holder.txtUserName.setText(arrCreditHisory.get(position).getFirstName());
        holder.txtMobileNumber.setText(arrCreditHisory.get(position).getMobile());
        holder.txtPoints.setText(arrCreditHisory.get(position).getAmount() + " Points");
        holder.txtEarn.setText(arrCreditHisory.get(position).getStatus());

        if (arrCreditHisory.get(position).getStatus().equalsIgnoreCase("1")) {

            holder.txtEarn.setText("Earned");
            holder.txtEarn.setTextColor(mActivity.getResources().getColor(R.color.colorGreen));
            holder.txtInitiatedDate.setTextColor(mActivity.getResources().getColor(R.color.colorGreen));

            holder.txtInitiatedDate.setText("Initiated On: " + convertDate(arrCreditHisory.get(position).getInitiateDate()));
            holder.txtExpiryDate.setText("Expires On: " + convertDate(arrCreditHisory.get(position).getExpiryTime()));

            holder.txtExpiryDate.setVisibility(View.VISIBLE);

        } else if (arrCreditHisory.get(position).getStatus().equalsIgnoreCase("0")) {
            holder.txtEarn.setText("Pending");
            holder.txtEarn.setTextColor(mActivity.getResources().getColor(R.color.colorRed));
            holder.txtInitiatedDate.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));

            holder.txtInitiatedDate.setText("Register On: " + convertDate(arrCreditHisory.get(position).getCreatedDate()));
            holder.txtExpiryDate.setVisibility(View.GONE);
        }

        holder.llHistoryRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) {

                    mClickListener.onBtnClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrCreditHisory.size();
    }

    public interface BtnClickListener {
        void onBtnClick(int position);
    }

    private String convertDate(String stDate) {
        try {
            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = df.parse(stDate);
            df.setTimeZone(TimeZone.getDefault());
            SimpleDateFormat date_fmt = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault());
            String _date = date_fmt.format(date);

            return _date;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
