package com.ohicabs.user.ohi.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OfferDetailActivity extends BaseActivity {

    @BindView(R.id.txt_city_name) TextView txt_city_name;
    @BindView(R.id.txt_offer) TextView txt_offer;
    @BindView(R.id.txt_offer_details) TextView txt_offer_details;
    @BindView(R.id.img_back) ImageView img_back;
    @BindView(R.id.img_offer) ImageView img_offer;

    String stImage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_detail);
        ButterKnife.bind(this);
        setContent();
    }

    private void setContent() {

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (getIntent().hasExtra("zonename")) {
            txt_city_name.setText(getIntent().getStringExtra("zonename"));
        }
        if (getIntent().hasExtra("offer_title")) {
            txt_offer.setText(getIntent().getStringExtra("offer_title"));
        }
        if (getIntent().hasExtra("description")) {
            txt_offer_details.setText(getIntent().getStringExtra("description"));
        }

        img_offer.setVisibility(View.GONE);

        if (getIntent().hasExtra("image")) {
            stImage = getIntent().getStringExtra("image");
            if (!stImage.equals("")) {

                img_offer.setVisibility(View.VISIBLE);

                Glide.with(this).load(Global_ServiceApi.API_IMAGE_HOST + stImage)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(img_offer);

            } else {
                img_offer.setVisibility(View.GONE);
            }
        }

//        img_offer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(), OfferImageActivity.class);
//                intent.putExtra("image", stImage);
//                startActivity(intent);
//
//            }
//        });

    }
}
