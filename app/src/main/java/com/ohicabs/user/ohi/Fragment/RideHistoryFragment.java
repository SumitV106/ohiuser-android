package com.ohicabs.user.ohi.Fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Activity.BookingDetailsActivity;
import com.ohicabs.user.ohi.Adpater.UserAllRidesListAdapter;
import com.ohicabs.user.ohi.CustomWidgets.CustomBoldFontTextView;
import com.ohicabs.user.ohi.Utils.Global_Typeface;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;
import com.ohicabs.user.ohi.ohiOutStation.OutStationBookingDetailsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

/**
 * A simple {@link Fragment} subclass.
 */
public class RideHistoryFragment extends Fragment {


    public RideHistoryFragment() {
        // Required empty public constructor
    }

    private ToastDialog toastDialog;
    private Global_Typeface global_typeface;
    private SharedPreferencesUtility sharedPreferencesUtility;

    private ArrayList<HashMap<String, String>> _arr_BookedRideList = new ArrayList<>();

    ApiInterface apiService;

    @BindView(R.id.list_my_rides)
    ListView _listview_my_rides;

    @BindView(R.id.txt_no_data)
    CustomBoldFontTextView _txt_no_data;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toastDialog = new ToastDialog(getActivity());
        global_typeface = new Global_Typeface(getActivity());
        sharedPreferencesUtility = new SharedPreferencesUtility(getActivity());
        apiService = ApiClient.getClient().create(ApiInterface.class);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ride_history, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onStart() {
        super.onStart();
        if (isInternetOn(getContext())) {
            try {
                Call<JsonObject> call = apiService.user_all_ride_list(sharedPreferencesUtility.getuser_id());

                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        Log.e("user_all_ride_list", response.body().toString());
                        GetAllBookedRideList(response.body().toString());
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
        Log.e("TAG_Start", "onStart");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.e("TAG_Destroy", "onDestroy");
    }


    /**
     * Get All Booked Rides List by User
     *
     * @param mResult
     */
    private void GetAllBookedRideList(String mResult) {


        try {
            JSONObject objBookingList = new JSONObject(mResult);
            if (objBookingList.getString("status").equalsIgnoreCase("1")) {
                _txt_no_data.setVisibility(View.GONE);
                _listview_my_rides.setVisibility(View.VISIBLE);
                JSONArray jarrPayload = objBookingList.getJSONArray("payload");
                if (jarrPayload.length() > 0) {
                    _arr_BookedRideList.clear();

                    for (int i = 0; i < jarrPayload.length(); i++) {
                        JSONObject objBookingData = jarrPayload.getJSONObject(i);

                        Iterator<String> iter = objBookingData.keys();
                        HashMap<String, String> map_keyValue = new HashMap<>();
                        while (iter.hasNext()) {
                            String key = iter.next();
                            try {
                                Object value = objBookingData.get(key);
                                map_keyValue.put(key, value.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        _arr_BookedRideList.add(map_keyValue);
                    }
                }
            } else {
                _txt_no_data.setVisibility(View.VISIBLE);
                _listview_my_rides.setVisibility(View.GONE);
                _txt_no_data.setText(objBookingList.getString("Message"));
            }
            UserAllRidesListAdapter userAllRidesListAdapter = new UserAllRidesListAdapter(getActivity(),
                    R.layout.layout_myrides_row, _arr_BookedRideList,
                    new UserAllRidesListAdapter.BtnClickListener() {
                        @Override
                        public void onBtnClick(int position, String ride_id, String ride_status, String stRideType) {
                            final int rideType = Integer.parseInt(stRideType);
                            if (rideType > 3) {
                                Intent _intent = new Intent(getActivity(), OutStationBookingDetailsActivity.class);
                                _intent.putExtra("booking_id", ride_id);
                                startActivity(_intent);

                            } else {
                                Intent _intent = new Intent(getActivity(), BookingDetailsActivity.class);
                                _intent.putExtra("booking_id", ride_id);
                                startActivity(_intent);
                            }
                        }
                    });
            _listview_my_rides.setAdapter(userAllRidesListAdapter);

        } catch (Exception ex) {
            Log.e("error", ex.toString());
            ex.printStackTrace();
        }
    }
}
