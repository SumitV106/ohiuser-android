package com.ohicabs.user.ohi.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonArray;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.CustomWidgets.CustomBoldFontButton;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontEditText;
import com.ohicabs.user.ohi.DBModel.ZoneList_Model;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontTextView;
import com.ohicabs.user.ohi.Utils.TypefaceSpan;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;
import static com.ohicabs.user.ohi.Utils.Constant.BACK_TO_ACTIVITY;

public class RegisterActivity extends BaseActivity {

    public static final int REQUEST_STORAGE_PERMISSION = 5001;

    @BindView(R.id.tvheader) TextView _tvheader;
    @BindView(R.id.rbtnmale) RadioButton _rbtnmale;
    @BindView(R.id.rbtnfemale) RadioButton _rbtnfemale;

    @BindView(R.id.passwordWrapper) TextInputLayout passwordWrapper;
    @BindView(R.id.cpasswordWrapper) TextInputLayout cpasswordWrapper;
    @BindView(R.id.nameWrapper) TextInputLayout nameWrapper;
    @BindView(R.id.emailWrapper) TextInputLayout emailWrapper;
    @BindView(R.id.mobileWrapper) TextInputLayout mobileWrapper;
    @BindView(R.id.wrapper_promo_code) TextInputLayout promoCodeWrapper;
    @BindView(R.id.referralWrapper) TextInputLayout ReferralCodeWrapper;

    @BindView(R.id.imgcancle) ImageView _imgcancle;
    @BindView(R.id.btnregister) CustomBoldFontButton _btnregister;

    @BindView(R.id.edname) AppCompatEditText _edname;
    @BindView(R.id.edemail) AppCompatEditText _edemail;
    @BindView(R.id.edpwd) AppCompatEditText _edpwd;
    @BindView(R.id.edcpwd) AppCompatEditText _edcpwd;
    @BindView(R.id.edmobile) AppCompatEditText _edmobile;
    @BindView(R.id.edt_promo_code) AppCompatEditText edtPromoCode;
    @BindView(R.id.edt_referral_code) AppCompatEditText edtReferralCode;

    @BindView(R.id.chk_agree_terms_condition) AppCompatCheckBox chk_agree_terms_condition;
    @BindView(R.id.txt_terms) CustomRegularFontTextView txt_terms;
    @BindView(R.id.txt_disclaimer) CustomRegularFontTextView txt_disclaimer;
    @BindView(R.id.btn_redeem) AppCompatButton btnRedeem;
    @BindView(R.id.spinner_zone) AppCompatSpinner spinnerZoneList;

    private String gender = "", stAgree = "", zoneID;
    ApiInterface apiService;
    ArrayList<ZoneList_Model> arrZoneList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        setContent();
    }

    private void setContent() {

        _tvheader.setTypeface(global_typeface.Sansation_Bold());
        _rbtnfemale.setTypeface(global_typeface.Sansation_Regular());
        _rbtnmale.setTypeface(global_typeface.Sansation_Regular());

        chk_agree_terms_condition.setTypeface(global_typeface.Sansation_Regular());

        _edname.addTextChangedListener(new RegisterActivity.MyTextWatcher(_edname));
        _edemail.addTextChangedListener(new RegisterActivity.MyTextWatcher(_edemail));
        _edpwd.addTextChangedListener(new RegisterActivity.MyTextWatcher(_edpwd));
        _edcpwd.addTextChangedListener(new RegisterActivity.MyTextWatcher(_edcpwd));
        _edmobile.addTextChangedListener(new RegisterActivity.MyTextWatcher(_edmobile));
        edtPromoCode.addTextChangedListener(new RegisterActivity.MyTextWatcher(edtPromoCode));

        passwordWrapper.setTypeface(global_typeface.Sansation_Regular());
        cpasswordWrapper.setTypeface(global_typeface.Sansation_Regular());

        _imgcancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(RegisterActivity.this);
                finish();
            }
        });

        chk_agree_terms_condition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    stAgree = "true";
                } else {
                    stAgree = "false";
                }
            }
        });

        txt_terms.setText(Html.fromHtml("I agree to the <u>terms and conditions</u> including the"));
        txt_disclaimer.setText(Html.fromHtml("<u>Disclaimer</u>"));

        txt_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i_terms_condition = new Intent(RegisterActivity.this, TermsConditionActivity.class);
                i_terms_condition.putExtra("link", "terms");
                startActivity(i_terms_condition);
            }
        });

        txt_disclaimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i_terms_condition = new Intent(RegisterActivity.this, TermsConditionActivity.class);
                i_terms_condition.putExtra("link", "disclaimer");
                startActivity(i_terms_condition);
            }
        });

        apiService = ApiClient.getClient().create(ApiInterface.class);

        _btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (submitForm()) {

                    if (zoneID.equalsIgnoreCase("0")) {
                        ShowToast("Please select your City");

                    } else if (stAgree.equalsIgnoreCase("") || stAgree.equalsIgnoreCase("false")) {
                        ShowToast("Please select Terms & Condition");

                    } else {
                        if (isInternetOn(getApplicationContext())) {
                            userRegistration();
                        } else {
                            ShowToast(getResources().getString(R.string.network_lost));
                        }
                    }
                }
            }
        });

        btnRedeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edtPromoCode.getText().toString().equalsIgnoreCase("OHI100")) {
                    ShowToast("Invalid PromoCode!");
                } else {
                    ShowToast("You got instant discount upto Rs. 100 on all your rides till 31st December, 2018 on payment via Paytm");
                }
            }
        });

        btnRedeem.setEnabled(false);

        edtPromoCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 0) {
                    btnRedeem.setEnabled(true);
                } else {
                    btnRedeem.setEnabled(false);
                }
            }
        });

        spinnerZoneList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View convertView, int position, long id) {
                AppCompatTextView textView = (AppCompatTextView) convertView;
                ((AppCompatTextView) parent.getChildAt(0)).setTextColor(Color.WHITE);

                ZoneList_Model zoneList = (ZoneList_Model) parent.getSelectedItem();
                zoneID = zoneList.getZone_id();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        PackageInfo pinfo;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            int versionCode = pinfo.versionCode;
            Log.e("*****Version","Code****"+versionCode);
            //ET2.setText(versionNumber);
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        requestPermission();

        /*
         * Get Zone List
         */
        GetZoneList();
    }

    private void userRegistration() {

        if (_rbtnmale.isChecked()) {
            gender = "M";
        } else {
            gender = "F";
        }

        if (isInternetOn(getApplicationContext())) {

            progressDialog.show();
            Call<JsonObject> call = apiService.user_register(_edname.getText().toString(),
                    "", _edemail.getText().toString(), gender, _edmobile.getText().toString(),
                    _edpwd.getText().toString(), "1", preferencesUtility.gettoken(), "Android", edtReferralCode.getText().toString(),
                    mSocket.id(), zoneID);

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    progressDialog.dismiss();

                    if (response.isSuccessful()) {

                        JsonObject object = response.body();
                        if (object.get("success").getAsString().equalsIgnoreCase("true") && object.get("status").getAsString().equalsIgnoreCase("1")) {
                            JsonObject jarrayLogin = object.getAsJsonObject("payload");

                            preferencesUtility.setuser_id(jarrayLogin.get("user_id").getAsString());
                            preferencesUtility.setfirstname(jarrayLogin.get("firstname").getAsString());
                            preferencesUtility.setemail(jarrayLogin.get("email").getAsString());
                            preferencesUtility.setgender(jarrayLogin.get("gender").getAsString());
                            preferencesUtility.setmobile(jarrayLogin.get("mobile").getAsString());
                            preferencesUtility.setauth_token(jarrayLogin.get("auth_token").getAsString());
                            preferencesUtility.setimage(jarrayLogin.get("image").getAsString());
                            preferencesUtility.setrefer_code(jarrayLogin.get("refer_code").getAsString());
                            preferencesUtility.setstatus(jarrayLogin.get("status").getAsString());
                            preferencesUtility.setZoneId(jarrayLogin.get("zone_id").getAsString());

                            preferencesUtility.setLogedin(true);
                            preferencesUtility.setLoginType("true");
                            toastDialog.ShowToastMessage(getResources().getString(R.string.st_register_success));

                            // for close previous activity
                            Intent intent1=new Intent();
                            intent1.putExtra("MESSAGE","finish");
                            setResult(BACK_TO_ACTIVITY,intent1);

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                            finish();

                        } else if (object.get("status").getAsString().equalsIgnoreCase("0") || object.get("success").getAsString().equalsIgnoreCase("false")) {
                            toastDialog.ShowToastMessage(getString(R.string.st_already_register));

                        } else if (object.get("status").getAsString().equalsIgnoreCase("2")) {
                            toastDialog.ShowToastMessage(getResources().getString(R.string.st_register_success));

                            // for close previous activity
                            Intent intent1=new Intent();
                            intent1.putExtra("MESSAGE","finish");
                            setResult(BACK_TO_ACTIVITY,intent1);

                            Intent intent = new Intent(RegisterActivity.this, VerificationActivity.class);

                            JsonObject jarrayLogin = object.getAsJsonObject("payload");

                            Bundle b_data = new Bundle();
                            b_data.putString("email", _edemail.getText().toString());
                            b_data.putString("verify_mobile_no", _edmobile.getText().toString());
                            b_data.putString("user_id", jarrayLogin.get("user_id").getAsString());
                            intent.putExtras(b_data);
                            startActivity(intent);
                            finish();
                        } else {
                            toastDialog.ShowToastMessage(object.get("Message").getAsString());
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                }
            });
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean validateName() {
        if (_edname.getText().toString().trim().isEmpty()) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_no_name));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            nameWrapper.setError(ssbuilder);
            requestFocus(_edname);
            return false;
        } else {
            nameWrapper.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail() {
        if (_edemail.getText().toString().trim().equals("")) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_email_blank));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            emailWrapper.setError(ssbuilder);
            requestFocus(_edemail);
            return false;
        } else if (!isValidEmail(_edemail.getText().toString())) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_email_no_valid));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            emailWrapper.setError(ssbuilder);
            requestFocus(_edemail);
            return false;
        } else {
            emailWrapper.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMobile() {
        if (_edmobile.getText().toString().trim().equals("")) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_no_mobile));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            mobileWrapper.setError(ssbuilder);
            requestFocus(_edmobile);
            return false;
        } else if (_edmobile.getText().toString().trim().length() != 10) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_no_valid_mobile));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            mobileWrapper.setError(ssbuilder);
            requestFocus(_edmobile);
            return false;
        } else {
            mobileWrapper.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        if (_edpwd.getText().toString().trim().isEmpty()) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_password_blank));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            passwordWrapper.setError(ssbuilder);
            requestFocus(_edpwd);
            return false;
        } else if (_edpwd.getText().toString().trim().length() < 6) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_password_minimum));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            passwordWrapper.setError(ssbuilder);
            requestFocus(_edpwd);
            return false;
        } else {
            passwordWrapper.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateConfirmPassword() {
        if (_edcpwd.getText().toString().trim().isEmpty()) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_password_confirm_blank));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            cpasswordWrapper.setError(ssbuilder);
            requestFocus(_edcpwd);
            return false;
        } else if (!_edcpwd.getText().toString().equals(_edpwd.getText().toString())) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_password_no_match));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            cpasswordWrapper.setError(ssbuilder);
            requestFocus(_edcpwd);
            return false;
        } else {
            cpasswordWrapper.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateterms() {
        Log.e("stAgree", stAgree);
        if (stAgree.equalsIgnoreCase("") && stAgree.equalsIgnoreCase("false")) {
            ShowToast("Please select Terms & Condition");
            return false;
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    private boolean submitForm() {
        boolean result = true;

        if (!validateName()) {
            result = false;
        }
        if (!validateEmail()) {
            result = false;
        }
        if (!validatePassword()) {
            result = false;
        }
        if (!validateConfirmPassword()) {
            result = false;
        }
        if (!validateMobile()) {
            result = false;
        }

        return result;
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.edname:
                    validateName();
                    break;
                case R.id.edemail:
                    validateEmail();
                    break;
                case R.id.edpwd:
                    validatePassword();
                    break;
                case R.id.edmobile:
                    validateMobile();
                    break;
                case R.id.edcpwd:
                    validateConfirmPassword();
                    break;
            }
        }
    }

    /**
     * Get Zone List Data
     */
    private void GetZoneList() {
        try {
            Call<JsonObject> call = apiService.getZoneList();

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    progressDialog.dismiss();

                    if (response.isSuccessful()) {
                        JsonObject objState = response.body();

                        if (objState.get("success").getAsString().equalsIgnoreCase("true")) {
                            if (objState.get("status").getAsString().equalsIgnoreCase("1")) {
                                JsonArray arrState = objState.get("payload").getAsJsonArray();
                                if (arrState.size() > 0) {

                                    ZoneList_Model zoneListModelTemp = new ZoneList_Model();
                                    zoneListModelTemp.setZone_id("0");
                                    zoneListModelTemp.setZone_name("Select City");
                                    arrZoneList.add(zoneListModelTemp);

                                    for (int i = 0; i < arrState.size(); i++) {
                                        JsonObject objStateData = arrState.get(i).getAsJsonObject();

                                        ZoneList_Model zoneListModel = new ZoneList_Model();
                                        zoneListModel.setZone_id(objStateData.get("zone_id").getAsString());
                                        zoneListModel.setZone_name(objStateData.get("zone_name").getAsString());
                                        arrZoneList.add(zoneListModel);
                                    }
                                }

                                //fill data in spinner
                                ArrayAdapter<ZoneList_Model> adapter = new ArrayAdapter<ZoneList_Model>(getApplicationContext(),
                                        R.layout.spinner_items_zone, arrZoneList);
                                spinnerZoneList.setAdapter(adapter);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("error", t.toString());
                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_STORAGE_PERMISSION) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                // Finish Activity - when permission not accepted
                finish();

//                requestPermission();
            }
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSION);
        }
    }
}
