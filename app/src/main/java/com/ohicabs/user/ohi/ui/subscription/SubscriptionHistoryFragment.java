package com.ohicabs.user.ohi.ui.subscription;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.Adpater.SubscriptionHistoryAdapter;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.PojoModel.SubscriptionHistoryModel;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.Global_Typeface;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

public class SubscriptionHistoryFragment extends Fragment {

    @BindView(R.id.recyclerView_history) RecyclerView recyclerViewHistory;

    private RecyclerView.LayoutManager mLayoutManager;

    private ProgressDialog progressDialog;
    private ApiInterface apiService;
    private ToastDialog toastDialog;
    private Global_Typeface global_typeface;
    private SharedPreferencesUtility sharedPreferencesUtility;
    private ArrayList<SubscriptionHistoryModel> arrSubscriptionHistory = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toastDialog = new ToastDialog(getActivity());
        global_typeface = new Global_Typeface(getActivity());
        sharedPreferencesUtility = new SharedPreferencesUtility(getActivity());
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_subscription_history, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        recyclerViewHistory.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewHistory.setLayoutManager(mLayoutManager);

        getSubscriptionHistory();
    }

    private void getSubscriptionHistory() {

        if (isInternetOn(getActivity())) {
            try {
                progressDialog.show();
                Call<JsonObject> call = apiService.subscription_history(sharedPreferencesUtility.getuser_id(), sharedPreferencesUtility.getauth_token(), "0");

                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try {
                            progressDialog.dismiss();

                            JSONObject jsonObjectDetails = new JSONObject(response.body().toString());
                            setSubscriptionSubscription(jsonObjectDetails);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        progressDialog.dismiss();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                progressDialog.dismiss();
            }
        }
    }

    private void setSubscriptionSubscription(JSONObject jsonObjectDetails) {
        try {

            if (jsonObjectDetails.optString("status").equalsIgnoreCase("1")) {

                JSONArray jsonArrayPayload = jsonObjectDetails.getJSONArray("payload");
                for (int i = 0; i < jsonArrayPayload.length(); i++) {
                    JSONObject jsonObjectData = jsonArrayPayload.getJSONObject(i);

                    SubscriptionHistoryModel subscriptionHistoryModel = new SubscriptionHistoryModel();

                    subscriptionHistoryModel.setSub_buy_id(jsonObjectData.optString("sub_buy_id"));
                    subscriptionHistoryModel.setPlan_id(jsonObjectData.optString("plan_id"));
                    subscriptionHistoryModel.setActive_date(jsonObjectData.optString("active_date"));
                    subscriptionHistoryModel.setPayment_type(jsonObjectData.optString("payment_type"));
                    subscriptionHistoryModel.setAmount(jsonObjectData.optString("amount"));
                    subscriptionHistoryModel.setPlan_name(jsonObjectData.optString("plan_name"));
                    subscriptionHistoryModel.setDetails(jsonObjectData.optString("details"));
                    subscriptionHistoryModel.setDays(jsonObjectData.optString("days"));
                    subscriptionHistoryModel.setMax_ride(jsonObjectData.optString("max_ride"));
                    subscriptionHistoryModel.setMax_discount(jsonObjectData.optString("max_discount"));
                    subscriptionHistoryModel.setDiscount_per(jsonObjectData.optString("discount_per"));
                    subscriptionHistoryModel.setImage(jsonObjectData.optString("image"));
                    subscriptionHistoryModel.setTerm_and_condition(jsonObjectData.optString("term_and_condition"));
                    subscriptionHistoryModel.setExpiry_date(jsonObjectData.optString("expiry_date"));
                    subscriptionHistoryModel.setStatus(jsonObjectData.optString("status"));
                    subscriptionHistoryModel.setService_name(jsonObjectData.optString("service_name"));

                    subscriptionHistoryModel.setSubscriptionDetails(jsonObjectData.toString());

                    arrSubscriptionHistory.add(subscriptionHistoryModel);
                }

                SubscriptionHistoryAdapter subscriptionHistoryAdapter = new SubscriptionHistoryAdapter(getActivity(),
                        arrSubscriptionHistory, new SubscriptionHistoryAdapter.BtnClickListener() {
                    @Override
                    public void onBtnClick(int position, String details) {
                        Intent intent = new Intent(getActivity(), SubscriptionDetailsActivity.class);
                        intent.putExtra("subscription_details", details);
                        intent.putExtra("subscription_history", "History");
                        startActivity(intent);
                    }
                });

                recyclerViewHistory.setAdapter(subscriptionHistoryAdapter);
            } else {

                toastDialog.showAlertDialog("No Records Found", "No more subscription data.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
