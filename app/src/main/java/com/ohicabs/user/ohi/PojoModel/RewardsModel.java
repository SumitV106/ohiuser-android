package com.ohicabs.user.ohi.PojoModel;

public class RewardsModel {

    String reward_user_id, initated_date, reference_no, period_start, period_end, amount, reason, term_and_condition, created_date;

    public String getReward_user_id() {
        return reward_user_id;
    }

    public void setReward_user_id(String reward_user_id) {
        this.reward_user_id = reward_user_id;
    }

    public String getInitated_date() {
        return initated_date;
    }

    public void setInitated_date(String initated_date) {
        this.initated_date = initated_date;
    }

    public String getReference_no() {
        return reference_no;
    }

    public void setReference_no(String reference_no) {
        this.reference_no = reference_no;
    }

    public String getPeriod_start() {
        return period_start;
    }

    public void setPeriod_start(String period_start) {
        this.period_start = period_start;
    }

    public String getPeriod_end() {
        return period_end;
    }

    public void setPeriod_end(String period_end) {
        this.period_end = period_end;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getTerm_and_condition() {
        return term_and_condition;
    }

    public void setTerm_and_condition(String term_and_condition) {
        this.term_and_condition = term_and_condition;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }
}
