package com.ohicabs.user.ohi.Activity;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.CustomWidgets.CustomBoldFontButton;
import com.ohicabs.user.ohi.CustomWidgets.CustomBoldFontTextView;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontTextView;
import com.ohicabs.user.ohi.Utils.Constant;
import com.ohicabs.user.ohi.Utils.DataParser;
import com.ohicabs.user.ohi.Utils.PermissionUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;
import static com.ohicabs.user.ohi.Utils.Constant.BACK_TO_ACTIVITY;
import static com.ohicabs.user.ohi.Utils.Constant.LOCATION_PERMISSION_REQUEST_CODE;

public class BookingActivityStep2Activity extends BaseActivity implements View.OnClickListener, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, LocationListener, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.txt_estimated_price) AppCompatTextView _txt_estimated_price;
    @BindView(R.id.txt_dis_estimated_price) AppCompatTextView _txt_dis_estimated_price;
    @BindView(R.id.txt_estimated_time) AppCompatTextView _txt_estimated_time;
    @BindView(R.id.txt_set_payment) AppCompatTextView _txt_set_payment;
    @BindView(R.id.txt_pickup) AppCompatTextView _txt_pickup;
    @BindView(R.id.txt_dropto) AppCompatTextView _txt_dropto;
    @BindView(R.id.btnbooking) CustomBoldFontButton _btnbooking;
    @BindView(R.id.imgback) ImageView _imgback;
    @BindView(R.id.imgfab) ImageButton _imgfab;
    @BindView(R.id.lypayment) LinearLayout _lypayment;
    @BindView(R.id.lyseat) LinearLayout _lyseat;
    @BindView(R.id.relative_seats) RelativeLayout _relative_seats;
    @BindView(R.id.linear_seat1) LinearLayout _linear_seat1;
    @BindView(R.id.linear_seat2) LinearLayout _linear_seat2;
    @BindView(R.id.txt_seat) CustomBoldFontTextView _txt_seat;
    @BindView(R.id.txt_seat2price) CustomRegularFontTextView _txt_seat2price;
    @BindView(R.id.txt_seat1price) CustomRegularFontTextView _txt_seat1price;

    @BindView(R.id.txt_ride_term) TextView _txt_ride_term;
    @BindView(R.id.lydropto) RelativeLayout lydropto;

    @BindView(R.id.txt_total_amount) AppCompatTextView txtTotalAmount;
    @BindView(R.id.txt_paytm_discount) AppCompatTextView txtPaytmDiscount;
    @BindView(R.id.txt_paytm_discount_amount) AppCompatTextView txtPaytmDiscountAmount;
    @BindView(R.id.txt_paytm_discount_total) AppCompatTextView txtPaytmDiscountTotal;

    @BindView(R.id.txt_subscription_discount) AppCompatTextView txtSubscriptionDiscount;
    @BindView(R.id.txt_subscription_discount_amount) AppCompatTextView txtSubscriptionDiscountAmount;

    @BindView(R.id.txt_final_total) AppCompatTextView txtFinalTotal;

    Double mPickUpLat = 0.0, mPickUpLng = 0.0, mDropLat = 0.0, mDropLng = 0.0, mUserCurrentLat, mUserCurrentLng;

    String isTexiService = "", stPaymentType = "Cash", stPaymentStatus = "4", stRideType, stServiceID, stPriceID, pick_date, stCardID = "", stZoneID = "",
            est_duration = "", est_total_distance = "", max_offer_amount = "", paytm_offer = "";

    int twoseatprice, price;
    Double Amount, Discount_Amount;

    String book_seat = "1", stIsSubscription;
    private ArrayList<LatLng> points = new ArrayList<>();
    private List<Polyline> polylines = new ArrayList<Polyline>();

    private GoogleMap mGoogleMap;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private SupportMapFragment mSupportMapFragment;

    Dialog dialogFindDriver;
    CountDownTimer cT;

    Boolean isRentalRide = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_step2);
        ButterKnife.bind(this);
        setViews();
    }

    @Override
    protected void onResume() {
        super.onResume();

        /* This call Receive after user_request_accept */
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver_UserRideRequest,
                new IntentFilter("User_Ride_Request"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver_UserRideRequest);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    /**
     * Refresh User Ride List Service
     */
    private BroadcastReceiver mMessageReceiver_UserRideRequest = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (cT != null)
                    cT.cancel();
                if (dialogFindDriver != null) {
                    dialogFindDriver.dismiss();
                }
                FinishActivty();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void FinishActivty() {
        Intent intent1 = new Intent();
        intent1.putExtra("MESSAGE", "finish");
        setResult(BACK_TO_ACTIVITY, intent1);
        finish();
    }

    private void setViews() {
        mSupportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map);
        mSupportMapFragment.getMapAsync(this);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            mGoogleApiClient.connect();
        }

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(Constant.UPDATE_INTERVAL)
                .setFastestInterval(Constant.FASTEST_INTERVAL)
                .setSmallestDisplacement(10);

        if (getIntent().hasExtra("stServiceID") && getIntent().hasExtra("isRentalRide")) {

            isRentalRide = true;

            Intent intent = getIntent();
            _txt_dropto.setText(intent.getStringExtra("dropto"));
            _txt_pickup.setText(intent.getStringExtra("pickup"));

            _txt_estimated_price.setText("₹ " + intent.getStringExtra("price"));

            _txt_dis_estimated_price.setText("");

            _txt_estimated_time.setText(intent.getStringExtra("stServiceName") + ", " + intent.getStringExtra("time") + " away");

            mPickUpLat = Double.parseDouble(intent.getStringExtra("mPickUpLat"));
            mPickUpLng = Double.parseDouble(intent.getStringExtra("mPickUpLng"));

            isTexiService = intent.getStringExtra("isTexiService");
            stRideType = intent.getStringExtra("stRideType");
            stServiceID = intent.getStringExtra("stServiceID");
            stPriceID = intent.getStringExtra("stPriceID");
            pick_date = intent.getStringExtra("pick_date");

            price = Integer.parseInt(intent.getStringExtra("price"));

            max_offer_amount = intent.getStringExtra("max_offer_amount");
            paytm_offer = intent.getStringExtra("paytm_offer");
            stZoneID = intent.getStringExtra("stZoneID");

            _lyseat.setVisibility(View.GONE);
            lydropto.setVisibility(View.GONE);

        } else {
            Intent intent = getIntent();
            _txt_dropto.setText(intent.getStringExtra("dropto"));
            _txt_pickup.setText(intent.getStringExtra("pickup"));

            _txt_estimated_price.setText("₹ " + intent.getStringExtra("price"));

            _txt_dis_estimated_price.setText("");

            _txt_estimated_time.setText(intent.getStringExtra("stServiceName") + ", " + intent.getStringExtra("time") + " away");

            mPickUpLat = Double.parseDouble(intent.getStringExtra("mPickUpLat"));
            mPickUpLng = Double.parseDouble(intent.getStringExtra("mPickUpLng"));
            mDropLat = Double.parseDouble(intent.getStringExtra("mDropLat"));
            mDropLng = Double.parseDouble(intent.getStringExtra("mDropLng"));
            isTexiService = intent.getStringExtra("isTexiService");
            stRideType = intent.getStringExtra("stRideType");
            stServiceID = intent.getStringExtra("stServiceID");
            stPriceID = intent.getStringExtra("stPriceID");
            pick_date = intent.getStringExtra("pick_date");

            twoseatprice = Integer.parseInt(intent.getStringExtra("2seatprice"));
            price = Integer.parseInt(intent.getStringExtra("price"));


            est_duration = intent.getStringExtra("est_duration");
            est_total_distance = intent.getStringExtra("est_total_distance");
            max_offer_amount = intent.getStringExtra("max_offer_amount");
            paytm_offer = intent.getStringExtra("paytm_offer");
            stZoneID = intent.getStringExtra("stZoneID");

            _txt_seat1price.setText("₹ " + price);
            _txt_seat2price.setText("₹ " + twoseatprice + "");

            if (isTexiService.equals("2")) {
                _lyseat.setVisibility(View.VISIBLE);
            } else {
                _lyseat.setVisibility(View.GONE);
            }
        }

        _imgback.setOnClickListener(this);
        _btnbooking.setOnClickListener(this);
        _lypayment.setOnClickListener(this);
        _imgfab.setOnClickListener(this);
        _txt_ride_term.setOnClickListener(this);

        _txt_set_payment.setText(stPaymentType); // Default Payment type is Cash
        setPrice();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnbooking:
                UserBookingRide();
                break;

            case R.id.imgback:
                Intent intent = new Intent();
                intent.putExtra("MESSAGE", "");
                setResult(BACK_TO_ACTIVITY, intent);
                finish();
                break;

            case R.id.lypayment:
                showPaymentSelectionDialog();
                break;

            case R.id.imgfab:
                try {
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mUserCurrentLat, mUserCurrentLng)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.txt_ride_term:

                Intent intent_term = new Intent(getApplicationContext(), MonthlySpendingTermsActivity.class);
                if (isRentalRide) {
                    intent_term.putExtra("term_type", "5");
                } else {
                    intent_term.putExtra("term_type", "3");
                }
                startActivity(intent_term);

                break;

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
        mGoogleMap.getUiSettings().setCompassEnabled(false);
        mGoogleMap.getUiSettings().setTiltGesturesEnabled(false);
        mGoogleMap.setBuildingsEnabled(false);

        //Disable Marker click event
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });

        enableMyLocation();
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mGoogleMap != null) {
            // Access to the location has been granted to the app.
            mGoogleMap.setMyLocationEnabled(true);
        }

        // set pick up and drop to marker
        setMarkerOnMap();

    }

    // set pick up and drop to marker
    private void setMarkerOnMap() {

        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mPickUpLat, mPickUpLng)).draggable(false).title("pick up")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_up_pin)));

        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(mPickUpLat, mPickUpLng)).zoom(14.0f).build();
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        if (!isRentalRide) {

            mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mDropLat, mDropLng)).draggable(false).title("drop to")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_pin)));

            try {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(new LatLng(mPickUpLat, mPickUpLng));
                builder.include(new LatLng(mDropLat, mDropLng));
                LatLngBounds bounds = builder.build();
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 150));


                LatLng pickup = new LatLng(mPickUpLat, mPickUpLng);
                LatLng dest = new LatLng(mDropLat, mDropLng);

                String url = getUrl(pickup, dest);
                FetchUrl FetchUrl = new FetchUrl();
                // Start downloading json data from Google Directions API
                FetchUrl.execute(url);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /*
    * Called by Location Services when the request to connect the
    * client finishes successfully. At this point, you can
    * request the current location or start periodic updates
    */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
                return;
            }
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            handleNewLocation(location);
        }
    }

    private void handleNewLocation(Location location) {
        if (mLocationRequest != null) {
            try {
                mUserCurrentLat = location.getLatitude();
                mUserCurrentLng = location.getLongitude();
                location.setAccuracy(10);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            ShowToast(getResources().getString(R.string.network_disconnect));
        } else if (i == CAUSE_NETWORK_LOST) {
            ShowToast(getResources().getString(R.string.network_lost));
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    /**
     * User Booking Ride
     */
    private void UserBookingRide() {

        if (_txt_set_payment.getText().toString().equalsIgnoreCase("SET PAYMENT")) {
            ShowToast(getResources().getString(R.string.st_select_payment_option));
        } else {

            if (isInternetOn(getApplicationContext())) {

                progressDialog.show();
                Call<JsonObject> call = apiService.user_booking(preferencesUtility.getuser_id(),
                        stServiceID, stPriceID, String.valueOf(mPickUpLat),
                        String.valueOf(mPickUpLng), _txt_pickup.getText().toString(),
                        String.valueOf(mDropLat), String.valueOf(mDropLng),
                        _txt_dropto.getText().toString(), pick_date,
                        stPaymentStatus, stRideType, stCardID, book_seat, est_total_distance, est_duration,
                        Math.round(Amount) + "", Math.round(Discount_Amount) + "", stIsSubscription);

                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject objBookingData = new JSONObject(response.body().toString());


                            JSONArray jsonArray = objBookingData.getJSONArray("payload");

                            if (jsonArray.length() >= 1) {

                                JSONObject obj = jsonArray.getJSONObject(0);

                                if (objBookingData.getString("status").equalsIgnoreCase("1")) {
                                    showAlertDialogForFindingDriver(getResources().getString(R.string.st_ride_booked), objBookingData.getString("message"), "1", obj.getString("booking_id"), obj.getString("booking_status"));
                                } else if (objBookingData.getString("status").equalsIgnoreCase("2")) {
//                                showAlertDialog(getResources().getString(R.string.st_no_driver), objBookingData.getString("message"), "2");
                                    showAlertDialogForFindingDriver(getResources().getString(R.string.st_no_driver), objBookingData.getString("message"), "2", obj.getString("booking_id"), obj.getString("booking_status"));
                                } else if (objBookingData.getString("status").equalsIgnoreCase("0")) {
                                    showAlertDialog(getResources().getString(R.string.st_booking_failed), objBookingData.getString("message"), "0");
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        progressDialog.dismiss();
                    }
                });
            } else {
                toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
            }
        }
    }

    public Dialog showAlertDialog(String title, String message, final String status) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_popup);
        dialog.setTitle("");
        dialog.setCancelable(false);
        TextView txt_popup_header = (TextView) dialog.findViewById(R.id.txt_popup_header);
        TextView txt_popup_message = (TextView) dialog.findViewById(R.id.txt_popup_message);
        TextView btn_ok = (TextView) dialog.findViewById(R.id.btn_ok);

        txt_popup_header.setText(title);
        txt_popup_message.setText(message);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("0")) {
                    dialog.dismiss();
                } else if (status.equalsIgnoreCase("1")) {
                    dialog.dismiss();
                    Intent intent = new Intent();
                    intent.putExtra("MESSAGE", "finish");
                    setResult(BACK_TO_ACTIVITY, intent);
                    finish();
                } else if (status.equalsIgnoreCase("2")) {
                    dialog.dismiss();
                    Intent intent = new Intent();
                    intent.putExtra("MESSAGE", "finish");
                    setResult(BACK_TO_ACTIVITY, intent);
                    finish();
                }
            }
        });

        dialog.show();
        return null;
    }

    public String checkDigit(int number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }

    public Dialog showAlertDialogForFindingDriver(String title, String message, final String status, final String stBookingID, final String stBooking_Status) {

        // This is for Get get height and width of Screen
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        try {
            display.getRealSize(size);
        } catch (NoSuchMethodError err) {
            display.getSize(size);
        }
        final int width = size.x;
        int height = size.y;

        dialogFindDriver = new Dialog(this);
        dialogFindDriver.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogFindDriver.setContentView(R.layout.layout_dialog_popup_finding_driver);
        dialogFindDriver.setTitle("");
        dialogFindDriver.setCancelable(false);
        if (dialogFindDriver != null) {
            try {
                dialogFindDriver.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        final TextView txt_counter = (TextView) dialogFindDriver.findViewById(R.id.txt_counter);
        final TextView txt_popup_message = (TextView) dialogFindDriver.findViewById(R.id.txt_popup_message);
        TextView txt_cancel = (TextView) dialogFindDriver.findViewById(R.id.txt_cancel);

        LinearLayout mainLayout = (LinearLayout) dialogFindDriver.findViewById(R.id.mainLayout);
        mainLayout.setMinimumWidth((int) (width / 0.7));

        ProgressBar progressBar = (ProgressBar) dialogFindDriver.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        final int[] time = {90};

        cT = new CountDownTimer(90000, 1000) {
            public void onTick(long millisUntilFinished) {
                txt_popup_message.setText(getResources().getString(R.string.st_finding_driver_msg) + " " + checkDigit(time[0]));
                time[0]--;
            }

            public void onFinish() {

                try {

                    final Dialog onfinishDialog = new Dialog(BookingActivityStep2Activity.this);
                    onfinishDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    onfinishDialog.setContentView(R.layout.layout_dialog_ontimer_finish);
                    onfinishDialog.show();

                    final Button btn_ok = (Button) onfinishDialog.findViewById(R.id.btn_ok);

                    btn_ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            onfinishDialog.dismiss();

                            if (dialogFindDriver != null) {
                                dialogFindDriver.dismiss();
                            }
                            FinishActivty();
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        cT.start();

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog onRideCancelDialog = new Dialog(BookingActivityStep2Activity.this);
                onRideCancelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                onRideCancelDialog.setContentView(R.layout.layout_dialog_ride_cancel);

                onRideCancelDialog.show();
                final Button btn_yes = (Button) onRideCancelDialog.findViewById(R.id.btn_yes);
                final Button btn_no = (Button) onRideCancelDialog.findViewById(R.id.btn_no);

                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isInternetOn(getApplicationContext())) {

                            try {
                                progressDialog.show();
                                Call<JsonObject> call = apiService.user_ride_force_cancel(stBookingID, preferencesUtility.getuser_id());
                                call.enqueue(new Callback<JsonObject>() {
                                    @Override
                                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                        String mResult = response.body().toString();
                                        progressDialog.dismiss();
                                        try {
                                            JSONObject objCancelData = new JSONObject(mResult);
                                            if (objCancelData.getString("status").equalsIgnoreCase("1")) {
                                                toastDialog.ShowToastMessage(objCancelData.getString("message"));

                                                onRideCancelDialog.dismiss();

                                                if (dialogFindDriver != null) {
                                                    dialogFindDriver.dismiss();
                                                }
                                                if (cT != null) {
                                                    cT.cancel();
                                                }

                                                FinishActivty();
                                            } else {
                                                toastDialog.ShowToastMessage(objCancelData.getString("message"));
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<JsonObject> call, Throwable t) {
                                        t.printStackTrace();
                                        progressDialog.dismiss();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                        }
                    }
                });

                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onRideCancelDialog.dismiss();
                    }
                });
            }
        });
        return null;
    }

    /**
     * Select Payment Option Dialog
     *
     * @return
     */
    public Dialog showPaymentSelectionDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_payment_option_dialog);
        dialog.setTitle("");
        dialog.setCancelable(false);

        ListView list_card = (ListView) dialog.findViewById(R.id.listview_cards);
        list_card.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        Button btn_add_card = (Button) dialog.findViewById(R.id.btn_add_card);
        AppCompatTextView txt_done = (AppCompatTextView) dialog.findViewById(R.id.txt_done);
        AppCompatTextView txt_select_payment = (AppCompatTextView) dialog.findViewById(R.id.txt_select_payment);
        final AppCompatTextView txt_pay_by_paypal = (AppCompatTextView) dialog.findViewById(R.id.txt_pay_by_paypal);
        final AppCompatTextView txt_pay_by_paytm = (AppCompatTextView) dialog.findViewById(R.id.txt_pay_by_paytm);
        final AppCompatTextView txt_pay_by_cash = (AppCompatTextView) dialog.findViewById(R.id.txt_pay_by_cash);
        final AppCompatCheckBox chkbox_pay_by_reward = (AppCompatCheckBox) dialog.findViewById(R.id.chkbox_pay_by_reward);
        final AppCompatCheckBox chkbox_pay_by_Paytm = (AppCompatCheckBox) dialog.findViewById(R.id.chkbox_pay_by_Paytm);
        final AppCompatCheckBox chkbox_pay_by_OhiPayment = (AppCompatCheckBox) dialog.findViewById(R.id.chkbox_pay_by_OhiPayment);
        final AppCompatCheckBox chkbox_pay_by_PayUmoneyWithRewards = (AppCompatCheckBox) dialog.findViewById(R.id.chkbox_pay_by_PayUmoneyWithRewards);
        final AppCompatCheckBox chkbox_pay_by_Crash = (AppCompatCheckBox) dialog.findViewById(R.id.chkbox_pay_by_Crash);

        final AppCompatTextView txt_msg = (AppCompatTextView) dialog.findViewById(R.id.txt_msg);
        final AppCompatTextView txt_terms = (AppCompatTextView) dialog.findViewById(R.id.txt_terms);
        final LinearLayout layout_paytm = (LinearLayout) dialog.findViewById(R.id.layout_paytm);

        chkbox_pay_by_OhiPayment.setTypeface(global_typeface.Sansation_Regular());
        chkbox_pay_by_reward.setTypeface(global_typeface.Sansation_Regular());
        txt_select_payment.setTypeface(global_typeface.Sansation_Regular());
        txt_done.setTypeface(global_typeface.Sansation_Regular());
        btn_add_card.setTypeface(global_typeface.Sansation_Regular());
        txt_terms.setTypeface(global_typeface.Sansation_Regular());

        txt_msg.setText("Get " + paytm_offer + "% instant discount upto Rs. " + max_offer_amount + " per ride");

        txt_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(BookingActivityStep2Activity.this, PaytmTermsActivity.class);
                intent.putExtra("stZoneID", stZoneID);
                startActivity(intent);

            }
        });

        layout_paytm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stPaymentType = "Paytm";
                stPaymentStatus = "0";

                setPrice();

                _txt_set_payment.setText(stPaymentType);
                layout_paytm.setBackgroundColor(getResources().getColor(R.color.colorEditBackground));
                txt_pay_by_cash.setBackgroundColor(getResources().getColor(R.color.colorwhite));

                dialog.dismiss();
            }
        });

        txt_pay_by_cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stPaymentType = "Cash";
                stPaymentStatus = "4";

                setPrice();

                _txt_set_payment.setText(stPaymentType);
                txt_pay_by_cash.setBackgroundColor(getResources().getColor(R.color.colorEditBackground));
                layout_paytm.setBackgroundColor(getResources().getColor(R.color.colorwhite));

                dialog.dismiss();
            }
        });

        chkbox_pay_by_Paytm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                chkbox_pay_by_Paytm.setChecked(b);
                chkbox_pay_by_Crash.setChecked(false);
            }
        });

        chkbox_pay_by_Crash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                chkbox_pay_by_Crash.setChecked(b);
                chkbox_pay_by_Paytm.setChecked(false);
            }
        });

        txt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chkbox_pay_by_OhiPayment.isChecked()) {
                    if (stPaymentType.equalsIgnoreCase("Cash payment")) {
                        _txt_set_payment.setText(stPaymentType);

                    } else {
                        _txt_set_payment.setText("wallet & " + stPaymentType);
                    }
                }

                if (!stPaymentType.equalsIgnoreCase("")) {
                    dialog.dismiss();
                } else {
                    toastDialog.ShowToastMessage(getResources().getString(R.string.st_select_payment_option));
                }
            }
        });

        chkbox_pay_by_reward.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                } else {

                }
            }
        });


        dialog.show();
        return null;
    }

    private void setPrice() {
        if (book_seat.equals("1")) {

            // Paytm Payment
            if (stPaymentStatus.equals("0")) {

                Discount_Amount = (double) (price * Double.parseDouble(paytm_offer) / 100);
                if (Discount_Amount > Double.parseDouble(max_offer_amount)) {
                    Discount_Amount = Double.parseDouble(max_offer_amount);
                }

                Amount = (price - Discount_Amount);
                _txt_estimated_price.setText(" ₹ " + Math.round(Amount));

                _txt_dis_estimated_price.setPaintFlags(_txt_dis_estimated_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                _txt_dis_estimated_price.setText(" ₹ " + price);

                // Cash Payment
            } else if (stPaymentStatus.equals("4")) {

                _txt_estimated_price.setText("₹ " + price);
                _txt_dis_estimated_price.setText("");

                Amount = Double.valueOf(price);
                Discount_Amount = 0.0;
            }

            // This condition work for Share Ride
        } else if (book_seat.equals("2")) {

            // Paytm Payment
            if (stPaymentStatus.equals("0")) {

                Discount_Amount = (double) (twoseatprice * Double.parseDouble(paytm_offer) / 100);
                if (Discount_Amount > Double.parseDouble(max_offer_amount)) {
                    Discount_Amount = Double.parseDouble(max_offer_amount);
                }

                Amount = (twoseatprice - Discount_Amount);
                _txt_estimated_price.setText(" ₹ " + Math.round(Amount));

                _txt_dis_estimated_price.setPaintFlags(_txt_dis_estimated_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                _txt_dis_estimated_price.setText(" ₹ " + twoseatprice);


                // Cash Payment
            } else if (stPaymentStatus.equals("4")) {

                _txt_estimated_price.setText("₹ " + twoseatprice);
                _txt_dis_estimated_price.setText("");

                Amount = Double.valueOf(twoseatprice);
                Discount_Amount = 0.0;
            }
        }

        if (isInternetOn(getApplicationContext())) {
            checkSubscriptionEligible();
        }
    }

    /**
     * To check user is eligible for subscription
     *
     */
    private void checkSubscriptionEligible() {

        try {
            progressDialog.show();
            Call<JsonObject> call = apiService.check_subscription_eligible(preferencesUtility.getuser_id(),
                    preferencesUtility.getauth_token(), stPriceID, Math.round(Amount) + "", Math.round(Discount_Amount) + "");

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        progressDialog.dismiss();

                        JSONObject jsonObjectDetails = new JSONObject(response.body().toString());

                        if (jsonObjectDetails.optString("status").equalsIgnoreCase("1")) {

                            stIsSubscription = "1";
                            JSONObject jsonObjectPayload = jsonObjectDetails.getJSONObject("payload");

                            _txt_estimated_price.setText("Rs. " + jsonObjectPayload.optString("new_user_payable_amount")); // Price after discount

                            txtTotalAmount.setText("Rs. " + Math.round(price) + ""); // Amount
                            txtPaytmDiscount.setText("Paytm Discount(" + paytm_offer + "%):"); //Paytm Discount
                            txtPaytmDiscountAmount.setText("Rs. " + Math.round(Discount_Amount) + ""); //Paytm Discount Amount
                            txtPaytmDiscountTotal.setText("Rs. " + Math.round(Amount) + ""); //Paytm Total Amount after discount

                            txtSubscriptionDiscount.setText("Subscription Discount(" + jsonObjectPayload.optString("plan_discount_per") + "%):"); // Subscription Discount
                            txtSubscriptionDiscountAmount.setText("Rs. " + jsonObjectPayload.optString("subscription_discount_amount")); // Subscription Amount

                            txtFinalTotal.setText("Rs. " + jsonObjectPayload.optString("new_user_payable_amount")); // Final Amount
                        } else {

                            stIsSubscription = "0";
                            JSONObject jsonObjectPayload = jsonObjectDetails.getJSONObject("payload");
                            _txt_estimated_price.setText("Rs. " + jsonObjectPayload.optString("new_user_payable_amount")); // Price after discount

                            txtTotalAmount.setText("Rs. " + Math.round(price) + ""); // Amount
                            txtPaytmDiscount.setText("Paytm Discount(" + paytm_offer + "%):"); //Paytm Discount
                            txtPaytmDiscountAmount.setText("Rs. " + Math.round(Discount_Amount) + ""); //Paytm Discount Amount
                            txtPaytmDiscountTotal.setText("Rs. " + Math.round(Amount) + ""); //Paytm Total Amount after discount

                            txtSubscriptionDiscount.setText("Subscription Discount(0%):"); // Subscription Discount
                            txtSubscriptionDiscountAmount.setText("Rs. 0"); // Subscription Amount

                            txtFinalTotal.setText("Rs. " + jsonObjectPayload.optString("new_user_payable_amount")); // Final Amount
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            progressDialog.dismiss();
        }
    }

    private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        String mode = "mode=driving";

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode + "&key=AIzaSyAfye39JZYcYdS196GZrvxRBfMShOCaA3w";

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
//            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;
            points.clear();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.BLUE);

                Log.d("onPostExecute", "onPostExecute lineoptions decoded");

            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
//                mGoogleMap.addPolyline(lineOptions);
                polylines.add(mGoogleMap.addPolyline(lineOptions));
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }
    }

    @OnClick(R.id.lyseat)
    public void seatOnclick() {
        _relative_seats.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.linear_seat1)
    public void onclickseat1() {
        _relative_seats.setVisibility(View.GONE);
        _txt_seat.setText("1  Seat");
        book_seat = "1";
        _txt_estimated_price.setText(_txt_seat1price.getText().toString());

        setPrice();
    }

    @OnClick(R.id.linear_seat2)
    public void onclickseat2() {
        _relative_seats.setVisibility(View.GONE);
        _txt_seat.setText("2  Seat");
        book_seat = "2";
        _txt_estimated_price.setText(_txt_seat2price.getText().toString());

        setPrice();
    }
}
