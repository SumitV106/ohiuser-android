package com.ohicabs.user.ohi.Adpater;

import android.app.Activity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.PojoModel.OfferModel;
import com.ohicabs.user.ohi.Utils.Global_Typeface;

import java.util.ArrayList;


public class OfferListAdapter extends RecyclerView.Adapter<OfferListAdapter.MyViewHolder> implements Filterable {

    private Activity activity;
    int ResourceId;
    private ArrayList<OfferModel>  _arr_userBookedRides;
    private ArrayList<OfferModel>  _arr_userBookedRides_Serch;
    private Global_Typeface global_typeface;
    private BtnClickListener mClickListener = null;

    public OfferListAdapter(Activity act, int resId, ArrayList<OfferModel>  hashMapArrayList, BtnClickListener listener) {
        this.activity = act;
        this.ResourceId = resId;
        this._arr_userBookedRides = hashMapArrayList;
        this._arr_userBookedRides_Serch = hashMapArrayList;
        mClickListener = listener;
        global_typeface = new Global_Typeface(activity);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView txt_city, txt_offer;
        CardView card_view;

        public MyViewHolder(View view) {
            super(view);
            txt_city = (AppCompatTextView) view.findViewById(R.id.txt_city);
            txt_offer = (AppCompatTextView) view.findViewById(R.id.txt_offer);

            card_view = (CardView) view.findViewById(R.id.card_view);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_offer_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.txt_city.setText(_arr_userBookedRides_Serch.get(position).getZone_name());
        holder.txt_offer.setText(_arr_userBookedRides_Serch.get(position).getOffer_title());

        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mClickListener!=null){
                    mClickListener.onBtnClick(position,_arr_userBookedRides_Serch.get(position).getZone_name(),
                            _arr_userBookedRides_Serch.get(position).getOffer_title(),
                            _arr_userBookedRides_Serch.get(position).getDescription(),
                            _arr_userBookedRides_Serch.get(position).getImage());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return _arr_userBookedRides_Serch.size();
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    _arr_userBookedRides_Serch = _arr_userBookedRides;
                } else {
                    ArrayList<OfferModel> filteredList = new ArrayList<>();
                    for (OfferModel row : _arr_userBookedRides) {

                        if (row.getZone_name().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                            Log.e("row",charString+"--"+row.getZone_name()+"--"+row.getOffer_id());

                        }
                    }

                    _arr_userBookedRides_Serch = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = _arr_userBookedRides_Serch;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                _arr_userBookedRides_Serch = (ArrayList<OfferModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface BtnClickListener {
        void onBtnClick(int position, String zonename, String offer_title,String description,String image);
    }

}
