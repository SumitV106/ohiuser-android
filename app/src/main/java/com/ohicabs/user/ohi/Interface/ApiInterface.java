package com.ohicabs.user.ohi.Interface;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by crayon on 15/09/17.
 */

public interface ApiInterface {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/x-www-form-urlencoded"
    })
    @FormUrlEncoded
    @POST("api/users/register")
    Call<JsonObject> user_signup(@Field("user") String userData);

    @POST("api/zone_list")
    Call<JsonObject> getZoneList();


    // Login Module //
    @FormUrlEncoded
    @POST("api/register")
    Call<JsonObject> user_register(@Field("firstname") String firstname,
                                   @Field("lastname") String lastname,
                                   @Field("email") String email,
                                   @Field("gender") String gender,
                                   @Field("mobile") String mobile,
                                   @Field("password") String password,
                                   @Field("user_type") String user_type,
                                   @Field("device_token") String device_token,
                                   @Field("device_source") String device_source,
                                   @Field("refer_code") String refer_code,
                                   @Field("socket_id") String socket_id,
                                   @Field("zone_id") String zone_id);


    @FormUrlEncoded
    @POST("api/login")
    Call<JsonObject> user_login(@Field("device_token") String device_token,
                                @Field("device_source") String device_source,
                                @Field("email_or_mobile") String email_or_mobile,
                                @Field("password") String password,
                                @Field("user_type") String user_type,
                                @Field("socket_id") String socket_id);

    @FormUrlEncoded
    @POST("api/social_login")
    Call<JsonObject> user_truecaller_login(@Field("name") String name,
                                           @Field("email") String email,
                                           @Field("mobile") String mobile,
                                           @Field("user_type") String user_type,
                                           @Field("device_source") String device_source,
                                           @Field("device_token") String device_token,
                                           @Field("socket_id") String socket_id);

    @FormUrlEncoded
    @POST("api/social_new_user")
    Call<JsonObject> user_truecaller_new_login(@Field("name") String name,
                                               @Field("email") String email,
                                               @Field("mobile") String mobile,
                                               @Field("user_type") String user_type,
                                               @Field("password") String password,
                                               @Field("device_source") String device_source,
                                               @Field("device_token") String device_token,
                                               @Field("socket_id") String socket_id,
                                               @Field("zone_id") String zone_id,
                                               @Field("refer_code") String refer_code);

    @FormUrlEncoded
    @POST("api/forgot_password")
    Call<JsonObject> user_forgot_password(@Field("user_type") String user_type,
                                          @Field("email") String email);

    @FormUrlEncoded
    @POST("api/invite")
    Call<JsonObject> invite(@Field("email") String email,
                            @Field("name") String name,
                            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("api/referral")
    Call<JsonObject> referral(@Field("user_id") String user_id,
                              @Field("refer_code") String refer_code);

    @FormUrlEncoded
    @POST("api/change_password")
    Call<JsonObject> change_password(@Field("user_id") String user_id,
                                     @Field("old_password") String old_password,
                                     @Field("new_password") String new_password);

    @FormUrlEncoded
    @POST("api/forgot_new_password")
    Call<JsonObject> forgot_new_password(@Field("user_id") String user_id,
                                         @Field("forgot_code") String forgot_code,
                                         @Field("new_password") String new_password);

    @FormUrlEncoded
    @POST("api/profile_update")
    Call<JsonObject> profile_update(@Field("user_id") String user_id,
                                    @Field("gender") String gender,
                                    @Field("firstname") String firstname,
                                    @Field("lastname") String lastname,
                                    @Field("user_type") String user_type,
                                    @Field("zone_id") String zone_id,
                                    @Field("mobile") String mobile);


    @FormUrlEncoded
    @POST("api/resend_email_signup")
    Call<JsonObject> resend_email_signup(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("api/user_logout")
    Call<JsonObject> logout(@Field("user_id") String user_id,
                            @Field("auth_token") String auth_token);

    /// Service Module

    @FormUrlEncoded
    @POST("api/server_last_data")
    Call<JsonObject> server_last_data(@Field("last_datetime") String last_datetime);

    @FormUrlEncoded
    @POST("api/booking")
    Call<JsonObject> user_booking(@Field("user_id") String user_id,
                                  @Field("service_id") String service_id,
                                  @Field("price_id") String price_id,
                                  @Field("pickup_latitude") String pickup_latitude,
                                  @Field("pickup_longitude") String pickup_longitude,
                                  @Field("pickup_address") String pickup_address,
                                  @Field("drop_latitude") String drop_latitude,
                                  @Field("drop_longitude") String drop_longitude,
                                  @Field("drop_address") String drop_address,
                                  @Field("pickup_date") String pickup_date,
                                  @Field("payment_type") String payment_type,
                                  @Field("is_now") String is_now,
                                  @Field("card_id") String card_id,
                                  @Field("book_seat") String book_seat,
                                  @Field("est_total_distance") String est_total_distance,
                                  @Field("est_duration") String est_duration,
                                  @Field("amount") String amount,
                                  @Field("discount_amount") String discount_amount,
                                  @Field("is_subscription") String is_subscription);

    @FormUrlEncoded
    @POST("api/booking_outstation")
    Call<JsonObject> booking_outstation(@Field("user_id") String user_id,
                                        @Field("service_id") String service_id,
                                        @Field("access_token") String access_token,
                                        @Field("pickup_latitude") String pickup_latitude,
                                        @Field("pickup_longitude") String pickup_longitude,
                                        @Field("pickup_address") String pickup_address,
                                        @Field("drop_latitude") String drop_latitude,
                                        @Field("drop_longitude") String drop_longitude,
                                        @Field("drop_address") String drop_address,
                                        @Field("pickup_date") String pickup_date,
                                        @Field("payment_type") String payment_type,
                                        @Field("price_id") String price_id,
                                        @Field("est_total_distance") String est_total_distance,
                                        @Field("est_duration") String est_duration,
                                        @Field("amount") String amount,
                                        @Field("is_subscription") String is_subscription);

    @FormUrlEncoded
    @POST("api/user_save_card_list")
    Call<JsonObject> user_save_card_list(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("api/user_save_card")
    Call<JsonObject> user_save_card(@Field("user_id") String user_id,
                                    @Field("data") String data,
                                    @Field("card_type") String card_type);

    @FormUrlEncoded
    @POST("api/user_save_card_delete")
    Call<JsonObject> user_save_card_delete(@Field("user_id") String user_id,
                                           @Field("card_id") String card_id);

    // Booking Module

    @FormUrlEncoded
    @POST("api/booking_detail")
    Call<JsonObject> booking_detail(@Field("booking_id") String booking_id,
                                    @Field("user_type") String user_type);

    @FormUrlEncoded
    @POST("api/user_ride_cancel")
    Call<JsonObject> user_ride_cancel(@Field("booking_id") String booking_id,
                                      @Field("user_id") String user_id,
                                      @Field("booking_status") String booking_status);

    @FormUrlEncoded
    @POST("api/user_ride_force_cancel")
    Call<JsonObject> user_ride_force_cancel(@Field("booking_id") String booking_id,
                                            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("api/suggestion")
    Call<JsonObject> suggestion(@Field("email") String email,
                                @Field("title") String title,
                                @Field("message") String message);

    @FormUrlEncoded
    @POST("api/wallet")
    Call<JsonObject> wallet(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("api/tracking")
    Call<JsonObject> tracking(@Field("user_id") String user_id,
                              @Field("track_user_id") String track_user_id,
                              @Field("booking_id") String booking_id,
                              @Field("booking_status") String booking_status,
                              @Field("user_type") String user_type);

    @FormUrlEncoded
    @POST("api/ride_stop_tip")
    Call<JsonObject> ride_stop_tip(@Field("booking_id") String booking_id,
                                   @Field("user_id") String user_id,
                                   @Field("tip_amount") String tip_amount,
                                   @Field("paytm_transaction_id") String paytm_transaction_id,
                                   @Field("is_toll_accept") String is_toll_accept,
                                   @Field("success_payload") String success_payload,
                                   @Field("order_id") String order_id);
//    @Field("tip_request_token") String tip_request_token,

    @FormUrlEncoded
    @POST("api/ride_rating")
    Call<JsonObject> ride_rating(@Field("user_id") String user_id,
                                 @Field("booking_id") String booking_id,
                                 @Field("user_type") String user_type,
                                 @Field("rating") String rating,
                                 @Field("comment") String comment);


    //Support Module

    @FormUrlEncoded
    @POST("api/support_connect")
    Call<JsonObject> support_connect(@Field("user_id") String user_id,
                                     @Field("send_id") String send_id,
                                     @Field("socket_id") String socket_id);

    @FormUrlEncoded
    @POST("api/support_message_clear")
    Call<JsonObject> support_message_clear(@Field("user_id") String user_id,
                                           @Field("send_id") String send_id);

    @FormUrlEncoded
    @POST("api/support_message")
    Call<JsonObject> support_message(@Field("user_id") String user_id,
                                     @Field("send_id") String send_id,
                                     @Field("message") String message,
                                     @Field("user_type") String user_type,
                                     @Field("socket_id") String socket_id);

    // Ride List Module
    @FormUrlEncoded
    @POST("api/user_all_ride_list")
    Call<JsonObject> user_all_ride_list(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("api/user_current_booking_list")
    Call<JsonObject> user_current_booking_list(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("api/user_promo")
    Call<JsonObject> applyPromoCode(@Field("user_id") String user_id,
                                    @Field("car_number") String car_number);


    @FormUrlEncoded
    @POST("api/UpdateSocket")
    Call<JsonObject> UpdateSocket(@Field("user_id") String user_id,
                                  @Field("auth_token") String auth_token);

    @FormUrlEncoded
    @POST("api/near_by_driver")
    Call<JsonObject> near_by_driver(@Field("latitude") String latitude,
                                    @Field("longitude") String longitude,
                                    @Field("service_id") String service_id);

    @FormUrlEncoded
    @POST("api/otp_verify")
    Call<JsonObject> otp_verify(@Field("user_id") String user_id,
                                @Field("otp_code") String otp_code,
                                @Field("user_type") String user_type);

    @FormUrlEncoded
    @POST("api/resend_otp")
    Call<JsonObject> otp_resend(@Field("user_id") String user_id,
                                @Field("user_type") String user_type);

    @FormUrlEncoded
    @POST("api/social_login")
    Call<JsonObject> social_login(@Field("device_token") String device_token,
                                  @Field("device_source") String device_source,
                                  @Field("email") String email,
                                  @Field("social_id") String social_id,
                                  @Field("social_type") String social_type,
                                  @Field("name") String name,
                                  @Field("socket_id") String socket_id);

    @FormUrlEncoded
    @POST("api/social_new_user")
    Call<JsonObject> social_new_user(@Field("device_token") String device_token,
                                     @Field("device_source") String device_source,
                                     @Field("email") String email,
                                     @Field("social_id") String social_id,
                                     @Field("social_type") String social_type,
                                     @Field("name") String name,
                                     @Field("mobile") String mobile,
                                     @Field("socket_id") String socket_id);


    @FormUrlEncoded
    @POST("api/generate_checksum")
    Call<JsonObject> generate_checksum(@Field("booking_id") String booking_id,
                                       @Field("user_id") String user_id,
                                       @Field("amount") String amount,
                                       @Field("tip_amount") String tip_amount);


    @FormUrlEncoded
    @POST("api/paytm_payment_fail")
    Call<JsonObject> paytm_payment_fail(@Field("booking_id") String booking_id,
                                        @Field("fail_payload") String fail_payload,
                                        @Field("paytm_transaction_id") String paytm_transaction_id,
                                        @Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("api/user_ride_route")
    Call<JsonObject> user_ride_route(@Field("user_id") String user_id,
                                     @Field("driver_id") String driver_id);

    @FormUrlEncoded
    @POST("api/ride_paytm_payment")
    Call<JsonObject> ride_paytm_payment(@Field("booking_id") String booking_id,
                                        @Field("user_id") String user_id,
                                        @Field("tip_amount") String tip_amount,
                                        @Field("paytm_transaction_id") String paytm_transaction_id,
                                        @Field("success_payload") String success_payload,
                                        @Field("order_id") String order_id,
                                        @Field("is_toll_accept") String is_toll_accept);

    @FormUrlEncoded
    @POST("api/offer_list")
    Call<JsonObject> offer_list();

    @FormUrlEncoded
    @POST("api/user_reward_list")
    Call<JsonObject> user_reward_list(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("api/user_monthly_spend")
    Call<JsonObject> user_monthly_spend(@Field("user_id") String user_id,
                                        @Field("start_date") String start_date,
                                        @Field("end_date") String end_date);

    @FormUrlEncoded
    @POST("api/invite_detail")
    Call<JsonObject> invite_detail(@Field("user_type") String user_type);

    @FormUrlEncoded
    @POST("api/term_and_condition")
    Call<JsonObject> term_and_condition(@Field("term_type") String term_type);

    @FormUrlEncoded
    @POST("api/subscription_plan_list")
    Call<JsonObject> subscriptionPlanList(@Field("user_id") String user_id, @Field("user_type") String user_type);

    @FormUrlEncoded
    @POST("api/buy_user_subscription_check")
    Call<JsonObject> checkUserSubscription(@Field("user_id") String user_id,
                                           @Field("access_token") String access_token,
                                           @Field("plan_id") String plan_id);

    @FormUrlEncoded
    @POST("api/subscription_paytm_payment_fail")
    Call<JsonObject> paytm_subscription_fail(@Field("user_id") String user_id,
                                             @Field("access_token") String access_token,
                                             @Field("sub_buy_id") String sub_buy_id,
                                             @Field("fail_payload") String fail_payload,
                                             @Field("paytm_transaction_id") String paytm_transaction_id,
                                             @Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("api/subscription_paytm_payment_done")
    Call<JsonObject> paytm_subscription_sucess(@Field("user_id") String user_id,
                                               @Field("access_token") String access_token,
                                               @Field("sub_buy_id") String sub_buy_id,
                                               @Field("success_payload") String success_payload,
                                               @Field("paytm_transaction_id") String paytm_transaction_id,
                                               @Field("order_id") String order_id);


    @FormUrlEncoded
    @POST("api/subscription_history")
    Call<JsonObject> subscription_history(@Field("user_id") String user_id,
                                          @Field("access_token") String access_token,
                                          @Field("is_exclusive") String is_exclusive);

    @FormUrlEncoded
    @POST("api/check_subscription_eligible")
    Call<JsonObject> check_subscription_eligible(@Field("user_id") String user_id,
                                                 @Field("access_token") String access_token,
                                                 @Field("price_id") String price_id,
                                                 @Field("amount") String amount,
                                                 @Field("discount_amount") String discount_amount);

    @FormUrlEncoded
    @POST("api/check_subscription_outstation_eligible")
    Call<JsonObject> check_subscription_outstation_eligible(@Field("user_id") String user_id,
                                                 @Field("access_token") String access_token,
                                                 @Field("price_id") String price_id,
                                                 @Field("amount") String amount,
                                                 @Field("payment_type") String payment_type);

    @FormUrlEncoded
    @POST("api/upfront_generate_checksum")
    Call<JsonObject> upfront_generate_checksum(@Field("user_id") String user_id,
                                               @Field("access_token") String access_token,
                                               @Field("booking_id") String booking_id);


    @FormUrlEncoded
    @POST("api/upfront_payment")
    Call<JsonObject> upfront_payment(@Field("user_id") String user_id,
                                     @Field("access_token") String access_token,
                                     @Field("booking_id") String booking_id,
                                     @Field("paytm_transaction_id") String paytm_transaction_id,
                                     @Field("success_payload") String success_payload,
                                     @Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("api/user_location_list")
    Call<JsonObject> user_location_list(@Field("user_id") String user_id,
                                               @Field("access_token") String access_token);

    @FormUrlEncoded
    @POST("api/user_location_add")
    Call<JsonObject> user_location_add(@Field("user_id") String user_id,
                                     @Field("access_token") String access_token,
                                     @Field("tag_name") String tag_name,
                                     @Field("address") String address,
                                     @Field("lati") double lati,
                                     @Field("longi") double longi);

    @FormUrlEncoded
    @POST("api/user_location_delete")
    Call<JsonObject> user_location_delete(@Field("user_id") String user_id,
                                       @Field("access_token") String access_token,
                                       @Field("location_id") String location_id);

    @FormUrlEncoded
    @POST("api/referral_plan_list")
    Call<JsonObject> referral_plan_list(@Field("user_id") String user_id,
                                          @Field("access_token") String access_token);

    @FormUrlEncoded
    @POST("api/referral_history_list")
    Call<JsonObject> referral_history_list(@Field("user_id") String user_id,
                                        @Field("access_token") String access_token);

    @FormUrlEncoded
    @POST("api/referral_redemption")
    Call<JsonObject> referral_redemption(@Field("user_id") String user_id,
                                           @Field("access_token") String access_token);

    @FormUrlEncoded
    @POST("api/buy_user_subscription_on_referral_point")
    Call<JsonObject> buy_user_subscription_on_referral_point(@Field("user_id") String user_id,
                                                             @Field("access_token") String access_token,
                                                             @Field("plan_id") String plan_id);
}