package com.ohicabs.user.ohi.Adpater;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ohicabs.user.ohi.R;

import java.util.ArrayList;
import java.util.HashMap;

public class PriceListAdapter extends RecyclerView.Adapter<PriceListAdapter.MyViewHolder>{

    private Activity mActivity;
    ArrayList<HashMap> arrPriceList;

    private static AppCompatRadioButton lastChecked = null;
    private int lastCheckedPos = 0;
    private int selectedPosition = -1;
    private BtnClickListener mClickListener = null;

    public PriceListAdapter(Activity act, ArrayList<HashMap> arrPrice, BtnClickListener btnClickListener) {
        this.mActivity = act;
        arrPriceList = arrPrice;
        mClickListener = btnClickListener;
    }

    @Override
    public PriceListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_row_outstation_price, parent, false);

        return new PriceListAdapter.MyViewHolder(itemView);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public AppCompatRadioButton rBtnDuration;
        public AppCompatTextView txtRidePrice;

        public MyViewHolder(View view) {
            super(view);

            rBtnDuration = view.findViewById(R.id.rBtnDuration);
            txtRidePrice = view.findViewById(R.id.txt_price);
        }
    }

    @Override
    public void onBindViewHolder(PriceListAdapter.MyViewHolder holder, final int position) {

        holder.rBtnDuration.setText(arrPriceList.get(position).get("rental_time").toString());
        holder.txtRidePrice.setText("₹" + arrPriceList.get(position).get("price"));

        holder.rBtnDuration.setChecked(selectedPosition == position);

        holder.rBtnDuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = holder.getAdapterPosition();
                notifyDataSetChanged();

                if (mClickListener != null) {
                    double eta = Double.parseDouble(arrPriceList.get(position).get("eta").toString());
                    mClickListener.onBtnClick(position, eta, arrPriceList.get(position).get("price_id").toString(),
                            (long) arrPriceList.get(position).get("price"),
                            arrPriceList.get(position).get("paytm_offer").toString(),
                            arrPriceList.get(position).get("max_offer_amount").toString());

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrPriceList.size();
    }

    public interface BtnClickListener {
        void onBtnClick(int position, double time, String priceID, long price, String paytmOfferAmt, String maxPaytmOfferAmt);
    }

}
