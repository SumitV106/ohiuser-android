package com.ohicabs.user.ohi.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRatingBar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.CustomWidgets.CustomBoldFontButton;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontEditText;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontTextView;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReviewRideActivity extends BaseActivity {

    @BindView(R.id.txt_booking_id)
    CustomRegularFontTextView txt_booking_id;

    @BindView(R.id.txt_total_km)
    CustomRegularFontTextView txt_total_km;

    @BindView(R.id.txt_total_time)
    CustomRegularFontTextView txt_total_time;

    @BindView(R.id.txt_total_pay)
    CustomRegularFontTextView txt_toll_pay;

    @BindView(R.id.txt_total_amount)
    CustomRegularFontTextView txt_total_amount;

    @BindView(R.id.radioGroup_tip)
    RadioGroup radioGroup_tip;

    @BindView(R.id.radioGroup_toll)
    RadioGroup radioGroup_toll;

    @BindView(R.id.btn_done)
    CustomBoldFontButton btn_done;

    @BindView(R.id.img_popup_close)
    AppCompatImageView img_popup_close;

    @BindView(R.id.edt_tip_amount)
    CustomRegularFontEditText edt_tip_amount;

    String mStopRideDetails, IsTollAccept = "1", stBookingId, stTipToken;

    @BindView(R.id.img_cancle)
    ImageView _img_cancle;

    @BindView(R.id.txt_addtip)
    CustomRegularFontTextView txt_addtip;
    @BindView(R.id.txt_paid_as_toll)
    CustomRegularFontTextView txt_paid_as_toll;
    @BindView(R.id.txt_ride_notes)
    CustomRegularFontTextView txt_ride_notes;

    @BindView(R.id.view_01)
    View _view_01;
    @BindView(R.id.view_02)
    View _view_02;
    @BindView(R.id.view_03)
    View _view_03;
    @BindView(R.id.view_04)
    View _view_04;

    String paytm_transaction_id = "", order_id = "", fail_payload = "", success_payload = "", toll_amount, payment_type = "";
    Double amount = 0.0;
    public static final String TAG = ReviewRideActivity.class.getSimpleName();

    String MID, ORDER_ID, CUST_ID, INDUSTRY_TYPE_ID, CHANNEL_ID, TXN_AMOUNT, WEBSITE, CALLBACK_URL, CHECKSUMHASH;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_ride);
        ButterKnife.bind(this);

        setContent();
    }

    private void setContent() {

        radioGroup_toll.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.checked_toll_yes) {
                    IsTollAccept = "1";
                } else if (checkedId == R.id.checked_toll_no) {
                    IsTollAccept = "2";
                }
            }
        });

        edt_tip_amount.setVisibility(View.GONE);
        radioGroup_tip.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.checked_tip_yes) {
                    edt_tip_amount.setVisibility(View.VISIBLE);
                } else if (checkedId == R.id.checked_tip_no) {
                    edt_tip_amount.setVisibility(View.GONE);
                    edt_tip_amount.setText("");
                }
            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_tip_amount.getVisibility() == View.VISIBLE) {
                    if (edt_tip_amount.getText().toString().equalsIgnoreCase("")) {
                        toastDialog.ShowToastMessage(getResources().getString(R.string.st_enter_tip_amount));

                    } else {
                        PaytmPayment();
                    }
                } else {
                    PaytmPayment();
                }
            }
        });

        _img_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GiveRatting();
//                finish();
//                Intent intent = new Intent("Review_Activity_finish");
//                LocalBroadcastManager.getInstance(ReviewRideActivity.this).sendBroadcast(intent);
//                finish();
            }
        });

        img_popup_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (getIntent().hasExtra("payment_details")) {
            mStopRideDetails = getIntent().getStringExtra("payment_details");
            Log.e("payment_details", mStopRideDetails);
            try {
                JSONObject objData = new JSONObject(mStopRideDetails);
                Log.e("payment_details", objData.toString());

                stBookingId = objData.getString("booking_id");

                txt_booking_id.setText("#" + stBookingId);
                txt_total_amount.setText("₹" + Math.round(Double.parseDouble(objData.getString("amount").toString())));

                amount = roundTwoDecimals(Double.parseDouble(objData.getString("amount")));
                txt_total_km.setText(roundTwoDecimals(Double.parseDouble(objData.getString("total_distance"))) + " km");
                txt_total_time.setText(objData.getString("duration") + " min");
                txt_toll_pay.setText("₹" + objData.getString("toll_tax"));
                toll_amount = objData.getString("toll_tax");
                Log.e("check", objData.getString("payment_type"));
                payment_type = objData.getString("payment_type");

            } catch (Exception e) {

            }
        } else if (getIntent().hasExtra("ride_stop_details")) {
            mStopRideDetails = getIntent().getStringExtra("ride_stop_details");
            try {
                JSONObject objData = new JSONObject(mStopRideDetails);
                Log.e("rideStopPayload", objData.toString());


                if (objData.getString("status").equalsIgnoreCase("1")) {

                    JSONArray arrPayload = objData.getJSONArray("payload");
                    JSONObject objPayload = arrPayload.getJSONObject(0);

                    stBookingId = objPayload.getString("booking_id");
//                    stTipToken = objPayload.getString("tip_request_token");

                    txt_booking_id.setText("#" + stBookingId);
                    txt_total_amount.setText("₹" + Math.round(Double.parseDouble(objPayload.getString("amount").toString())));

                    amount = roundTwoDecimals(Double.parseDouble(objPayload.getString("amount")));
                    txt_total_km.setText(roundTwoDecimals(Double.parseDouble(objPayload.getString("total_distance"))) + " km");
                    txt_total_time.setText(objPayload.getString("duration") + " min");
                    txt_toll_pay.setText("₹" + objPayload.getString("toll_tax"));
                    toll_amount = objPayload.getString("toll_tax");
                    Log.e("check", objPayload.getString("payment_type"));

                    payment_type = objPayload.getString("payment_type");


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (getIntent().hasExtra("panding_payment")) {

            mStopRideDetails = getIntent().getStringExtra("panding_payment");
            try {
                JSONArray arrData = new JSONArray(mStopRideDetails);
                Log.e("panding_payment", arrData.toString());

                if(arrData.length()>0){
                    JSONObject objPayload = arrData.getJSONObject(0);

                    stBookingId = objPayload.getString("booking_id");
//                    stTipToken = objPayload.getString("tip_request_token");

                    txt_booking_id.setText("#" + stBookingId);
                    txt_total_amount.setText("₹" + Math.round(Double.parseDouble(objPayload.getString("amount").toString())));

                    amount = roundTwoDecimals(Double.parseDouble(objPayload.getString("amount")));
                    txt_total_km.setText(roundTwoDecimals(Double.parseDouble(objPayload.getString("total_distance"))) + " km");
                    txt_total_time.setText(objPayload.getString("duration") + " min");
                    txt_toll_pay.setText("₹" + objPayload.getString("toll_tax"));
                    toll_amount = objPayload.getString("toll_tax");

                    payment_type = "";

                }



            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (payment_type.equals("4")) {
            _view_01.setVisibility(View.GONE);
            _view_02.setVisibility(View.GONE);
            _view_03.setVisibility(View.GONE);
            _view_04.setVisibility(View.GONE);

            txt_addtip.setVisibility(View.GONE);
            edt_tip_amount.setVisibility(View.GONE);
            txt_paid_as_toll.setVisibility(View.GONE);
            btn_done.setVisibility(View.GONE);
            txt_ride_notes.setVisibility(View.GONE);

            radioGroup_tip.setVisibility(View.GONE);
            radioGroup_toll.setVisibility(View.GONE);

        } else {
//            _img_cancle.setVisibility(View.GONE);
            _view_01.setVisibility(View.GONE);
            _view_02.setVisibility(View.GONE);
            _view_03.setVisibility(View.GONE);
            _view_04.setVisibility(View.GONE);

            txt_addtip.setVisibility(View.GONE);
            edt_tip_amount.setVisibility(View.GONE);
            txt_paid_as_toll.setVisibility(View.GONE);
            btn_done.setVisibility(View.GONE);
            txt_ride_notes.setVisibility(View.GONE);

            radioGroup_tip.setVisibility(View.GONE);
            edt_tip_amount.setVisibility(View.GONE);
            radioGroup_toll.setVisibility(View.GONE);
        }

    }

    private void PaytmPayment() {

        Log.e("PaytmPayment","called");

        String tip_amount = "";
        if (edt_tip_amount.getText().toString().equalsIgnoreCase("")) {
            tip_amount = "0";
        } else {
            tip_amount = edt_tip_amount.getText().toString();
        }

//        final PaytmPGService paytmPGService = PaytmPGService.getStagingService(); // Stagging

        Call<JsonObject> call = apiService.generate_checksum(stBookingId, preferencesUtility.getuser_id(), amount.toString(), tip_amount);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e("generate_checksum", response.body().toString());
                try {
                    JSONObject objData = new JSONObject(response.body().toString());
                    if (objData.getString("status").equalsIgnoreCase("1")) {

                        JSONObject objPayload = objData.getJSONObject("payload");

                        // Create a HASHMAP Object holding the Order Information
                        HashMap<String, String> paramMap = new HashMap<String, String>();
                        paramMap.put("MID", objPayload.getString("MID"));
                        paramMap.put("ORDER_ID", objPayload.getString("ORDER_ID"));
                        paramMap.put("CUST_ID", objPayload.getString("CUST_ID"));
                        paramMap.put("INDUSTRY_TYPE_ID", objPayload.getString("INDUSTRY_TYPE_ID"));
                        paramMap.put("CHANNEL_ID", objPayload.getString("CHANNEL_ID"));
                        paramMap.put("TXN_AMOUNT", objPayload.getString("TXN_AMOUNT"));
                        paramMap.put("WEBSITE", objPayload.getString("WEBSITE"));
                        paramMap.put("CALLBACK_URL", objPayload.getString("CALLBACK_URL"));

                        if (objPayload.has("EMAIL")) {
                            paramMap.put("EMAIL", objPayload.getString("EMAIL"));
                        }
                        if (objPayload.has("MOBILE_NO")) {
                            paramMap.put("MOBILE_NO", objPayload.getString("MOBILE_NO"));
                        }

                        paramMap.put("CHECKSUMHASH", objPayload.getString("CHECKSUMHASH"));


                        final PaytmPGService paytmPGService = PaytmPGService.getProductionService(); // Production

                        // Create Paytm Order Object
                        PaytmOrder paytmOrder = new PaytmOrder(paramMap);

                        paytmPGService.initialize(paytmOrder, null);

                        paytmPGService.startPaymentTransaction(ReviewRideActivity.this, true, true,
                                new PaytmPaymentTransactionCallback() {
                                    @Override
                                    public void onTransactionResponse(Bundle bundle) {
                                        try {

                                            Log.e("LOG", "Payment Transaction : " + bundle);
                                            Log.e("LOG", "Payment Transaction : " + bundle.getString("STATUS"));

                                            if (bundle.getString("STATUS").equals("TXN_SUCCESS")) {
                                                JSONObject json = new JSONObject();
                                                Set<String> keys = bundle.keySet();
                                                for (String key : keys) {
                                                    try {
                                                        // json.put(key, bundle.get(key)); see edit below
                                                        json.put(key, JSONObject.wrap(bundle.get(key)));
                                                    } catch (JSONException e) {
                                                        //Handle exception here
                                                    }
                                                }

                                                Log.e("JSon_data", json.toString());
                                                success_payload = json.toString();
                                                if (bundle.containsKey("TXNID")) {
                                                    paytm_transaction_id = bundle.getString("TXNID").toString();
                                                }
                                                order_id = bundle.getString("ORDERID").toString();

                                                GiveTipToDriver();

                                            } else {
                                                JSONObject json = new JSONObject();
                                                Set<String> keys = bundle.keySet();
                                                for (String key : keys) {
                                                    try {
                                                        // json.put(key, bundle.get(key)); see edit below
                                                        json.put(key, JSONObject.wrap(bundle.get(key)));
                                                    } catch (JSONException e) {
                                                        //Handle exception here
                                                    }
                                                }

                                                Log.e("JSon_data", json.toString());
                                                fail_payload = json.toString();
                                                if (bundle.containsKey("TXNID")) {
                                                    paytm_transaction_id = bundle.getString("TXNID").toString();
                                                }
                                                order_id = bundle.getString("ORDERID").toString();

                                                failServiceCall();
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void networkNotAvailable() {
                                        Log.d("LOG", "UI Error Occur.");
                                        Toast.makeText(getApplicationContext(), " UI Error Occur. ", Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void clientAuthenticationFailed(String inErrorMessage) {
                                        Log.d("LOG", "clientAuthenticationFailed.");
                                        Toast.makeText(getApplicationContext(), " Sever-side Error " + inErrorMessage, Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void someUIErrorOccurred(String s) {
                                        Log.d("LOG", "UI Error Occur.");
                                    }

                                    @Override
                                    public void onErrorLoadingWebPage(int i, String s, String s1) {
                                        Log.d("LOG", "onErrorLoadingWebPage.");
                                    }

                                    @Override
                                    public void onBackPressedCancelTransaction() {
                                        Log.d("LOG", "onBackPressedCancelTransaction");
                                    }

                                    @Override
                                    public void onTransactionCancel(String s, Bundle bundle) {
                                        Log.d("LOG", "Payment_Transaction_Failed " + bundle);
                                        Toast.makeText(getBaseContext(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
                                    }
                                });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });


    }

    private void failServiceCall() {
        Call<JsonObject> call = apiService.paytm_payment_fail(stBookingId, fail_payload, paytm_transaction_id, order_id);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e("paytm_payment_fail", response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }


    /**
     * User give Tip to Driver
     */
    private void GiveTipToDriver() {
        try {

            String tip_amount = "";
            if (edt_tip_amount.getText().toString().equalsIgnoreCase("")) {
                tip_amount = "0";
            } else {
                tip_amount = edt_tip_amount.getText().toString();
            }
//            Log.e("payUmoney_trans_id", paytm_transaction_id);
            progressDialog.show();
            Call<JsonObject> call = apiService.ride_stop_tip(stBookingId, preferencesUtility.getuser_id(), tip_amount,
                    paytm_transaction_id, IsTollAccept, success_payload, order_id);

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        String mResult = response.body().toString();
                        Log.e("ride_stop_tip", mResult);
                        progressDialog.dismiss();

                        JSONObject objData = new JSONObject(mResult);
                        if (objData.getString("status").equalsIgnoreCase("1")) {
                            toastDialog.ShowToastMessage(objData.getString("message"));

                            final Dialog dialog = new Dialog(ReviewRideActivity.this);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.dialog_ratting);
                            dialog.setTitle("");
                            dialog.setCancelable(false);

                            CustomBoldFontButton btn_submit = (CustomBoldFontButton) dialog.findViewById(R.id.btn_submit);
                            CustomBoldFontButton btn_cancel = (CustomBoldFontButton) dialog.findViewById(R.id.btn_cancel);
                            final CustomRegularFontEditText edt_vale_review = (CustomRegularFontEditText) dialog.findViewById(R.id.edt_vale_review);
                            final AppCompatRatingBar rating_vale = (AppCompatRatingBar) dialog.findViewById(R.id.rating_vale);

                            rating_vale.setRating(5);

                            btn_submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    String stRatting = "";
                                    if (edt_vale_review.getText().toString().equalsIgnoreCase("")) {
                                        stRatting = "";
                                    } else {
                                        stRatting = edt_vale_review.getText().toString();
                                    }

                                    try {
                                        Call<JsonObject> call = apiService.ride_rating(preferencesUtility.getuser_id(),
                                                stBookingId, "1", rating_vale.getProgress() + "", stRatting);

                                        call.enqueue(new Callback<JsonObject>() {
                                            @Override
                                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                                onRideRating(response.body().toString());
                                            }

                                            @Override
                                            public void onFailure(Call<JsonObject> call, Throwable t) {

                                            }
                                        });

                                        dialog.dismiss();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            btn_cancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent("Review_Activity_finish");
                                    LocalBroadcastManager.getInstance(ReviewRideActivity.this).sendBroadcast(intent);
                                    finish();
                                    dialog.dismiss();
                                    finish();
                                }
                            });
                            dialog.show();
                        } else {
//                            finish();
                        }

                    } catch (Exception e) {
                        progressDialog.dismiss();
                        Log.e("Exception", e.toString());
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("onFailure", t.getMessage().toString());
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* Open Give Ratting Dialog*/
    private void GiveRatting() {
        final Dialog dialog = new Dialog(ReviewRideActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_ratting);
        dialog.setTitle("");
        dialog.setCancelable(false);

        CustomBoldFontButton btn_submit = (CustomBoldFontButton) dialog.findViewById(R.id.btn_submit);
        CustomBoldFontButton btn_cancel = (CustomBoldFontButton) dialog.findViewById(R.id.btn_cancel);
        final CustomRegularFontEditText edt_vale_review = (CustomRegularFontEditText) dialog.findViewById(R.id.edt_vale_review);
        final AppCompatRatingBar rating_vale = (AppCompatRatingBar) dialog.findViewById(R.id.rating_vale);

        rating_vale.setRating(5);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String stRatting = "";
                if (edt_vale_review.getText().toString().equalsIgnoreCase("")) {
                    stRatting = "";
                } else {
                    stRatting = edt_vale_review.getText().toString();
                }

                try {

                    Call<JsonObject> call = apiService.ride_rating(preferencesUtility.getuser_id(),
                            stBookingId, "1", rating_vale.getProgress() + "", stRatting);

                    call.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            onRideRating(response.body().toString());
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {

                        }
                    });

                    dialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent("Review_Activity_finish");
                LocalBroadcastManager.getInstance(ReviewRideActivity.this).sendBroadcast(intent);
                finish();
            }
        });
        dialog.show();
    }


    /**
     * User Ride Rating
     */
    private void onRideRating(String result) {
        Log.e("onRideRating", result);
        try {
            String mResult = result;
            JSONObject objRideRating = new JSONObject(mResult);
            if (objRideRating.getString("status").equalsIgnoreCase("1")) {
                toastDialog.ShowToastMessage(objRideRating.getString("message"));

                final Dialog dialog = new Dialog(ReviewRideActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.layout_dialog_popup);
                dialog.setTitle("");
                dialog.setCancelable(false);

                TextView txt_popup_header = (TextView) dialog.findViewById(R.id.txt_popup_header);
                TextView txt_popup_message = (TextView) dialog.findViewById(R.id.txt_popup_message);
                TextView btn_ok = (TextView) dialog.findViewById(R.id.btn_ok);

                txt_popup_header.setText("Ride Rating");
                txt_popup_message.setText(objRideRating.getString("message"));

                txt_popup_header.setTypeface(global_typeface.Sansation_Bold());
                txt_popup_message.setTypeface(global_typeface.Sansation_Regular());
                btn_ok.setTypeface(global_typeface.Sansation_Regular());

                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                        Intent intent = new Intent("Review_Activity_finish");
                        LocalBroadcastManager.getInstance(ReviewRideActivity.this).sendBroadcast(intent);
                        finish();
                    }
                });
                dialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {

    }
}
