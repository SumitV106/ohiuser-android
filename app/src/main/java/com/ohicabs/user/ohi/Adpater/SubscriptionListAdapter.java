package com.ohicabs.user.ohi.Adpater;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.PojoModel.SubscriptionModel;
import com.ohicabs.user.ohi.R;

import java.util.ArrayList;

public class SubscriptionListAdapter extends PagerAdapter {

    private LayoutInflater mLayoutInflater;
    private Context context;
    private ArrayList<SubscriptionModel> arrSubscriptions = new ArrayList<>();
    private BtnClickListener mClickListener = null;
    private BtnOKClickListener mOKClickListener = null;

    public SubscriptionListAdapter(Context ctx, float baseElevation, ArrayList<SubscriptionModel> subscriptions,
                                   BtnClickListener btnClickListener, BtnOKClickListener btnOKClickListener) {
        this.context = ctx;
        arrSubscriptions = subscriptions;
        mClickListener = btnClickListener;
        mOKClickListener = btnOKClickListener;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    @Override
    public int getCount() {
        return arrSubscriptions.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.layout_subscription, container, false);

        AppCompatImageView imgSubscription = (AppCompatImageView) itemView.findViewById(R.id.img_subscription);
        AppCompatTextView txtTitle = itemView.findViewById(R.id.txt_subscription_title);
        AppCompatTextView txtDetails = itemView.findViewById(R.id.txt_subscription_details);
        AppCompatButton btnSubscribe = itemView.findViewById(R.id.btn_subscription);
        AppCompatButton btnOk = itemView.findViewById(R.id.btn_ok);

        Glide.with(context).load(Global_ServiceApi.API_IMAGE_HOST + arrSubscriptions.get(position).getPlan_image().toString())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgSubscription);

        if (arrSubscriptions.get(position).getIs_offer().equalsIgnoreCase("0")
                && arrSubscriptions.get(position).getIs_buy().equalsIgnoreCase("1")) {

            btnSubscribe.setText("SUBSCRIPTION");
            btnSubscribe.setAlpha(1.0f);
            btnOk.setVisibility(View.GONE);

        } else if (arrSubscriptions.get(position).getIs_offer().equalsIgnoreCase("0")
                && arrSubscriptions.get(position).getIs_buy().equalsIgnoreCase("0")) {

            btnSubscribe.setEnabled(false);
            btnSubscribe.setText("SUBSCRIPTION");
            btnSubscribe.setAlpha(0.5f);

        } else {
            btnSubscribe.setVisibility(View.GONE);
            btnOk.setVisibility(View.VISIBLE);
        }

        btnSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) {
                    mClickListener.onBtnSubscribeClick(position, arrSubscriptions.get(position).getSubscriptionDetails(),
                            arrSubscriptions.get(position).getIs_offer(), arrSubscriptions.get(position).getIs_buy());
                }
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOKClickListener != null) {
                    mOKClickListener.onBtnOKClick("Finish");
                }
            }
        });

        txtTitle.setText(arrSubscriptions.get(position).getPlan_name());
        txtDetails.setText(arrSubscriptions.get(position).getDetails());

        container.addView(itemView);

        return itemView;
    }

    public interface BtnOKClickListener {
        void onBtnOKClick(String status);
    }

    public interface BtnClickListener {
        void onBtnSubscribeClick(int position, String subscription_details, String isOffer, String isBuy);
    }

}
