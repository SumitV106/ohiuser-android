package com.ohicabs.user.ohi.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ohicabs.user.ohi.R;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sumit on 19/06/17.
 */

public class DialogActivity extends BaseActivity {

    private String mBookingDetails;

    @BindView(R.id.txt_popup_header)
    TextView txt_popup_header;

    @BindView(R.id.txt_popup_message)
    TextView txt_popup_message;

    @BindView(R.id.btn_ok)
    Button btn_ok;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_popup);
        ButterKnife.bind(this);

        setContent();
    }

    private void setContent() {

        txt_popup_header.setTypeface(global_typeface.Sansation_Bold());
        txt_popup_message.setTypeface(global_typeface.Sansation_Regular());
        btn_ok.setTypeface(global_typeface.Sansation_Regular());

        if (getIntent().hasExtra("booking_status_details")) {
            mBookingDetails = getIntent().getStringExtra("booking_status_details");
        }

        try {
            JSONObject objData = new JSONObject(mBookingDetails);
            Log.e("DialogActivity",objData.toString());
            if (objData.getString("status").equalsIgnoreCase("1")) {
                JSONArray arrPayload = objData.getJSONArray("payload");
                JSONObject objPayload = arrPayload.getJSONObject(0);

                txt_popup_header.setText("Booking ID : #"+objPayload.getString("booking_id"));
                txt_popup_message.setText(objData.getString("message"));

                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent("User_Current_Booking_List");
                        LocalBroadcastManager.getInstance(DialogActivity.this).sendBroadcast(intent);
                        finish();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
    }
}
