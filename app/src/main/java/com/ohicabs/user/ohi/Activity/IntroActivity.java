package com.ohicabs.user.ohi.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.CustomWidgets.CustomBoldFontButton;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.Global_Typeface;
import com.onesignal.OSSubscriptionObserver;
import com.onesignal.OSSubscriptionStateChanges;
import com.onesignal.OneSignal;
import com.truecaller.android.sdk.ITrueCallback;
import com.truecaller.android.sdk.TrueButton;
import com.truecaller.android.sdk.TrueError;
import com.truecaller.android.sdk.TrueProfile;
import com.truecaller.android.sdk.TrueSDK;
import com.truecaller.android.sdk.TrueSdkScope;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;
import static com.ohicabs.user.ohi.Utils.Constant.BACK_TO_ACTIVITY;

public class IntroActivity extends BaseActivity implements OSSubscriptionObserver {

    @BindView(R.id.btnlogin) CustomBoldFontButton _btnlogin;
    @BindView(R.id.btnregister) CustomBoldFontButton _btnregister;
    @BindView(R.id.welcommsg) TextView _welcommsg;
    @BindView(R.id.tvdesc) TextView _tvdesc;
    @BindView(R.id.btn_truecaller) AppCompatButton btnTrueCaller;
    @BindView(R.id.rl_truecaller) RelativeLayout rlTrueCaller;

    private String mTruecallerRequestNonce = null;

    Global_Typeface global_typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        ButterKnife.bind(this);

        _btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivityForResult(intent, BACK_TO_ACTIVITY);
            }
        });

        _btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivityForResult(intent, BACK_TO_ACTIVITY);
            }
        });

        global_typeface = new Global_Typeface(getApplicationContext());

        _welcommsg.setTypeface(global_typeface.Roboto_Medium());
        _tvdesc.setTypeface(global_typeface.Roboto_Light());

        /**
         * Open Disclaimer Dialog Activity only once
         */
        try {
            if (!preferencesUtility.CheckPreferences("is_disclaimer")) {
                if (preferencesUtility.getDisclaimer() == false) {

                    Intent intent_vale_request = new Intent(this, DisclaimerActivity.class);
                    intent_vale_request.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent_vale_request);
                    preferencesUtility.setDisclaimer(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        OneSignal.addSubscriptionObserver(this);

        setViews();
        initTrueCaller();
    }

    private void setViews() {

        btnTrueCaller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TrueSDK.getInstance().getUserProfile(IntroActivity.this);
            }
        });
    }

    private void initTrueCaller() {
        TrueSdkScope trueScope = new TrueSdkScope.Builder(this, sdkCallback)
                .consentMode(TrueSdkScope.CONSENT_MODE_POPUP) //Consent Mode
                .consentTitleOption(TrueSdkScope.SDK_CONSENT_TITLE_SIGN_IN)
                .footerType(TrueSdkScope.FOOTER_TYPE_SKIP) //Footer Type
                .build();

        TrueSDK.init(trueScope);

        boolean isTrueCallerInstalled = TrueSDK.getInstance().isUsable();
        if (isTrueCallerInstalled) {
            Log.e("TAG_CHECK_APP","Installed");
            rlTrueCaller.setVisibility(View.VISIBLE);
        } else {
            Log.e("TAG_CHECK_APP","Not Installed");
            rlTrueCaller.setVisibility(View.GONE);
        }
    }

    private final ITrueCallback sdkCallback = new ITrueCallback() {

        @Override
        public void onSuccessProfileShared(@NonNull final TrueProfile trueProfile) {
            // This method is invoked when the truecaller app is installed on the device and the user gives his
            // consent to share his truecaller profile

            String stEmail, stFirstName, stLastName, stPhoneNumber;

            stPhoneNumber = trueProfile.phoneNumber;

            if (trueProfile.firstName != null) {
                stFirstName = trueProfile.firstName;
            } else {
                stFirstName = "";
            }

            if (trueProfile.lastName != null) {
                stLastName = trueProfile.lastName;
            } else {
                stLastName = "";
            }

            if (trueProfile.email != null) {
                stEmail = trueProfile.email;
            } else {
                stEmail = "";
            }

            if (isInternetOn(getApplicationContext())) {
                checkTrueCallerLogin(stEmail, stFirstName + " " + stLastName, stPhoneNumber);
            }
        }

        @Override
        public void onFailureProfileShared(@NonNull final TrueError trueError) {
            // This method is invoked when some error occurs or if an invalid request for verification is made
            Log.e("TAG_ERROR", trueError.getErrorType() + " == " + trueError.describeContents());
        }

        @Override
        public void onOtpRequired() {

        }
    };

    @Override
    public void onOSSubscriptionChanged(OSSubscriptionStateChanges stateChanges) {

        if (!stateChanges.getFrom().getSubscribed() && stateChanges.getTo().getSubscribed()) {
            // get player ID
            preferencesUtility.settoken(stateChanges.getTo().getUserId());
        }
    }

    // change on Date 27-12-2017
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //for close previous screen
        if (resultCode == BACK_TO_ACTIVITY) {
            String message = data.getStringExtra("MESSAGE");
            if (data.getStringExtra("MESSAGE").toString().equals("finish")) {
                finish();
            }

        } else {
            //for truecaller data result
            try {
                TrueSDK.getInstance().onActivityResultObtained( this, resultCode, data);
            } catch (Exception e) { }
        }
    }

    /**
     * Check TrueCaller user register or not
     *
     * @param email
     * @param name
     * @param mobile
     */
    private void checkTrueCallerLogin(String email, String name, String mobile) {
        if (isInternetOn(getApplicationContext())) {

            progressDialog.show();
            Call<JsonObject> call = apiService.user_truecaller_login(name, email, mobile, "1", "Android", preferencesUtility.gettoken(), mSocket.id());

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    progressDialog.dismiss();
                    try {

                        if (response.isSuccessful()) {
                            JsonObject object = response.body();

                            if (object.get("status").getAsString().equalsIgnoreCase("1")) {

                                JsonObject jarrayLogin = object.getAsJsonObject("payload");

                                preferencesUtility.setuser_id(jarrayLogin.get("user_id").getAsString());
                                preferencesUtility.setfirstname(jarrayLogin.get("firstname").getAsString());
                                preferencesUtility.setemail(jarrayLogin.get("email").getAsString());
                                preferencesUtility.setgender(jarrayLogin.get("gender").getAsString());
                                preferencesUtility.setmobile(jarrayLogin.get("mobile").getAsString());
                                preferencesUtility.setauth_token(jarrayLogin.get("auth_token").getAsString());
                                preferencesUtility.setimage(jarrayLogin.get("image").getAsString());
                                preferencesUtility.setrefer_code(jarrayLogin.get("refer_code").getAsString());
                                preferencesUtility.setstatus(jarrayLogin.get("status").getAsString());
                                preferencesUtility.setZoneId(jarrayLogin.get("zone_id").getAsString());

                                preferencesUtility.setLogedin(true);
                                preferencesUtility.setLoginType("true");
                                toastDialog.ShowToastMessage(getResources().getString(R.string.st_login_success));

                                // for close previous activity
                                Intent intent1 = new Intent();
                                intent1.putExtra("MESSAGE", "finish");
                                setResult(BACK_TO_ACTIVITY, intent1);

                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                                finish();

                            } else if (object.get("status").getAsString().equalsIgnoreCase("2")) {

                                Intent i = new Intent(IntroActivity.this, TrueCallerRegisterActivity.class);
                                i.putExtra("username", name);
                                i.putExtra("email", email);
                                i.putExtra("mobile_number", mobile);
                                startActivity(i);
                                finish();

                            } else {
                                toastDialog.ShowToastMessage(object.get("message").toString());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("error", t.toString());
                }
            });
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }

}
