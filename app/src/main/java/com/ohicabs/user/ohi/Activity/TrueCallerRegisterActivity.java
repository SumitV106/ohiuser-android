package com.ohicabs.user.ohi.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.CustomWidgets.CustomBoldFontButton;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontEditText;
import com.ohicabs.user.ohi.CustomWidgets.CustomRegularFontTextView;
import com.ohicabs.user.ohi.DBModel.ZoneList_Model;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.TypefaceSpan;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;
import static com.ohicabs.user.ohi.Utils.Constant.BACK_TO_ACTIVITY;

public class TrueCallerRegisterActivity extends BaseActivity {

    public static final int REQUEST_STORAGE_PERMISSION = 5001;

    @BindView(R.id.tvheader) TextView tvHeader;

    @BindView(R.id.passwordWrapper) TextInputLayout passwordWrapper;
    @BindView(R.id.cpasswordWrapper) TextInputLayout confirmPasswordWrapper;
    @BindView(R.id.nameWrapper) TextInputLayout nameWrapper;
    @BindView(R.id.emailWrapper) TextInputLayout emailWrapper;
    @BindView(R.id.mobileWrapper) TextInputLayout mobileWrapper;
    @BindView(R.id.referralWrapper) TextInputLayout ReferralCodeWrapper;

    @BindView(R.id.imgcancle) ImageView imgClose;
    @BindView(R.id.btnregister) CustomBoldFontButton btnRegister;

    @BindView(R.id.edname) AppCompatEditText edtName;
    @BindView(R.id.edemail) AppCompatEditText edtEmail;
    @BindView(R.id.edpwd) AppCompatEditText edtPassword;
    @BindView(R.id.edcpwd) AppCompatEditText edtConfirmPassword;
    @BindView(R.id.edmobile) AppCompatEditText edtMobile;
    @BindView(R.id.edt_referral_code) AppCompatEditText edtReferralCode;

    @BindView(R.id.chk_agree_terms_condition) AppCompatCheckBox chk_agree_terms_condition;
    @BindView(R.id.txt_terms) CustomRegularFontTextView txt_terms;
    @BindView(R.id.txt_disclaimer) CustomRegularFontTextView txt_disclaimer;
    @BindView(R.id.spinner_zone) AppCompatSpinner spinnerZoneList;

    private String stAgree = "", zoneID;
    ApiInterface apiService;
    ArrayList<ZoneList_Model> arrZoneList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_truecaller_register);
        ButterKnife.bind(this);

        setContent();
    }

    private void setContent() {

        tvHeader.setTypeface(global_typeface.Sansation_Bold());
        passwordWrapper.setTypeface(global_typeface.Sansation_Regular());
        confirmPasswordWrapper.setTypeface(global_typeface.Sansation_Regular());
        chk_agree_terms_condition.setTypeface(global_typeface.Sansation_Regular());

        edtName.addTextChangedListener(new MyTextWatcher(edtName));
        edtEmail.addTextChangedListener(new MyTextWatcher(edtEmail));
        edtPassword.addTextChangedListener(new MyTextWatcher(edtPassword));
        edtConfirmPassword.addTextChangedListener(new MyTextWatcher(edtConfirmPassword));
        edtMobile.addTextChangedListener(new MyTextWatcher(edtMobile));

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(TrueCallerRegisterActivity.this);
                finish();
            }
        });

        chk_agree_terms_condition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    stAgree = "true";
                } else {
                    stAgree = "false";
                }
            }
        });

        txt_terms.setText(Html.fromHtml("I agree to the <u>terms and conditions</u> including the"));
        txt_disclaimer.setText(Html.fromHtml("<u>Disclaimer</u>"));

        txt_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i_terms_condition = new Intent(TrueCallerRegisterActivity.this, TermsConditionActivity.class);
                i_terms_condition.putExtra("link", "terms");
                startActivity(i_terms_condition);
            }
        });

        txt_disclaimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i_terms_condition = new Intent(TrueCallerRegisterActivity.this, TermsConditionActivity.class);
                i_terms_condition.putExtra("link", "disclaimer");
                startActivity(i_terms_condition);
            }
        });

        apiService = ApiClient.getClient().create(ApiInterface.class);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String stEmail;

                String stName = getIntent().getStringExtra("username");
                stEmail = getIntent().getStringExtra("email");

                if (stEmail.equalsIgnoreCase("")) {
                    stEmail = edtEmail.getText().toString();
                }

                String stMobileNumber = getIntent().getStringExtra("mobile_number");

                if (submitForm()) {
                    if (zoneID.equalsIgnoreCase("0")) {
                        ShowToast("Please select your City");

                    } else if (stAgree.equalsIgnoreCase("") || stAgree.equalsIgnoreCase("false")) {
                        ShowToast("Please select Terms & Condition");

                    } else {
                        if (isInternetOn(getApplicationContext())) {
                            userRegistration(stName, stEmail, stMobileNumber);
                        } else {
                            ShowToast(getResources().getString(R.string.network_lost));
                        }
                    }
                }
            }
        });

        spinnerZoneList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View convertView, int position, long id) {
                AppCompatTextView textView = (AppCompatTextView) convertView;
                ((AppCompatTextView) parent.getChildAt(0)).setTextColor(Color.WHITE);

                ZoneList_Model zoneList = (ZoneList_Model) parent.getSelectedItem();
                zoneID = zoneList.getZone_id();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        edtName.setText(getIntent().getStringExtra("username"));
        edtEmail.setText(getIntent().getStringExtra("email"));
        edtMobile.setText(getIntent().getStringExtra("mobile_number"));

        PackageInfo packageInfo;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            int versionCode = packageInfo.versionCode;

        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        requestPermission();

        /*
         * Get Zone List
         */
        GetZoneList();
    }

    private void userRegistration(String name, String email, String phoneNumber) {

        if (isInternetOn(getApplicationContext())) {

            progressDialog.show();
            Call<JsonObject> call = apiService.user_truecaller_new_login(name,
                    email, phoneNumber, "1", edtPassword.getText().toString(),"Android",
                    preferencesUtility.gettoken(), mSocket.id(), zoneID, edtReferralCode.getText().toString());

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    progressDialog.dismiss();

                    if (response.isSuccessful()) {

                        JsonObject object = response.body();
                        if (object.get("success").getAsString().equalsIgnoreCase("true")
                                && object.get("status").getAsString().equalsIgnoreCase("1")) {
                            JsonObject jarrayLogin = object.getAsJsonObject("payload");

                            preferencesUtility.setuser_id(jarrayLogin.get("user_id").getAsString());
                            preferencesUtility.setfirstname(jarrayLogin.get("firstname").getAsString());
                            preferencesUtility.setemail(jarrayLogin.get("email").getAsString());
                            preferencesUtility.setgender(jarrayLogin.get("gender").getAsString());
                            preferencesUtility.setmobile(jarrayLogin.get("mobile").getAsString());
                            preferencesUtility.setauth_token(jarrayLogin.get("auth_token").getAsString());
                            preferencesUtility.setimage(jarrayLogin.get("image").getAsString());
                            preferencesUtility.setrefer_code(jarrayLogin.get("refer_code").getAsString());
                            preferencesUtility.setstatus(jarrayLogin.get("status").getAsString());
                            preferencesUtility.setZoneId(jarrayLogin.get("zone_id").getAsString());

                            preferencesUtility.setLogedin(true);
                            preferencesUtility.setLoginType("true");
                            toastDialog.ShowToastMessage(object.get("message").getAsString().toString());

                            // for close previous activity
                            Intent intent1=new Intent();
                            intent1.putExtra("MESSAGE","finish");
                            setResult(BACK_TO_ACTIVITY,intent1);

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                            finish();

                        } else if (object.get("status").getAsString().equalsIgnoreCase("0")
                                || object.get("success").getAsString().equalsIgnoreCase("false")) {
                            toastDialog.ShowToastMessage(object.get("message").getAsString().toString());

                        } else if (object.get("status").getAsString().equalsIgnoreCase("2")) {
                            toastDialog.ShowToastMessage(object.get("message").getAsString().toString());

                            // for close previous activity
                            Intent intent1=new Intent();
                            intent1.putExtra("MESSAGE","finish");
                            setResult(BACK_TO_ACTIVITY,intent1);

                            Intent intent = new Intent(TrueCallerRegisterActivity.this, VerificationActivity.class);

                            JsonObject jarrayLogin = object.getAsJsonObject("payload");

                            Bundle b_data = new Bundle();
                            b_data.putString("email", edtEmail.getText().toString());
                            b_data.putString("verify_mobile_no", edtEmail.getText().toString());
                            b_data.putString("user_id", jarrayLogin.get("user_id").getAsString());
                            intent.putExtras(b_data);
                            startActivity(intent);
                            finish();
                        } else {
                            toastDialog.ShowToastMessage(object.get("message").getAsString().toString());
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                }
            });
        } else {
            toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
        }
    }

    /**
     * Get Zone List Data
     */
    private void GetZoneList() {
        try {
            Call<JsonObject> call = apiService.getZoneList();

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    progressDialog.dismiss();

                    if (response.isSuccessful()) {
                        JsonObject objState = response.body();

                        if (objState.get("success").getAsString().equalsIgnoreCase("true")) {
                            if (objState.get("status").getAsString().equalsIgnoreCase("1")) {
                                JsonArray arrState = objState.get("payload").getAsJsonArray();
                                if (arrState.size() > 0) {

                                    ZoneList_Model zoneListModelTemp = new ZoneList_Model();
                                    zoneListModelTemp.setZone_id("0");
                                    zoneListModelTemp.setZone_name("Select City");
                                    arrZoneList.add(zoneListModelTemp);

                                    for (int i = 0; i < arrState.size(); i++) {
                                        JsonObject objStateData = arrState.get(i).getAsJsonObject();

                                        ZoneList_Model zoneListModel = new ZoneList_Model();
                                        zoneListModel.setZone_id(objStateData.get("zone_id").getAsString());
                                        zoneListModel.setZone_name(objStateData.get("zone_name").getAsString());
                                        arrZoneList.add(zoneListModel);
                                    }
                                }

                                //fill data in spinner
                                ArrayAdapter<ZoneList_Model> adapter = new ArrayAdapter<ZoneList_Model>(getApplicationContext(),
                                        R.layout.spinner_items_zone, arrZoneList);
                                spinnerZoneList.setAdapter(adapter);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("error", t.toString());
                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean validateName() {
        if (edtName.getText().toString().trim().isEmpty()) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_no_name));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            nameWrapper.setError(ssbuilder);
            requestFocus(edtName);
            return false;
        } else {
            nameWrapper.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail() {
        if (edtEmail.getText().toString().trim().equals("")) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_email_blank));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            emailWrapper.setError(ssbuilder);
            requestFocus(edtEmail);
            return false;
        } else if (!isValidEmail(edtEmail.getText().toString())) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_email_no_valid));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            emailWrapper.setError(ssbuilder);
            requestFocus(edtEmail);
            return false;
        } else {
            emailWrapper.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMobile() {
        if (edtMobile.getText().toString().trim().equals("")) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_no_mobile));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            mobileWrapper.setError(ssbuilder);
            requestFocus(edtMobile);
            return false;

        } else if (edtMobile.getText().toString().trim().length() != 13) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_no_valid_mobile));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            mobileWrapper.setError(ssbuilder);
            requestFocus(edtMobile);
            return false;
        } else {
            mobileWrapper.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        if (edtPassword.getText().toString().trim().isEmpty()) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_password_blank));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            passwordWrapper.setError(ssbuilder);
            requestFocus(edtPassword);
            return false;
        } else if (edtPassword.getText().toString().trim().length() < 6) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_password_minimum));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            passwordWrapper.setError(ssbuilder);
            requestFocus(edtPassword);
            return false;
        } else {
            passwordWrapper.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateConfirmPassword() {
        if (edtConfirmPassword.getText().toString().trim().isEmpty()) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_password_confirm_blank));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            confirmPasswordWrapper.setError(ssbuilder);
            requestFocus(edtConfirmPassword);
            return false;
        } else if (!edtConfirmPassword.getText().toString().equals(edtConfirmPassword.getText().toString())) {
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(getString(R.string.st_password_no_match));
            ssbuilder.setSpan(new TypefaceSpan(global_typeface.Sansation_Regular()), 0, ssbuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            confirmPasswordWrapper.setError(ssbuilder);
            requestFocus(edtConfirmPassword);
            return false;
        } else {
            confirmPasswordWrapper.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateterms() {
        Log.e("stAgree", stAgree);
        if (stAgree.equalsIgnoreCase("") && stAgree.equalsIgnoreCase("false")) {
            ShowToast("Please select Terms & Condition");
            return false;
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    private boolean submitForm() {
        boolean result = true;

        if (!validateName()) {
            result = false;
        }
        if (!validateEmail()) {
            result = false;
        }
        if (!validatePassword()) {
            result = false;
        }
        if (!validateConfirmPassword()) {
            result = false;
        }
        if (!validateMobile()) {
            result = false;
        }

        return result;
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.edname:
                    validateName();
                    break;
                case R.id.edemail:
                    validateEmail();
                    break;
                case R.id.edpwd:
                    validatePassword();
                    break;
                case R.id.edmobile:
                    validateMobile();
                    break;
                case R.id.edcpwd:
                    validateConfirmPassword();
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_STORAGE_PERMISSION) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finish();
            }
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSION);
        }
    }
}
