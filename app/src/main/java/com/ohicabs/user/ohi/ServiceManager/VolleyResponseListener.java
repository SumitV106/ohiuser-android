package com.ohicabs.user.ohi.ServiceManager;

public interface VolleyResponseListener {

    void onError(String message);

    void onResponse(Object response);
}
