package com.ohicabs.user.ohi.Adpater;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.Constant;
import com.ohicabs.user.ohi.Utils.Global_Typeface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;


public class UserAllRidesListAdapter extends BaseAdapter {

    private Activity activity;
    int ResourceId;
    private ArrayList<HashMap<String, String>> _arr_userBookedRides;
    private Global_Typeface global_typeface;
    private BtnClickListener mClickListener = null;

    public UserAllRidesListAdapter(Activity act, int resId, ArrayList<HashMap<String, String>> hashMapArrayList, BtnClickListener listener) {
        this.activity = act;
        this.ResourceId = resId;
        this._arr_userBookedRides = hashMapArrayList;
        mClickListener = listener;
        global_typeface = new Global_Typeface(activity);
    }

    @Override
    public int getCount() {
        return _arr_userBookedRides.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        final ViewHolder viewHolder;
        if (itemView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(ResourceId, parent, false);

            viewHolder.tv_date_time = (AppCompatTextView) itemView.findViewById(R.id.tv_date_time);
            viewHolder.txt_status = (AppCompatTextView) itemView.findViewById(R.id.txt_status);
            viewHolder.txt_ride_id = (AppCompatTextView) itemView.findViewById(R.id.txt_ride_id);
            viewHolder.txt_from_address = (AppCompatTextView) itemView.findViewById(R.id.txt_from_address);
            viewHolder.txt_to_address = (AppCompatTextView) itemView.findViewById(R.id.txt_to_address);

            viewHolder.img_car_type = (AppCompatImageView) itemView.findViewById(R.id.img_car_type);
            viewHolder.img_ride_status = (AppCompatImageView) itemView.findViewById(R.id.img_ride_status);
            viewHolder.layout_to_address = (LinearLayout) itemView.findViewById(R.id.layout_to_address);
            //  viewHolder.img_alert = (AppCompatImageView) itemView.findViewById(R.id.img_alert);

            itemView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) itemView.getTag();
        }

        viewHolder.txt_status.setText(_arr_userBookedRides.get(position).get("service_name"));
        viewHolder.txt_ride_id.setText("#" + _arr_userBookedRides.get(position).get("booking_id"));

        if (_arr_userBookedRides.get(position).get("is_taxi").equalsIgnoreCase("3")) {

            viewHolder.layout_to_address.setVisibility(View.GONE);
        } else {

            viewHolder.layout_to_address.setVisibility(View.VISIBLE);
        }


        // viewHolder.txt_status.setTextColor(Color.parseColor("#" + _arr_userBookedRides.get(position).get("color")));

//        try {
//            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
//            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
//            df.setTimeZone(TimeZone.getTimeZone("UTC"));
//            Date date = df.parse(_arr_userBookedRides.get(position).get("pickup_date").toString());
//            df.setTimeZone(TimeZone.getDefault());
//            SimpleDateFormat date_fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
//            String _date = date_fmt.format(date);
//
//            viewHolder.tv_date_time.setText(_date.toString());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        // 25-09-2017 7:30am
        try {
            String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = df.parse(_arr_userBookedRides.get(position).get("pickup_date").toString());
            df.setTimeZone(TimeZone.getDefault());
            SimpleDateFormat date_fmt = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.getDefault());
            String _date = date_fmt.format(date);

            viewHolder.tv_date_time.setText(_date.toString().toLowerCase());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (_arr_userBookedRides.get(position).get("booking_status").equalsIgnoreCase(Constant.STATUS_PENDING)) {
            viewHolder.img_ride_status.setImageResource(R.drawable.ic_pending);

        } else if (_arr_userBookedRides.get(position).get("booking_status").equalsIgnoreCase(Constant.STATUS_ACCEPT)) {
            viewHolder.img_ride_status.setImageResource(R.drawable.ic_accepted);

        } else if (_arr_userBookedRides.get(position).get("booking_status").equalsIgnoreCase(Constant.STATUS_GOTO_USER)) {
            viewHolder.img_ride_status.setImageResource(R.drawable.ic_go_user);

        } else if (_arr_userBookedRides.get(position).get("booking_status").equalsIgnoreCase(Constant.STATUS_USER_WAIT)) {
            viewHolder.img_ride_status.setImageResource(R.drawable.ic_wait_user);

        } else if (_arr_userBookedRides.get(position).get("booking_status").equalsIgnoreCase(Constant.STATUS_START_TRIP)) {
            viewHolder.img_ride_status.setImageResource(R.drawable.ic_started);

        } else if (_arr_userBookedRides.get(position).get("booking_status").equalsIgnoreCase(Constant.STATUS_COMPLETE_TRIP)) {
            viewHolder.img_ride_status.setImageResource(R.drawable.ic_completed);

        } else if (_arr_userBookedRides.get(position).get("booking_status").equalsIgnoreCase(Constant.STATUS_CANCEL_TRIP)) {
            viewHolder.img_ride_status.setImageResource(R.drawable.ic_cancelled);

        } else if (_arr_userBookedRides.get(position).get("booking_status").equalsIgnoreCase(Constant.STATUS_AUTO_REJECT)) {
            viewHolder.img_ride_status.setImageResource(R.drawable.ic_auto_rejected);
        }

        Glide.with(activity).load(Global_ServiceApi.API_IMAGE_HOST + _arr_userBookedRides.get(position).get("icon").toString())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(viewHolder.img_car_type);

        viewHolder.txt_from_address.setText(_arr_userBookedRides.get(position).get("pickup_address"));
        viewHolder.txt_to_address.setText(_arr_userBookedRides.get(position).get("drop_address"));

        viewHolder.tv_date_time.setTypeface(global_typeface.Sansation_Regular());
        viewHolder.txt_status.setTypeface(global_typeface.Sansation_Regular());
        viewHolder.txt_ride_id.setTypeface(global_typeface.Sansation_Regular());
        viewHolder.txt_from_address.setTypeface(global_typeface.Sansation_Regular());
        viewHolder.txt_to_address.setTypeface(global_typeface.Sansation_Regular());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null)
                    mClickListener.onBtnClick(position, _arr_userBookedRides.get(position).get("booking_id"), _arr_userBookedRides.get(position).get("booking_status"), _arr_userBookedRides.get(position).get("is_taxi"));
            }
        });

        return itemView;
    }

    public class ViewHolder {
        AppCompatTextView tv_date_time, txt_status, txt_ride_id, txt_from_address, txt_to_address;
        AppCompatImageView img_car_type, img_ride_status, img_alert;
        LinearLayout layout_to_address;
    }

    public interface BtnClickListener {
        void onBtnClick(int position, String ride_id, String ride_status, String rideType);
    }

    public String convertDate(Date d) {
        SimpleDateFormat sdfAmerica = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");
        String actualTime = sdfAmerica.format(d);
        //Changed timezone
        TimeZone tzInAmerica = TimeZone.getTimeZone("GMT+10");
        sdfAmerica.setTimeZone(tzInAmerica);

        String convertedTime = sdfAmerica.format(d);
        System.out.println("actual : " + actualTime + "  converted " + convertedTime);

        return convertedTime;
    }
}
