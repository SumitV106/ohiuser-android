package com.ohicabs.user.ohi.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.ohicabs.user.ohi.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import im.delight.android.webview.AdvancedWebView;

public class RegisterTermsConditionActivity extends BaseActivity implements AdvancedWebView.Listener {
    @BindView(R.id.webview_terms_condition)
    AdvancedWebView _webview_terms_condition;

    @BindView(R.id.img_back)
    ImageView _img_back;

    @BindView(R.id.txt_header)
    TextView _txt_header;

    String url_type = "", url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_condition);
        ButterKnife.bind(this);
        setContent();
    }

    private void setContent() {
        if (getIntent().hasExtra("link")) {
            url_type = getIntent().getExtras().getString("link");

            if (url_type.equals("terms")) {
                url = "https://ohicabs.com/terms_and_conditions.html";

            } else if (url_type.equals("disclaimer")) {
                url = "https://ohicabs.com/Disclaimer.html";
                _txt_header.setText("DISCLAIMER");
            }
        }

        _txt_header.setTypeface(global_typeface.Sansation_Bold());

        _webview_terms_condition.setListener(this, this);
        _webview_terms_condition.setGeolocationEnabled(false);
        _webview_terms_condition.setMixedContentAllowed(true);
        _webview_terms_condition.setCookiesEnabled(true);
        _webview_terms_condition.setThirdPartyCookiesEnabled(true);

        _webview_terms_condition.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {

            }
        });

        _webview_terms_condition.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
            }
        });

        _webview_terms_condition.loadUrl(url);

        _img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();
        _webview_terms_condition.onResume();
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        _webview_terms_condition.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        _webview_terms_condition.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        _webview_terms_condition.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
    }

    @Override
    public void onPageFinished(String url) {
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
    }

    @Override
    public void onExternalPageRequest(String url) {
    }
}

