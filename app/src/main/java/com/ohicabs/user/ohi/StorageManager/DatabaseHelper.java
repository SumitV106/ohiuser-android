package com.ohicabs.user.ohi.StorageManager;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ohicabs.user.ohi.DBModel.CareList_Model;
import com.ohicabs.user.ohi.DBModel.ServiceDetail_Model;
import com.ohicabs.user.ohi.DBModel.ServicePrice_Model;
import com.ohicabs.user.ohi.DBModel.ZoneList_Model;
import com.ohicabs.user.ohi.Utils.Constant;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by sumit on 07/01/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = DatabaseHelper.class.getSimpleName().toString();

    private final Context myContext;
    private SQLiteDatabase myDataBase;
    private static final int DATABASE_VERSION = 3;

    public DatabaseHelper(Context context) {
        super(context, Constant.DB_NAME, null, DATABASE_VERSION);
        this.myContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_ZONELIST_TABLE = "CREATE TABLE [zone_list] (zone_id integer PRIMARY KEY AUTOINCREMENT,zone_name text,document_id text,required_document_id text,google_zone_name text,city text,created_date text,modify_date text,status text,tax text,gst_tax text,paytm_term_html text)";
        db.execSQL(CREATE_ZONELIST_TABLE);

        String CREATE_SERVICE_DEATIL_TABLE = "CREATE TABLE [service_detail] (service_id integer PRIMARY KEY,service_name text,seat integer,color text,icon text,top_icon text,gender text,is_taxi text,created_date date,modify_date date,status integer,image_details text,description text)";
        db.execSQL(CREATE_SERVICE_DEATIL_TABLE);

        String CREATE_PRICE_TABLE = "CREATE TABLE [price_mnt] (price_id integer PRIMARY KEY,zone_id text,service_id text," +
                " base_charge text,per_km_charge text,per_minute_charge text,booking_charge text,minimum_fair text,created_at text," +
                " modify_date text,status text,minimum_km text,sec_ride_per text,paytm_offer text,max_offer_amount text,rental_time text, allowance text DEFAULT 0, " +
                " token_per text DEFAULT 0, max_token text DEFAULT 0, gst text DEFAULT 0, max_tax text DEFAULT 0)";
        db.execSQL(CREATE_PRICE_TABLE);

        String CREATE_NOTIFICATION_TABLE = "CREATE TABLE [notification] (id integer PRIMARY KEY AUTOINCREMENT,notification_id text,message text,date text,status text DEFAULT 0)";
        db.execSQL(CREATE_NOTIFICATION_TABLE);

        String CREATE_ALERT_TABLE = "CREATE TABLE [alert] (id integer PRIMARY KEY AUTOINCREMENT,booking_id text,alert_message text,alert_date date,status text DEFAULT 0)";
        db.execSQL(CREATE_ALERT_TABLE);

        String CREATE_CARE_TABLE = "CREATE TABLE [care_detail] (care_id integer PRIMARY KEY AUTOINCREMENT, care_name text, care_number text,status text DEFAULT 0," +
                "created_date text, modify_date text, address text, care_time text, languages text, care_driver_time text, care_driver_number text)";
        db.execSQL(CREATE_CARE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "Database ON Upgarade called. . . . " + oldVersion + " . . . . " + newVersion);

        try {
            if (oldVersion < 3) {
                migrateToNewVersion3(db);
            }

            db.setVersion(newVersion);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Error("Error copying database");
        }

        Log.e(TAG, "Database ON Upgarade called. . . . " + oldVersion + " . . . . " + newVersion);
    }

    // version 3 migration
    private void migrateToNewVersion3(SQLiteDatabase db) {

        // Price Table

        if (!isFieldExist(db, "price_mnt", "allowance")) {
            db.execSQL("ALTER TABLE price_mnt ADD COLUMN allowance text DEFAULT '0'");
        }

        if (!isFieldExist(db, "price_mnt", "token_per")) {
            db.execSQL("ALTER TABLE price_mnt ADD COLUMN token_per text DEFAULT '0'");
        }

        if (!isFieldExist(db, "price_mnt", "max_token")) {
            db.execSQL("ALTER TABLE price_mnt ADD COLUMN max_token text DEFAULT '0'");
        }

        if (!isFieldExist(db, "price_mnt", "gst")) {
            db.execSQL("ALTER TABLE price_mnt ADD COLUMN gst text DEFAULT '0'");
        }

        if (!isFieldExist(db, "price_mnt", "max_tax")) {
            db.execSQL("ALTER TABLE price_mnt ADD COLUMN max_tax text DEFAULT '0'");
        }
    }

    // check if database table field exist or not
    public boolean isFieldExist(SQLiteDatabase sqLiteDatabase,
                                String tableName,
                                String columnToFind) {
        Cursor cursor = null;

        try {
            cursor = sqLiteDatabase.rawQuery(
                    "PRAGMA table_info(" + tableName + ")",
                    null
            );

            int nameColumnIndex = cursor.getColumnIndexOrThrow("name");

            while (cursor.moveToNext()) {
                String name = cursor.getString(nameColumnIndex);

                if (name.equals(columnToFind)) {
                    return true;
                }
            }

            return false;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    @Override
    public synchronized void close() {
        if (myDataBase != null)
            myDataBase.close();
        super.close();
    }

    /**
     * Insert Zone List Data
     * <p>
     * Table: zone_list
     *
     * @param list_zoneBoundry
     * @return
     */
    public boolean InsertZoneListData(ArrayList<ZoneList_Model> list_zoneBoundry) {

        SQLiteDatabase db = this.getWritableDatabase();
        try {

            for (int i = 0; i < list_zoneBoundry.size(); i++) {

                String Created_Date = list_zoneBoundry.get(i).getCreated_date();
                String City = list_zoneBoundry.get(i).getCity();
                String Status = list_zoneBoundry.get(i).getStatus();
                String ZoneName = list_zoneBoundry.get(i).getZone_name();
                String ZoneID = list_zoneBoundry.get(i).getZone_id();
                String Tax = list_zoneBoundry.get(i).getTax();
                String Modified = list_zoneBoundry.get(i).getModify_date();
                String GoogleZone = list_zoneBoundry.get(i).getGoogle_zone_name();
                String Gst_Tax = list_zoneBoundry.get(i).getGst_tax();
                String Paytm_Term_Html = list_zoneBoundry.get(i).getPaytm_term_html();

                String[] args_story = {ZoneID, ZoneName, GoogleZone, City, Created_Date, Modified, Status, Tax, Gst_Tax, Paytm_Term_Html};
                db.execSQL(
                        "INSERT OR REPLACE INTO zone_list('zone_id','zone_name','google_zone_name','city','created_date','modify_date','status','tax','gst_tax','paytm_term_html')" +
                                " VALUES (?,?,?,?,?,?,?,?,?,?)",
                        args_story);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            SQLiteDatabase.releaseMemory();
        }
        return false;
    }

    /**
     * Insert Service Details Data
     * <p>
     * Table: service_detail
     *
     * @param list_ServiceDetails
     * @return
     */
    public boolean InsertServiceDetailsData(ArrayList<ServiceDetail_Model> list_ServiceDetails) {

        SQLiteDatabase db = this.getWritableDatabase();
        try {

            for (int i = 0; i < list_ServiceDetails.size(); i++) {

                String ServiceID = list_ServiceDetails.get(i).getService_id();
                String ServiceName = list_ServiceDetails.get(i).getService_name();
                String Seat = list_ServiceDetails.get(i).getSeat();
                String Color = list_ServiceDetails.get(i).getColor();
                String Icon = list_ServiceDetails.get(i).getIcon();
                String Top_icon = list_ServiceDetails.get(i).getTop_icon();
                String Gender = list_ServiceDetails.get(i).getGender();
                String Is_taxi = list_ServiceDetails.get(i).getIs_taxi();
                String Created = list_ServiceDetails.get(i).getCreated_date();
                String Modified = list_ServiceDetails.get(i).getModify_date();
                String Status = list_ServiceDetails.get(i).getStatus();
                String ImageDetails = list_ServiceDetails.get(i).getImage_details();
                String Description = list_ServiceDetails.get(i).getDescription();

                String[] args_story = {ServiceID, ServiceName, Seat, Color, Icon, Top_icon, Gender, Is_taxi, Created, Modified, Status, ImageDetails, Description};
                db.execSQL(
                        "INSERT OR REPLACE INTO service_detail(service_id, service_name, seat, " +
                                "color, icon,top_icon, gender,is_taxi, created_date, modify_date, status,image_details,description) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",
                        args_story);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            SQLiteDatabase.releaseMemory();
        }
        return false;
    }

    /**
     * Insert Service Price Details Data
     * <p>
     * Table: price_mnt
     *
     * @param list_ServicePrice
     * @return
     */
    public boolean InsertServicePriceData(ArrayList<ServicePrice_Model> list_ServicePrice) {

        SQLiteDatabase db = this.getWritableDatabase();
        try {

            for (int i = 0; i < list_ServicePrice.size(); i++) {

                String PriceID = list_ServicePrice.get(i).getPrice_id();
                String ZoneID = list_ServicePrice.get(i).getZone_id();
                String ServiceID = list_ServicePrice.get(i).getService_id();
                String BaseCharge = list_ServicePrice.get(i).getBase_charge();
                String KmCharge = list_ServicePrice.get(i).getPer_km_charge();
                String MinuteCharge = list_ServicePrice.get(i).getPer_minute_charge();
                String BookingCharge = list_ServicePrice.get(i).getBooking_charge();
                String MinFair = list_ServicePrice.get(i).getMinimum_fair();
                String Created = list_ServicePrice.get(i).getCreated_at();
                String Modified = list_ServicePrice.get(i).getModify_date();
                String Status = list_ServicePrice.get(i).getStatus();
                String minimum_km = list_ServicePrice.get(i).getMinimum_km();
                String sec_ride_per = list_ServicePrice.get(i).getsec_ride_per();
                String paytm_offer = list_ServicePrice.get(i).getPaytm_offer();
                String max_offer_amount = list_ServicePrice.get(i).getMax_offer_amount();
                String rental_time = list_ServicePrice.get(i).getRental_time();

                String allowance = list_ServicePrice.get(i).getRental_time();
                String tokenPer = list_ServicePrice.get(i).getRental_time();
                String maxToken = list_ServicePrice.get(i).getRental_time();
                String gst = list_ServicePrice.get(i).getRental_time();
                String maxTax = list_ServicePrice.get(i).getRental_time();

                String[] args_story = {PriceID, ZoneID, ServiceID, BaseCharge, KmCharge, MinuteCharge, BookingCharge, MinFair,
                        Created, Modified, Status, minimum_km, sec_ride_per, paytm_offer, max_offer_amount,rental_time, allowance, tokenPer, maxToken, gst, maxTax};
                db.execSQL(
                        "INSERT OR REPLACE INTO price_mnt(price_id, zone_id, service_id, " +
                                "base_charge, per_km_charge, per_minute_charge, booking_charge, minimum_fair, created_at, modify_date, " +
                                "status,minimum_km,sec_ride_per,paytm_offer,max_offer_amount,rental_time, allowance, token_per, max_token, gst, max_tax) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                        args_story);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            SQLiteDatabase.releaseMemory();
        }
        return false;
    }

    /**
     * Insert Care List
     * <p>
     * Table: care_detail
     *
     * @param list_CareList
     * @return
     */
    public boolean InsertCareList(ArrayList<CareList_Model> list_CareList) {

        SQLiteDatabase db = this.getWritableDatabase();
        try {

            for (int i = 0; i < list_CareList.size(); i++) {

                String CareID = list_CareList.get(i).getCare_id();
                String CareName = list_CareList.get(i).getCare_name();
                String CareNumber = list_CareList.get(i).getCare_number();
                String Status = list_CareList.get(i).getStatus();
                String CreatedDate = list_CareList.get(i).getCreated_date();
                String ModifyDate = list_CareList.get(i).getModify_date();
                String Address = list_CareList.get(i).getAddress();

                String careTime = list_CareList.get(i).getCare_time();
                String langaues = list_CareList.get(i).getLanguages();
                String careDriverNumber = list_CareList.get(i).getCare_driver_number();
                String careDriverTime = list_CareList.get(i).getCare_driver_time();

                String[] args_story = {CareID, CareName, CareNumber, Status, CreatedDate, ModifyDate, Address, careTime, langaues, careDriverNumber, careDriverTime};

                db.execSQL(
                        "INSERT OR REPLACE INTO care_detail(care_id, care_name, care_number, " +
                                "status, created_date, modify_date, address, care_time, languages, care_driver_time, care_driver_number) VALUES (?,?,?,?,?,?,?,?,?,?,?)",
                        args_story);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            SQLiteDatabase.releaseMemory();
        }
        return false;
    }

    /**
     * Get ZoneList
     */
    public ArrayList<HashMap> GetZoneListFromDB() {

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap> list_Zone = new ArrayList<>();

        try {
            Cursor c;
            String select = "select `zl`.* from `zone_list` AS `zl` INNER JOIN `price_mnt` AS `pm` ON `pm`.`zone_id` = `zl`.`zone_id` AND `pm`.`status` = 0 where `zl`.`status` = 0 AND `zl`.`zone_id` != 1 GROUP BY `zl`.`zone_id`";
//            String select = "SELECT * FROM zone_list WHERE status = 0";
            // String select = "SELECT * FROM zone_list";

            c = db.rawQuery(select, null);
            String[] columnNames = c.getColumnNames();

            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToNext();

                    HashMap<String, String> map_zonelist = new HashMap<String, String>();
                    for (String j : columnNames) {
                        map_zonelist.put(j, c.getString(c.getColumnIndex(j)));
                    }
                    list_Zone.add(map_zonelist);
                }
            }
            db.close();
            c.close();
            SQLiteDatabase.releaseMemory();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list_Zone;
    }

    /**
     * Get ZoneList
     */
    public ArrayList<HashMap> GetZoneListFromDBForProfile() {

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap> list_Zone = new ArrayList<>();

        try {
            Cursor c;
            String select = "select `zl`.* from `zone_list` AS `zl` INNER JOIN `price_mnt` AS `pm` ON `pm`.`zone_id` = `zl`.`zone_id` AND `pm`.`status` = 0 where `zl`.`status` = 0 AND `zl`.`zone_id` != 1 GROUP BY `zl`.`zone_id`";

            c = db.rawQuery(select, null);
            String[] columnNames = c.getColumnNames();

            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToNext();

                    HashMap<String, String> map_zonelist = new HashMap<String, String>();
                    for (String j : columnNames) {
                        map_zonelist.put(j, c.getString(c.getColumnIndex(j)));
                    }
                    list_Zone.add(map_zonelist);
                }
            }
            db.close();
            c.close();
            SQLiteDatabase.releaseMemory();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list_Zone;
    }

    /**
     * Get Default Zone
     * <p>
     * When User can't find any zone area for service then, Return Default Zone Area
     *
     * @return
     */
    public ArrayList<HashMap> GetDefaultZoneFromDB() {

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap> list_Zone = new ArrayList<>();

        try {
            Cursor c;
            String stQuery_AllZone = "SELECT * FROM zone_list WHERE zone_id = 1";
            c = db.rawQuery(stQuery_AllZone, null);
            String[] columnNames_AllZone = c.getColumnNames();

            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToNext();

                    HashMap<String, String> map_zonelist = new HashMap<String, String>();
                    for (String j : columnNames_AllZone) {
                        map_zonelist.put(j, c.getString(c.getColumnIndex(j)));
                    }
                    list_Zone.add(map_zonelist);
                }
            }
            db.close();
            c.close();
            SQLiteDatabase.releaseMemory();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list_Zone;
    }

    /**
     * Ger Price list diff with ZoneID and ServiceID
     *
     * @param zone_id
     * @param servce_id
     * @return
     */
    public ArrayList<HashMap> GetPriceList(String zone_id, String servce_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap> list_Price = new ArrayList<>();

        try {
            Cursor c;
            String stQuery_Price = "SELECT pm.*, sd.is_taxi FROM price_mnt as pm INNER JOIN service_detail as sd ON `sd`.`service_id` = `pm`.`service_id` WHERE `pm`.`zone_id` =" + zone_id + " AND `pm`.`service_id` = " + servce_id + " AND `pm`.`status` = 0";
            c = db.rawQuery(stQuery_Price, null);
            String[] columnNames_Price = c.getColumnNames();

            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToNext();

                    HashMap<String, String> map_zonelist = new HashMap<String, String>();
                    for (String j : columnNames_Price) {
                        map_zonelist.put(j, c.getString(c.getColumnIndex(j)));
                    }
                    list_Price.add(map_zonelist);
                }
            }
            db.close();
            c.close();
            SQLiteDatabase.releaseMemory();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return list_Price;
    }

    /**
     * Ger GST TAX diff with ZoneID
     *
     * @param zone_id
     * @return
     */
    public ArrayList<HashMap> GetGSTTax(String zone_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap> list_Price = new ArrayList<>();

        try {
            Cursor c;
            String stQuery_Price = "SELECT * FROM zone_list WHERE zone_id =" + zone_id;
            c = db.rawQuery(stQuery_Price, null);
            String[] columnNames_Price = c.getColumnNames();

            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToNext();

                    HashMap<String, String> map_zonelist = new HashMap<String, String>();
                    for (String j : columnNames_Price) {
                        map_zonelist.put(j, c.getString(c.getColumnIndex(j)));
                    }
                    list_Price.add(map_zonelist);
                }
            }
            db.close();
            c.close();
            SQLiteDatabase.releaseMemory();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return list_Price;
    }


    /**
     * Insert Alert Notification Data
     *
     * @param stAlertMsg
     * @param stBookingId
     * @param stAlertDate
     * @return
     */
    public boolean InsertAlertData(String stAlertMsg, String stBookingId, String stAlertDate) {

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            String[] args_story = {stBookingId, stAlertMsg, stAlertDate};
            db.execSQL(
                    "INSERT OR REPLACE INTO alert(booking_id, alert_message, alert_date) VALUES (?,?,?)", args_story);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            SQLiteDatabase.releaseMemory();
        }
        return false;
    }

    /**
     * Get Alert Notification Data
     *
     * @param bookingID
     * @return
     */
    public int GetAlertData(String bookingID) {
        SQLiteDatabase db = this.getWritableDatabase();
        int count = 0;

        try {
            Cursor c;
            String select = "SELECT count(*) as count From alert where booking_id=" + bookingID + " AND status = 0";
            c = db.rawQuery(select, null);
            c.moveToFirst();
            count = c.getInt(0);
            Log.e("TAG_COUNT", "=" + count);

            db.close();
            c.close();
            SQLiteDatabase.releaseMemory();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }


    /**
     * Insert Current Location Data
     *
     * @param booking_id
     * @param latitude
     * @param longitude
     * @param stDate
     * @return
     */
    public boolean InsertLocationUpdateData(String booking_id, String latitude, String longitude, String stDate) {

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            String[] args_story = {booking_id, latitude, longitude, stDate};
            db.execSQL(
                    "INSERT OR REPLACE INTO ride_location(booking_id, latitude, longitude, date_time) VALUES (?,?,?,?)", args_story);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            SQLiteDatabase.releaseMemory();
        }
        return false;
    }

    public ArrayList<HashMap> GetLocationData(String stBookingId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap> list_Location = new ArrayList<>();

        try {
            Cursor c;
            String stQuery = "SELECT * FROM ride_location where booking_id = " + stBookingId;
            c = db.rawQuery(stQuery, null);
            String[] columnNames_Price = c.getColumnNames();

            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToNext();

                    HashMap<String, String> map_locationlist = new HashMap<String, String>();
                    for (String j : columnNames_Price) {
                        map_locationlist.put(j, c.getString(c.getColumnIndex(j)));
                    }
                    list_Location.add(map_locationlist);
                }
            }
            db.close();
            c.close();
            SQLiteDatabase.releaseMemory();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return list_Location;
    }

    public ArrayList<HashMap> GetPriceData(String price_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap> list_Price = new ArrayList<>();

        try {
            Cursor c;
            String stQuery_Price = "SELECT * FROM price_mnt as pm INNER JOIN zone_list as zl ON zl.zone_id = pm.zone_id WHERE price_id = " + price_id;
            c = db.rawQuery(stQuery_Price, null);
            String[] columnNames_Price = c.getColumnNames();

            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToNext();

                    HashMap<String, String> map_zonelist = new HashMap<String, String>();
                    for (String j : columnNames_Price) {
                        map_zonelist.put(j, c.getString(c.getColumnIndex(j)));
                    }
                    list_Price.add(map_zonelist);
                }
            }
            db.close();
            c.close();
            SQLiteDatabase.releaseMemory();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list_Price;
    }


    /**
     * Insert Notification Data
     */
    public boolean InsertPushNotificationData(String notification_id, String message, String date) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            try {
                String[] args_story = {notification_id, message, date};
                db.execSQL(
                        "INSERT OR REPLACE INTO notification(notification_id, message, date) VALUES (?,?,?)", args_story);

                return true;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                db.close();
                SQLiteDatabase.releaseMemory();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Get Push Notification Data
     *
     * @return
     */
    public ArrayList<HashMap> GetPushNotificationFromDB() {

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap> list_Zone = new ArrayList<>();

        try {
            Cursor c;
            String stQuery_AllZone = "SELECT * FROM notification";
            c = db.rawQuery(stQuery_AllZone, null);
            String[] columnNames_AllZone = c.getColumnNames();

            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToNext();

                    HashMap<String, String> map_zonelist = new HashMap<String, String>();
                    for (String j : columnNames_AllZone) {
                        map_zonelist.put(j, c.getString(c.getColumnIndex(j)));
                    }
                    list_Zone.add(map_zonelist);
                }
            }
            db.close();
            c.close();
            SQLiteDatabase.releaseMemory();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list_Zone;
    }

    public ArrayList<HashMap> GetCarList(String stGender, String stZoneId) {

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap> list_Car = new ArrayList<>();

        try {
            Cursor c;
//            String stQuery_AllCars = "SELECT `sd`.* FROM `service_detail` as `sd` INNER JOIN `price_mnt` as `pm` ON `sd`.`service_id` = `pm`.`service_id` " +
//                    " WHERE `sd`.`status`= 0 AND (`sd`.`gender` LIKE '%" + stGender + "%' ) AND `pm`.`zone_id`=" + stZoneId + " AND `pm`.`status` != 2";

            String stQuery_AllCars="select `sd`.* from `service_detail` as `sd` INNER JOIN `price_mnt` as `pm` ON `sd`.`service_id` = `pm`.`service_id`  "+
                    "WHERE `sd`.`status`= 0 AND (`sd`.`is_taxi` = '0' OR `sd`.`is_taxi` = '1' OR `sd`.`is_taxi` = '2') AND (`sd`.`gender` LIKE '%" + stGender + "%' ) AND `pm`.`zone_id`=" + stZoneId + "  AND `pm`.`status` != 2";

            c = db.rawQuery(stQuery_AllCars, null);
            String[] columnNames_AllCar = c.getColumnNames();

            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToNext();

                    HashMap<String, String> map_carlist = new HashMap<String, String>();
                    for (String j : columnNames_AllCar) {
                        map_carlist.put(j, c.getString(c.getColumnIndex(j)));
                    }
                    list_Car.add(map_carlist);
                }
            }
            db.close();
            c.close();
            SQLiteDatabase.releaseMemory();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list_Car;
    }

    /**
     *
     * Get Car list for outstation
     *
     * is_taxi = 4 ---> Outstation oneway
     * is_taxi = 5 ---> Outstation roundtrip
     *
     * @param stGender
     * @param stZoneId
     * @return
     */
    public ArrayList<HashMap> GetCarListForOutStation(String stGender, String stZoneId) {

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap> list_Car = new ArrayList<>();

        try {
            Cursor c;

            String stQuery_AllCars="select `sd`.* from `service_detail` as `sd` INNER JOIN `price_mnt` as `pm` ON `sd`.`service_id` = `pm`.`service_id`  "+
                    "WHERE `sd`.`status`= 0 AND (`sd`.`is_taxi` = '5' OR `sd`.`is_taxi` = '4') AND (`sd`.`gender` LIKE '%" + stGender + "%' ) AND `pm`.`zone_id`=" + stZoneId + "  AND `pm`.`status` != 2 GROUP BY `sd`.`service_id`";

            c = db.rawQuery(stQuery_AllCars, null);
            String[] columnNames_AllCar = c.getColumnNames();

            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToNext();

                    HashMap<String, String> map_carlist = new HashMap<String, String>();
                    for (String j : columnNames_AllCar) {
                        map_carlist.put(j, c.getString(c.getColumnIndex(j)));
                    }
                    list_Car.add(map_carlist);
                }
            }
            db.close();
            c.close();
            SQLiteDatabase.releaseMemory();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list_Car;
    }

    public ArrayList<HashMap> GetCarListForRental(String stGender, String stZoneId) {

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap> list_Car = new ArrayList<>();

        try {
            Cursor c;
//            String stQuery_AllCars = "SELECT `sd`.* FROM `service_detail` as `sd` INNER JOIN `price_mnt` as `pm` ON `sd`.`service_id` = `pm`.`service_id` " +
//                    " WHERE `sd`.`status`= 0 AND (`sd`.`gender` LIKE '%" + stGender + "%' ) AND `pm`.`zone_id`=" + stZoneId + " AND `pm`.`status` != 2";

            String stQuery_AllCars="select `sd`.* from `service_detail` as `sd` INNER JOIN `price_mnt` as `pm` ON `sd`.`service_id` = `pm`.`service_id` "+
                    " WHERE `sd`.`status` = 0 AND `sd`.`is_taxi` == 3 AND (`sd`.`gender` LIKE '%" + stGender + "%' ) AND `pm`.`zone_id`=" + stZoneId + " AND `pm`.`status` != 2 GROUP BY `sd`.`service_id`";

            c = db.rawQuery(stQuery_AllCars, null);
            String[] columnNames_AllCar = c.getColumnNames();

            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToNext();

                    HashMap<String, String> map_carlist = new HashMap<String, String>();
                    for (String j : columnNames_AllCar) {
                        map_carlist.put(j, c.getString(c.getColumnIndex(j)));
                    }
                    list_Car.add(map_carlist);
                }
            }
            db.close();
            c.close();
            SQLiteDatabase.releaseMemory();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list_Car;
    }

    public ArrayList<HashMap> GetRentalPriceList(String stServiceId, String stZoneId) {

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap> list_Car = new ArrayList<>();

        try {
            Cursor c;

            String stQuery_AllCars="select * from price_mnt as pm inner join service_detail as sd on pm.service_id = sd.service_id  where pm.zone_id = " + stZoneId +
                    " AND pm.service_id = " + stServiceId + " AND pm.status = 0 ORDER BY CAST(`pm`.`rental_time` AS INTEGER)";

            c = db.rawQuery(stQuery_AllCars, null);
            String[] columnNames_AllCar = c.getColumnNames();

            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToNext();

                    HashMap<String, String> map_carlist = new HashMap<String, String>();
                    for (String j : columnNames_AllCar) {
                        map_carlist.put(j, c.getString(c.getColumnIndex(j)));
                    }
                    list_Car.add(map_carlist);
                }
            }
            db.close();
            c.close();
            SQLiteDatabase.releaseMemory();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list_Car;
    }



    public ArrayList<HashMap> GetCareList() {

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap> list_Car = new ArrayList<>();

        try {
            Cursor c;
            String stQuery_AllCars = "SELECT * FROM `care_detail` WHERE `status`='0'";
            c = db.rawQuery(stQuery_AllCars, null);
            String[] columnNames_AllCar = c.getColumnNames();

            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToNext();

                    HashMap<String, String> map_carlist = new HashMap<String, String>();
                    for (String j : columnNames_AllCar) {
                        map_carlist.put(j, c.getString(c.getColumnIndex(j)));
                    }
                    list_Car.add(map_carlist);
                }
            }
            db.close();
            c.close();
            SQLiteDatabase.releaseMemory();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list_Car;
    }

    public ArrayList<HashMap> GetServiceDetail(String stServiceId) {

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap> list_Car = new ArrayList<>();

        try {
            Cursor c;
            String stQuery_AllCars = "SELECT * FROM service_detail WHERE service_id = " + stServiceId;
            c = db.rawQuery(stQuery_AllCars, null);
            String[] columnNames_AllCar = c.getColumnNames();

            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToNext();

                    HashMap<String, String> map_carlist = new HashMap<String, String>();
                    for (String j : columnNames_AllCar) {
                        map_carlist.put(j, c.getString(c.getColumnIndex(j)));
                    }
                    list_Car.add(map_carlist);
                }
            }
            db.close();
            c.close();
            SQLiteDatabase.releaseMemory();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list_Car;
    }


    /**
     * Get OutStation Price list
     *
     * @param zoneID
     * @param serviceID
     * @return
     */
    public ArrayList<HashMap> GetOutStationPriceList(String serviceID, String zoneID) {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap> list_Price = new ArrayList<>();

        try {
            Cursor c;
            String stQuery_Price = "SELECT * FROM price_mnt AS pm INNER JOIN service_detail AS sd ON pm.service_id = sd.service_id WHERE `pm`.`zone_id` =" + zoneID + " AND `pm`.`service_id` = " + serviceID + " AND `pm`.`status` = 0 ORDER BY `pm`.`rental_time`";
            c = db.rawQuery(stQuery_Price, null);
            String[] columnNames_Price = c.getColumnNames();

            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToNext();

                    HashMap<String, String> map_zonelist = new HashMap<String, String>();
                    for (String j : columnNames_Price) {
                        map_zonelist.put(j, c.getString(c.getColumnIndex(j)));
                    }
                    list_Price.add(map_zonelist);
                }
            }
            db.close();
            c.close();
            SQLiteDatabase.releaseMemory();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return list_Price;
    }
}
