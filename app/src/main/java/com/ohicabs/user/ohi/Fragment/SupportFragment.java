package com.ohicabs.user.ohi.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.bassaer.chatmessageview.models.LeftMessage;
import com.github.bassaer.chatmessageview.models.Message;
import com.github.bassaer.chatmessageview.models.User;
import com.github.bassaer.chatmessageview.views.MessageView;
import com.google.gson.JsonObject;
import com.ohicabs.user.ohi.API.Global_ServiceApi;
import com.ohicabs.user.ohi.Application.OhiApplication;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Utils.Global_Typeface;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ohicabs.user.ohi.API.Global_ServiceApi.isInternetOn;

/**
 * A simple {@link Fragment} subclass.
 */
public class SupportFragment extends Fragment {

    private ToastDialog toastDialog;
    private Global_Typeface global_typeface;
    private SharedPreferencesUtility preferencesUtility;
    private Socket mSocket;
    OhiApplication appDelegate;
    public SupportFragment() {
        // Required empty public constructor
    }
    @BindView(R.id.message_view)
    MessageView messageView;

    @BindView(R.id.edt_send_msg)
    EditText _edt_send_msg;

    @BindView(R.id.img_send)
    ImageView _img_send;

    TextView _txt_msg_clear;
    private ArrayList<HashMap<String, String>> _arrChatList = new ArrayList<>();
    private ArrayList<Message> messages = new ArrayList<>();
    String userIcon, driverIcon, stDriverImageURL;
    ApiInterface apiService;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toastDialog = new ToastDialog(getActivity());
        global_typeface = new Global_Typeface(getActivity());
        preferencesUtility = new SharedPreferencesUtility(getActivity());
        apiService= ApiClient.getClient().create(ApiInterface.class);

        appDelegate = (OhiApplication) getActivity().getApplication();
        mSocket = appDelegate.getSocket();
        mSocket.connect();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_support, container, false);
        ButterKnife.bind(this,view);
        setContent();
        return view;
    }

    private void setContent() {

        mSocket.off("support_message");
        mSocket.on("support_message", onSupport);

        _edt_send_msg.setTypeface(global_typeface.Sansation_Regular());
        GetAllMessages();

        _txt_msg_clear = (TextView) getActivity().findViewById(R.id.txt_msg_clear);
        _txt_msg_clear.setTypeface(global_typeface.Sansation_Regular());
        _txt_msg_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<JsonObject> call = apiService.support_message_clear(preferencesUtility.getuser_id(),"1");
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        try{
                            Log.e("response",response.body().toString());
                            JSONObject obj = new JSONObject(response.body().toString());
                            if (obj.getString("status").equalsIgnoreCase("1") && obj.getString("success").equalsIgnoreCase("true") ) {
                                _arrChatList.clear();
                                messages.clear();
                                messageView.init(messages);
                                _txt_msg_clear.setVisibility(View.GONE);
                            }
                        }
                        catch (Exception e)
                        {
                            Log.e("error",e.toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });
            }
        });

        _img_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!_edt_send_msg.getText().toString().equalsIgnoreCase("")) {
                    if(isInternetOn(getContext())) {
                        try {
                            Call<JsonObject> call = apiService.support_message(preferencesUtility.getuser_id(), "1",
                                    _edt_send_msg.getText().toString(), "1", mSocket.id());

                            call.enqueue(new Callback<JsonObject>() {
                                @Override
                                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                    try {
                                        JSONObject objChatList = new JSONObject(response.body().toString());
                                        if (objChatList.getString("status").equalsIgnoreCase("1")) {
                                            JSONArray jarrPayload = objChatList.getJSONArray("payload");
                                            if (jarrPayload.length() > 0) {
                                                JSONObject objChatData = jarrPayload.getJSONObject(0);

                                                Iterator<String> iter = objChatData.keys();
                                                HashMap<String, String> map_keyValue = new HashMap<>();
                                                while (iter.hasNext()) {
                                                    String key = iter.next();
                                                    try {
                                                        Object value = objChatData.get(key);
                                                        map_keyValue.put(key, value.toString());
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                                GetAllMessages();
                                            }
                                        }
                                    } catch (Exception e) {
                                        Log.e("error", e.toString());
                                    }

                                }

                                @Override
                                public void onFailure(Call<JsonObject> call, Throwable t) {
                                    Log.e("error", t.toString());
                                }
                            });

                            _edt_send_msg.setText("");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    else{
                        toastDialog.ShowToastMessage(getString(R.string.st_internet_not_available));
                    }
                }
            }
        });

    }

    private void GetAllMessages() {
        try {

            Call<JsonObject> call = apiService.support_connect(preferencesUtility.getuser_id(),"1",mSocket.id());
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    Log.e("GetAllMessages",response.body().toString());
                    SetMessageData(response.body().toString());
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void SetMessageData(String mResult) {
        try {
            Log.e("TAG_REsult", mResult);
            JSONObject objChatList = new JSONObject(mResult);
            if (objChatList.getString("status").equalsIgnoreCase("1")) {
                JSONObject objPayload = objChatList.getJSONObject("payload");
                JSONArray jarrPayload = objPayload.getJSONArray("messages");
                if (jarrPayload.length() > 0) {
                    _txt_msg_clear.setVisibility(View.VISIBLE);
                    _arrChatList.clear();
                    messages.clear();

                    for (int i = 0; i < jarrPayload.length(); i++) {
                        JSONObject objChatData = jarrPayload.getJSONObject(i);

                        Iterator<String> iter = objChatData.keys();
                        HashMap<String, String> map_keyValue = new HashMap<>();
                        while (iter.hasNext()) {
                            String key = iter.next();
                            try {
                                Object value = objChatData.get(key);
                                map_keyValue.put(key, value.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        _arrChatList.add(map_keyValue);
                    }

                    JSONArray jarryDriverInfo = objPayload.getJSONArray("send_user_info");
                    if (jarryDriverInfo.length() > 0) {
                        JSONObject objDriverData = jarryDriverInfo.getJSONObject(0);
                        stDriverImageURL = objDriverData.getString("image");
                    }
                }

                //User Details
                int userID = Integer.parseInt(preferencesUtility.getuser_id());
                String userName = "";
                try {
                    userIcon = (Global_ServiceApi.API_IMAGE_HOST + preferencesUtility.getimage());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //Driver Details
                int driverID = Integer.parseInt(_arrChatList.get(0).get("sender_id"));
                String driverName = "";
                try {
                    driverIcon = (Global_ServiceApi.API_IMAGE_HOST  + stDriverImageURL);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                final User _userDetails = new User(userID, userName, userIcon);
                final User _driverDetails = new User(driverID, driverName, driverIcon);
                Log.e("Size",_arrChatList.size()+"--");
                for (int i = 0; i < _arrChatList.size(); i++) {

                    String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                    SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
                    df.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date date = df.parse(_arrChatList.get(i).get("created_date").toString());
                    df.setTimeZone(TimeZone.getDefault());
                    SimpleDateFormat date_fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
                    String _date = date_fmt.format(date);

                    if (preferencesUtility.getuser_id().toString().equalsIgnoreCase(_arrChatList.get(i).get("sender_id"))) {
                        Message message_user = new Message.Builder()
                                .setUser(_userDetails)
                                .setMessageText(_arrChatList.get(i).get("message"))
                                .setMessageDateTime(_date)
                                .setRightMessage(true)
                                .build();
                        messages.add(message_user);
                    } else {
                        Message message_driver = new LeftMessage.Builder()
                                .setUser(_driverDetails)
                                .setMessageText(_arrChatList.get(i).get("message"))
                                .setMessageDateTime(_date)
                                .setRightMessage(false)
                                .build();
                        messages.add(message_driver);
                    }
                }
                messageView.init(messages);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Get Sent and recevie Message List
     */
    private Emitter.Listener onSupport = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String mResult = args[0].toString();
                        Log.e("TAG_MSGLIST",mResult);
                        JSONObject objChatList = new JSONObject(mResult);
                        if (objChatList.getString("status").equalsIgnoreCase("1")) {
                            JSONArray jarrPayload = objChatList.getJSONArray("payload");
                            if (jarrPayload.length() > 0) {
                                JSONObject objChatData = jarrPayload.getJSONObject(0);

                                Iterator<String> iter = objChatData.keys();
                                HashMap<String, String> map_keyValue = new HashMap<>();
                                while (iter.hasNext()) {
                                    String key = iter.next();
                                    try {
                                        Object value = objChatData.get(key);
                                        map_keyValue.put(key, value.toString());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                GetAllMessages();
                            }
                        }
                        else if(objChatList.getString("status").equalsIgnoreCase("2")){
                            GetAllMessages();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
}


