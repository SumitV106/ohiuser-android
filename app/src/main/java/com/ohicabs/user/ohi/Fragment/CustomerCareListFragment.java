package com.ohicabs.user.ohi.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ohicabs.user.ohi.Activity.MonthlySpendingTermsActivity;
import com.ohicabs.user.ohi.Activity.TermsConditionActivity;
import com.ohicabs.user.ohi.Client.ApiClient;
import com.ohicabs.user.ohi.Interface.ApiInterface;
import com.ohicabs.user.ohi.R;
import com.ohicabs.user.ohi.Adpater.CareListAdapter;
import com.ohicabs.user.ohi.CustomWidgets.CustomBoldFontTextView;
import com.ohicabs.user.ohi.StorageManager.DatabaseHelper;
import com.ohicabs.user.ohi.Utils.Global_Typeface;
import com.ohicabs.user.ohi.Utils.SharedPreferencesUtility;
import com.ohicabs.user.ohi.Utils.ToastDialog;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomerCareListFragment extends Fragment {

    private ToastDialog toastDialog;
    private Global_Typeface global_typeface;
    private SharedPreferencesUtility sharedPreferencesUtility;

    ApiInterface apiService;
    DatabaseHelper mHelper;

    @BindView(R.id.list_care) ListView _list_care;
    @BindView(R.id.txt_no_data) CustomBoldFontTextView _txt_no_data;
    @BindView(R.id.btn_terms_condition) AppCompatButton btnTermsCondition;

    public CustomerCareListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toastDialog = new ToastDialog(getActivity());
        global_typeface = new Global_Typeface(getActivity());
        sharedPreferencesUtility = new SharedPreferencesUtility(getActivity());
        apiService= ApiClient.getClient().create(ApiInterface.class);

        mHelper = new DatabaseHelper(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_customer_care_list, container, false);
        ButterKnife.bind(this,view);

        return view;
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setView();

    }

    private void setView() {

        ArrayList<HashMap> _arr_BookedRideList = mHelper.GetCareList();

        CareListAdapter userAllRidesListAdapter = new CareListAdapter(getActivity(),
                R.layout.layout_care_row, _arr_BookedRideList,
                new CareListAdapter.BtnClickListener() {
                    @Override
                    public void onBtnClick(int position, String ride_id,String ride_status) {

                    }
                });
        _list_care.setAdapter(userAllRidesListAdapter);

        btnTermsCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), MonthlySpendingTermsActivity.class);
                intent.putExtra("term_type", "4");
                getActivity().startActivity(intent);
            }
        });
    }

}
